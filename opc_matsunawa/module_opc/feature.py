#!/usr/bin/python

import os
import sys
import time
import gdspy
import Image, ImageDraw
import numpy as np
import scipy.fftpack
from multiprocessing import Pool 
#import cv2

import cython
sys.path.insert(0, os.path.join('./'))
from extlist import CExtList
from cglac import GetGLAC
from calchlac import CGetHLAC

def ExtList(tnum, crange, adist, cpoints, cntp, getp_t, pdir):
	mlist = []
	ncntp = cntp.shape[0]
	for i in xrange(tnum):
		k = 0
		for j in xrange(ncntp):
			tbx = cntp[j,0]-adist/2.0
			tby = cntp[j,1]-adist/2.0
			ttx = cntp[j,0]+adist/2.0
			tty = cntp[j,1]+adist/2.0
			if cpoints[i,0] >= tbx and cpoints[i,0] <= ttx and cpoints[i,1] >= tby and cpoints[i,1] <= tty:
				k = j
			else:
				pass
		mlist.append([getp_t[k][0],cpoints[i],crange,pdir[i]])
	return mlist

### Extract polygons
def MakeList(xData):
	getp    = xData[0]
	pcenter = xData[1]
	crange  = xData[2]
	pdir    = xData[3]

	spx = float(pcenter[0])-crange/2.0
	spy = float(pcenter[1])-crange/2.0
	epx = spx + crange
	epy = spy + crange

	getpxs = []
	for i in xrange(len(getp)):
		xs = np.where(getp[i][:,0] >= spx)
		if len(xs[0]) == 0:
			continue
		else:
			getpxs.append(getp[i])
	getpxe = []
	for i in xrange(len(getpxs)):
		xe = np.where(getpxs[i][:,0] <= epx)
		if len(xe[0]) == 0:
			continue
		else:
			getpxe.append(getpxs[i])
	getpys = []
	for i in xrange(len(getpxe)):
		ys = np.where(getpxe[i][:,1] >= spy)
		if len(ys[0]) == 0:
			continue
		else:
			getpys.append(getpxe[i])
	getpf = []
	for i in xrange(len(getpys)):
		ye = np.where(getpys[i][:,1] <= epy)
		if len(ye[0]) == 0:
			continue
		else:
			getpf.append(getpys[i])
	return [getpf,[round(spx,4),round(epx,4),round(spy,4),round(epy,4)],pdir]

### Slice and rotate GDS
def SliceGDS(xData):
	getp   = xData[0]
	points = xData[1]
	pdir   = xData[2]
	tgds   = gdspy.slice(getp, [points[0],points[1]], 0)
	fgds   = gdspy.slice(tgds[1], [points[2],points[3]], 1)

	cx     = points[0]+(points[1]-points[0])/2
	cy     = points[2]+(points[3]-points[2])/2

	### up
	if pdir == 0:
		rgds = fgds[1]
	### right:90
	elif pdir == 1:
		rgds = fgds[1].rotate(np.pi/2,center=(cx,cy))
	### down:180
	elif pdir == 2:
		rgds = fgds[1].rotate(np.pi,center=(cx,cy))
	### left:270
	elif pdir == 3:
		rgds = fgds[1].rotate(-np.pi/2,center=(cx,cy))

	#print cx,cy
	#cname = 'test'+str(pdir)
	#fname = cname+'.test.gds'
	#test_cell = gdspy.Cell(cname)
	#test_cell.add(rgds)
	#gdspy.gds_print(fname,cells=[cname],unit=1.0e-6,precision=1.0e-9)
	#gdspy.LayoutViewer()
	
	return rgds

### Compute density vector
def GetArea(xData):
	sgds   = xData[0]
	crange = xData[1]
	ddim   = xData[2]
	dwidth = xData[3]
	dnum   = xData[4]
	pcenter= xData[5]

	### set calc. area
	spx = float(pcenter[0])-crange/2.0
	spy = float(pcenter[1])-crange/2.0
	epx = spx + crange
	epy = spy + crange

	### create img array
	igrid = int(crange*1000)
	im    = Image.new('L',(igrid,igrid))
	draw  = ImageDraw.Draw(im)
	lbx   = spx*1000
	lby   = spy*1000
	tdif  = np.array([lbx,lby],dtype=int)

	for pol in sgds.polygons:
		test  = np.array(pol*1000 - tdif, dtype=int)
		test2 = []

		### grid2pix
		nn = test.shape[0]
		for j in xrange(nn):
			if j == 0 or j == nn-1:
				test2.append(test[j])
				continue
			if test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] > test[j,1] and test[j,0] < test[j+1,0]:
				test2.append(test[j])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
				test2.append([test[j,0]-1,test[j,1]])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
				test2.append(test[j])
			elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] > test[j+1,0]:
				test2.append([test[j,0]-1,test[j,1]])
			elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] < test[j+1,0]:
				test2.append([test[j,0]-1,test[j,1]])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
				test2.append(test[j])
			elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
				test2.append([test[j,0],test[j,1]-1])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] < test[j+1,1]:
				test2.append([test[j,0]-1,test[j,1]])
			else:
				test2.append(test[j])
		test2 = np.array(test2,dtype=int)

		### pix_cord2poly
		tp   = [tuple([test2[j,0],test2[j,1]]) for j in xrange(test.shape[0])]

		### draw polygon
		draw.polygon(tp,fill=255)

	### transpose
	im = im.transpose(Image.FLIP_TOP_BOTTOM)

	### check img
	#tname = 'test_'+str(pcenter[0])+'_'+str(pcenter[1])+'.png'
	#im.save(tname)

	### img2array
	imArray = np.asarray(im)
	del im

	### calc. area
	pwidth = int(np.ceil((float(crange)/float(ddim)*1000)))

	### area vectors
	area_vecs = np.zeros(dnum,dtype=int)

	### extract area value / segment
	for i in xrange(ddim):
		for j in xrange(ddim):
			k  = i+j*ddim
			sx = i*pwidth
			sy = j*pwidth
			ex = sx + pwidth
			ey = sy + pwidth
			
			sArray = imArray[sy:ey,sx:ex]
			area_vecs[k] = len(np.where(sArray==255)[0])
	
	return area_vecs

### Concentric square sampling
def GetCSS(xData):
	sgds   = xData[0]
	crange = xData[1]
	dimn   = xData[2]
	pcenter= xData[3]
	pdir   = 0

	### set calc. area
	spx = float(pcenter[0])-crange/2.0
	spy = float(pcenter[1])-crange/2.0
	epx = spx + crange
	epy = spy + crange

	### create img array
	igrid = int(crange*1000)
	im    = Image.new('L',(igrid,igrid))
	draw  = ImageDraw.Draw(im)
	lbx   = spx*1000
	lby   = spy*1000
	tdif  = np.array([lbx,lby],dtype=int)

	for pol in sgds.polygons:
		test  = np.array(pol*1000 - tdif, dtype=int)
		test2 = []

		### grid2pix
		nn = test.shape[0]
		for j in xrange(nn):
			if j == 0 or j == nn-1:
				test2.append(test[j])
				continue
			if test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] > test[j,1] and test[j,0] < test[j+1,0]:
				test2.append(test[j])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
				test2.append([test[j,0]-1,test[j,1]])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
				test2.append(test[j])
			elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] > test[j+1,0]:
				test2.append([test[j,0]-1,test[j,1]])
			elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] < test[j+1,0]:
				test2.append([test[j,0]-1,test[j,1]])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
				test2.append(test[j])
			elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
				test2.append([test[j,0],test[j,1]-1])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] < test[j+1,1]:
				test2.append([test[j,0]-1,test[j,1]])
			else:
				test2.append(test[j])
		test2 = np.array(test2,dtype=int)

		### pix_cord2poly
		tp   = [tuple([test2[j,0],test2[j,1]]) for j in xrange(test.shape[0])]

		### draw polygon
		draw.polygon(tp,fill=255)

	### transpose
	im = im.transpose(Image.FLIP_TOP_BOTTOM)

	### resize
	#rsize = igrid/5
	#im    = im.resize((rsize,rsize))

	### check img
	#tname = 'test_'+str(pcenter[0])+'_'+str(pcenter[1])+'.png'
	#im.save(tname)

	### img2array
	imArray = np.asarray(im)
	del im

	### calc. dim.
	instep  = 10
	oustep  = 20
	pradius = int(imArray.shape[0])/2
	xdim    = int((dimn/instep+np.floor((pradius-dimn)/oustep))*8+1)

	### pix vectors
	pix_vecs = np.zeros(xdim,dtype=int)
	xstep    = np.r_[np.arange(0,dimn,instep,dtype=int),np.arange(dimn,pradius,oustep,dtype=int)]
	
	### concentric square sampling
	zz = 1
	for ii in xstep:
		if ii == 0:
			pix_vecs[0] = imArray[pradius,pradius]
		else:
			pix_vecs[zz]   = imArray[pradius-ii,pradius+ii] ### Left top
			pix_vecs[zz+1] = imArray[pradius,pradius+ii]    ### Center top
			pix_vecs[zz+2] = imArray[pradius+ii,pradius+ii] ### Right top
			pix_vecs[zz+3] = imArray[pradius+ii,pradius]    ### Right center
			pix_vecs[zz+4] = imArray[pradius+ii,pradius-ii] ### Right bottom
			pix_vecs[zz+5] = imArray[pradius,pradius-ii]    ### Center bottom
			pix_vecs[zz+6] = imArray[pradius-ii,pradius-ii] ### Left bottom
			pix_vecs[zz+7] = imArray[pradius-ii,pradius]    ### Left center
			zz += 8
		# endif
	# end for
	pix_vecs[pix_vecs==255] = 1
	
	return pix_vecs

### Concentric Circle with Area Sampling
def GetCCSA(xData):
	sgds   = xData[0]
	crange = xData[1]
	dimn   = xData[2]
	pcenter= xData[3]
	pdir   = 0

	### set calc. area
	spx = float(pcenter[0])-crange/2.0
	spy = float(pcenter[1])-crange/2.0
	epx = spx + crange
	epy = spy + crange

	### create img array
	igrid = int(crange*1000)
	im    = Image.new('L',(igrid,igrid))
	draw  = ImageDraw.Draw(im)
	lbx   = spx*1000
	lby   = spy*1000
	tdif  = np.array([lbx,lby],dtype=int)

	for pol in sgds.polygons:
		test  = np.array(pol*1000 - tdif, dtype=int)
		test2 = []

		### grid2pix
		nn = test.shape[0]
		for j in xrange(nn):
			if j == 0 or j == nn-1:
				test2.append(test[j])
				continue
			if test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] > test[j,1] and test[j,0] < test[j+1,0]:
				test2.append(test[j])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
				test2.append([test[j,0]-1,test[j,1]])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
				test2.append(test[j])
			elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] > test[j+1,0]:
				test2.append([test[j,0]-1,test[j,1]])
			elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] < test[j+1,0]:
				test2.append([test[j,0]-1,test[j,1]])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
				test2.append(test[j])
			elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
				test2.append([test[j,0],test[j,1]-1])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] < test[j+1,1]:
				test2.append([test[j,0]-1,test[j,1]])
			else:
				test2.append(test[j])
		test2 = np.array(test2,dtype=int)

		### pix_cord2poly
		tp   = [tuple([test2[j,0],test2[j,1]]) for j in xrange(test.shape[0])]

		### draw polygon
		draw.polygon(tp,fill=255)

	### transpose
	im = im.transpose(Image.FLIP_TOP_BOTTOM)

	### resize
	#rsize = igrid/5
	#im    = im.resize((rsize,rsize))

	### check img
	#tname = 'test_'+str(pcenter[0])+'_'+str(pcenter[1])+'.png'
	#im.save(tname)

	### img2array
	imArray = np.asarray(im)
	del im

	### calc. dim.
	instep  = 10
	oustep  = 20
	prad    = int(imArray.shape[0])/2
	xdim    = int((dimn/instep+np.floor((prad-dimn)/oustep))*8+1)

	### pix vectors
	pix_vecs = np.zeros(xdim,dtype=int)
	xstep    = np.r_[np.arange(0,dimn,instep,dtype=int),np.arange(dimn,prad,oustep,dtype=int)]
	#print xstep
	
	### concentric circle sampling
	gr    = 3 # (lambda/(2NA(1+sigma)))^2
	ry,rx = np.ogrid[-gr:gr+1,-gr:gr+1]
	mask  = rx*rx+ry*ry <= gr*gr
	zz    = 1

	pix_vecs[0] = int(np.sum(imArray[prad-gr:prad+gr+1,prad-gr:prad+gr+1][mask])/255)
	for ii in xstep[1:]:
		disp = int(np.floor( ii*np.cos(45*np.pi/180.0) ))
		pmd = prad-disp
		ppd = prad+disp
		pmi = prad-ii
		ppi = prad+ii
		pix_vecs[zz]   = int(np.sum(imArray[pmd-gr:pmd+gr+1,ppd-gr:ppd+gr+1][mask])/255) ### Left top
		pix_vecs[zz+1] = int(np.sum(imArray[prad-gr:prad+gr+1,ppi-gr:ppi+gr+1][mask])/255) ### Center top
		pix_vecs[zz+2] = int(np.sum(imArray[ppd-gr:ppd+gr+1,ppd-gr:ppd+gr+1][mask])/255) ### Right top
		pix_vecs[zz+3] = int(np.sum(imArray[ppi-gr:ppi+gr+1,prad-gr:prad+gr+1][mask])/255) ### Right center
		pix_vecs[zz+4] = int(np.sum(imArray[ppd-gr:ppd+gr+1,pmd-gr:pmd+gr+1][mask])/255) ### Right bottom
		pix_vecs[zz+5] = int(np.sum(imArray[prad-gr:prad+gr+1,pmi-gr:pmi+gr+1][mask])/255) ### Center bottom
		pix_vecs[zz+6] = int(np.sum(imArray[pmd-gr:pmd+gr+1,pmd-gr:pmd+gr+1][mask])/255) ### Left bottom
		pix_vecs[zz+7] = int(np.sum(imArray[pmi-gr:pmi+gr+1,prad-gr:prad+gr+1][mask])/255) ### Left center
		zz += 8
	# end for
	
	return pix_vecs

###HLAC
def GetHLAC(xData):
	sgds   = xData[0]
	crange = xData[1]
	pcenter= xData[2]
	iStep  = xData[3]
	rSize  = xData[4]

	### set calc. area
	spx = float(pcenter[0])-crange/2.0
	spy = float(pcenter[1])-crange/2.0
	epx = spx + crange
	epy = spy + crange

	### create img array
	igrid = int(crange*1000)
	im    = Image.new('L',(igrid,igrid))
	draw  = ImageDraw.Draw(im)
	lbx   = spx*1000
	lby   = spy*1000
	tdif  = np.array([lbx,lby],dtype=int)

	for pol in sgds.polygons:
		test  = np.array(pol*1000 - tdif, dtype=int)
		test2 = []

		### grid2pix
		nn = test.shape[0]
		for j in xrange(nn):
			### cv_lb:1, cv_rb:2, cv_lt:3, cv_rt:4, cc_lt:5, cc_rt:6, cc_rb:7, cc_lb:8
			if j == 0 or j == nn-1:
				test2.append(test[j])
				continue

			if test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] > test[j,1] and test[j,0] < test[j+1,0]:
				test2.append(test[j])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
				test2.append([test[j,0]-1,test[j,1]])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
				test2.append(test[j])
			elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] > test[j+1,0]:
				test2.append([test[j,0]-1,test[j,1]])
			elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] < test[j+1,0]:
				test2.append([test[j,0]-1,test[j,1]])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
				test2.append(test[j])
			elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
				test2.append([test[j,0],test[j,1]-1])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] < test[j+1,1]:
				test2.append([test[j,0]-1,test[j,1]])
			else:
				test2.append(test[j])
		test2 = np.array(test2,dtype=int)

		### pix_cord2poly
		tp   = [tuple([test2[j,0],test2[j,1]]) for j in xrange(test.shape[0])]

		### draw polygon
		draw.polygon(tp,fill=255)

	### transpose
	im = im.transpose(Image.FLIP_TOP_BOTTOM)

	### resize (x nm/pix)
	imArray = []
	for j in rSize:
		rsize = int(igrid/j)
		imArray.append(im.resize((rsize,rsize)))

	im = np.array(im,dtype=int)
	r1,r2 = im.shape

	### compute HLAC
	hvec_temp = []
	for j in iStep:
		hvec_temp.extend( CGetHLAC(im,r1,r2,j) )

	if len(rSize) == 1:
		pass
	else:
		for j in xrange(1,len(rSize)):
			im_tmp = np.array(imArray[j],dtype=int)
			r1,r2  = im_tmp.shape
			hvec_temp.extend( CGetHLAC(im_tmp,r1,r2,1) )
	del im,imArray

	hvec = np.array(hvec_temp,dtype=int)
	return hvec

### HOG
def GetHOG(xData):
	sgds   = xData[0]
	crange = xData[1]
	ksize  = xData[2] ### Gaussian filter size
	pcenter= xData[3]

	### set calc. area
	spx = float(pcenter[0])-crange/2.0
	spy = float(pcenter[1])-crange/2.0
	epx = spx + crange
	epy = spy + crange

	### create img array
	igrid = int(crange*1000)
	im    = Image.new('L',(igrid,igrid))
	draw  = ImageDraw.Draw(im)
	lbx   = spx*1000
	lby   = spy*1000
	tdif  = np.array([lbx,lby],dtype=int)

	for pol in sgds.polygons:
		test  = np.array(pol*1000 - tdif, dtype=int)
		test2 = []

		### grid2pix
		nn = test.shape[0]
		for j in xrange(nn):
			### cv_lb:1, cv_rb:2, cv_lt:3, cv_rt:4, cc_lt:5, cc_rt:6, cc_rb:7, cc_lb:8
			if j == 0 or j == nn-1:
				test2.append(test[j])
				continue

			if test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] > test[j,1] and test[j,0] < test[j+1,0]:
				test2.append(test[j])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
				test2.append([test[j,0]-1,test[j,1]])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
				test2.append(test[j])
			elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] > test[j+1,0]:
				test2.append([test[j,0]-1,test[j,1]])
			elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] < test[j+1,0]:
				test2.append([test[j,0]-1,test[j,1]])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
				test2.append(test[j])
			elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
				test2.append([test[j,0],test[j,1]-1])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] < test[j+1,1]:
				test2.append([test[j,0]-1,test[j,1]])
			else:
				test2.append(test[j])
		test2 = np.array(test2,dtype=int)

		### pix_cord2poly
		tp   = [tuple([test2[j,0],test2[j,1]]) for j in xrange(test.shape[0])]

		### draw polygon
		draw.polygon(tp,fill=255)

	### transpose
	im = im.transpose(Image.FLIP_TOP_BOTTOM)

	### resize (5nm/pix)
	rsize = igrid/5
	im    = im.resize((rsize,rsize))

	### Sigma = 0.3*(ksize/2-1)+0.8
	im_gs = cv2.GaussianBlur(np.asarray(im),(ksize,ksize),0)
	#cv2.imshow('test',im_gs)
	#cv2.waitKey()

	### Window size, Block size(16,16)only, Block stride, Cell size(8,8)only, nbins 9only
	cv_hog = cv2.HOGDescriptor((64,64),(16,16),(8,8),(8,8),9)
	hvecs  = cv_hog.compute(np.asarray(im_gs))
	del im,im_gs

	### calc. dim.
	hog_dim = len(hvecs)

	### check img
	#tname = 'test_'+str(pcenter[0])+'_'+str(pcenter[1])+'.png'
	#im.save(tname)

	return hvecs

### Gradient Local Auto Correlation
def GetGLACv(xData):
	sgds   = xData[0]
	crange = xData[1]
	ksize  = xData[2]
	pcenter= xData[3]

	### set calc. area
	spx = float(pcenter[0])-crange/2.0
	spy = float(pcenter[1])-crange/2.0
	epx = spx + crange
	epy = spy + crange

	### create img array
	igrid = int(crange*1000)
	im    = Image.new('L',(igrid,igrid))
	draw  = ImageDraw.Draw(im)
	lbx   = spx*1000
	lby   = spy*1000
	tdif  = np.array([lbx,lby],dtype=int)

	for pol in sgds.polygons:
		test  = np.array(pol*1000 - tdif, dtype=int)
		test2 = []

		### grid2pix
		nn = test.shape[0]
		for j in xrange(nn):
			### cv_lb:1, cv_rb:2, cv_lt:3, cv_rt:4, cc_lt:5, cc_rt:6, cc_rb:7, cc_lb:8
			if j == 0 or j == nn-1:
				test2.append(test[j])
				continue

			if test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] > test[j,1] and test[j,0] < test[j+1,0]:
				test2.append(test[j])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
				test2.append([test[j,0]-1,test[j,1]])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
				test2.append(test[j])
			elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] > test[j+1,0]:
				test2.append([test[j,0]-1,test[j,1]])
			elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] < test[j+1,0]:
				test2.append([test[j,0]-1,test[j,1]])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
				test2.append(test[j])
			elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
				test2.append([test[j,0],test[j,1]-1])
			elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] < test[j+1,1]:
				test2.append([test[j,0]-1,test[j,1]])
			else:
				test2.append(test[j])
		test2 = np.array(test2,dtype=int)

		### pix_cord2poly
		tp   = [tuple([test2[j,0],test2[j,1]]) for j in xrange(test.shape[0])]

		### draw polygon
		draw.polygon(tp,fill=255)

	### transpose
	im = im.transpose(Image.FLIP_TOP_BOTTOM)

	### resize (5nm/pix)
	#rsize = igrid/5
	#im    = im.resize((rsize,rsize))

	### Sigma = 0.3*(ksize/2-1)+0.8
	im_gs = cv2.GaussianBlur(np.asarray(im),(ksize,ksize),0)

	### Compute GLAC
	glacvecs = GetGLAC(im_gs.astype(float))
	del im,im_gs

	### calc. dim.
	#glac_dim = len(glacvecs)

	return glacvecs

### Feature extraction for OPC regression
def GetFeature(pp,gdsii,ss,data):

	ttime1 = time.time()

	print '### Feature extraction...',

	if pp['Feature'] == 'Density':
		fmode = 'dens'
		ddim  = pp['Dense_Grid']
		dnum  = int(ddim*ddim)
	elif pp['Feature'] == 'Diffr':
		fmode = 'diffr'
	elif pp['Feature'] == 'CSS':
		fmode = 'css'
		n_dim = pp['CSS_Rin']
	elif pp['Feature'] == 'CCSA':
		fmode = 'ccsa'
		n_dim = pp['CCS_Rin']
	elif pp['Feature'] == 'HLAC':
		fmode   = 'hlac'
		corrwid = map(int,pp['HLAC_Corr'].split(','))
		pyramid = map(int,pp['HLAC_Pyramid'].split(','))
	elif pp['Feature'] == 'Gauss_HOG':
		ksize = pp['HOG_Gauss']
		fmode = 'hog'
		if ksize <= 0 or ksize %2 != 1:
			print ' Gauss kernel size error'
			quit()
	elif pp['Feature'] == 'Gauss_GLAC':
		ksize = pp['GLAC_Gauss']
		fmode = 'glac'
		if ksize <= 0 or ksize %2 != 1:
			print ' Gauss kernel size error'
			quit()
	else:
		print ' feature mode error.'
		quit()

	### Common parameters
	if data == 'train':
		layer    = pp['TrainLayer']
		dtype    = pp['TrainDtype']
		ngds     = pp['TrainGDS']
		vecf     = pp['TrainVecs']
	elif data == 'test':
		layer    = pp['TestLayer']
		dtype    = pp['TestDtype']
		ngds     = pp['TestGDS']
		vecf     = pp['TestVecs']
	crange   = pp['F_Wx']	 ### calc. area
	proc_num = pp['ProcNum'] ### #of cores
	cp       = ss[:,2:4]	 ### ss: plus_dir, disp, gdsx, gdsy, edge_dir, edge_length, edge_type

	### Check feature vecs (if feature vector exists, skip feature extraction)
	if os.path.isfile(vecf) and pp['F_Gsearch'] != 'ON':
		if fmode == 'dens' or fmode == 'css' or fmode == 'ccsa' or fmode == 'hlac':
			fvec = np.genfromtxt(vecf)
		else:
			fvec = np.load(vecf)

		if pp['Dsize'] != 'All' and data == 'train':
			nd   = pp['Dindex']
			if pp['Dsize'] == 'Large':
				nd = nd[0:10000]
			elif pp['Dsize'] == 'Middle':
				nd = nd[0:5000]
			elif pp['Dsize'] == 'Small':
				nd = nd[0:1000]
			fvec = fvec[nd]

		print '\tFeature file exists'
		print '\tFeature type\t\t: %s' %fmode
		print '\t#samples\t\t: %d' %len(fvec)
		print '\t#dimensions\t\t: %d' %len(fvec[0])
		print '\telapsed time (sec)\t: %.3f' %(time.time()-ttime1)

		return fvec

	### Extract features
	if fmode == 'diffr' or fmode == 'dens' or fmode == 'css' or fmode == 'ccsa' or fmode == 'hlac' or fmode == 'hog' or fmode == 'glac':
		### Import polygons
		if data == 'train':
			ingds = gdsii.extract('IN_TOP_tr')
		elif data == 'test':
			ingds = gdsii.extract('IN_TOP_te')
		getp  = ingds.get_polygons(True)
		getp  = getp[(layer,dtype)]
	
		### Set parameters
		centerp = np.array(cp,dtype=float)/1000.0
		tnum    = len(centerp)
		
		### Clip GDS
		xmin = np.min([np.min(getp[i][:,0]) for i in xrange(len(getp))])
		xmax = np.max([np.max(getp[i][:,0]) for i in xrange(len(getp))])
		ymin = np.min([np.min(getp[i][:,1]) for i in xrange(len(getp))])
		ymax = np.max([np.max(getp[i][:,1]) for i in xrange(len(getp))])
		adist = ((xmax-xmin)+(ymax-ymin))/5.0
		anumx = 1
		anumy = 1
		if xmax-xmin > adist: 
			anumx = int(np.ceil((xmax-xmin)/adist))
		if ymax-ymin > adist:
			anumy = int(np.ceil((ymax-ymin)/adist))
		total_anum = int(anumx*anumy)
	
		cntp = []
		for i in xrange(anumy):
			for j in xrange(anumx):
				cntx = xmin+j*adist+adist/2.0
				cnty = ymin+i*adist+adist/2.0
				cntp.append([round(cntx,2),round(cnty,2)])
	
		cntp   = np.array(cntp)
		mp     = Pool(proc_num)
		trange = adist+crange
		adlist = [[getp,cntp[i],trange,ss[i,0]] for i in xrange(len(cntp))]
		getp_t = mp.map(MakeList,adlist)
		#mlist  = ExtList(len(centerp),crange,adist,centerp,cntp,getp_t,ss[:,0])
		mlist  = CExtList(len(centerp),crange,adist,centerp,cntp,getp_t,ss[:,0])
		mplist = mp.map(MakeList,mlist)
		cgds   = mp.map(SliceGDS,mplist)
		
		### Compute density vector
		if fmode == 'dens':
			mxlist = [[cgds[i],crange,ddim,crange/float(ddim),dnum,centerp[i]] for i in xrange(tnum)]
			fvec   = mp.map(GetArea,mxlist)
		### Compute css
		elif fmode == 'css':
			mxlist = [[cgds[i],crange,n_dim,centerp[i]] for i in xrange(tnum)]
			fvec   = mp.map(GetCSS,mxlist)
		elif fmode == 'ccsa':
			mxlist = [[cgds[i],crange,n_dim,centerp[i]] for i in xrange(tnum)]
			fvec   = mp.map(GetCCSA,mxlist)
		elif fmode == 'hlac':
			mxlist = [[cgds[i],crange,centerp[i],corrwid,pyramid] for i in xrange(tnum)]
			fvec   = mp.map(GetHLAC,mxlist)
		elif fmode == 'hog':
			mxlist = [[cgds[i],crange,ksize,centerp[i]] for i in xrange(tnum)]
			fvec   = mp.map(GetHOG,mxlist)
		elif fmode == 'glac':
			mxlist = [[cgds[i],crange,ksize,centerp[i]] for i in xrange(tnum)]
			fvec   = mp.map(GetGLACv,mxlist)
		else:
			print ' Param. file error'
			quit()
	
		mp.close()
	else:
		print ' Param. file error'
		quit()

	print '  done.'
	print '\tFeature type\t\t: %s' %fmode
	print '\t#samples\t\t: %d' %len(fvec)
	print '\t#dimensions\t\t: %d' %len(fvec[0])
	print '\telapsed time (sec)\t: %.3f' %(time.time()-ttime1)

	### Output feature vector
	pp['OutFeature'] = fmode
	fname = './result/feature.' + data + '.' + fmode + '.init.vec'
	if pp['F_Gsearch'] == 'OFF':
		if fmode == 'dens' or fmode == 'css' or fmode == 'ccsa' or fmode == 'hlac':
			np.savetxt(fname,fvec,fmt='%d')
		else:
			fname = fname+'.npy'
			np.save(fname,fvec)
	else:
		pass

	return np.array(fvec)

