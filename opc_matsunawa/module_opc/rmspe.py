#!/usr/jin/python

import numpy as np
import matplotlib
matplotlib.use('Agg')
import pylab as pl
#from matplotlib.pylab import *

### Output summary def GetSummary(pp,ss_tr,yh_tr,ss_te,yh_te,tt):
def GetRMSPE(pp,ss_tr,yh_tr,ss_te,yh_te,tt):

	print '### Summary...'

	y_tr = ss_tr[:,1]
	y_te = ss_te[:,1]

	### Compute RMSPE
	if pp['Mmode'] == 'all' or pp['Model'] == 'HBM':
		yh_tr  = yh_tr[0]
		yh_te  = yh_te[0]

		tr_tmp = yh_tr-y_tr
		te_tmp = yh_te-y_te

		yh_tr_norm = yh_tr[ss_tr[:,6]==0]
		yh_tr_conv = yh_tr[ss_tr[:,6]==1]
		yh_tr_conc = yh_tr[ss_tr[:,6]==2]
		yh_tr_lend = yh_tr[ss_tr[:,6]==3]

		yh_te_norm = yh_te[ss_te[:,6]==0]
		yh_te_conv = yh_te[ss_te[:,6]==1]
		yh_te_conc = yh_te[ss_te[:,6]==2]
		yh_te_lend = yh_te[ss_te[:,6]==3]

	elif pp['Mmode'] == 'sep':
		yh_tr_norm,yh_tr_conv,yh_tr_conc,yh_tr_lend = yh_tr
		yh_te_norm,yh_te_conv,yh_te_conc,yh_te_lend = yh_te

		tr_tmp = np.r_[yh_tr_norm-y_tr[ss_tr[:,6]==0],np.r_[yh_tr_conv-y_tr[ss_tr[:,6]==1],np.r_[yh_tr_conc-y_tr[ss_tr[:,6]==2],yh_tr_lend-y_tr[ss_tr[:,6]==3]]]]
		te_tmp = np.r_[yh_te_norm-y_te[ss_te[:,6]==0],np.r_[yh_te_conv-y_te[ss_te[:,6]==1],np.r_[yh_te_conc-y_te[ss_te[:,6]==2],yh_te_lend-y_te[ss_te[:,6]==3]]]]

	### RMSPE(nm) all
	rmse_tr_all  = np.sqrt(np.sum(tr_tmp**2)/y_tr.shape[0])
	rmse_te_all  = np.sqrt(np.sum(te_tmp**2)/y_te.shape[0])

	### RMSPE(nm) sep
	rmse_tr_norm = np.sqrt(np.sum((yh_tr_norm-y_tr[ss_tr[:,6]==0])**2)/y_tr[ss_tr[:,6]==0].shape[0])
	rmse_tr_conv = np.sqrt(np.sum((yh_tr_conv-y_tr[ss_tr[:,6]==1])**2)/y_tr[ss_tr[:,6]==1].shape[0])
	rmse_tr_conc = np.sqrt(np.sum((yh_tr_conc-y_tr[ss_tr[:,6]==2])**2)/y_tr[ss_tr[:,6]==2].shape[0])
	rmse_tr_lend = np.sqrt(np.sum((yh_tr_lend-y_tr[ss_tr[:,6]==3])**2)/y_tr[ss_tr[:,6]==3].shape[0])

	rmse_te_norm = np.sqrt(np.sum((yh_te_norm-y_te[ss_te[:,6]==0])**2)/y_te[ss_te[:,6]==0].shape[0])
	rmse_te_conv = np.sqrt(np.sum((yh_te_conv-y_te[ss_te[:,6]==1])**2)/y_te[ss_te[:,6]==1].shape[0])
	rmse_te_conc = np.sqrt(np.sum((yh_te_conc-y_te[ss_te[:,6]==2])**2)/y_te[ss_te[:,6]==2].shape[0])
	rmse_te_lend = np.sqrt(np.sum((yh_te_lend-y_te[ss_te[:,6]==3])**2)/y_te[ss_te[:,6]==3].shape[0])

	### Compute y distributions for testing results
	y_te_norm = y_te[ss_te[:,6]==0]
	y_te_conv = y_te[ss_te[:,6]==1]
	y_te_conc = y_te[ss_te[:,6]==2]
	y_te_lend = y_te[ss_te[:,6]==3]
	yd_norm = np.hstack([np.array([np.sort(y_te_norm)]).T,np.array([yh_te_norm[np.argsort(y_te_norm)]]).T])
	yd_conv = np.hstack([np.array([np.sort(y_te_conv)]).T,np.array([yh_te_conv[np.argsort(y_te_conv)]]).T])
	yd_conc = np.hstack([np.array([np.sort(y_te_conc)]).T,np.array([yh_te_conc[np.argsort(y_te_conc)]]).T])
	yd_lend = np.hstack([np.array([np.sort(y_te_lend)]).T,np.array([yh_te_lend[np.argsort(y_te_lend)]]).T])

	### Plot
	matplotlib.rc('font',family='times new roman')
	pl.figure(figsize=(10,6))
	pl.subplot(2,2,1)
	tx = np.arange(yd_norm.shape[0])
	pl.xlim([0,np.max(tx)])
	pl.plot(tx,yd_norm[:,0],c='b',label='real')
	pl.scatter(tx,yd_norm[:,1],c='y',marker='.',edgecolor='none',label='predicted')
	pl.legend(prop={'size':10})
	pl.subplot(2,2,2)
	tx = np.arange(yd_conv.shape[0])
	pl.xlim([0,np.max(tx)])
	pl.plot(tx,yd_conv[:,0],c='b',label='real')
	pl.scatter(tx,yd_conv[:,1],c='y',marker='.',edgecolor='none',label='predicted')
	pl.legend(prop={'size':10})
	pl.subplot(2,2,3)
	tx = np.arange(yd_conc.shape[0])
	pl.xlim([0,np.max(tx)])
	pl.plot(tx,yd_conc[:,0],c='b',label='real')
	pl.scatter(tx,yd_conc[:,1],c='y',marker='.',edgecolor='none',label='predicted')
	pl.legend(prop={'size':10})
	pl.subplot(2,2,4)
	tx = np.arange(yd_lend.shape[0])
	pl.xlim([0,np.max(tx)])
	pl.plot(tx,yd_lend[:,0],c='b',label='real')
	pl.scatter(tx,yd_lend[:,1],c='y',marker='.',edgecolor='none',label='predicted')
	pl.legend(prop={'size':10})
	pl.savefig('./result/re_frag_movement.png')
	pl.close()

	### Plot CDF
	nbin = 50
	his_norm,bin_norm = np.histogram(np.abs(yd_norm[:,1]-yd_norm[:,0]),bins=nbin,normed=True)
	cdf_norm = np.r_[[0],np.cumsum(his_norm*np.diff(bin_norm))]
	his_conv,bin_conv = np.histogram(np.abs(yd_conv[:,1]-yd_conv[:,0]),bins=nbin,normed=True)
	cdf_conv = np.r_[[0],np.cumsum(his_conv*np.diff(bin_conv))]
	his_conc,bin_conc = np.histogram(np.abs(yd_conc[:,1]-yd_conc[:,0]),bins=nbin,normed=True)
	cdf_conc = np.r_[[0],np.cumsum(his_conc*np.diff(bin_conc))]
	his_lend,bin_lend = np.histogram(np.abs(yd_lend[:,1]-yd_lend[:,0]),bins=nbin,normed=True)
	cdf_lend = np.r_[[0],np.cumsum(his_lend*np.diff(bin_lend))]
	pl.figure()
	pl.xlim([0,20])
	pl.ylim([0.0,1.0])
	pl.plot(np.r_[[0],bin_norm[1:]],cdf_norm,'-',label='normal')
	pl.plot(np.r_[[0],bin_conv[1:]],cdf_conv,'--',label='convex')
	pl.plot(np.r_[[0],bin_conc[1:]],cdf_conc,'-+',label='concave')
	pl.plot(np.r_[[0],bin_lend[1:]],cdf_lend,'-x',label='le')
	pl.legend(loc=4)
	pl.savefig('./result/re_cdf.png')
	pl.close()

	rdir_norm = np.sum([1.0 if yd_norm[i,0]*yd_norm[i,1] >= 0.0 else 0.0 for i in xrange(len(yd_norm))])/len(yd_norm) * 100.0
	rdir_conv = np.sum([1.0 if yd_conv[i,0]*yd_conv[i,1] >= 0.0 else 0.0 for i in xrange(len(yd_conv))])/len(yd_conv) * 100.0
	rdir_conc = np.sum([1.0 if yd_conc[i,0]*yd_conc[i,1] >= 0.0 else 0.0 for i in xrange(len(yd_conc))])/len(yd_conc) * 100.0
	rdir_lend = np.sum([1.0 if yd_lend[i,0]*yd_lend[i,1] >= 0.0 else 0.0 for i in xrange(len(yd_lend))])/len(yd_lend) * 100.0

	### Output results
	print '\tRMSE@tr(all,norm,conv,conc,le)  : %.3f %.3f %.3f %.3f %.3f' %(rmse_tr_all,rmse_tr_norm,rmse_tr_conv,rmse_tr_conc,rmse_tr_lend)
	print '\tRMSE@te(all,norm,conv,conc,le)  : %.3f %.3f %.3f %.3f %.3f' %(rmse_te_all,rmse_te_norm,rmse_te_conv,rmse_te_conc,rmse_te_lend)
	print '\tRight Dir.@te(norm,conv,conc,le): %.1f %.1f %.1f %.1f' %(rdir_norm,rdir_conv,rdir_conc,rdir_lend)

	fout = open('./result/rmse.tmp.txt','w')
	fout.write('tr\n')
	fout.write('%.3f\n%.3f\n%.3f\n%.3f\n%.3f\n' %(rmse_tr_all,rmse_tr_norm,rmse_tr_conv,rmse_tr_conc,rmse_tr_lend))
	fout.write('%.3f\n%.3f\n' %(tt[0],tt[1]))
	fout.write('\nte\n')
	fout.write('%.3f\n%.3f\n%.3f\n%.3f\n%.3f\n' %(rmse_te_all,rmse_te_norm,rmse_te_conv,rmse_te_conc,rmse_te_lend))
	fout.write('%.3f\n%.3f\n' %(tt[0],tt[2]))
	fout.write('\nRight Dir. te(%)\n')
	fout.write('%.1f\n%.1f\n%.1f\n%.1f\n' %(rdir_norm,rdir_conv,rdir_conc,rdir_lend))
	fout.close()

	return [rmse_tr_all,rmse_te_all]
	### End GetRMSPE

