#!/usr/bin/python

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext as build_pyx

import numpy
setup(
	name = 'ExtList',
	cmdclass = {'build_ext': build_pyx},
	ext_modules=[Extension('extlist', ['extlist.pyx'],)])
