#!/usr/bin/python

import gdspy

### Import GDS
def GdsIn(pp,data):

	print '### Import GDS...',

	if data == 'train':
		gname  = pp['TrainGDS']
		tcell  = pp['TrainTop']
		tcell2 = 'IN_TOP_tr'
		nlay   = (pp['TrainLayer'],pp['TrainDtype'])
	elif data == 'test':
		gname  = pp['TestGDS']
		tcell  = pp['TestTop']
		tcell2 = 'IN_TOP_te'
		nlay   = (pp['TestLayer'],pp['TestDtype'])
	gdsii = gdspy.GdsImport(gname,rename={tcell:tcell2})

	### Plot GDS
	#gdsii.extract(tcell2)
	#gdspy.LayoutViewer()

	### Get bounding box
	bbox = gdsii.extract(tcell2).get_bounding_box()
	lx   = bbox[1,0]-bbox[0,0]
	ly   = bbox[1,1]-bbox[0,1]

	### Get area of all polygons
	area = gdsii.extract(tcell2).area(True)
	tarea= area.get(nlay)

	print '  done.'
	print '\tGDS\t\t\t: %s' %gname
	print '\tBounding box (x,y)\t: %.3f %.3f (um)' %(lx,ly)
	print '\tTotal area\t\t: %.4f (um2)' %tarea
	
	return gdsii
