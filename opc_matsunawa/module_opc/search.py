#!/usr/bin/python

import os
import sys
import time
import numpy as np

sys.path.insert(0,os.path.join('./'))
from gdsii    import GdsIn
from cal_rule import GetOPCedData,RedCalcPoint
from feature  import GetFeature
from comp     import DimReduction,Projection
from model    import FitModel,Predict
from rmspe    import GetRMSPE

### Grid search for feature parameters
def OptFparam(pp):
	tt = time.time()

	print '### Grid search...'
	wxr = map(float,pp['F_Wx_range'].split(','))

	if pp['Feature'] == 'Density':
		param2 = map(int,pp['Dense_GridR'].split(','))
	elif pp['Feature'] == 'CSS':
		param2 = map(int,pp['CSS_RinRange'].split(','))
	elif pp['Feature'] == 'CCSA':
		param2 = map(int,pp['CCS_RinRange'].split(','))
	elif pp['Feature'] == 'HLAC':
		param2 = [0]
	elif pp['Feature'] == 'Gauss_HOG':
		param2 = map(int,pp['HOG_Grange'].split(','))
	elif pp['Feature'] == 'Gauss_GLAC':
		param2 = map(int,pp['GLAC_Grange'].split(','))
	
	res = []

	gds_tr   = GdsIn(pp,'train')				### Read gds
	ss_tr    = GetOPCedData(pp,gds_tr,'train')		### Run OPC/DRC to get opced data
	ss_tr    = RedCalcPoint(pp,ss_tr,'train')		### Reduce #of samples
	gds_te   = GdsIn(pp,'test')				### Read gds
	ss_te    = GetOPCedData(pp,gds_te,'test')		### Run OPC/DRC to get opced data
	ss_te    = RedCalcPoint(pp,ss_te,'test')		### Reduce #of samples

	for ii in wxr:
		for jj in param2:
			pp['F_Wx'] = ii
			if pp['Feature'] == 'Density':
				pp['Dense_Grid'] = jj
			elif pp['Feature'] == 'CSS':
				pp['CSS_Rin'] = jj
			elif pp['Feature'] == 'CCSA':
				pp['CCS_Rin'] = jj
			elif pp['Feature'] == 'HLAC':
				pass
			elif pp['Feature'] == 'Gauss_HOG':
				pp['HOG_Gauss'] = jj
			elif pp['Feature'] == 'Gauss_GLAC':
				pp['GLAC_Gauss'] = jj
	
			### Training phase
			try:
				fvec_tr  = GetFeature(pp,gds_tr,ss_tr,'train')	### Feature extraction
				rfv_tr   = DimReduction(pp,fvec_tr,ss_tr)	### Dimensionality reduction
				opcmodel,t0 = FitModel(pp,rfv_tr[0],ss_tr)	### Model training
			
				print '### Finish training ###\n'
			
				### Testing pahse
				fvec_te  = GetFeature(pp,gds_te,ss_te,'test')		### Feature extraction
				rfv_te   = Projection(pp,rfv_tr[1],fvec_tr,fvec_te,ss_tr,ss_te) 	### Projection
			
				yh_tr,t1 = Predict(pp,opcmodel,rfv_tr[0],ss_tr)		### Predict @ training data
				yh_te,t2 = Predict(pp,opcmodel,rfv_te,ss_te)		### Predict @ testing data
			
				print '### Finish testing ###\n'
			
				rmse = GetRMSPE(pp,ss_tr,yh_tr,ss_te,yh_te,[t0,t1,t2])	### Summary
	
				res.append([ii,jj,rmse[0],rmse[1],rfv_tr[0].shape[1]])
			except:
				res.append([ii,jj,999,999,0])
		### End for
	### End for

	fout = open('./result/gridsearch.txt','w')
	for i in xrange(len(res)):
		fout.write('%.1f %d %.5f %.5f %d\n' %(res[i][0],res[i][1],res[i][2],res[i][3],res[i][4]))
	fout.close()

	print '### Elapsed Time (sec): %.4f' %(time.time()-tt)
	### End

