#!/bin/csh -f

set cpu_num = 8
set opcdeck = ./cali/opc.rule 
set lccdeck = ./cali/lcc.rule 

### make tcc
cd model
calibrewb -a opticsgen -opticalfile optfile -o optical.opt
cd ../

### OPC
calibre -drc -hier -turbo -turbo_litho ${cpu_num} -64 -hyper ${opcdeck} 

### LCC
calibre -drc -hier -turbo -turbo_litho ${cpu_num} -64 -hyper ${lccdeck} 


