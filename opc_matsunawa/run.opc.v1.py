#!/usr/bin/python

import os
import sys
import time
import numpy as np

sys.path.insert(0,os.path.join('./module_opc'))
from init     import Init
from search   import OptFparam
from gdsii    import GdsIn
from cal_rule import GetOPCedData,RedCalcPoint
from feature  import GetFeature
from comp     import DimReduction,Projection
from model    import FitModel,Predict
from rmspe    import GetRMSPE
from epe      import GetEPE

### Print welcome messages
def printWelcome():
	print '\n'
	print '# ===================================='
	print '#         OPCRpy - version 0.1        '
	print '#                                     '
	print '# Function    : Regression-based OPC  '
	#print '# Authors     : Tetsuaki Matsunawa    '
	print '# Last update : 12/11/2014            '
	print '# ===================================='

### Main
def main():
	printWelcome()
	tt = time.time()
	pp = Init(sys.argv)					### Read parameter file

	### Feature parameter tuning
	if pp['F_Gsearch'] == 'ON':
		OptFparam(pp)
		quit()
	else:
		pass

	### Training phase
	gds_tr      = GdsIn(pp,'train')				### Read gds
	ss_tr       = GetOPCedData(pp,gds_tr,'train')		### Run OPC/DRC to get opced data
	ss_tr       = RedCalcPoint(pp,ss_tr,'train')		### Reduce #of samples
	fvec_tr     = GetFeature(pp,gds_tr,ss_tr,'train')	### Feature extraction
	rfv_tr      = DimReduction(pp,fvec_tr,ss_tr)		### Dimensionality reduction
	opcmodel,t0 = FitModel(pp,rfv_tr[0],ss_tr)		### Model training

	print '### Finish training ###\n'

	### Testing pahse
	gds_te   = GdsIn(pp,'test')				### Read gds
	ss_te    = GetOPCedData(pp,gds_te,'test')		### Run OPC/DRC to get opced data
	ss_te    = RedCalcPoint(pp,ss_te,'test')		### Reduce #of samples
	fvec_te  = GetFeature(pp,gds_te,ss_te,'test')		### Feature extraction
	rfv_te   = Projection(pp,rfv_tr[1],fvec_tr,fvec_te,ss_tr,ss_te)	### Projection

	yh_tr,t1 = Predict(pp,opcmodel,rfv_tr[0],ss_tr)		### Predict @ training data
	yh_te,t2 = Predict(pp,opcmodel,rfv_te,ss_te)		### Predict @ testing data

	print '### Finish testing ###\n'

	GetRMSPE(pp,ss_tr,yh_tr,ss_te,yh_te,[t0,t1,t2])		### Compute RMSPE
	#GetEPE(pp,ss_tr,yh_tr,gds_tr,'train')
	#GetEPE(pp,ss_te,yh_te,gds_te,'test')

	print '### Elapsed Time (sec): %.4f' %(time.time()-tt)
	### End

if __name__ == "__main__":
	main()
