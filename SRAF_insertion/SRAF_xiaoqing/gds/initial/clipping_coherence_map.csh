#!/bin/csh -f 

#clip region file
set filename = "clip_regions"

#directories
set script_folder = "/home/intern3/SRAF_insertion/SRAF_xiaoqing/gds/initial"
set map_clip_folder = "/home/intern3/SRAF_insertion/SRAF_xiaoqing/gds/train/coherence_map_clips_all"
set map_out_folder = "/home/intern3/MAP_OUT"

#replace the test_contact.gds
cp "./test_contact.gds" "$map_out_folder"

#create each layout clip
set clip_index = 0
foreach line ("`cat $filename`")
	@ clip_index += 1
	cd $map_out_folder
  echo clipping window - $line
	./_start_bat_arg.csh $line
	cp "opc_sim.gds" "$map_clip_folder/test_contact_clip_$clip_index.gds"
	/bin/rm image_map_*
	cd $script_folder
end
