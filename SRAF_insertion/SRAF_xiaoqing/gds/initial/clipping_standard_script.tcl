set filename "running_script.tcl"
set fileId [open $filename "w"]
set filename "clip_regions"
set clipId [open $filename "w"]
# load source layout
puts $fileId "set l_src \[layout create \"test_standard.gds\"\]"
set l_src [layout create "test_standard.gds"]
puts $fileId "set top_cell \[\$l_src topcell\]"
set top_cell [$l_src topcell]
# create empty layout to be used as output
set texts [$l_src textout $top_cell]

#create each layout clip
#half box width
set dist 3500
set flag 1
foreach text_item $texts {
	set index 0
	set x 0
  set y 0
	#set l_clip [layout create]
	#$l_clip create cell TOP
	#$l_clip units database [$l_src units database]
	foreach item $text_item {
		if {$index == 1} { set x [expr $item] }
		if {$index == 2} { set y [expr $item] }
		incr index
	}
	if { $flag == 1} { 
		set flag 0 
		continue
	}
	#puts $text_item
	#create clipping region
  set llx [expr {$x-$dist}]
  set lly [expr {$y-$dist}]
  set urx [expr {$x+$dist}]
	set ury [expr {$y+$dist}]
  set clip_region "\{$llx $lly $urx $ury\}"
	#puts $clip_region
	#$l_src create clip $top_cell -clip {7000 7000 14000 14000} 
	puts $fileId "\$l_src create clip \$top_cell -clip $clip_region"
	set llx [expr {$llx/1000.0}]
	set lly [expr {$lly/1000.0}]
	set urx [expr {$urx/1000.0}]
	set ury [expr {$ury/1000.0}]
  set clip_region "$llx $lly $urx $ury"
	puts $clipId "$clip_region"
}

#puts $fileId "\$l_src clipsout test_standard.clips $top_cell"
puts $fileId "\$l_src gdsout test_standard.gds -clipsSplit"
close $fileId
close $clipId

source running_script.tcl
file delete -force "./running_script.tcl"
foreach file_item [glob test_standard_Clip_*] {
	#move the offset to (0, 0)
	set l_src [layout create $file_item]
  set l_bbox [split [$l_src bbox $top_cell]]
	#puts $l_bbox
	set index 0
	foreach item $l_bbox {
		incr index
		if { $index == 1 } { set llx [expr {$item}] }
		if { $index == 2 } { set lly [expr {$item}] }
	}
	$l_src modify origin $top_cell $llx $lly
  #set l_bbox [split [$l_src bbox $top_cell]]
	#puts $l_bbox
	$l_src gdsout $file_item
	#move the layout
	exec mv $file_item "./standard_clips"
}
