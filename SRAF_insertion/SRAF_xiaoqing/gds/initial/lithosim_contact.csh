#!/bin/csh -f 

#directories
set script_folder = "/home/intern3/SRAF_insertion/SRAF_xiaoqing/gds/initial"
set clip_folder = "/home/intern3/SRAF_insertion/SRAF_xiaoqing/gds/initial/contact_clips"
set clip_sim_folder = "/home/intern3/SRAF_insertion/SRAF_xiaoqing/gds/initial/contact_lithosim"
set lithosim_folder = "/home/intern3/SRAF_insertion/OPC_Matsunawa/lithosim"

#litho simulation for each layout clip
cd $script_folder
foreach gds_file ("`ls $clip_folder`")
	set gds_name = `echo $gds_file | cut -d'.' -f1`
	#echo $gds_name
	cp "$clip_folder/$gds_file" "$lithosim_folder/gds"
	
	#calibre rule file modifications
	cd "$lithosim_folder/cali"
  ls opc.rule >& /dev/null
	if `$status ==  0` then 
		rm ./opc.rule 
	endif
  ls lcc.rule >& /dev/null
	if `$status == 0` then 
		rm ./lcc.rule
	endif

	cat ./opc.rule.template | sed -e s/@LAYOUT@/${gds_name}/g > opc.rule
  cat ./lcc.rule.template | sed -e s/@LAYOUT@/${gds_name}/g > lcc.rule
	cd "../"
	#run litho simulation and move the output to clip_sim_folder
	./run_sim.csh
	mv "./results/${gds_name}_lccout.gds" $clip_sim_folder
  cd $script_folder	
	#break
end
