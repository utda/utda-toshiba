#!/home/intern3/local/bin/python2.7

import os
import sys

def expand_edge(dir, cell):
	
	drc_code = """
//Expand edge

LAYOUT PATH    '%s/%s.gds'
LAYOUT PRIMARY 'TOP'
DRC RESULTS DATABASE '%s/%s_EXPAND.gds' GDSII
PRECISION 1000
LAYOUT SYSTEM GDSII
DRC MAXIMUM RESULTS ALL
DRC MAXIMUM VERTEX 4000
//LAYOUT WINDOW 0 0 100 100
//LAYOUT WINDOW CLIP YES

LAYER MAP 1  datatype 0 1001 LAYER char       1001
LAYER MAP 2  datatype 0 1002 LAYER target     1002
LAYER MAP 4  datatype 0 1004 LAYER bias       1004
LAYER MAP 20 datatype 0 1020 LAYER slayer     1020

// r0
hole1 = SIZE target BY 0.100
// r1
hole2 = SIZE target BY 0.500

hole3 = hole2 NOT hole1

OUT_target     {COPY target    } DRC CHECK MAP OUT_target      2  0
OUT_bias       {COPY bias      } DRC CHECK MAP OUT_bias        4  0
OUT_sraf       {COPY slayer    } DRC CHECK MAP OUT_sraf        20 0
OUT_char       {COPY char      } DRC CHECK MAP OUT_char        1  0

OUT_H1         {COPY hole1     } DRC CHECK MAP OUT_H1          21 0
OUT_H2         {COPY hole2     } DRC CHECK MAP OUT_H2          22 0
OUT_H3         {COPY hole3     } DRC CHECK MAP OUT_H3          23 0
""" %(dir,cell,dir,cell)
	
	fs = open(('%s/expand_edge.rule' %dir),'w')
	fs.write(drc_code)
	fs.close()

	### expand the edge using calibre DRC 
	os.system('calibre -drc -hier -64 %s/expand_edge.rule >& %s/expand_edge.log' %(dir,dir))
	os.system('mv %s/%s_EXPAND.gds %s/%s.gds' %(dir,cell,dir,cell))
	
	return 0

def random_clips_expand_edge():
	dir = "./random_clips_out"
	for gdsfile in os.listdir(dir):
		cell = gdsfile.split('.gds')[0]
		expand_edge(dir,cell)
	
	return 0

def main():
	random_clips_expand_edge()
	return 0

if __name__ == "__main__":
	main()
