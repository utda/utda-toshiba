#!/home/intern3/local/bin/python2.7

import os
import sys
import time
import numpy as np

sys.path.insert(0, os.path.join('./module_sraf'))

from init import Init
from gdsii import GdsIn
from gdsii import BatchGdsIn
from gdsii import gridInsertion
from gdsii import BenchGen
from search import OptFparam
from sraf_rule import GetSrafData, InsertSraf
from feature import GetFeature, AnalyzeFeature, SampleSizeReduction
from comp import DimReduction, Projection
from model import FitModel, Predict, Threshold
from rmspe    import GetRMSPE
from epe      import GetEPE
from pvband   import PVBandSim
from plot     import plot_histogram

### print welcome messages
def printWelcome():
  print '#========================================='
  print '#         SRAF Regression - v 0.0         '
  print '# Authors: Xiaoqing Xu, Tetsuaki Matsunawa'
  print '# Last update: 06/22/2015                 '
  print '#========================================='

## main function
def main():
    printWelcome()
    tt_begin = time.time()
    pp = Init(sys.argv)   #read parameter file
    debug_function = 0

    if(debug_function): 
        plot_histogram()
        return 0
    
    if pp['RandomOption'] == 'ON':
        BenchGen(pp,'train')
        #BenchGen(pp,'test')
        quit()

  ### Layout Clipping and litho-simulation
    if pp['ClipOption'] == 'ON':
        print '### Import gds clips and insert grids.'
        index = 0
        for gdsfile in os.listdir(pp['ClipGDSDir']):
            #if(debug_function):
            #    print gdsfile 
            #    if (index < 10):
            #        index += 1
            #        continue
            gdsii = GdsIn(pp, 'clip', gdsfile)
            gridInsertion(pp, gdsii, 'clip', gdsfile)
            #lithosimulation(pp)
            print '### Elapsed time (sec): %.4f' %(time.time()-tt_begin)
            quit()
  
  ### Feature parameter tuning
    if pp['F_Gsearch'] == 'ON':
        OptFparam(pp)
        quit()
    else:
        pass
  

  ### Training phase
    gds_tr_list, cell_tr_list  = BatchGdsIn(pp, 'train')   # Read gds files, various layout clips
    ss_tr         = GetSrafData(pp,gds_tr_list,cell_tr_list,'train') # Read the potential SRAF positions or usefulness map
    #ss_tr        = RedCalcPoint(pp, ss_tr, 'train')     # Reduce the # of samples
    fvec_tr       = GetFeature(pp,gds_tr_list,cell_tr_list,ss_tr,'train') # Feature extraction   
    #dict to array
    ss_tr_dict = ss_tr
    ss_tr = np.array([ss for item in ss_tr for ss in ss_tr_dict[item]])
    fvec_tr, ss_tr = SampleSizeReduction(pp,fvec_tr,ss_tr, 'train')  # Sample size reduction
    if(debug_function): AnalyzeFeature(pp,fvec_tr,ss_tr)
    rfv_tr        = DimReduction(pp, fvec_tr, ss_tr)     # Dimension reduction
    srafmodel, t0, score = FitModel(pp, rfv_tr[0], ss_tr)       # Model training

    print '### Finish training ###\n'

  ### Testing phase
    gds_te_list, cell_te_list   = BatchGdsIn(pp,'test')                ### Read gds    
    ss_te    = GetSrafData(pp,gds_te_list,cell_te_list,'test')        ### Read the potential SRAF positions or usefulness map
    #ss_te    = RedCalcPoint(pp,ss_te,'test')        ### Reduce #of samples
    tt_test = time.time()
    fvec_te  = GetFeature(pp,gds_te_list,cell_te_list,ss_te,'test')        ### Feature extraction
    print '### testing feature extraction time (ec): %.4f' %(time.time()-tt_test)
    #dict to array
    ss_te_dict = ss_te
    ss_te = np.array([ss for item in cell_te_list for ss in ss_te_dict[item]])
    #fvec_te, ss_te = SampleSizeReduction(pp,fvec_te,ss_te, 'test')          ### Sample size reduction
    rfv_te   = Projection(pp,rfv_tr[1],fvec_tr,fvec_te,ss_tr,ss_te)    ### Projection
    print '### testing Projection time (ec): %.4f' %(time.time()-tt_test)

    yh_te,t2 = Predict(pp,srafmodel,rfv_te,ss_te)        ### Predict @ testing data
    print '### testing Predict time (ec): %.4f' %(time.time()-tt_test)

    print '### Finish testing ###\n'

    print '\n### Insert SRAF features for test layout'
    #if pp['Model'] == 'LGR' or pp['Model'] == 'LGR_AdaBoost' or pp['Model'] == 'SVC':
    #    th = srafmodel[0].predict_proba(rfv_te)
    #else:
    #    th = Threshold(pp,srafmodel,rfv_te)
    th = Threshold(pp,srafmodel,rfv_te)
    xy_te_dict = InsertSraf(pp,gds_te_list,cell_te_list,ss_te_dict,fvec_te,yh_te,th,'test')   ### Insert SRAF features bassed on predicted usefulness map
    print '### Total testing time (ec): %.4f' %(time.time()-tt_test)
    
    yh_tr,t1 = Predict(pp,srafmodel,rfv_tr[0],ss_tr)        ### Predict @ training data
    GetRMSPE(pp,ss_tr,yh_tr,ss_te,yh_te,[t0,t1,t2])        ### Compute RMSPE
    #GetEPE(pp,ss_tr,yh_tr,gds_tr,'train')
    #GetEPE(pp,ss_te,yh_te,gds_te,'test')

    PVBandSim(pp,cell_te_list,xy_te_dict)
    #plot_histogram()

    print ':)-Happy ending'
    print '### Elapsed time (ec): %.4f' %(time.time()-tt_begin)
  ## END

if __name__ == "__main__":
  main()
