#!/usr/jin/python

import os
import numpy as np
#import matplotlib
#matplotlib.use('Agg')
import pylab as pl
from matplotlib.pylab import *

def GetOPCrule(ingds,topcell,layer,dtype,opcfile,outgds,rxy,tuq):

	opc_code = """
//Calibre rule for opc

LAYOUT PATH    'xGDS'
LAYOUT PRIMARY 'xTOP'
DRC RESULTS DATABASE 'xOutGDS' OASIS
PRECISION 1000
LAYOUT SYSTEM GDSII
DRC MAXIMUM RESULTS ALL
DRC MAXIMUM VERTEX 4000
LAYOUT WINDOW xX1 xY1 xX2 xY2
LAYOUT WINDOW CLIP YES

LAYER MAP xLayer datatype xDtype 1000 LAYER target     1000
LAYER MAP 11 datatype 0 5001 LAYER frame1     5001
LAYER MAP 12 datatype 0 5002 LAYER frame2     5002
LAYER MAP 13 datatype 0 5003 LAYER frame_good 5003
LAYER MAP 21 datatype 0 5004 LAYER core1      5004
LAYER MAP 22 datatype 0 5005 LAYER core2      5005
LAYER MAP 23 datatype 0 5006 LAYER core_good  5006
"""
	for i in xrange(tuq.shape[0]):
		lname = 'lay_m'+str(int(abs(tuq[i]))) if tuq[i] < 0 else 'lay_'+str(int(tuq[i]))
		lnum  = int(100+i)
		lnum2 = int(5007+i)
		opc_code += "LAYER MAP %d datatype 0 %d LAYER %s %d\n" %(lnum,lnum2,lname,lnum2)

	opc_code += """

//-------------- add SRAF
futo_line        = SIZE target by 0.04 underover
hoso_line        = NOT target futo_line

hoso_long_edge   = LENGTH hoso_line > 0.1

hle_wospc1       = EXTERNAL [hoso_long_edge] target < 0.210 OPPOSITE EXTENDED 0.10
hle_wospc2       = EXTERNAL  [hoso_long_edge] target < 0.100 OPPOSITE ABUT == 90
hle_wspc_tmp1    = NOT COIN EDGE hoso_long_edge hle_wospc1
hle_wspc         = NOT COIN EDGE hle_wspc_tmp1  hle_wospc2

block_hle_wspc1  = EXPAND EDGE hle_wspc OUTSIDE BY 0.08 EXTEND BY 0.05
block_hle_wspc2  = EXPAND EDGE hle_wspc OUTSIDE BY 0.05 EXTEND BY 0.05
Slayer           = NOT block_hle_wspc1 block_hle_wspc2

conv_corner	= LITHO OPC FILE "./log/nsetup.train.in" target Slayer """
	for i in xrange(tuq.shape[0]):
		lname = 'lay_m'+str(int(abs(tuq[i]))) if tuq[i] < 0 else 'lay_'+str(int(tuq[i]))
		opc_code += lname+str(' ')
	opc_code += """MAPNUMBER 400
conc_corner	= LITHO OPC FILE "./log/nsetup.train.in" target Slayer """
	for i in xrange(tuq.shape[0]):
		lname = 'lay_m'+str(int(abs(tuq[i]))) if tuq[i] < 0 else 'lay_'+str(int(tuq[i]))
		opc_code += lname+str(' ')
	opc_code += """MAPNUMBER 401
line_end	= LITHO OPC FILE "./log/nsetup.train.in" target Slayer """
	for i in xrange(tuq.shape[0]):
		lname = 'lay_m'+str(int(abs(tuq[i]))) if tuq[i] < 0 else 'lay_'+str(int(tuq[i]))
		opc_code += lname+str(' ')
	opc_code += """MAPNUMBER 402
lay_OPC		= LITHO OPC FILE "./log/nsetup.train.in" target Slayer """
	for i in xrange(tuq.shape[0]):
		lname = 'lay_m'+str(int(abs(tuq[i]))) if tuq[i] < 0 else 'lay_'+str(int(tuq[i]))
		opc_code += lname+str(' ')
	opc_code += """MAPNUMBER 1
OUT_target     {COPY target    } DRC CHECK MAP OUT_target      0  0
OUT_opc        {COPY lay_opc   } DRC CHECK MAP OUT_opc         1  0
OUT_sraf       {COPY Slayer    } DRC CHECK MAP OUT_sraf        2  0
OUT_frame1     {COPY frame1    } DRC CHECK MAP OUT_frame1      11 0
OUT_frame2     {COPY frame2    } DRC CHECK MAP OUT_frame2      12 0
OUT_frame_good {COPY frame_good} DRC CHECK MAP OUT_frame_good  13 0
OUT_core1      {COPY core1     } DRC CHECK MAP OUT_core1       21 0
OUT_core2      {COPY core2     } DRC CHECK MAP OUT_core2       22 0
OUT_core_good  {COPY core_good } DRC CHECK MAP OUT_core_good   23 0
OUT_CONVEX     {COPY conv_corner} DRC CHECK MAP OUT_CONVEX     100 0
OUT_CONCAVE    {COPY conc_corner} DRC CHECK MAP OUT_CONCAVE    200 0
OUT_LE         {COPY line_end   } DRC CHECK MAP OUT_LE         300 0
"""
	opc_code = opc_code.replace('xGDS',str(ingds))
	opc_code = opc_code.replace('xTOP',str(topcell))
	opc_code = opc_code.replace('xLayer',str(layer))
	opc_code = opc_code.replace('xDtype',str(dtype))
	opc_code = opc_code.replace('xOutGDS',str(outgds))
	opc_code = opc_code.replace('xX1',str(rxy[0]))
	opc_code = opc_code.replace('xY1',str(rxy[1]))
	opc_code = opc_code.replace('xX2',str(rxy[2]))
	opc_code = opc_code.replace('xY2',str(rxy[3]))

	fs = open(opcfile,'w')
	fs.write(opc_code)
	fs.close()

def GetEdgeRule(edgefile,ingds,outgds):
	
	edge_code = """
//Calibre rule for lcc

LAYOUT PATH		'xInGDS'
LAYOUT PRIMARY '*'
//DRC SUMMARY REPORT	'./test_lccout.rpt'
//DRC RESULTS DATABASE	'xOutGDS' OASIS
DRC RESULTS DATABASE   'xOutGDS' ASCII
PRECISION 1000
LAYOUT SYSTEM OASIS
DRC MAXIMUM RESULTS ALL
DRC MAXIMUM VERTEX 4000

LAYER MAP 0   datatype 0 1000 LAYER target     1000
LAYER MAP 1   datatype 0 1001 LAYER lay_OPC    1001
LAYER MAP 2   datatype 0 1002 LAYER lay_sraf   1002
//LAYER MAP 11  datatype 0 1003 LAYER frame1     1003
//LAYER MAP 12  datatype 0 1004 LAYER frame2     1004
//LAYER MAP 13  datatype 0 1005 LAYER frame_good 1005
LAYER MAP 21  datatype 0 1006 LAYER core1      1006
LAYER MAP 22  datatype 0 1007 LAYER core2      1007
LAYER MAP 23  datatype 0 1008 LAYER core_good  1008
LAYER MAP 100 datatype 0 1005 LAYER lay_conv   1005
LAYER MAP 200 datatype 0 1006 LAYER lay_conc   1006
LAYER MAP 300 datatype 0 1007 LAYER lay_le     1007

//cover_lay  = core1 OR (core2 OR core_good)
//test_conv1 = EXPAND EDGE (target INSIDE EDGE lay_conv) by 0.001
//test_conv2 = INSIDE test_conv1 cover_lay

OUT_CONVEX { @CONVEX
   //target INSIDE EDGE test_conv2
   target INSIDE EDGE lay_conv
}

OUT_CONCAVE { @CONCAVE
   //target INSIDE EDGE lay_conc
   target INSIDE EDGE lay_conc
}

OUT_LINEEND { @LINEEND
   //not touch edge (target INSIDE EDGE lay_le ) cover_lay
   target INSIDE EDGE lay_le
}

/// ignore
//OUT_ICONVEX { @ICONVEX
//   target INSIDE EDGE (temp_conv1 NOT test_conv2)
//}
//OUT_ILINEEND { @ILINEEND
//   touch edge (target INSIDE EDGE lay_le) cover_lay
//}

"""

	edge_code = edge_code.replace('xInGDS',str(ingds))
	edge_code = edge_code.replace('xOutGDS',str(outgds))

	fs = open(edgefile,'w')
	fs.write(edge_code)
	fs.close()

	return 0

### Compute EPE based on predicted edge movements
def GetEPE(pp,ss,yht,gdsii,flg):
	if pp['Mmode'] == 'all':
		yh = yht[0]
	elif pp['Mmode'] == 'sep':
		yh = np.zeros(len(ss))
		yh_norm,yh_conv,yh_conc,yh_lend = yht
		yh[ss[:,6]==0] = yh_norm
		yh[ss[:,6]==1] = yh_conv
		yh[ss[:,6]==2] = yh_conc
		yh[ss[:,6]==3] = yh_lend

	if flg == 'train':
		fmode  = 'train'
		gs     = './log/opc.train.in.oas'
		ingds  = pp['TrainGDS']
		intop  = pp['TrainTop']
		inlay  = pp['TrainLayer']
		intype = pp['TrainDtype']
		newsetup = './log/nsetup.train.in'

		opcf  = './log/opc.predict.train.rule'
		opco  = './log/opc.predict.train.out.oas'
		bbox  = gdsii.extract('IN_TOP_tr').get_bounding_box()
		fss   = './result/gauge.predict.traing.ss'
		epen  = './result/re_epe_hist_predict_train.png'
	elif flg == 'test':
		fmode  = 'test'
		gs     = './log/opc.test.in.oas'
		ingds  = pp['TestGDS']
		intop  = pp['TestTop']
		inlay  = pp['TestLayer']
		intype = pp['TestDtype']
		newsetup = './log/nsetup.test.in'

		opcf  = './log/opc.predict.test.rule'
		opco  = './log/opc.predict.test.out.oas'
		bbox  = gdsii.extract('IN_TOP_te').get_bounding_box()
		fss   = './result/gauge.predict.test.ss'
		epen  = './result/re_epe_hist_predict_test.png'
	else:
		'parameter file error'
		quit()

	prule = [ingds,intop,inlay,intype,opcf,opco]

	### Sort&Round displacement values
	tuq  = np.unique(np.round(yh))
	clay = ['//cover layer\n']*tuq.shape[0]
	for i in xrange(tuq.shape[0]):
		dmin    = tuq[i]-0.5
		dmax    = tuq[i]+0.5
		tcoords = ss[(yh>dmin)&(yh<=dmax),2:4]
		tss     = ss[(yh>dmin)&(yh<=dmax)]

		### Create cover layer for all fragments
		for j in xrange(len(tcoords)):
			xx,yy = tcoords[j]/1000.
			# if edge direction ss[j,4] == 0 or 2:horizontal edge
			# elif edge direction == 1 or 3:vertical edge
			elen = tss[j,5]
			if tss[j,4] == 0 or tss[j,4] == 2:
				x0,y0 = xx-elen/2000.,yy-0.001
				x1,y1 = xx+elen/2000.,yy+0.001
			else:
				x0,y0 = xx-0.001,yy-elen/2000.
				x1,y1 = xx+0.001,yy+elen/2000.
			tlay  = 'lay_m'+str(int(abs(tuq[i]))) if tuq[i] < 0 else 'lay_'+str(int(tuq[i]))
			tmp   = 'POLYGON %.3f %.3f %.3f %.3f %s\n' %(x0,y0,x1,y1,tlay)
			clay[i] += tmp

	### Add cover layers to input GDS
	create_clay_rule = """

//Create cover polygons
LAYOUT PATH    'xGDS'
LAYOUT PRIMARY 'xTOP'
DRC RESULTS DATABASE 'xOutGDS' OASIS
PRECISION 1000
LAYOUT SYSTEM GDSII
DRC MAXIMUM RESULTS ALL
DRC MAXIMUM VERTEX 4000

LAYER MAP xLayer DATATYPE xDtype 1000 LAYER TARGET     1000
LAYER MAP 11     DATATYPE 0      1001 LAYER FRAME1     1001
LAYER MAP 12     DATATYPE 0      1002 LAYER FRAME2     1002
LAYER MAP 13     DATATYPE 0      1003 LAYER FRAME_GOOD 1003
LAYER MAP 21     DATATYPE 0      1004 LAYER CORE1      1004
LAYER MAP 22     DATATYPE 0      1005 LAYER CORE2      1005
LAYER MAP 23     DATATYPE 0      1006 LAYER CORE_GOOD  1006

"""
	for i in xrange(tuq.shape[0]):
		lname = 'lay_m'+str(int(abs(tuq[i]))) if tuq[i] < 0 else 'lay_'+str(int(tuq[i]))
		create_clay_rule += 'LAYER %s %d\n' %(lname,int(100+i))
		create_clay_rule += clay[i]
	
	create_clay_rule += """
OUT_TARGET     {COPY TARGET    } DRC CHECK MAP OUT_TARGET     0  0
OUT_FRAME1     {COPY FRAME1    } DRC CHECK MAP OUT_FRAME1     11 0
OUT_FRAME2     {COPY FRAME2    } DRC CHECK MAP OUT_FRAME2     12 0
OUT_FRAME_GOOD {COPY FRAME_GOOD} DRC CHECK MAP OUT_FRAME_GOOD 13 0
OUT_CORE1      {COPY CORE1     } DRC CHECK MAP OUT_CORE1      21 0
OUT_CORE2      {COPY CORE2     } DRC CHECK MAP OUT_CORE2      22 0
OUT_CORE_GOOD  {COPY CORE_GOOD } DRC CHECK MAP OUT_CORE_GOOD  23 0
"""
	for i in xrange(tuq.shape[0]):
		lname = 'lay_m'+str(int(abs(tuq[i]))) if tuq[i] < 0 else 'lay_'+str(int(tuq[i]))
		create_clay_rule += 'OUT_%s {COPY %s} DRC CHECK MAP OUT_%s %d 0\n' %(lname,lname,lname,int(100+i))

	create_clay_rule = create_clay_rule.replace('xGDS',ingds)
	create_clay_rule = create_clay_rule.replace('xTOP',intop)
	create_clay_rule = create_clay_rule.replace('xLayer',str(inlay))
	create_clay_rule = create_clay_rule.replace('xDtype',str(intype))
	create_clay_rule = create_clay_rule.replace('xOutGDS',gs)

	fs = open('./log/clay.rule','w')
	fs.write(create_clay_rule)
	fs.close()

	### Run DRC (Create cover layers)
	os.system('calibre -drc -hier -64 ./log/clay.rule >& ./log/cal.clay.log')

	### Create new setup file for OPC
	nset = """
# ---------------- Simulation models ---------------
modelpath /home/polaris/matsunawa/classification/FeatureExtraction/LithoSim/model
opticalmodel optical.opt
resistpolyfile resist.mod

# ---------------- Simulation Parameters ------------
iterations 2
tilemicrons 100
gridsize 0.001
stepsize 0.001
siteinfo RESIST 
cornerRadius 0.04
cornerSiteStyle SITES_ON_ARC 0.060 concave SITES_ON_EDGE 0.050 lea SITES_ON_EDGE 0.06
lineEndAdjDist 0.20
convexAdjDist 0.12
concaveAdjDist 0.14

# ---------------- Fragmentation ---------------------
minfeature 0.040
minedgelength 0.040
maxedgelength 1000
minjog 0.04
cornedge 0.060 0.045 0.040 priority
concavecorn 0.055 0.045 0.040
interfeature -interdistance 0.4 -ripplelen 0.08 -num 1
seriftype 0
lineEndLength 0.20

# ---------------- Layer info ------------------------
background atten 0.06
layer 1 target 0 0 opc clear
layer 2 Slayer 0 0 visible clear
"""
	for i in xrange(tuq.shape[0]):
		lnum  = int(100+i)
		lname = 'lay_m'+str(int(abs(tuq[i]))) if tuq[i] < 0 else 'lay_'+str(int(tuq[i]))
		nset += 'layer %d %s 0 0 hidden clear\n' %(lnum,lname)
	nset += """

#----- Arbitrary Commands Can Follow This Line. Don't delete this line! -----
sse OPC_MIN_EXTERNAL 0.025
sse OPC_MIN_INTERNAL 0.045
sse OPC_FEEDBACK -0.5 -0.5 -0.4 -0.4 -0.3 -0.3 -0.2 -0.2 -0.3 -0.3 -0.4 -0.4

tags2boxes -tags convex_corner -layers 400
tags2boxes -tags concave_corner -layers 401
tags2boxes -tags line_end -layers 402
"""
	#nset += 'newTag everything -how EPE all -100 100\n'
	for i in xrange(tuq.shape[0]):
		lname = 'lay_m'+str(int(abs(tuq[i]))) if tuq[i] < 0 else 'lay_'+str(int(tuq[i]))
		tagn  = 'tag_m'+str(int(abs(tuq[i]))) if tuq[i] < 0 else 'tag_'+str(int(tuq[i]))
		#nset += 'newTag %s -how TOUCHEDGE %s target\n' %(tagn,lname)
		nset += 'newTag %s -how INSIDEEDGE %s target\n' %(tagn,lname)
		nset += 'opcTag %s -hintOffset %.3f\n' %(tagn,float(tuq[i])/1000.)
	
	nset += 'dump ./log/epe_check.txt -iters on\n'
	nset += '# --- END ---\n'
	
	fs = open(newsetup,'w')
	fs.write(nset)
	fs.close()

	### ====================
	###      OPC & DRC
	### ====================

	### Compute bounding box
	wx    = bbox[1,0] - bbox[0,0]
	wy    = bbox[1,1] - bbox[0,1]

	### Set tilemicrons
	ntile = 100.0
	nx,ny = 1,1
	if wx > ntile:
		nx = int(np.ceil(wx/ntile))
	if wy > ntile:
		ny = int(np.ceil(wy/ntile))
	nn = nx*ny

	### Split calc. area based on tilemicrons
	carea = []
	if nn == 1:
		carea.append([bbox[0,0],bbox[0,1],bbox[1,0],bbox[1,1]])
	else:
		for ii in xrange(nx):
			for jj in xrange(ny):
				kk = int(ii*nx+jj)
				if kk == 0:
					lbx = round(bbox[0,0],4)
					lby = round(bbox[0,1],4)
					rtx = round(lbx + ntile,4)
					rty = round(lby + ntile,4)
				else:
					lbx = carea[kk-1][0]
					lby = carea[kk-1][3]
					rtx = carea[kk-1][2]
					rty = round(carea[kk-1][3]+ntile,4)
				if ii != 0:
					zz = ii*nx
					lbx = carea[zz-1][2]
					rtx = round(carea[zz-1][2]+ntile,4)
				if ii != 0 and jj == 0:
					lby = carea[0][1]
					rty = carea[0][3]
				carea.append([lbx,lby,rtx,rty])

	print '### Computing EPEs (Running OPC)...'
	print '\tLayout\t\t\t: %s' %fmode
	print '\tBounding box (x,y)\t: %.3f %.3f (um)' %(wx,wy)
	print '\t#tiles\t\t\t: %d' %nn

	### Define parameters
	d_coords = [] ### coordinates
	d_disp   = [] ### displacement
	d_flag   = [] ### flag
	d_epe    = [] ### epe
	d_conv   = [] ### convex edge flag
	d_conc   = [] ### concave edge flag
	d_le     = [] ### line end edge flag

	### Run OPC & DRC
	for ii in xrange(nn):
		### Make OPC rule file
		### Extract unique displacements in carea
		ssid = np.where((ss[:,2]>=carea[ii][0])&(ss[:,2]<=carea[ii][2])&(ss[:,3]>=carea[ii][1])&(ss[:,3]<=carea[ii][3]))
		cauq = np.unique(np.round(yh[ssid]))
		GetOPCrule(prule[0],prule[1],prule[2],prule[3],prule[4],prule[5],carea[ii],cauq)
		os.system('calibre -drc -hier -64 -turbo 8 -turbo_litho 8 %s >& ./log/cal.predict.opc.log' %opcf)

		### Read dump-file and extract edge/displacement information
		#fname = './log/epe_check.txt.0'
		fname = './log/epe_check.txt.2'
		tidx  = 1
		for i in open(fname).readlines():
			ftmp = i[:-1].split()
			if ftmp[0] == '<fragment>':
				tidx = 0
			if tidx == 0:
				if ftmp[0] == 'coords':
					d_coords.append([int(ftmp[1]),int(ftmp[2]),int(ftmp[3]),int(ftmp[4])])
				if ftmp[0] == 'disp':
					d_disp.append(int(ftmp[1]))
				if ftmp[0] == 'flag':
					d_flag.append(int(ftmp[1]))
				if ftmp[0] == 'epe':
					d_epe.append(float(ftmp[1]))
				if ftmp[0] == '</fragment>':
					tidx = 1
			if tidx == 1 and (len(d_coords)!=len(d_epe)):
				d_epe.append(0.0)

		### Run DRC for edge type extraction
		efile = opco + '.edge.rule'
		eout  = opco + '.edge.out.err'
		GetEdgeRule(efile,opco,eout)
		os.system('calibre -drc -hier -64 %s >& ./log/cal.predict.edge.log' %efile)

		### Read drc result file
		fs   = open(eout)
		drcr = fs.read().split('\n')
		fs.close()

		### Get index
		id_conv = drcr.index('OUT_CONVEX')
		id_conc = drcr.index('OUT_CONCAVE')
		id_le   = drcr.index('OUT_LINEEND')

		### Get #of edges
		n_conv = int(drcr[id_conv+1].split()[0])
		n_conc = int(drcr[id_conc+1].split()[0])
		n_le   = int(drcr[id_le+1].split()[0])

		### Get gdsx/gdsy
		t_conv = drcr[id_conv+5:id_conv+4+n_conv*2][::2]
		t_conc = drcr[id_conc+5:id_conc+4+n_conc*2][::2]
		t_le   = drcr[id_le+5:id_le+4+n_le*2][::2]

		### Compute center x,y / edge
		for jj in xrange(n_conv):
			txy = t_conv[jj].split()
			d_conv.append([(int(txy[0])+int(txy[2]))/2,(int(txy[1])+int(txy[3]))/2])
		for jj in xrange(n_conc):
			txy = t_conc[jj].split()
			d_conc.append([(int(txy[0])+int(txy[2]))/2,(int(txy[1])+int(txy[3]))/2])
		for jj in xrange(n_le):
			txy = t_le[jj].split()
			d_le.append([(int(txy[0])+int(txy[2]))/2,(int(txy[1])+int(txy[3]))/2])
	### End OPC & DRC

	### List2array
	d_conv = np.array(d_conv,dtype=int)
	d_conc = np.array(d_conc,dtype=int)
	d_le   = np.array(d_le,dtype=int)

	### Compute edge dir, length, type
	n_data = len(d_coords)			### #of total edges
	a_data = np.zeros((n_data,7),dtype=int)	### data array
	a_flg  = np.zeros(n_data,dtype=int)	### edge type: Normal:0, Convex:1, Concave:2, LE:3

	for ii in xrange(n_data):
		t_x   = (d_coords[ii][0]+d_coords[ii][2])/2
		t_y   = (d_coords[ii][1]+d_coords[ii][3])/2

		### Check edge direction and length
		eleng = 0
		edir  = 0
		pdir  = -1
		if d_coords[ii][0] == d_coords[ii][2]:
			edir  = 1
			eleng = np.abs(d_coords[ii][1] - d_coords[ii][3])
			if d_coords[ii][3] > d_coords[ii][1]:
				pdir = 3
			else:
				pdir = 1
		else:
			eleng = np.abs(d_coords[ii][0] - d_coords[ii][2])
			if d_coords[ii][2] > d_coords[ii][0]:
				pdir = 0
			else:
				pdir = 2
		
		### Check convex edge
		flg = np.where((d_conv[:,0]==t_x)&(d_conv[:,1]==t_y))
		if len(flg[0]) != 0:
			a_flg[ii] = 1

		### Check concave edge
		flg = np.where((d_conc[:,0]==t_x)&(d_conc[:,1]==t_y))
		if len(flg[0]) != 0:
			a_flg[ii] = 2

		### Check line end
		flg = np.where((d_le[:,0]==t_x)&(d_le[:,1]==t_y))
		if len(flg[0]) != 0:
			a_flg[ii] = 3

		### Summarize edge information
		a_data[ii] = pdir,d_disp[ii],t_x,t_y,edir,eleng,a_flg[ii]

	### Make gauge file
	### --> id, edge_type, gdsx, gdsy, plus_dir, edge_dir, edge_lenght, dispacement
	fout = open(fss,'w')
	fout.write('gauge\t\t\tGDSX\tGDSY\tL/S\tmask\tother\twafer\tweight\n')
	for ii in xrange(n_data):
		fout.write('test\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%f\t%d\n' %(ii+1,a_data[ii,6],a_data[ii,2],a_data[ii,3],a_data[ii,0],a_data[ii,4],a_data[ii,5],d_epe[ii],a_data[ii,1]))
	fout.write('\n')
	fout.write('variance\t:%.6f\n' %np.var(d_epe))
	fout.write('std\t:%.6f\n' %np.std(d_epe))
	fout.close()

	### Extract EPEs and edge_type
	edat = np.r_[np.array([d_epe]).T,np.array([a_data[:,6]]).T]

	### Plot EPEs (histogram, variance, std. dev)
	(nn,nbins) = np.histogram(edat[:,0],bins=50,normed=True)
	pl.figure()
	pl.plot(.5*(nbins[1:]+nbins[:-1]),nn)
	pl.savefig(epen)

	### End

