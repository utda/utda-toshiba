#!/work/lpkopt/python/2.7.3/bin/python

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext as build_pyx

import numpy
setup(
	name = 'CCAS',
	cmdclass = {'build_ext': build_pyx},
	ext_modules=[Extension('compute', 
               ['compute.pyx'], 
               libraries=['m'],
               extra_compile_args=['-O3', '-ffast-math', '-march=native', '-fopenmp'], 
               extra_link_args=['-fopenmp'],)])
