#!/work/lpkopt/python/2.7.3/bin/python

#import numpy as np
cimport numpy as np
cimport cython
from libc.math cimport M_PI,sin,cos,floor
from libc.stdlib cimport malloc, free
from cython.parallel cimport prange

### Concentric Circle with Area Sampling
@cython.boundscheck(False)
cpdef inline void ccompute(int[][] pix_vecs, int x_index, int[][] xdisp_lut, int[][] ydisp_lut, np.ndarray[np.uint8_t, ndim=2] imArray)
    for jj in prange(32,nogil=True):
        #lc_x[0] = spoints[kk,0]+xdisp_lut[ii][jj]
        #lc_y[0] = spoints[kk,1]+ydisp_lut[ii][jj]
        lc_x[0] = s_kk_x[0]+xdisp_lut[ii][jj]
        lc_y[0] = s_kk_y[0]+ydisp_lut[ii][jj]
        for i0 in xrange(msy):
            if lc_y[0]-gr+i0 < 0 or lc_y[0]-gr+i0 >= shape0: continue
                for j0 in xrange(msx):
                    if lc_x[0]-gr+j0 < 0 or lc_x[0]-gr+j0 >= shape1: continue
                    if imArray[lc_y[0]-gr+i0,lc_x[0]-gr+j0] == 255 and mask[i0][j0] == 1:
                        pix_vecs[x_index[0]][jj] += 1
                    else: 
                        continue
