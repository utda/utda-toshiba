#!/usr/bin/python

import os
import sys
import time
import numpy as np

from sklearn import linear_model
from sklearn import ensemble
from sklearn import svm
from sklearn import tree
#from sklearn.cross_validation import train_test_split
#from sklearn.grid_search import GridSearchCV

sys.path.insert(0, os.path.join('./'))
from hbm import FitHBM

### Unifying model
def FitModel(pp,xx,ss):

    print '### %s Model fitting...' %pp['Model']
    ttime1 = time.time()

    #yy = np.squeeze(labels[:,1]).astype(int)
    #ss: gdsx, gdsy, weight
    yy = ss[:,2]
    #yy  = ss[:,1]
    nn  = xx.shape[0]
    
    if pp['Model'] == 'HBM':
        fmodel = 'Hierarchical Bayes'
        m_hbm  = FitHBM(pp,xx,ss[:,6],yy)
        model  = [m_hbm[0]]
        score  = m_hbm[1]
    else:
        if pp['Model'] == 'linear_model':
            fmodel = 'LinearRegression'
            m_all  = linear_model.LinearRegression()
            m_norm = linear_model.LinearRegression()
            m_conv = linear_model.LinearRegression()
            m_conc = linear_model.LinearRegression()
            m_lend = linear_model.LinearRegression()
        elif pp['Model'] == 'ridge':
            fmodel = 'Ridge'
            alist  = [0.1,1,10]
            #alist  = [0.1,1,10,100,500,1000]
            m_all  = linear_model.RidgeCV(alphas=alist)
            m_norm = linear_model.RidgeCV(alphas=alist)
            m_conv = linear_model.RidgeCV(alphas=alist)
            m_conc = linear_model.RidgeCV(alphas=alist)
            m_lend = linear_model.RidgeCV(alphas=alist)
        elif pp['Model'] == 'SVR_poly':
            fmodel = 'SVR_poly'
            cc     = 1.0
            dd     = 3
            m_all  = svm.SVR(kernel='poly',C=cc,degree=dd)
            m_norm = svm.SVR(kernel='poly',C=cc,degree=dd)
            m_conv = svm.SVR(kernel='poly',C=cc,degree=dd)
            m_conc = svm.SVR(kernel='poly',C=cc,degree=dd)
            m_lend = svm.SVR(kernel='poly',C=cc,degree=dd)
        elif pp['Model'] == 'SVR_rbf':
            fmodel = 'SVR_rbf'
            cc     = 1e3
            gg     = 0.1
            m_all  = svm.SVR(kernel='rbf',C=cc,gamma=gg)
            m_norm = svm.SVR(kernel='rbf',C=cc,gamma=gg)
            m_conv = svm.SVR(kernel='rbf',C=cc,gamma=gg)
            m_conc = svm.SVR(kernel='rbf',C=cc,gamma=gg)
            m_lend = svm.SVR(kernel='rbf',C=cc,gamma=gg)
        elif pp['Model'] == 'SVR_linear':
            fmodel = 'SVR_linear'
            cc     = 1.0
            m_all  = svm.SVR(kernel='linear',C=cc)
            m_norm = svm.SVR(kernel='linear',C=cc)
            m_conv = svm.SVR(kernel='linear',C=cc)
            m_conc = svm.SVR(kernel='linear',C=cc)
            m_lend = svm.SVR(kernel='linear',C=cc)
        elif pp['Model'] == 'SVC':
            fmodel = 'SVC'
            cc     = 1.0
            m_all  = svm.SVC(kernel='linear',C=cc,probability=True)
        elif pp['Model'] == 'LGR':
            fmodel = 'LGR'
            m_all = linear_model.LogisticRegression()
        elif pp['Model'] == 'LGR_AdaBoost':
            fmodel = 'LGR_AdaBoost'
            m_all = ensemble.AdaBoostClassifier(base_estimator=linear_model.SGDClassifier(loss='log'), n_estimators=10, algorithm='SAMME')
        elif pp['Model'] == 'DTree':
            fmodel = 'DTree'
            #m_all = ensemble.AdaBoostClassifier(base_estimator=tree.DecisionTreeClassifier(), n_estimators=15, algorithm='SAMME')
            m_all = tree.DecisionTreeClassifier()
        else:
            print ' model type error.'
            quit()

        ### Fit linear/nonlinear model
        if pp['Mmode'] == 'all':
            ### unifying model
            m_all.fit(xx,yy)
            score = m_all.score(xx,yy)
            model = [m_all]
        elif pp['Mmode'] == 'sep':
            ### sep. model
            y_norm = yy[ss[:,6]==0]
            y_conv = yy[ss[:,6]==1]
            y_conc = yy[ss[:,6]==2]
            y_lend = yy[ss[:,6]==3]
    
            x_norm = xx[ss[:,6]==0]
            x_conv = xx[ss[:,6]==1]
            x_conc = xx[ss[:,6]==2]
            x_lend = xx[ss[:,6]==3]
    
            m_norm.fit(x_norm,y_norm)
            m_conv.fit(x_conv,y_conv)
            m_conc.fit(x_conc,y_conc)
            m_lend.fit(x_lend,y_lend)
    
            s_norm = m_norm.score(x_norm,y_norm)
            s_conv = m_conv.score(x_conv,y_conv)
            s_conc = m_conc.score(x_conc,y_conc)
            s_lend = m_lend.score(x_lend,y_lend)
    
            model = [m_norm,m_conv,m_conc,m_lend]
        else:
            print ' model type error.'
            quit()
    ### end if

    ### Get fitting TAT
    timex = time.time()-ttime1

    #print 'done.'
    print '\tAlgorithm\t\t: %s' %fmodel
    print '\t#of samples\t\t: %d' %nn
    if pp['Model'] == 'HBM':
        print '\tScore (R^2)\t\t: %.2f' %score
    else:
        print '\tMode\t\t\t: %s' %pp['Mmode']
        if fmodel == 'Ridge':
            if pp['Mmode'] == 'all':
                print '\talpha\t\t\t: %.1f' %m_all.alpha_
            else:
                print '\talpha\t\t\t: %.1f %.1f %.1f %.1f' %(m_norm.alpha_,m_conv.alpha_,m_conc.alpha_,m_lend.alpha_)
        if pp['Mmode'] == 'all':
            print '\tScore (R^2)\t\t: %.2f' %score
        else:
            print '\tScore (R^2)\t\t: %.2f %.2f %.2f %.2f' %(s_norm,s_conv,s_conc,s_lend)
    print '\telapsed time (sec)\t: %.3f' %timex

    return model,timex,score

def Predict(pp,model,xx,ss):
    #print '### Testing...'
    #ttime1 = time.time()

    if pp['Model'] == 'HBM':
        m_all  = model[0]
        cc     = ss[:,6]
        ndim   = xx.shape[1]
        aa     = m_all[0:ndim]
        bb     = m_all[-1]
        yh_all = np.zeros(xx.shape[0])
        ### y = (a_fixed + a_random)x + b_fixed + b_random
        for i in xrange(xx.shape[0]):
            tmp       = np.sum([ (aa[j,0]+aa[j,cc[i]+1])*xx[i,j] for j in xrange(ndim) ])
            yh_all[i] = tmp + bb[0] + bb[cc[i]+1]
        yh     = [yh_all]
    else:
        if pp['Mmode'] == 'all':
            m_all  = model[0]
            if pp['Model'] == 'LGR' or pp['Model'] == 'LGR_AdaBoost' or pp['Model'] == 'DTree' or pp['Model'] == 'SVC':
                yh_all = m_all.predict_proba(xx)
            else:
                yh_all = m_all.predict(xx)
            yh     = [yh_all]
        elif pp['Mmode'] == 'sep':
            m_norm,m_conv,m_conc,m_lend = model
            yh_norm = m_norm.predict(xx[ss[:,6]==0])
            yh_conv = m_conv.predict(xx[ss[:,6]==1])
            yh_conc = m_conc.predict(xx[ss[:,6]==2])
            yh_lend = m_lend.predict(xx[ss[:,6]==3])
            yh      = [yh_norm,yh_conv,yh_conc,yh_lend]

    #timex = time.time()-ttime1
    #print '\telapsed time (sec)\t: %.3f' %timex

    return yh,0.0

def Threshold(pp,model,xx):

    ### Check coherence map level set look-up-table
    if(pp['ToolMode']=='Toshiba'):
        fcp_lut = "./gds/train/cm_level_lut.txt"
        if(os.path.isfile(fcp_lut)):
            a_t = np.genfromtxt(fcp_lut)
            #5000 is the normalization parameter for the coherence map intensity
            cp_lut = [a_t[i,1]*5000.0 for i in xrange(len(a_t))]
            #print cp_lut
            return np.min(np.array(cp_lut))
        else:
            print "###ERROR: coherence map level set look-up-table in %s is missing" %fcp_lut
            quit()
    else:
        if pp['Model'] == 'LGR' or pp['Model'] == 'LGR_AdaBoost' or pp['Model'] == 'DTree' or pp['Model'] == 'SVC':
            return 0.5
        else: 
            return model[0].predict(np.zeros((1,xx.shape[1]),dtype=float))
