#!/usr/bin/python

import os
import sys
import struct
from numpy import *

print("----- The coherence map analysis for model based SRAFs -----")

#the parameters, add parsing options later on
in_grid_size = 1
out_grid_size = 5
xdim = 2652
ydim = 2652
#xdim = 265
#ydim = 265
lx = 1
ly = 1


# Read the entire coherence map, 2D array
coherence_map = zeros( (xdim, ydim), dtype=double)
xindex = 0
yindex = 0
print("##read in coherence map - begin\n")
with open('image_map_0_2652_0.000000', 'rb') as map_file :
  bytes = map_file.read(8)
  while bytes != b"":
    #do something with the bytes
    data = struct.unpack('d', bytes)
    yindex = yindex + xindex / xdim
    xindex = xindex % xdim
    coherence_map[xindex][yindex] = data[0]
    xindex = xindex + 1
    #print data
    bytes = map_file.read(8)

print("##read in coherence map - end\n")

#for debug the I/O, the same as the litho-simulation output
#data_file = open("coherence_map.txt", 'w+')
#for yindex in range(ydim):
#  for xindex in range(xdim):
#    data_file.write("%f %f %1.10f\n" %((double)(xindex)/(double)(xdim)*(double)(lx), (double)(ydim-1-yindex)/(double)(ydim)*(double)(ly), coherence_map[xindex][ydim-1-yindex]) )
#  data_file.write("\n")
#data_file.close

#print("----- Gradient image and rough decision on SRAFs -----\n")
#print("##gradient image calculation - begin\n")
#image_gradient = gradient(coherence_map)
#image_gradient_density = zeros( (xdim, ydim), dtype=double)
#min_density = sys.float_info.max
#max_density = 0.0
#for yindex in range(ydim):
#  for xindex in range(xdim):
#    #item_density = sqrt(image_gradient[0][xindex][yindex]**2 + image_gradient[1][xindex][yindex]**2);
#    if abs(image_gradient[0][xindex][yindex]) < abs(image_gradient[1][xindex][yindex]): item_density = image_gradient[1][xindex][yindex]
#    else: item_density = image_gradient[0][xindex][yindex]
#    image_gradient_density[xindex][yindex] = abs(item_density);
#    if abs(item_density) < abs(min_density): min_density = abs(item_density)
#    if abs(item_density) > max_density: max_density = abs(item_density)
#
#data_file = open("gradient_image.txt", 'w+')
#for yindex in range(ydim):
#  for xindex in range(xdim):
#    data_file.write("%f %f %1.10f\n" %((double)(xindex)/(double)(xdim)*(double)(lx), (double)(ydim-1-yindex)/(double)(ydim)*(double)(ly), image_gradient_density[xindex][ydim-1-yindex]) )
#  data_file.write("\n")
#data_file.close
#print("##gradient image calculation - end\n")

print("----- Rough decision on SRAFs -----\n")
print("##feature prediction - begin\n")
#based on the coherence map to compute feature postions with approximately zero gradient density
feature_prediction = zeros( (xdim, ydim), dtype=bool)
threshold = 0.05
for yindex in range(ydim):
  for xindex in range(xdim):
    if (coherence_map[xindex][yindex] <= threshold) & (coherence_map[xindex][yindex] > 0.0): feature_prediction[xindex][yindex] = 1
    else: feature_prediction[xindex][yindex] = 0
print("##feature prediction - end\n")

#rescale and dump to the bitmap, consider the gds format later on
scale = out_grid_size / in_grid_size
xdim_s = xdim / scale
ydim_s = ydim / scale
feature_map = zeros( (xdim_s, ydim_s), dtype=bool)
for yindex in range(ydim_s):
  for xindex in range(xdim_s):
    item_cnt = 0
    ones_cnt = 0
    for y in range(scale):
      yIndx = yindex*scale + y
      if yIndx >= ydim: break
      for x in range(scale):
        xIndx = xindex*scale + x
        if xIndx >= xdim: break
        if feature_prediction[xIndx][yIndx] == 1: ones_cnt += 1
        item_cnt += 1
    if ones_cnt >= 0.5*item_cnt: feature_map[xindex][yindex] = 1
    

data_file = open("feature_map.txt", 'w+')
for yindex in range(ydim_s):
  for xindex in range(xdim_s):
    data_file.write("%i" %feature_map[xindex][ydim_s-1-yindex])
  data_file.write("\n")
data_file.close

print("----- DONE -----\n")
