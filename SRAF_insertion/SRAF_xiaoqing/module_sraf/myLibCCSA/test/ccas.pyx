#!/work/lpkopt/python/2.7.3/bin/python

import numpy as np
cimport numpy as np
cimport cython
from libc.math cimport M_PI,sin,cos,floor

### Concentric Circle with Area Sampling
def CCSAcpy(np.ndarray[np.uint8_t,ndim=2] imArray,np.ndarray[np.int64_t,ndim=2] spoints, int x_range, int y_range, np.ndarray[np.int64_t,ndim=1] xstep, int gr, np.ndarray[np.uint8_t,ndim=2,cast=True] mask, np.ndarray[np.int64_t,ndim=2] rotation_lut, np.ndarray[np.int64_t,ndim=2] fvecs):
    cdef int ii, jj, kk, mm
    cdef np.ndarray[np.int64_t,ndim=2] pix_vecs = np.zeros((x_range, y_range),dtype=int)
    cdef np.ndarray[np.int64_t,ndim=2] tmp_vecs = np.zeros((x_range, y_range),dtype=int)
    cdef int shape0 = imArray.shape[0]
    cdef int shape1 = imArray.shape[1]
    cdef int xdisp, ydisp 
    cdef int x_index, lc_x, lc_y, x_start, x_end, y_start, y_end 
    cdef int step_size = xstep.shape[0]-1
    cdef int w_flag = 1

    cdef int divideCnt = 4
    cdef int divideSize = y_range/divideCnt
    cdef int flip_index = 0
    cdef int max_sum = 0
    cdef int sum = 0
    cdef int pcnt = spoints.shape[0]
    cdef int i0, j0
    cdef int msy = mask.shape[0]
    cdef int msx = mask.shape[1]
    #cdef np.ndarray[np.int64_t,ndim=2] fvecs = np.zeros((pcnt,(x_range-1)*y_range),dtype=int)
    cdef int sub_index
    
    #print 'imArray sum:'
    #print np.sum(imArray)

    for kk in xrange(pcnt):
        ### pix vectors initialize
        for ii in xrange(x_range):
            for jj in xrange(y_range):
                pix_vecs[ii,jj] = 0
        
        ###center sampling points
        x_index = 0
        #for ii in xstep[1:]:
        for ii in xrange(step_size):
            x_index += 1
            for jj in range(y_range):
                #xdisp = int(np.floor( ii*np.cos(np.pi*(1-2.0*jj/float(y_range))) ))
                #ydisp = int(np.floor( ii*np.sin(np.pi*(1-2.0*jj/float(y_range))) ))
                xdisp = int(floor( xstep[ii+1]*cos(M_PI*(1-2.0*jj/float(y_range))) ))
                ydisp = int(floor( xstep[ii+1]*sin(M_PI*(1-2.0*jj/float(y_range))) ))
                #lc_x = prad+xdisp
                #lc_y = prad+ydisp
                lc_x = spoints[kk,0]+xdisp
                lc_y = spoints[kk,1]+ydisp
                for i0 in xrange(msy):
                    if lc_y-gr+i0 < 0 or lc_y-gr+i0 >= shape0: continue
                    for j0 in xrange(msx):
                        if lc_x-gr+j0 < 0 or lc_x-gr+j0 >= shape1: continue
                        if imArray[lc_y-gr+i0,lc_x-gr+j0] == 255 and mask[i0,j0] == True:
                            pix_vecs[x_index,jj] += 1
                        else: 
                            continue
        # end for ii

        ###achieve independent feature matrix with column rotation
        ###trunk the sampling region to 4 or 2 parts
        ###scan the quadrant
        #for ii in np.arange(0,y_range,divideSize,dtype=int):
        #    sum = np.sum(pix_vecs[:,ii:ii+divideSize+1])
        #    ###handle boundary conditions
        #    if (ii+divideSize == y_range): sum += np.sum(pix_vecs[:,0:1])
        #    if(sum > max_sum):
        #        max_sum = sum
        #        flip_index = ii
        for mm in xrange(divideCnt):
            sub_index = mm*divideSize
            for ii in xrange(x_range):
                for jj in xrange(divideSize):
                    sum += pix_vecs[ii,jj+sub_index]
            ###handle boundary conditions
            if (sub_index+divideSize == y_range): 
                for ii in xrange(x_range):
                    sum += pix_vecs[ii,0]
            if(sum > max_sum):
                max_sum = sum
                #flip_index = sub_index
                flip_index = mm
        
        #print 'pix_vecs sum:'
        #print np.sum(pix_vecs)

        tmp_vecs = pix_vecs
        ###flip the matrix column
        #pix_vecs = np.roll(pix_vecs, -flip_index)    
        if(flip_index == 0): 
            pass
        elif(flip_index == 1):
            ##flip Y
            pix_vecs = np.roll(np.fliplr(np.roll(pix_vecs, -divideSize,axis=1)),divideSize+1,axis=1)
        elif(flip_index == 2):
            ##flip X, Y
            pix_vecs = np.roll(np.fliplr(np.roll(np.fliplr(pix_vecs),-divideSize,axis=1)),divideSize,axis=1)
        elif(flip_index == 3):
            ##flip X
            pix_vecs = np.fliplr(np.roll(pix_vecs,-1,axis=1))
        else: 
            pass
        
        for ii in xrange(x_range-1):
            for jj in xrange(y_range):
                sub_index = ii*y_range + jj
                if not (pix_vecs[ii+1,jj] == tmp_vecs[ii+1,rotation_lut[flip_index,jj]]): 
                        print flip_index, np.savetxt("vec.txt",tmp_vecs,fmt='%d'), quit()
    
        ###matrix to vector
        for ii in xrange(x_range-1):
            for jj in xrange(y_range):
                sub_index = ii*y_range + jj
                fvecs[kk,sub_index] = pix_vecs[ii+1,rotation_lut[flip_index,jj]]
    #return fvecs
