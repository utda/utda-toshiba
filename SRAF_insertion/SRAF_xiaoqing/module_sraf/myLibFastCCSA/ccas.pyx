#!/work/lpkopt/python/2.7.3/bin/python

#import numpy as np
cimport numpy as np
cimport cython
from libc.math cimport M_PI,sin,cos,floor
from libc.stdlib cimport malloc, free
from cython.parallel cimport prange

### Concentric Circle with Area Sampling
@cython.boundscheck(False)
cpdef inline void CCSAcpy(np.ndarray[np.uint8_t,ndim=2] imArray, np.ndarray[np.int64_t,ndim=2] spoints, int xmin, int ymin, int x_range, int y_range, np.ndarray[np.int64_t,ndim=1] xstep, int gr, np.ndarray[np.int64_t,ndim=2] fvecs):
  cdef int ii, jj, kk, mm
  #cdef np.ndarray[np.int64_t,ndim=2] pix_vecs = np.zeros((x_range, y_range),dtype=int)
  cdef int** pix_vecs
  cdef int shape0 = imArray.shape[0]
  cdef int shape1 = imArray.shape[1]
  #cdef int* xdisp
  #cdef int* ydisp 
  #cdef int* x_index
  cdef int* lc_x
  cdef int* lc_y 
  cdef int step_size = xstep.shape[0]-1

  cdef int divideCnt = 4
  cdef int divideSize = 8
  cdef int* flip_index
  cdef int* max_sum
  cdef int* _sum
  cdef int pcnt = spoints.shape[0]
  cdef int i0, j0
  #cdef int msy = mask.shape[0]
  #cdef int msx = mask.shape[1]
  cdef int msy = 7
  cdef int msx = 7
  cdef int* sub_index
  cdef float y_range_f = float(y_range)

  ###define rotation_lut
  cdef int[4][32] rotation_lut = [[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
                   [16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17],
                   [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                   [0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1]]
  ###define mask
  cdef bint[7][7] mask = [[0, 0, 0, 1, 0, 0, 0],
                          [0, 1, 1, 1, 1, 1, 0],
                          [0, 1, 1, 1, 1, 1, 0],
                          [1, 1, 1, 1, 1, 1, 1],
                          [0, 1, 1, 1, 1, 1, 0],
                          [0, 1, 1, 1, 1, 1, 0],
                          [0, 0, 0, 1, 0, 0, 0]]
  
  cdef int* s_kk_x
  cdef int* s_kk_y
  cdef int** xdisp_lut
  cdef int** ydisp_lut
  ###calculate the displacement table 
  xdisp_lut = <int**> malloc(sizeof(int*)*step_size)
  for ii in xrange(step_size):
    xdisp_lut[ii] = <int*> malloc(sizeof(int)*y_range)
    for jj in xrange(y_range):
      xdisp_lut[ii][jj] = int(floor( xstep[ii+1]*cos(M_PI*(1-2.0*jj/(y_range_f))) ))

  ydisp_lut = <int**> malloc(sizeof(int*)*step_size)
  for ii in xrange(step_size):
    ydisp_lut[ii] = <int*> malloc(sizeof(int)*y_range)
    for jj in xrange(y_range):
      ydisp_lut[ii][jj] = int(floor( xstep[ii+1]*sin(M_PI*(1-2.0*jj/(y_range_f))) ))
  
  for kk in prange(pcnt,nogil=True):
    ### variables initialize
    #xdisp = <int*> malloc(sizeof(int))
    #ydisp = <int*> malloc(sizeof(int))
    #x_index = <int*> malloc(sizeof(int))
    #x_index[0] = 0
    lc_x  = <int*> malloc(sizeof(int))
    lc_y  = <int*> malloc(sizeof(int))
    flip_index = <int*> malloc(sizeof(int))
    flip_index[0] = 0
    max_sum    = <int*> malloc(sizeof(int))
    max_sum[0] = 0
    _sum        = <int*> malloc(sizeof(int))
    _sum[0] = 0
    sub_index  = <int*> malloc(sizeof(int))
    sub_index[0] = 0
    s_kk_x     = <int*> malloc(sizeof(int))
    s_kk_x[0] = spoints[kk,0] - xmin
    s_kk_y     = <int*> malloc(sizeof(int))
    s_kk_y[0] = spoints[kk,1] - ymin
    ### pix vectors initialize
    #pix_vecs = <int**> malloc(sizeof(int*)*34)
    pix_vecs = <int**> malloc(sizeof(int*)*x_range)
    for ii in xrange(x_range):
      pix_vecs[ii] = <int*> malloc(sizeof(int)*32)
      for jj in xrange(y_range):
        pix_vecs[ii][jj] = 0
    
    ###center sampling points
    #for ii in xstep[1:]:
    for ii in xrange(step_size):
      #x_index[0] += 1
      for jj in xrange(y_range):
        #lc_x[0] = spoints[kk,0]+xdisp_lut[ii][jj]
        #lc_y[0] = spoints[kk,1]+ydisp_lut[ii][jj]
        lc_x[0] = s_kk_x[0]+xdisp_lut[ii][jj]
        lc_y[0] = s_kk_y[0]+ydisp_lut[ii][jj]
        for i0 in xrange(msy):
          if lc_y[0]-gr+i0 < 0 or lc_y[0]-gr+i0 >= shape0: continue
          for j0 in xrange(msx):
            if lc_x[0]-gr+j0 < 0 or lc_x[0]-gr+j0 >= shape1: continue
            if imArray[lc_y[0]-gr+i0,lc_x[0]-gr+j0] == 255 and mask[i0][j0] == 1:
              #pix_vecs[x_index[0]][jj] += 1
              pix_vecs[ii+1][jj] += 1
            else: 
              continue
    # end for ii

    ###achieve independent feature matrix with column rotation
    ###trunk the sampling region to 4 or 2 parts
    ###scan the quadrant
    for mm in xrange(divideCnt):
      sub_index[0] = mm*divideSize
      for ii in xrange(x_range):
        for jj in xrange(divideSize):
          _sum[0] += pix_vecs[ii][jj+sub_index[0]]
      ###handle boundary conditions
      if (sub_index[0]+divideSize == y_range): 
        for ii in xrange(x_range):
          _sum[0] += pix_vecs[ii][0]
      if(_sum[0] > max_sum[0]):
        max_sum[0] = _sum[0]
        #flip_index = sub_index
        flip_index[0] = mm
  
    ###matrix to vector
    for ii in xrange(x_range-1):
      for jj in xrange(y_range):
        sub_index[0] = ii*y_range + jj
        fvecs[kk,sub_index[0]] = pix_vecs[ii+1][rotation_lut[flip_index[0]][jj]]
        #fvecs[kk,sub_index[0]] = pix_vecs[ii+1][jj]

    for ii in xrange(x_range):
      free(pix_vecs[ii])
    free(pix_vecs)
    #free(xdisp)
    #free(ydisp)
    #free(x_index)
    free(lc_x)
    free(lc_y)
    free(flip_index)
    free(max_sum)
    free(_sum)
    free(sub_index)

  for ii in xrange(step_size):
    free(xdisp_lut[ii])
    free(ydisp_lut[ii])
  free(xdisp_lut)
  free(ydisp_lut)
  #return fvecs
