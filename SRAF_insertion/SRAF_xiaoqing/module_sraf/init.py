#!/usr/bin/python

import os
import sys
import ConfigParser
import numpy as np

### Read parameter file
def Init(argv):
	argc = len(argv)
	
	if argc != 2:
		print ' Usage: opc_regression.py param.ini'
		quit()

	print '### Reading parameters...',

	filename = argv[1]
	if not os.path.isfile(filename):
		print ' '
		print ' File \'%s\' not found.' %filename
		quit()
	infile = ConfigParser.SafeConfigParser()
	infile.read(filename)

	param = { 
		'RandomOption': infile.get('RANDOM_OPTION', 'RandomOption'),
		'BWidth'      : infile.getint('RANDOM_CONTACT', 'BWIDTH'),
		'BHeight'     : infile.getint('RANDOM_CONTACT', 'BHEIGHT'),
		'CWidth'      : infile.getint('RANDOM_CONTACT', 'CWIDTH'),
		'CPitch'      : infile.getint('RANDOM_CONTACT', 'CPITCH'),
		'CGrid'       : infile.getint('RANDOM_CONTACT', 'CGRID'),
		'RNoise'      : infile.getint('RANDOM_CONTACT', 'RNOISE'),
		'BenchDir'    : infile.get('RANDOM_CONTACT', 'BENCHDIR'),
		'OPCRegion'   : infile.getint('RANDOM_CONTACT', 'OPC_REGION'),
		'SRAFRegion'  : infile.getint('RANDOM_CONTACT', 'SRAF_REGION'),
		'GridClipDir' : infile.get('LAYOUT_CLIP', 'GRIDDIR'),
		'GridSize'    : infile.getint('LAYOUT_CLIP', 'GRIDSIZE'),
		'ImgGrid'     : infile.getint('LAYOUT_CLIP', 'IMGGRID'),
		'GridRange'   : infile.get('LAYOUT_CLIP', 'GRIDRange'),
		'Window_x'    : infile.getint('LAYOUT_CLIP', 'WINDOW_Y'),
		'Window_y'    : infile.getint('LAYOUT_CLIP', 'WINDOW_X'),
		'ClipGDSDir'  : infile.get('LAYOUT_CLIP', 'GDSDIR'),
    'ClipTop'     : infile.get('LAYOUT_CLIP', 'TOP'),
    'ClipLayer'   : infile.getint('LAYOUT_CLIP', 'Layer'),
    'ClipDtype'   : infile.getint('LAYOUT_CLIP', 'Dtype'),
    'ClipOption'  : infile.get('CLIP_OPTION', 'ClipOption'),
    'SrafSizeCalibre'    : infile.getint('SRAF_PARAM', 'SRAFSIZE_CALIBRE'),
    'SrafSizeToshiba'    : infile.getint('SRAF_PARAM', 'SRAFSIZE_Toshiba'),
    'SrafRange'   : infile.get('SRAF_PARAM', 'SRAFRange'),
    'FeatureMode'	  : infile.get('LAYOUT_LEARN','FEATUREMODE'),
    'ToolMode'	    : infile.get('LAYOUT_LEARN','TOOLMODE'),
    'TrainCMGDSDir'	: infile.get('LAYOUT_LEARN','CMGDSDIR'),
    'TrainMSGDSDir'	: infile.get('LAYOUT_LEARN','MSGDSDIR'),
    'TrainPVGDSDir'	: infile.get('LAYOUT_LEARN','PVGDSDIR'),
		'TrainTop'	: infile.get('LAYOUT_LEARN','TOP'),
		'TrainLayer'	: infile.getint('LAYOUT_LEARN','Layer'),
		'TrainDtype'	: infile.getint('LAYOUT_LEARN','Dtype'),
		'TestCMGDSDir'	: infile.get('LAYOUT_TEST','CMGDSDIR'),
		'TestMSGDSDir'	: infile.get('LAYOUT_TEST','MSGDSDIR'),
		'TestPVGDSDir'	: infile.get('LAYOUT_TEST','PVGDSDIR'),
		'SimDir'	: infile.get('LAYOUT_TEST','SIMDIR'),
		'TestTop'	: infile.get('LAYOUT_TEST','TOP'),
		'TestLayer'	: infile.getint('LAYOUT_TEST','Layer'),
		'TestDtype'	: infile.getint('LAYOUT_TEST','Dtype'),
		'ProcNum'	: infile.getint('PARAM','ProcNum'),
		'Dsize'		: infile.get('PARAM','DataSize'),
		'Dindex'	: [],
		'Feature'	: infile.get('FEATURE','Feature'),
		'F_Wx'		: infile.getfloat('FEATURE','Wx'),
		'F_Gsearch'	: infile.get('FEATURE','Gsearch'),
		'F_Wx_range'	: infile.get('FEATURE','WxRange'),
		'StoreOption'	: infile.get('RESULT','STOREOPTION'),
		'TrainVecs'	: infile.get('RESULT','TrainVecs'),
		'TestVecs'	: infile.get('RESULT','TestVecs'),
		'VecsDir'	  : infile.get('RESULT','VecsDir'),
		'DRMode'	: infile.get('DIM_REDUCTION','Mode'),
		'DRRate'	: infile.getfloat('DIM_REDUCTION','UseDim'),
		'DRNum'		: infile.getint('DIM_REDUCTION','UseDimNum'),
		'DRdim'		: int(0),
		'Diffr_lambda'	: infile.getfloat('PARAM_DIFFR','Lambda'),
		'Diffr_na'	: infile.getfloat('PARAM_DIFFR','NA'),
		'Diffr_lx'	: infile.getfloat('PARAM_DIFFR','Lx'),
		'Dense_Grid'	: infile.getint('PARAM_DENSITY','GridNum'),
		'Dense_GridR'	: infile.get('PARAM_DENSITY','GridRange'),
		'CSS_Rin'	: infile.getint('PARAM_CSS','Rin'),
		'CSS_RinRange'	: infile.get('PARAM_CSS','RinRange'),
		'CCS_Rin'	: infile.getint('PARAM_CCS','Rin'),
		'CCS_RinRange'	: infile.get('PARAM_CCS','RinRange'),
		'HOG_Gauss'	: infile.getint('PARAM_HOG','Gauss'),
		'HOG_size'	: infile.getint('PARAM_HOG','HOG'),
		'HOG_Grange'	: infile.get('PARAM_HOG','Grange'),
		'HLAC_Corr'	: infile.get('PARAM_HLAC','Corr_width'),
		'HLAC_Pyramid'	: infile.get('PARAM_HLAC','Pyramid'),
		'GLAC_Gauss'	: infile.getint('PARAM_GLAC','Gauss'),
		'GLAC_Corr'	: infile.getint('PARAM_GLAC','Corr_width'),
		'GLAC_Grange'	: infile.get('PARAM_GLAC','Grange'),
		'Model'		: infile.get('MODEL','Model_Type'),
		'Mmode'		: infile.get('MODEL','Mode'),
		'MCMCmodel'	: infile.get('MODEL','MCMC_model'),
		'MCMCn'		: infile.getint('MODEL','MCMC_iter'),
		'MCMCc'		: infile.getint('MODEL','MCMC_chains'),
		'MCMCt'		: infile.getint('MODEL','MCMC_thin'),
		'HBMmodel'	: infile.get('MODEL','HBM_model'),
		'OutFeature'	: 'dens',
		'OutRMode'	: 'nan',
		'OutCluster'	: 'kmeans',
		'OutName'	: 'test',
		'OutSumAll'	: 'sum.all',
		'OutSumCenters'	: 'sum.cnt'}

	if param['Feature'] == 'Density':
		param['OutFeature'] = 'dens'
	elif param['Feature'] == 'Diffr':
		param['OutFeature'] = 'diffr'
	elif param['Feature'] == 'GLAC':
		param['OutFeature'] = 'glac'

	### make working dir
	if os.path.isdir('log'):
		#print './log exists'
		pass
	else:
		os.system('mkdir log')

	if os.path.isdir('result'):
		#print './result exists'
		pass
	else:
		os.system('mkdir result')

	print '  done.'

	return param
