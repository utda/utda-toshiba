#!/home/intern3/local/bin/python2.7

import os
import gdspy
from PIL import Image, ImageDraw
import numpy as np
import pkg_resources

### Import GDS
def GdsIn(pp,data,fname=""):

#    if data != 'clip':
#        print '### Import GDS...',

    if data == 'train':
        if (pp['FeatureMode']=='CM'):
            if (pp['ToolMode']=='Toshiba'):
                gds_dir = pp['TrainCMGDSDir']
            elif (pp['ToolMode']=='Calibre'):
                gds_dir = pp['TrainMSGDSDir']
            else: 
                print '###ERROR: tool mode should be Toshiba or Calibre, instead of %s' %(pp['ToolMode'])
        elif (pp['FeatureMode']=='PV'): 
            gds_dir = pp['TrainPVGDSDir']
        else:
            print '###ERROR: train mode should be CM or PV, instead of %s' %train_mode
            quit()
        #gname  = pp['TrainGDS']
        gname = gds_dir + "/" + fname
        tcell  = pp['TrainTop']
        #tcell2 = 'IN_TOP_tr_' + fname.split('.')[0]
        tcell2 = fname.split('.gds')[0]
        nlay   = (pp['TrainLayer'],pp['TrainDtype'])
    elif data == 'test':
        if (pp['FeatureMode']=='CM'):
            if (pp['ToolMode']=='Toshiba'):
                gds_dir = pp['TestCMGDSDir']
            elif (pp['ToolMode']=='Calibre'):
                gds_dir = pp['TestMSGDSDir']
            else:
                print '###ERROR: tool mode should be Toshiba or Calibre, instead of %s' %(pp['ToolMode'])
        elif (pp['FeatureMode']=='PV'): 
            gds_dir = pp['TestPVGDSDir']
        else:
            print '###ERROR: train mode should be CM or PV, instead of %s' %train_mode
            quit()
        #gname  = pp['TestGDS']
        gname  = gds_dir + "/" + fname
        tcell  = pp['TestTop']
        #tcell2 = 'IN_TOP_te'
        tcell2 = fname.split('.gds')[0]
        nlay   = (pp['TestLayer'],pp['TestDtype'])
    elif data == 'clip':
        gname  = pp['ClipGDSDir'] + "/" + fname
        tcell  = pp['ClipTop']
        tcell2 = 'IN_TOP_' + fname.split('.gds')[0]
        nlay   = (pp['ClipLayer'],pp['ClipDtype'])
    else:
        print '###ERROR: GdsIn(pp,data,fname=\'\') used when data = train/test/clip instead of %s' %data
        quit()
    
    gdsii = gdspy.GdsImport(gname,unit=1e-9,rename={tcell:tcell2})

    ### Plot GDS
    #gdsii.extract(tcell2)
    #gdspy.LayoutViewer()

    ### Get bounding box
    bbox = gdsii.extract(tcell2).get_bounding_box()
    w_x   = bbox[1,0]-bbox[0,0]
    w_y   = bbox[1,1]-bbox[0,1]

    ### Get area of all polygons
    #area = gdsii.extract(tcell2).area(True)
    #tarea= area.get(nlay)
    
    #if data != 'clip':
        #print '  done.'
        #print '\tGDS\t\t\t: %s' %gname
        #print '\tBounding box (lx,ly,ux,uy)\t: %.3f %.3f %.3f %.3f (um)' %(bbox[0,0],bbox[0,1],bbox[1,0],bbox[1,1])
        #print '\tTotal area\t\t: %.4f (um2)' %tarea
    
    return gdsii

### Import training GDS clips
def BatchGdsIn(pp,data):
    if data == 'train':
        ###training data    
        if (pp['FeatureMode']=='CM'):
            if (pp['ToolMode']=='Toshiba'):
                gds_dir = pp['TrainCMGDSDir']
            elif (pp['ToolMode']=='Calibre'):
                gds_dir = pp['TrainMSGDSDir']
            else: 
                print '###ERROR: tool mode should be Toshiba or Calibre, instead of %s' %(pp['ToolMode'])
        elif (pp['FeatureMode']=='PV'): 
            gds_dir = pp['TrainPVGDSDir']
        else: 
            print '###ERROR: train mode should be CM or PV, instead of %s' %(pp['FeatureMode'])
            quit()
    elif data == 'test':
        ###testing data
        if (pp['FeatureMode']=='CM'):
            if (pp['ToolMode']=='Toshiba'):
                gds_dir = pp['TestCMGDSDir']
            elif (pp['ToolMode']=='Calibre'):
                gds_dir = pp['TestMSGDSDir']
            else:
                print '###ERROR: tool mode should be Toshiba or Calibre, instead of %s' %(pp['ToolMode'])
        elif (pp['FeatureMode']=='PV'):
            gds_dir = pp['TestPVGDSDir']
        else:
            print '###ERROR: train mode should be CM or PV, instead of %s' %(pp['FeatureMode'])
            quit()
    else:
        print '###ERROR: BatchGdsIn(pp,data) used to import training or testing gds clips.'
        quit()
    
    ###clear the cell instances
    if int(pkg_resources.get_distribution("gdspy").version.split(".")[0]) < 1: 
        gdspy.Cell.cell_dict.clear()
    else:
        print "gdspy version = ", pkg_resources.get_distribution("gdspy").version
        gdspy.current_library.cell_dict.clear()

    ###reading in gds data
    gdsii_list = []
    cell_list = []
    for gdsfile in os.listdir(gds_dir):
        gdsii = GdsIn(pp,data,gdsfile)
        gdsii_list.append(gdsii)
        cell_list.append(gdsfile.split('.gds')[0])
        
    return gdsii_list, cell_list

### Export GDS
def GdsOut(pp, cell, fname, data=''):
    if data != 'clip':
        print '###ERROR: GdsOut(pp, cell, fname, data) only for data = clip.\n'
        quit()
    
    debug_function = 0
    if(debug_function): print "output gds - %s" %fname
    
    ###output gds
    gds_prt = gdspy.GdsPrint(fname, unit=1.0e-9, precision=1.0e-9)
    gds_prt.write_cell(cell)
    gds_prt.close()
    ###move gds to target folder
    os.system("mv %s %s" %(fname, pp['GridClipDir']))
    return 0


### Insert grid for the clipped layout
def gridInsertion(pp, gdsii, data, clip=""):
    if data != 'clip':
        print '###ERROR: gridInsertion(pp, data) only for data = clip.\n'
        quit()

    debug_function = 0
    ###export polygons
    tcell  = pp['ClipTop']
    tcell2 = 'IN_TOP_' + clip.split('.')[0]
    nlay   = (pp['ClipLayer'], pp['ClipDtype'])
    
    clip_cell = gdsii.extract(tcell2)
    ###Get bounding box
    bbox = clip_cell.get_bounding_box()
    print "bounding box of this cell: (%.1f, %1f, %1f, %1f)" %(bbox[0,0], bbox[0,1], bbox[1,0], bbox[1,1])
    center_x = int(np.ceil((bbox[0, 0] + bbox[1, 0]) / 2))
    center_y = int(np.ceil((bbox[0, 1] + bbox[1, 1]) / 2))
    ### polygons
    getp = clip_cell.get_polygons(True)
    getp = getp[nlay]
    
    ###create bitmap
    l_img = Image.new('L', ( int(bbox[1,0]-bbox[0,0]), int(bbox[1,1]-bbox[0,1]) ))
    for ii in xrange(len(getp)): 
        #transfer getp[ii] to tuple format
        tp = [tuple([getp[ii][jj][0], getp[ii][jj][1]]) for jj in xrange(len(getp[ii]))]
        #ImageDraw.Draw(l_img).polygon(tp, outline=1, fill=1)
        ImageDraw.Draw(l_img).polygon(tp, fill=1)
    l_img = l_img.transpose(Image.FLIP_TOP_BOTTOM)
    mask = np.asarray(l_img)
    #if(debug_function): np.savetxt("test.txt", mask, fmt='%1d')
    del l_img

    ###create meshgrid
    if (pp['ToolMode'] == 'Toshiba'):
        sraf_size = pp['SrafSizeToshiba']
    elif (pp['ToolMode'] == 'Calibre'):
        sraf_size = pp['SrafSizeCalibre']
    else:
        print '###ERROR: the tool mode should be Toshiba or Calibre instead of %s' %(pp['ToolMode'])
    window_x = pp['Window_x']
    window_y = pp['Window_y']
    ###resize the window for the lithosim interference range
    window_x = np.min([window_x, np.ceil((bbox[1,0] - bbox[0,0])/grid_size)*grid_size + 10*grid_size])
    window_y = np.min([window_y, np.ceil((bbox[1,1] - bbox[0,1])/grid_size)*grid_size + 10*grid_size])
    min_x = int(np.ceil(center_x - window_x / 2))
    min_y = int(np.ceil(center_y - window_y / 2))
    x_row = int(np.ceil(window_x/grid_size))
    y_col = int(np.ceil(window_y/grid_size))

    mesh_grids = np.meshgrid(np.ones(x_row), np.ones(y_col))[0]
    if (debug_function): 
        if int(pkg_resources.get_distribution("gdspy").version.split(".")[0]) < 1: 
            test_cell = clip_cell.copy("TOP", exclude_from_global=True)
            clipping_cell = clip_cell.copy("TOP", exclude_from_global=True)
        else:
            print "gdspy version = ", pkg_resources.get_distribution("gdspy").version
            test_cell = clip_cell.copy("TOP", exclude_from_current=True)
            clipping_cell = clip_cell.copy("TOP", exclude_from_current=True)
    
    ###output lithosim layout clips
    grid_index = 0
    for ii in xrange(x_row):
        for jj in xrange(y_col):
            llx = ii * grid_size + min_x
            lly = jj * grid_size + min_y
            urx = llx + sraf_size
            ury = lly + sraf_size
            #if(llx >= bbox[1,0] or lly >= bbox[1,1] or urx <= bbox[0,0] or ury <= bbox[0,1]): continue
            pg = gdspy.Polygon([(llx, lly),(llx, ury),(urx, ury),(urx, lly)])
            if(debug_function): print "grid in region: (%2d, %2d, %2d, %2d)" %(llx, lly, urx, ury)
            if(llx < bbox[0,0]): llx = bbox[0,0]
            if(llx > bbox[1,0]): llx = bbox[1,0]+1
            if(urx < bbox[0,0]): urx = bbox[0,0]
            if(urx > bbox[1,0]): urx = bbox[1,0]
            if(lly < bbox[0,1]): lly = bbox[0,1]
            if(lly > bbox[1,1]): lly = bbox[1,1]+1
            if(ury < bbox[0,1]): ury = bbox[0,1]
            if(ury > bbox[1,1]): ury = bbox[1,1]
            #get the intersection area
            #area = np.sum(np.sum(mask[llx:urx+1])[lly:ury+1])
            area = np.sum(mask[lly:ury+1,:][:,llx:urx+1])
            if(debug_function): print "covered area: %2d" %area
            if(area >= (sraf_size**2)/4): 
                if(debug_function):
                    test_cell.add(pg)
                    #print "grid inserted: (%2d, %2d, %2d, %2d)" %(llx, lly, urx, ury)
            else:
                grid_index += 1
                if int(pkg_resources.get_distribution("gdspy").version.split(".")[0]) < 1: 
                    grid_cell = clip_cell.copy("TOP", exclude_from_global=True)
                else:
                    print "gdspy version = ", pkg_resources.get_distribution("gdspy").version
                    grid_cell = clip_cell.copy("TOP", exclude_from_current=True)
                grid_cell.add(pg)
                if(debug_function): clipping_cell.add(pg)
                cell_name = clip.split('.')[0] + "_" + str(grid_index) + ".gds"
                GdsOut(pp, grid_cell, cell_name, data)
                
            if(debug_function): print "\n"
    
    if(debug_function):        
        gds_prt = gdspy.GdsPrint('test_cell.gds', unit=1.0e-9, precision=1.0e-9)
        gds_prt.write_cell(test_cell)
        gds_prt.close()
        gds_prt = gdspy.GdsPrint('clipping_cell.gds', unit=1.0e-9, precision=1.0e-9)
        gds_prt.write_cell(clipping_cell)
        gds_prt.close()

    return 0

### make random 2-D contact holes 
### llx = lly = 0
def makeGDS(pp,phase,bwidth=0,bheight=0,cpitch=0,cwidth=0,cgrid=0,rnoise=0):
    if bwidth == 0: bwidth = pp['BWidth']
    else: pass
    if bheight == 0: bheight = pp['BHeight']
    else: pass
    if cpitch == 0: cpitch = pp['CPitch']
    else: pass
    if cwidth == 0: cwidth = pp['CWidth']
    else: pass
    if cgrid == 0: cgrid = pp['CGrid']
    else: pass
    if rnoise == 0: rnoise = pp['RNoise']
    else: pass
    
    if phase == 'train': rnoise = 0

    x_col = int(np.ceil(bwidth/float(cpitch)))
    y_row = int(np.ceil(bheight/float(cpitch)))
    
    x_coords = np.array([np.array([ii*cpitch for ii in range(x_col)]) for jj in range(y_row)])
    y_coords = np.transpose( np.array([np.array([ii*cpitch for ii in range(y_row)]) for jj in range(x_col)]) )
    
    x_noise = np.ceil(np.random.uniform(-0.5,0.5,size=(y_row,x_col))*rnoise/float(cgrid))*cgrid
    y_noise = np.ceil(np.random.uniform(-0.5,0.5,size=(y_row,x_col))*rnoise/float(cgrid))*cgrid

    x_coords += x_noise
    for ii in range(y_row): x_coords[ii][0] = np.max([0, x_coords[ii][0]])
    y_coords += y_noise
    for ii in range(x_col): y_coords[0][ii] = np.max([0, y_coords[0][ii]])
    
    cwidth = cwidth/1000.0
    ### create the cell instance
    if int(pkg_resources.get_distribution("gdspy").version.split(".")[0]) < 1: 
        random_cell = gdspy.Cell('TOP', exclude_from_global=True)
    else:
        print "gdspy version = ", pkg_resources.get_distribution("gdspy").version
        random_cell = gdspy.Cell('TOP', exclude_from_current=True)
    for ii in range(y_row):
        for jj in range(x_col):
            llx = x_coords[ii][jj]/1000.0
            urx = llx + cwidth
            lly = y_coords[ii][jj]/1000.0 
            ury = lly + cwidth
            random_cell.add(gdspy.Polygon([(llx,lly),(llx,ury),(urx,ury),(urx,lly)],layer=2,datatype=0))

    cwidth = int(cwidth*1000.0)
    cell_name = str(phase)+'randomclip_'+str(bwidth)+'_'+str(bheight)+'_'+str(cpitch)+'_'+str(cwidth)
    gds_prt = gdspy.GdsPrint(('%s/%s.gds' %(pp['BenchDir'],cell_name)),unit=1.0e-6,precision=1.0e-9)
    gds_prt.write_cell(random_cell)
    if int(pkg_resources.get_distribution("gdspy").version.split(".")[0]) < 1: 
        gdspy.Cell.cell_dict.clear()
    else:
        print "gdspy version = ", pkg_resources.get_distribution("gdspy").version
        gdspy.current_library.cell_dict.clear()
    gds_prt.close()
    
    return 0

def makeGDSDense(pp,phase,bwidth=0,bheight=0,cpitch=0,cwidth=0):
    if bwidth == 0: bwidth = pp['BWidth']
    else: pass
    if bheight == 0: bheight = pp['BHeight']
    else: pass
    if cpitch == 0: cpitch = pp['CPitch']
    else: pass
    if cwidth == 0: cwidth = pp['CWidth']
    else: pass
    
    x_col = int(np.ceil(bwidth/float(cpitch)))
    y_row = int(np.ceil(bheight/float(cpitch)))
    
    x_coords = np.array([np.array([ii*cpitch for ii in range(x_col)]) for jj in range(y_row)])
    y_coords = np.transpose( np.array([np.array([ii*cpitch for ii in range(y_row)]) for jj in range(x_col)]) )
    
    cwidth = cwidth/1000.0
    ### create the cell instance
    if int(pkg_resources.get_distribution("gdspy").version.split(".")[0]) < 1: 
        random_cell = gdspy.Cell('TOP', exclude_from_global=True)
    else:
        print "gdspy version = ", pkg_resources.get_distribution("gdspy").version
        random_cell = gdspy.Cell('TOP', exclude_from_current=True)
    for ii in range(y_row):
        for jj in range(x_col):
            llx = x_coords[ii][jj]/1000.0
            urx = llx + cwidth
            lly = y_coords[ii][jj]/1000.0 
            ury = lly + cwidth
            random_cell.add(gdspy.Polygon([(llx,lly),(llx,ury),(urx,ury),(urx,lly)],layer=2,datatype=0))

    cwidth = int(cwidth*1000.0)
    cell_name = 'denseclip_'+str(bwidth)+'_'+str(bheight)
    gds_prt = gdspy.GdsPrint(('%s/%s.gds' %(pp['BenchDir'],cell_name)),unit=1.0e-6,precision=1.0e-9)
    gds_prt.write_cell(random_cell)
    if int(pkg_resources.get_distribution("gdspy").version.split(".")[0]) < 1: 
        gdspy.Cell.cell_dict.clear()
    else:
        print "gdspy version = ", pkg_resources.get_distribution("gdspy").version
        gdspy.current_library.cell_dict.clear()
    gds_prt.close()
    
    return 0

### Benchmark generation with dense contact holes
def BenchGenDense(pp,phase):
    print "Dense contact hole benchmarks generation for %s" %phase 
    width = pp['CWidth']
    pitch = 2*width
    _bwidth = 100 
    _bheight = 100
    for ii in xrange(5):
        for jj in xrange(5):
            bwidth = _bwidth*(ii+1)
            bheight = _bheight*(jj+1)
            makeGDSDense(pp,phase,bwidth,bheight,pitch,width)
            ###expand edge o reserve opc and sraf regions
            cell_name = 'denseclip_'+str(bwidth)+'_'+str(bheight)
            reserve_opc_sraf_region(pp,cell_name,phase)

### Benchmark generation with 2-D random contact holes
def BenchGen(pp,phase):
    print 'Random 2-D contact hole benchmarks generation for %s' %phase

    _bwidth = pp['BWidth']
    _bheight = pp['BHeight']
    cpitch = pp['CPitch']
    cwidth = pp['CWidth']
    
    ### Generate each cell instance
    if phase == 'test':
        for ii in range(0,10):
            for jj in range(0,10):
                bwidth = _bwidth*(ii+1)
                bheight = _bheight*(jj+1)
                makeGDS(pp, phase, bwidth, bheight)
                ###expand edge to reserve opc and sraf regions
                cell_name = str(phase)+'randomclip_'+str(bwidth)+'_'+str(bheight)+'_'+str(cpitch)+'_'+str(cwidth)
                reserve_opc_sraf_region(pp,cell_name,phase)
    elif phase == 'train':
        _bwidth = 500
        _bheight = 500
        for ii in range(6):
            bwidth = _bwidth*(int(np.ceil((ii+1)/2.0)))
            bheight = _bheight*(int(np.ceil((ii+1)/2.0)))
            cpitch = cwidth*(2+ii)
            makeGDS(pp, phase, bwidth, bheight,cpitch)
            cell_name = str(phase)+'randomclip_'+str(bwidth)+'_'+str(bheight)+'_'+str(cpitch)+'_'+str(cwidth)
            reserve_opc_sraf_region(pp,cell_name,phase)
    else:
        print '###ERROR: the random benchmark generation phase should be train or test instead of %s' %phase
    
    return 0

### reserve the opc and sraf region 
def reserve_opc_sraf_region(pp,cell,phase):
    opc_region = pp['OPCRegion']/1000.0
    sraf_region = pp['SRAFRegion']/1000.0
    ###make calbre drc code
    drc_code = """
//Expand edge
"""
    drc_code += """
LAYOUT PATH    './%s.gds'
""" %cell
    drc_code += """
LAYOUT PRIMARY 'TOP'
"""
    drc_code += """
DRC RESULTS DATABASE './%s_EXPAND.gds' GDSII
""" %cell
    drc_code += """
PRECISION 1000
LAYOUT SYSTEM GDSII
DRC MAXIMUM RESULTS ALL
DRC MAXIMUM VERTEX 4000
//LAYOUT WINDOW 0 0 100 100
//LAYOUT WINDOW CLIP YES

LAYER MAP 1  datatype 0 1001 LAYER char       1001
LAYER MAP 2  datatype 0 1002 LAYER target     1002
LAYER MAP 4  datatype 0 1004 LAYER bias       1004
LAYER MAP 20 datatype 0 1020 LAYER slayer     1020
"""
    drc_code += """
// r0
hole1 = SIZE target BY %.1f
// r1
hole2 = SIZE target BY %.5f
""" %(opc_region, sraf_region)

    drc_code += """
hole3 = hole2 NOT hole1

OUT_target     {COPY target    } DRC CHECK MAP OUT_target      2  0
OUT_bias       {COPY bias      } DRC CHECK MAP OUT_bias        4  0
OUT_sraf       {COPY slayer    } DRC CHECK MAP OUT_sraf        20 0
OUT_char       {COPY char      } DRC CHECK MAP OUT_char        1  0

OUT_H1         {COPY hole1     } DRC CHECK MAP OUT_H1          21 0
OUT_H2         {COPY hole2     } DRC CHECK MAP OUT_H2          22 0
OUT_H3         {COPY hole3     } DRC CHECK MAP OUT_H3          23 0
"""
    
    fs = open(('%s/expand_edge.rule' %pp['BenchDir']),'w')
    fs.write(drc_code)
    fs.close()     
    
    os.chdir(pp['BenchDir'])
    os.system('calibre -drc -hier -turbo -turbo_litho 8 -64 -hyper expand_edge.rule >& expand_edge.log')
    os.system('mv %s_EXPAND.gds ./%s/%s.gds' %(cell,phase,cell))
    os.system('rm ./%s.gds' %(cell))
    os.chdir('../../../')
    
    return 0
