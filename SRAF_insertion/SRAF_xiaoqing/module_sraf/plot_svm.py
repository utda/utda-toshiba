#!/home/intern3/local/bin/python2.7

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
plt.legend(frameon=False)

def plot_histogram(fmodes=['LGR', 'DTree', 'SVC']):
	fmode = fmodes[0]
	#ns_epe_best = np.loadtxt('./result/ns_epe_best.txt')
        #print ns_epe_best
	#ns_epe_inner = np.loadtxt('./result/ns_epe_inner.txt')
        #print ns_epe_inner
	#ns_epe_outer = np.loadtxt('./result/ns_epe_outer.txt')
        #print ns_epe_outer
	ns_pvband = np.loadtxt('./result/spase_dense_train/ns_pvband.txt')
	mb_epe_best = np.loadtxt('./result/spase_dense_train/mb_epe_best.txt')
	mb_epe_inner = np.loadtxt('./result/spase_dense_train/mb_epe_inner.txt')
	mb_epe_outer = np.loadtxt('./result/spase_dense_train/mb_epe_outer.txt')
	mb_pvband = np.loadtxt('./result/spase_dense_train/mb_pvband.txt')
	ml_epe_best = np.loadtxt('./result/spase_dense_train/%s_epe_best.txt' %fmode)
	ml_epe_inner = np.loadtxt('./result/spase_dense_train/%s_epe_inner.txt' %fmode)
	ml_epe_outer = np.loadtxt('./result/spase_dense_train/%s_epe_outer.txt' %fmode)
	ml_pvband = np.loadtxt('./result/spase_dense_train/%s_pvband.txt' %fmode)
        #nf_epe_best = np.loadtxt('./result/spase_dense_train_full_wo_fopt/%s_epe_best.txt' %fmode)
        #nf_epe_inner = np.loadtxt('./result/spase_dense_train_full_wo_fopt/%s_epe_inner.txt' %fmode)
        #nf_epe_outer = np.loadtxt('./result/spase_dense_train_full_wo_fopt/%s_epe_outer.txt' %fmode)
        #nf_pvband = np.loadtxt('./result/spase_dense_train_full_wo_fopt/%s_pvband.txt' %fmode)
        #nf_epe_best = np.loadtxt('./result/%s_epe_best.txt' %fmode)
        #nf_epe_inner = np.loadtxt('./result/%s_epe_inner.txt' %fmode)
        #nf_epe_outer = np.loadtxt('./result/%s_epe_outer.txt' %fmode)
        #nf_pvband = np.loadtxt('./result/%s_pvband.txt' %fmode)
        fmode = 'DTree'
	dt_epe_best = np.loadtxt('./result/dtree/%s_epe_best.txt' %fmode)
	dt_epe_inner = np.loadtxt('./result/dtree/%s_epe_inner.txt' %fmode)
	dt_epe_outer = np.loadtxt('./result/dtree/%s_epe_outer.txt' %fmode)
        dt_pvband = np.loadtxt('./result/dtree/%s_pvband.txt' %fmode)
        fmode = 'SVC'
	svc_epe_best = np.loadtxt('./result/svc/%s_epe_best.txt' %fmode)
	svc_epe_inner = np.loadtxt('./result/svc/%s_epe_inner.txt' %fmode)
	svc_epe_outer = np.loadtxt('./result/svc/%s_epe_outer.txt' %fmode)
        svc_pvband = np.loadtxt('./result/svc/%s_pvband.txt' %fmode)

	fig_epe,ax_epe = plt.subplots()
	min_epe = int(np.min([np.min(mb_epe_best),np.min(dt_epe_best),np.min(svc_epe_best),np.min(ml_epe_best)]))
	max_epe = int(np.max([np.max(mb_epe_best),np.max(dt_epe_best),np.max(svc_epe_best),np.max(ml_epe_best)]))
	colors = ['red','tan','blue','pink']
	labels = ['Model-based', 'DTree', 'SVM', 'LGR']
	bins = np.array(range(min_epe-1,max_epe+2))
        plt.ylim([0,len(mb_epe_best)])
	n,bins,patches = ax_epe.hist([mb_epe_best,dt_epe_best,svc_epe_best,ml_epe_best],bins=bins,histtype='bar',color=colors,label=labels,normed=0)
        print "model based SRAF - epe nominal: %.4f" %(np.sum(mb_epe_best*1.0)/len(mb_epe_best))
        print "DTree SRAF w/ feature optimization - epe nominal: %.4f" %(np.sum(dt_epe_best*1.0)/len(dt_epe_best))
        print "SVC SRAF w/ feature optimization - epe nominal: %.4f" %(np.sum(svc_epe_best*1.0)/len(svc_epe_best))
        print "LGR SRAF w/ feature optimization - epe nominal: %.4f" %(np.sum(ml_epe_best*1.0)/len(ml_epe_best))
	ax_epe.legend(prop={'size':15},loc='upper left',frameon=False)
	#ax_epe.set_title('EPE distributions')
	ax_epe.set_xlabel('EPE at nominal contour (nm)',fontsize=20)
	ax_epe.set_ylabel('Normalized frequency',fontsize=20)
        for tick in ax_epe.xaxis.get_major_ticks():
            tick.label.set_fontsize(15)
        for tick in ax_epe.yaxis.get_major_ticks():
            tick.label.set_fontsize(15)
	plt.xticks(bins)
        to_percentage = lambda y, pos: str(round( ( y / float(len(mb_epe_best)) ), 2))
        plt.gca().yaxis.set_major_formatter(FuncFormatter(to_percentage))
	fig_epe.savefig('./result/epe_best_dist.pdf')
	#fig_epe.savefig('./result/epe_best_dist.png')
	
	fig_epe,ax_epe = plt.subplots()
	min_epe = int(np.min([np.min(mb_epe_inner),np.min(dt_epe_inner),np.min(svc_epe_inner),np.min(ml_epe_inner)]))
	max_epe = int(np.max([np.max(mb_epe_inner),np.max(dt_epe_inner),np.max(svc_epe_inner),np.max(ml_epe_inner)]))
	bins = np.array(range(min_epe-1,max_epe+2))
        plt.ylim([0,len(mb_epe_inner)])
	n,bins,patches = ax_epe.hist([mb_epe_inner,dt_epe_inner,svc_epe_inner,ml_epe_inner],bins=bins,histtype='bar',color=colors,label=labels,normed=0)
        print "model based SRAF - epe inner: %.4f" %(np.sum(mb_epe_inner*1.0)/len(mb_epe_inner))
        print "DTree SRAF w/ feature optimization - epe inner: %.4f" %(np.sum(dt_epe_inner*1.0)/len(dt_epe_inner))
        print "SVC SRAF w/ feature optimization - epe inner: %.4f" %(np.sum(svc_epe_inner*1.0)/len(svc_epe_inner))
        print "LGR SRAF w/ feature optimization - epe inner: %.4f" %(np.sum(ml_epe_inner*1.0)/len(ml_epe_inner))
	ax_epe.legend(prop={'size':15},loc='upper right',frameon=False)
	#ax_epe.set_title('EPE distributions')
	ax_epe.set_xlabel('EPE at inner contour (nm)',fontsize=20)
	ax_epe.set_ylabel('Normalized Frequency',fontsize=20)
        for tick in ax_epe.xaxis.get_major_ticks():
            tick.label.set_fontsize(15)
        for tick in ax_epe.yaxis.get_major_ticks():
            tick.label.set_fontsize(15)
	plt.xticks(bins)
        to_percentage = lambda y, pos: str(round( ( y / float(len(mb_epe_inner)) ), 2)) 
        plt.gca().yaxis.set_major_formatter(FuncFormatter(to_percentage))
	fig_epe.savefig('./result/epe_inner_dist.pdf')
	#fig_epe.savefig('./result/epe_inner_dist.png')

	fig_epe,ax_epe = plt.subplots()
	min_epe = int(np.min([np.min(mb_epe_outer),np,min(dt_epe_outer),np.min(svc_epe_outer),np.min(ml_epe_outer)]))
	max_epe = int(np.max([np.max(mb_epe_outer),np.max(dt_epe_outer),np.max(svc_epe_outer),np.max(ml_epe_outer)]))
	bins = np.array(range(min_epe-1,max_epe+2))
        plt.ylim([0,len(mb_epe_outer)])
	n,bins,patches = ax_epe.hist([mb_epe_outer,dt_epe_outer,svc_epe_outer,ml_epe_outer],bins=bins,histtype='bar',color=colors,label=labels,normed=0)
        print "model based SRAF - epe outer: %.4f" %(np.sum(mb_epe_outer*1.0)/len(mb_epe_outer))
        print "DTree SRAF w/ feature optimization - epe outer: %.4f" %(np.sum(dt_epe_outer*1.0)/len(dt_epe_outer))
        print "SVC SRAF w/ feature optimization - epe outer: %.4f" %(np.sum(svc_epe_outer*1.0)/len(svc_epe_outer))
        print "LGR SRAF w/ feature optimization - epe outer: %.4f" %(np.sum(ml_epe_outer*1.0)/len(ml_epe_outer))
	ax_epe.legend(prop={'size':15},loc='upper left',frameon=False)
	#ax_epe.set_title('EPE distributions')
	ax_epe.set_xlabel('EPE at outer contour (nm)',fontsize=20)
	ax_epe.set_ylabel('Normalized Frequency',fontsize=20)
        for tick in ax_epe.xaxis.get_major_ticks():
            tick.label.set_fontsize(15)
        for tick in ax_epe.yaxis.get_major_ticks():
            tick.label.set_fontsize(15)
	plt.xticks(bins)
        to_percentage = lambda y, pos: str(round( ( y / float(len(mb_epe_outer)) ), 2))
        plt.gca().yaxis.set_major_formatter(FuncFormatter(to_percentage))
	fig_epe.savefig('./result/epe_outer_dist.pdf')
	#fig_epe.savefig('./result/epe_outer_dist.png')

	labels = ['Model-based', 'DTree', 'SVM', 'LGR', 'No SRAF']
	colors = ['red','tan','blue','pink','black']
	fig_pvb,ax_pvb = plt.subplots()
        plt.ylim([0,len(mb_pvband)])
	n,bins,patches = ax_pvb.hist([mb_pvband*1000.0,dt_pvband*1000.0,svc_pvband*1000.0,ml_pvband*1000.0,ns_pvband*1000.0],bins=8,histtype='bar',color=colors,label=labels,normed=0)
        print "model based SRAF - pv band mean: %.4f" %(np.sum(mb_pvband*1000.0)/len(mb_pvband))
        print "DTree SRAF w/ feature optimization - pv band mean: %.4f" %(np.sum(dt_pvband*1000.0)/len(dt_pvband))
        print "SVC SRAF w/ feature optimization - pv band mean: %.4f" %(np.sum(svc_pvband*1000.0)/len(svc_pvband))
        print "LGR SRAF w/ feature optimization - pv band mean: %.4f" %(np.sum(ml_pvband*1000.0)/len(ml_pvband))
        print "no SRAF - pv band mean: %.4f" %(np.sum(ns_pvband*1000.0)/len(ns_pvband))
	ax_pvb.legend(prop={'size':15},loc='upper left',frameon=False)
	#ax_pvb.set_title('PV band distributions')
	ax_pvb.set_xlabel('PV band area (x0.001 um^2)',fontsize=20)
	ax_pvb.set_ylabel('Normalized Frequency',fontsize=20)
        for tick in ax_pvb.xaxis.get_major_ticks():
            tick.label.set_fontsize(15)
        for tick in ax_pvb.yaxis.get_major_ticks():
            tick.label.set_fontsize(15)
	plt.xticks(bins)
        to_percentage = lambda y, pos: str(round( ( y / float(len(mb_pvband)) ), 2))
        plt.gca().yaxis.set_major_formatter(FuncFormatter(to_percentage))
	fig_pvb.savefig('./result/pvb_dist.pdf')
	#fig_pvb.savefig('./result/pvb_dist.png')
	
	fig_rt,ax_rt = plt.subplots()
	ax_rt.legend(prop={'size':10},loc='upper left')
	#axis_x = ['clip152','clip15', 'clip16', 'dc_100_200', 'dc_100_500', 'dc_200_200', 'dc_200_300']
	#axis_x = ['1.07x1.35','1.35x1.35', '1.07x1.07', '1.07x1.21', '1.07x1.45', '1.21x1.21', '1.21x1.35']
	#axis_x = ['1.4445','1.8225', '1.1449', '1.2947', '1.5515', '1.4641', '1.6335']
	#axis_x = ['1.4','1.8', '1.1', '1.3', '1.6', '1.5', '1.6']
        axis_x = ['3x1', '3x3', '1x1', '2x1', '4x1', '2x2', '2x3']
	#index_x = np.arange(11)
	index_x = np.arange(7)
	bar_width = 0.2
	axis_y_mb = [5.0, 5.0, 5.0, 5.0, 6.0, 5.0, 5.0]
	axis_y_svm = [10.4, 12.2, 9.1, 9.3, 10.9, 10.4, 10.9]
	axis_y_dtree = [0.39, 0.52, 0.34, 0.35, 0.46, 0.38, 0.42]
	axis_y_lgr = [0.44, 0.48, 0.33, 0.36, 0.46, 0.39, 0.42]
        print "model based SRAF - runtime mean: %.4f" %(np.sum(axis_y_mb)/len(axis_y_mb))
        print "DTree SRAF w/ feature optimization - runtime mean: %.4f" %(np.sum(axis_y_dtree)/len(axis_y_dtree))
        print "SVC SRAF w/ feature optimization - runtime mean: %.4f" %(np.sum(axis_y_svm)/len(axis_y_svm))
        print "LGR SRAF w/ feature optimization - runtime mean: %.4f" %(np.sum(axis_y_lgr)/len(axis_y_lgr))
        plt.ylim([0,16])
	ax_rt.bar(index_x,axis_y_mb,bar_width,color=colors[0],label='Model-based')
	ax_rt.bar(index_x+bar_width,axis_y_svm,bar_width,color=colors[1],label='SVM')
	ax_rt.bar(index_x+2*bar_width,axis_y_dtree,bar_width,color=colors[2],label='DTree')
	ax_rt.bar(index_x+3*bar_width,axis_y_lgr,bar_width,color=colors[3],label='LGR')
	ax_rt.legend(prop={'size':15},loc='upper right',frameon=False)
	ax_rt.set_xlabel('Benchmark area (um x um)',fontsize=20)
	ax_rt.set_ylabel('Runtime (s)',fontsize=20)
        for tick in ax_rt.xaxis.get_major_ticks():
            tick.label.set_fontsize(15)
        for tick in ax_rt.yaxis.get_major_ticks():
            tick.label.set_fontsize(15)
	plt.xticks(index_x+bar_width,axis_x)
	fig_rt.savefig('./result/runtime.pdf')
