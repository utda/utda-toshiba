#!/usr/bin/python

import time
import numpy as np
import matplotlib
matplotlib.use('Agg')
import pylab as pl
import scipy.linalg
import scipy.sparse.linalg
from sklearn import manifold
from multiprocessing import Pool

### Principal Component Analysis
def myPCA(x):
	### Solve eigenvalue problem
	ll,vv = np.linalg.eig(np.dot(x.T,x))

	### Sort
	idx   = ll.argsort()[::-1]
	ll    = ll[idx]
	vv    = vv[:,idx].T

	return ll.real,vv.real

### Locality Preserving Projection
def myLPP(x):
	### Heat Kernel
	x2  = np.sum(x*x,axis=1)
	x2e = np.tile(x2,x2.shape[0]).reshape(x2.shape[0],x2.shape[0]).T
	tmp1= x2e+x2e.T
	del x2,x2e
	tmp2= -2.0*np.dot(x,x.T)
	tmpx= tmp1+tmp2
	del tmp1,tmp2
	W   = np.exp(-tmpx)
	del tmpx
	#W   = np.exp(-(x2e+x2e.T-2.0*np.dot(x,x.T)))
	D   = np.diag(np.sum(W,axis=1))
	L   = D-W
	z   = np.dot(x.T,np.dot(D,x))
	del D
	z   = (z+z.T)/2.0

	### Solve generalized eigenvector problem
	#ll,vv = scipy.linalg.eigh(np.dot(x.T,np.dot(L,x)),z)
	ll,vv = scipy.sparse.linalg.eigsh(np.dot(x.T,np.dot(L,x)),x.shape[1]-1,z,which='SM')
	vv    = vv/np.linalg.norm(vv)

	### Sort
	idx   = ll.argsort()
	ll    = ll[idx]
	vv    = vv[:,idx]

	return ll,vv

### Dimensionality Reduction
def DimReduction(param,x0,ss):

	print '### Dimensionality reduction...'
	ttime1 = time.time()

	### Centralization
	xx = x0-np.mean(x0,axis=0)

	### Normalization
	#xx = (x0-np.mean(x0,axis=0))/np.std(x0,axis=0)
	
	if param['DRMode'] == 'None':
		if param['Model'] == 'HBM':
			### add edge_length
			x0  = np.hstack([x0,np.array([ss[:,5]]).T])
			xxd = (x0-np.mean(x0,axis=0))/np.std(x0,axis=0)
		else:
			xxd = x0
		prvec = 0

	elif param['DRMode'] == 'PCA':
		rmode = 'pca'
		ll,vv = myPCA(xx)
		### Projection
		xxd   = np.dot(xx,vv.T)
		prvec = vv.T
		### Compute comulative proportion
		cp    = [np.sum([ll[j] for j in xrange(0,i+1)]) for i in xrange(xx.shape[1])]/np.sum(ll)

	elif param['DRMode'] == 'LPP':
		rmode = 'lpp'
		ll,vv = myLPP(xx)
		### Projection
		xxd   = np.dot(xx,vv)
		prvec = vv
		### Compute comulative proportion
		if np.sum(ll) == 0.0:
			cp = np.linspace(0,1.0,xx.shape[1])
		else:
			cp = [np.sum([ll[j] for j in xrange(0,i+1)]) for i in xrange(xx.shape[1]-1)]/np.sum(ll)
			#cp = [np.sum([ll[j] for j in xrange(0,i+1)]) for i in xrange(3)]/np.sum(ll)

	else:
		print ''
		print '  param.ini file error'
		quit()

	### Output log
	if param['DRMode'] == 'None':
		print '\tno dimensionality reduction'
	else:
		ndim50   = np.where(cp>=0.5)[0][0]+1 
		ndim80   = np.where(cp>=0.8)[0][0]+1 
		ndim90   = np.where(cp>=0.9)[0][0]+1 
		ndim99   = np.where(cp>=0.99)[0][0]+1 
		user_th  = float(param['DRRate'])/100.0
		userdim  = np.where(cp>=user_th)[0][0]+1
		userdimn = param['DRNum']

		print '\talgorithm\t\t: %s' %param['DRMode']
		print '\t#of initial dim.\t: %d' %xx.shape[1]
		print '\t#dim.[.5,.8,.9,.99]\t: %d,%d,%d,%d' %(ndim50,ndim80,ndim90,ndim99)
		if userdimn != 0:
			print '\tuser defined #dim.\t: %d' %userdimn
			userdim = userdimn
		else:
			print '\tuser defined #dim.\t: %d%% (%d)' %(int(param['DRRate']),userdim)
		#print '\tdone.'
		print '\telapsed time (sec)\t: %.3f' %(time.time()-ttime1)

		### Get compressed feature vector
		xxd = xxd[:,0:userdim]
		param['DRdim'] = userdim

		### Output feature vector
		param['OutRMode'] = rmode
		fname = './result/feature.' + param['OutFeature'] + '.' + rmode + '.vec.npy'
		#np.save(fname,xxd)

	return [xxd,prvec]

### Dimensionality reduction for testing data
def Projection(pp,prvec,x_tr,x_te,ss_tr,ss_te):
	if pp['DRMode'] == 'None':
		if pp['Model'] == 'HBM':
			x_tr = np.hstack([x_tr,np.array([ss_tr[:,5]]).T])
			x_te = np.hstack([x_te,np.array([ss_te[:,5]]).T])
			xxd  = (x_te-np.mean(x_tr,axis=0))/np.std(x_tr,axis=0)
		else:
			xxd = x_te
		print '\tno dimensionality reduction'
	else:
		### Centralization
		xx  = x_te-np.mean(x_tr,axis=0)

		### Reduction
		xxd = np.dot(xx,prvec)
		xxd = xxd[:,0:pp['DRdim']]

		print '\t#of initial dim.\t: %d' %x_te.shape[1]
		print '\t#of dim.\t\t: %d' %xxd.shape[1]

	return xxd
