#!/usr/bin/python

import os
import sys
import time
import numpy as np
#garbage collection
import gc

sys.path.insert(0,os.path.join('./'))
from gdsii    import BatchGdsIn
from sraf_rule import GetSrafData, InsertSraf
from feature  import GetFeature,SampleSizeReduction
from comp     import DimReduction,Projection
from model    import FitModel,Predict, Threshold
from rmspe    import GetRMSPE
from pvband   import PVBandSim

### Grid search for feature parameters
def OptFparam(pp):
    tt = time.time()

    print '### Grid search...'
    wxr = map(float,pp['F_Wx_range'].split(','))
    gridr = map(int,pp['GridRange'].split(','))
    srafr = map(int,pp['SrafRange'].split(','))

    if pp['Feature'] == 'Density':
        param2 = map(int,pp['Dense_GridR'].split(','))
    elif pp['Feature'] == 'CSS':
        param2 = map(int,pp['CSS_RinRange'].split(','))
    elif pp['Feature'] == 'CCSA':
        param2 = map(int,pp['CCS_RinRange'].split(','))
    elif pp['Feature'] == 'HLAC':
        param2 = [0]
    elif pp['Feature'] == 'Gauss_HOG':
        param2 = map(int,pp['HOG_Grange'].split(','))
    elif pp['Feature'] == 'Gauss_GLAC':
        param2 = map(int,pp['GLAC_Grange'].split(','))
    
    res = []

    gds_tr_list, cell_tr_list  = BatchGdsIn(pp, 'train')   # Read gds files, various layout clips
    gds_te_list, cell_te_list   = BatchGdsIn(pp,'test')    ### Read gds    
    
    #Enumerate grid size 
    for kk in gridr:
        pp['GridSize'] = kk
        #Enumerate sraf size
        for mm in srafr:
            if mm < kk: continue
            pp['SrafSizeCalibre'] = mm
            pp['SrafSizeToshiba'] = mm
            ss_tr_initial    = GetSrafData(pp,gds_tr_list,cell_tr_list,'train') # Read the potential SRAF positions or usefulness map
            ss_te_initial    = GetSrafData(pp,gds_te_list,cell_te_list,'test')        ### Read the potential SRAF positions or usefulness map
            #Enumerate feature extraction window size
            for ii in wxr:
                #Enumerate feature extraction step
                for jj in param2:
                    pp['F_Wx'] = ii
                    if pp['Feature'] == 'Density':
                        pp['Dense_Grid'] = jj
                    elif pp['Feature'] == 'CSS':
                        pp['CSS_Rin'] = jj
                    elif pp['Feature'] == 'CCSA':
                        pp['CCS_Rin'] = jj
                    elif pp['Feature'] == 'HLAC':
                        pass
                    elif pp['Feature'] == 'Gauss_HOG':
                        pp['HOG_Gauss'] = jj
                    elif pp['Feature'] == 'Gauss_GLAC':
                        pp['GLAC_Gauss'] = jj
            
                    #try:
                    if True:
                        ### Training phase
                        fvec_tr       = GetFeature(pp,gds_tr_list,cell_tr_list,ss_tr_initial,'train') # Feature extraction
                        #dict to array
                        ss_tr = np.array([ss for item in cell_tr_list for ss in ss_tr_initial[item]])
                        print "ss_tr sum: ", np.sum(ss_tr)
                        fvec_tr, ss_tr = SampleSizeReduction(pp,fvec_tr,ss_tr, 'train')  # Sample size reduction
                        rfv_tr        = DimReduction(pp, fvec_tr, ss_tr)     # Dimension reduction
                        srafmodel, t0, score = FitModel(pp, rfv_tr[0], ss_tr)       # Model training
                        print '### Finish training ###\n'
                
                        ### Testing pahse
                        fvec_te  = GetFeature(pp,gds_te_list,cell_te_list,ss_te_initial,'test')        ### Feature extraction
                        #dict to array
                        ss_te = np.array([ss for item in cell_te_list for ss in ss_te_initial[item]])
                        fvec_te, ss_te = SampleSizeReduction(pp,fvec_te,ss_te, 'test')          ### Sample size reduction
                        if pp['DRMode'] == 'None':
                            rfv_te   = fvec_te
                        else:
                            rfv_te   = Projection(pp,rfv_tr[1],fvec_tr,fvec_te,ss_tr,ss_te)    ### Projection
    
                        yh_tr,t1 = Predict(pp,srafmodel,rfv_tr[0],ss_tr)        ### Predict @ training data
                        yh_te,t2 = Predict(pp,srafmodel,rfv_te,ss_te)        ### Predict @ testing data
    
                        print '### Finish testing ###\n'
                        th = Threshold(pp,srafmodel,rfv_te)
                        xy_te_dict = InsertSraf(pp,gds_te_list,cell_te_list,ss_te_initial,fvec_te,yh_te,th,'test')   ### Insert SRAF features bassed on predicted usefulness map
                        
                        rmse = GetRMSPE(pp,ss_tr,yh_tr,ss_te,yh_te,[t0,t1,t2])        ### Compute RMSPE
                        PVBandSim(pp,cell_te_list,xy_te_dict)
                         
                        ###store the data
                        dir_name = '%s_%s_%s_%s' %(str(kk),str(mm),str(ii),str(jj))
                        #store the pvband, epe values
                        os.system('mkdir ./result/%s' %dir_name)
                        os.system('mv ./result/*.txt ./result/%s' %dir_name)
                        #store the output gds
                        os.system('mkdir ../lithosim/results/%s' %dir_name)
                        os.system('mv ../lithosim/results/*.oas ../lithosim/results/%s' %dir_name)
                        res.append([kk,mm,ii,jj,score,rmse[0],rmse[1]])
                    #except:
                    else:
                        res.append([kk,mm,ii,jj,0.0,999,999])
                    print res[-1]
                    del fvec_tr, fvec_te, ss_tr, ss_te, yh_tr, yh_te
                ### End for jj
            ### End for ii
            #print res
        ### End for mm
    ### End for kk

    fout = open('./result/gridsearch.txt','w')
    for i in xrange(len(res)):
        fout.write('%d %d %.1f %d %.4f %.5f %.5f\n' %(res[i][0],res[i][1],res[i][2],res[i][3],res[i][4],res[i][5],res[i][6]))
    fout.close()

    print '### Elapsed Time (sec): %.4f' %(time.time()-tt)
    ### End

