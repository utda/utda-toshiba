import os
import sys

# main function
def main():
	### gds directory
	dir = './gds/predict/runtime_measure'
	predict_dir = './gds/predict/MB_SRAF_clips'
        #batch_file = open('./batch_run.csh','w')
	for item in os.listdir(dir):
		os.system('rm %s/*' %predict_dir)
                #batch_file.write('rm %s/* \n' %predict_dir)
		print 'Process benchmark: %s' %item
		#batch_file.write('echo Process benchmark: %s \n' %item)
		os.system('cp %s/%s %s' %(dir,item,predict_dir))
	        #batch_file.write('cp %s/%s %s \n' %(dir,item,predict_dir))
		os.system('python2.7 run_sraf.py param.opc.ini >& ./log/%s.log' %(item.split('.gds')[0]))
		#batch_file.write('python2.7 run_sraf.py param.opc.ini >& ./log/%s.log \n\n' %(item.split('.gds')[0]))
	return 0

if __name__ == "__main__":
  main()
