#!/home/intern3/local/bin/python2.7

import numpy as np
import matplotlib.pyplot as plt

def plot_histogram(fmodes=['LGR']):
	fmode = fmodes[0]
	mb_epe_best = np.loadtxt('./result/mb_epe_best.txt')
	mb_epe_inner = np.loadtxt('./result/mb_epe_inner.txt')
	mb_epe_outer = np.loadtxt('./result/mb_epe_outer.txt')
	mb_pvband = np.loadtxt('./result/mb_pvband.txt')
	ml_epe_best = np.loadtxt('./result/%s_epe_best.txt' %fmode)
	ml_epe_inner = np.loadtxt('./result/%s_epe_inner.txt' %fmode)
	ml_epe_outer = np.loadtxt('./result/%s_epe_outer.txt' %fmode)
	ml_pvband = np.loadtxt('./result/%s_pvband.txt' %fmode)

	fig_epe,ax_epe = plt.subplots()
	min_epe = int(np.min([np.min(mb_epe_best),np.min(ml_epe_best)]))
	max_epe = int(np.max([np.max(mb_epe_best),np.max(ml_epe_best)]))
	colors = ['red','tan']
	labels = ['Model based SRAF', 'Machine Learning Based SRAF']
	bins = np.array(range(min_epe-1,max_epe+2))
	n,bins,patches = ax_epe.hist([mb_epe_best,ml_epe_best],bins=bins,histtype='bar',color=colors,label=labels,normed=1)
	ax_epe.legend(prop={'size':10},loc='upper left')
	#ax_epe.set_title('EPE distributions')
	ax_epe.set_xlabel('EPE at nominal contour (nm)')
	ax_epe.set_ylabel('Frequency')
	plt.xticks(bins)
	fig_epe.savefig('./result/epe_best_dist.pdf')
	#fig_epe.savefig('./result/epe_best_dist.png')
	
	fig_epe,ax_epe = plt.subplots()
	min_epe = int(np.min([np.min(mb_epe_inner),np.min(ml_epe_inner)]))
	max_epe = int(np.max([np.max(mb_epe_inner),np.max(ml_epe_inner)]))
	bins = np.array(range(min_epe-1,max_epe+2))
	n,bins,patches = ax_epe.hist([mb_epe_inner,ml_epe_inner],bins=bins,histtype='bar',color=colors,label=labels,normed=1)
	ax_epe.legend(prop={'size':10},loc='upper right')
	#ax_epe.set_title('EPE distributions')
	ax_epe.set_xlabel('EPE at inner contour (nm)')
	ax_epe.set_ylabel('Frequency')
	plt.xticks(bins)
	fig_epe.savefig('./result/epe_inner_dist.pdf')
	#fig_epe.savefig('./result/epe_inner_dist.png')

	fig_epe,ax_epe = plt.subplots()
	min_epe = int(np.min([np.min(mb_epe_outer),np.min(ml_epe_outer)]))
	max_epe = int(np.max([np.max(mb_epe_outer),np.max(ml_epe_outer)]))
	bins = np.array(range(min_epe-1,max_epe+2))
	n,bins,patches = ax_epe.hist([mb_epe_outer,ml_epe_outer],bins=bins,histtype='bar',color=colors,label=labels,normed=1)
	ax_epe.legend(prop={'size':10},loc='upper left')
	#ax_epe.set_title('EPE distributions')
	ax_epe.set_xlabel('EPE at outer contour (nm)')
	ax_epe.set_ylabel('Frequency')
	plt.xticks(bins)
	fig_epe.savefig('./result/epe_outer_dist.pdf')
	#fig_epe.savefig('./result/epe_outer_dist.png')

	fig_pvb,ax_pvb = plt.subplots()
	n,bins,patches = ax_pvb.hist([mb_pvband*1000.0,ml_pvband*1000.0],bins=8,histtype='bar',color=colors,label=labels,normed=1)
	ax_pvb.legend(prop={'size':10},loc='upper left')
	#ax_pvb.set_title('PV band distributions')
	ax_pvb.set_xlabel('PV band area (x0.001 um^2)')
	ax_pvb.set_ylabel('Frequency')
	plt.xticks(bins)
	fig_pvb.savefig('./result/pvb_dist.pdf')
	#fig_pvb.savefig('./result/pvb_dist.png')

	
	fig_rt,ax_rt = plt.subplots()
	ax_rt.legend(prop={'size':10},loc='upper left')
	#axis_x = ['1um x 1um', '2um x 2um', '3um x 3um', '4um x 4um', '5um x 5um', '6um x 6um', '7um x 7um', '8um x 8um', '9um x 9um', '10um x 10um']
	axis_x = ['1x1', '2x2', '3x3', '4x4', '5x5', '6x6', '7x7', '8x8', '9x9', '10x10']
	index_x = np.arange(10)
	bar_width = 0.35
	axis_y_mb = [5.0, 6.0, 7.0, 10.0, 18.0, 26.0, 36.0, 46.0, 53.0, 60.0]
	axis_y_ml = [1.1, 2.9, 6.2, 10.3, 14.8, 19.6, 26.8, 34.8, 46.2, 51.2]
	ax_rt.bar(index_x,axis_y_mb,bar_width,color=colors[0],label='Model Based SRAF')
	ax_rt.bar(index_x+bar_width,axis_y_ml,bar_width,color=colors[1],label='Machine Learning Based SRAF')
	ax_rt.legend(prop={'size':10},loc='upper left')
	ax_rt.set_xlabel('Benchmark area (um x um)')
	ax_rt.set_ylabel('Runtime (s)')
	plt.xticks(index_x+bar_width,axis_x)
	fig_rt.savefig('./result/runtime.pdf')
