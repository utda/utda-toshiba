#!/home/intern3/local/bin/python2.7

import os
import sys
import time
import gdspy
import numpy as np
from PIL import Image, ImageDraw
import scipy.fftpack
from multiprocessing import Pool

import cython
sys.path.insert(0, os.path.join('./'))
from extlist import CExtList
#from cglac import GetGLAC
from calchlac import CGetHLAC
from ccas import CCSAcpy


def ExtList(tnum, crange, adist, cpoints, cntp, getp_t, pdir):
  mlist = []
  ncntp = cntp.shape[0]
  for i in xrange(tnum):
    k = 0
    for j in xrange(ncntp):
      tbx = cntp[j,0]-adist/2.0
      tby = cntp[j,1]-adist/2.0
      ttx = cntp[j,0]+adist/2.0
      tty = cntp[j,1]+adist/2.0
      if cpoints[i,0] >= tbx and cpoints[i,0] <= ttx and cpoints[i,1] >= tby and cpoints[i,1] <= tty:
        k = j
      else:
        pass
    mlist.append([getp_t[k][0],cpoints[i],crange,pdir[i]])
  return mlist


### Extract polygons
def MakeList(xData):
  getp    = xData[0]
  pcenter = xData[1]
  crange  = xData[2]
  pdir    = xData[3]

  spx = float(pcenter[0])-crange/2.0
  spy = float(pcenter[1])-crange/2.0
  epx = spx + crange
  epy = spy + crange

  getpxs = []
  for i in xrange(len(getp)):
    xs = np.where(getp[i][:,0] >= spx)
    if len(xs[0]) == 0:
      continue
    else:
      getpxs.append(getp[i])
  getpxe = []
  for i in xrange(len(getpxs)):
    xe = np.where(getpxs[i][:,0] <= epx)
    if len(xe[0]) == 0:
      continue
    else:
      getpxe.append(getpxs[i])
  getpys = []
  for i in xrange(len(getpxe)):
    ys = np.where(getpxe[i][:,1] >= spy)
    if len(ys[0]) == 0:
      continue
    else:
      getpys.append(getpxe[i])
  getpf = []
  for i in xrange(len(getpys)):
    ye = np.where(getpys[i][:,1] <= epy)
    if len(ye[0]) == 0:
      continue
    else:
      getpf.append(getpys[i])
  return [getpf,[round(spx,4),round(epx,4),round(spy,4),round(epy,4)],pdir]

### Slice and rotate GDS
def SliceGDS(xData):
  debug_function = 0
  getp   = xData[0]
  points = xData[1]
  pdir   = xData[2]
  tgds   = gdspy.slice(getp, [points[0],points[1]], 0)
  fgds   = gdspy.slice(tgds[1], [points[2],points[3]], 1)

  cx     = points[0]+(points[1]-points[0])/2
  cy     = points[2]+(points[3]-points[2])/2

  ### up
  if pdir == 0:
    rgds = fgds[1]
  ### right:90
  elif pdir == 1:
    rgds = fgds[1].rotate(np.pi/2,center=(cx,cy))
  ### down:180
  elif pdir == 2:
    rgds = fgds[1].rotate(np.pi,center=(cx,cy))
  ### left:270
  elif pdir == 3:
    rgds = fgds[1].rotate(-np.pi/2,center=(cx,cy))

  #if(debug_function & len(rgds.polygons) != 0):
  if(debug_function and len(rgds.polygons) != 0):
    print cx,cy
    fname = 'test.gds'
    test_cell = gdspy.Cell('test')
    for pg in rgds.polygons: test_cell.add(gdspy.Polygon(pg*1000.0))
    gds_prt = gdspy.GdsPrint(fname,unit=1.0e-9,precision=1.0e-9)
    gds_prt.write_cell(test_cell)
    gds_prt.close()
    #gdspy.LayoutViewer()
    gdspy.Cell.cell_dict.clear()
  
  return rgds

### Compute density vector
def GetArea(xData):
  sgds   = xData[0]
  crange = xData[1]
  ddim   = xData[2]
  dwidth = xData[3]
  dnum   = xData[4]
  pcenter= xData[5]

  ### set calc. area
  spx = float(pcenter[0])-crange/2.0
  spy = float(pcenter[1])-crange/2.0
  epx = spx + crange
  epy = spy + crange

  ### create img array
  igrid = int(crange*1000)
  im    = Image.new('L',(igrid,igrid))
  draw  = ImageDraw.Draw(im)
  lbx   = spx*1000
  lby   = spy*1000
  tdif  = np.array([lbx,lby],dtype=int)

  for pol in sgds.polygons:
    test  = np.array(pol*1000 - tdif, dtype=int)
    test2 = []

    ### grid2pix
    nn = test.shape[0]
    for j in xrange(nn):
      if j == 0 or j == nn-1:
        test2.append(test[j])
        continue
      if test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] > test[j,1] and test[j,0] < test[j+1,0]:
        test2.append(test[j])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] > test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] < test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append([test[j,0],test[j,1]-1])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      else:
        test2.append(test[j])
    test2 = np.array(test2,dtype=int)

    ### pix_cord2poly
    tp   = [tuple([test2[j,0],test2[j,1]]) for j in xrange(test.shape[0])]

    ### draw polygon
    draw.polygon(tp,fill=255)

  ### transpose
  im = im.transpose(Image.FLIP_TOP_BOTTOM)

  ### check img
  #tname = 'test_'+str(pcenter[0])+'_'+str(pcenter[1])+'.png'
  #im.save(tname)

  ### img2array
  imArray = np.asarray(im)
  del im

  ### calc. area
  pwidth = int(np.ceil((float(crange)/float(ddim)*1000)))

  ### area vectors
  area_vecs = np.zeros(dnum,dtype=int)

  ### extract area value / segment
  for i in xrange(ddim):
    for j in xrange(ddim):
      k  = i+j*ddim
      sx = i*pwidth
      sy = j*pwidth
      ex = sx + pwidth
      ey = sy + pwidth
      
      sArray = imArray[sy:ey,sx:ex]
      area_vecs[k] = len(np.where(sArray==255)[0])
  
  return area_vecs

### Compute Diffraction spectrum
def GetDiffr(xData):
  sgds   = xData[0]
  crange = xData[1]
  #ddim   = xData[2]
  #dwidth = xData[2]
  #dnum   = xData[3]
  pcenter= xData[2]
  
  ### Pupil size (#of dimension): 4*NA*Lx/lambda
  pna     = xData[3][0]  ### NA
  plambda = xData[3][1]  ### Lambda
  pxl     = xData[3][2]  ### Calc. length
  dim     = int(np.ceil(4.0*pna*pxl/plambda))
  
  ptrans  = 0.06
  pphase  = np.pi
  #pshift  = ptrans*np.exp(1.0j*pphase)
  #ppattn  = 1.0*np.exp(1.0j*0.0)
  pshift  = 0
  ppattn  = 1

  ### set calc. area
  spx = float(pcenter[0])-crange/2.0
  spy = float(pcenter[1])-crange/2.0
  epx = spx + crange
  epy = spy + crange

  ### create img array
  igrid = int(crange*1000)
  rsize = igrid/10
  im    = Image.new('L',(igrid,igrid))
  draw  = ImageDraw.Draw(im)
  lbx   = spx*1000
  lby   = spy*1000
  tdif  = np.array([lbx,lby],dtype=int)

  for pol in sgds.polygons:
    test  = np.array(pol*1000 - tdif, dtype=int)
    test2 = []

    ### grid2pix
    nn = test.shape[0]
    for j in xrange(nn):
      if j == 0 or j == nn-1:
        test2.append(test[j])
        continue
      if test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] > test[j,1] and test[j,0] < test[j+1,0]:
        test2.append(test[j])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] > test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] < test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append([test[j,0],test[j,1]-1])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      else:
        test2.append(test[j])
    test2 = np.array(test2,dtype=int)

    ### pix_cord2poly
    tp   = [tuple([test2[j,0],test2[j,1]]) for j in xrange(test.shape[0])]

    ### draw polygon
    draw.polygon(tp,fill=255)

  im = im.resize((rsize,rsize))
  im = im.transpose(Image.FLIP_TOP_BOTTOM)
  imArray = np.asarray(im,dtype=int)
  imArray[imArray==255] = ppattn
  imArray[imArray==0]   = pshift
  
  ### Check img
  #fname = 'test_'+str(pcenter[0])+'_'+str(pcenter[1])+'.png'
  #im.save(fname)
  del im
  
  ### FFT
  xfft   = scipy.fftpack.fft2(imArray)
  imfft  = np.abs(xfft)**2 /rsize/rsize
  simfft = scipy.fftpack.fftshift(imfft)
  
  ### Pupil filter
  xst    = int(rsize/2-dim/2)
  xed    = int(rsize/2+dim/2+1)

  diffr_vec = simfft[xst:xed,xst:xed]
    
  return diffr_vec.flatten()

### Concentric square sampling
def GetCSS(xData):
  sgds   = xData[0]
  crange = xData[1]
  dimn   = xData[2]
  pcenter= xData[3]
  pdir   = 0

  ### set calc. area
  spx = float(pcenter[0])-crange/2.0
  spy = float(pcenter[1])-crange/2.0
  epx = spx + crange
  epy = spy + crange

  ### create img array
  igrid = int(crange*1000)
  im    = Image.new('L',(igrid,igrid))
  draw  = ImageDraw.Draw(im)
  lbx   = spx*1000
  lby   = spy*1000
  tdif  = np.array([lbx,lby],dtype=int)

  for pol in sgds.polygons:
    test  = np.array(pol*1000 - tdif, dtype=int)
    test2 = []

    ### grid2pix
    nn = test.shape[0]
    for j in xrange(nn):
      if j == 0 or j == nn-1:
        test2.append(test[j])
        continue
      if test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] > test[j,1] and test[j,0] < test[j+1,0]:
        test2.append(test[j])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] > test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] < test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append([test[j,0],test[j,1]-1])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      else:
        test2.append(test[j])
    test2 = np.array(test2,dtype=int)

    ### pix_cord2poly
    tp   = [tuple([test2[j,0],test2[j,1]]) for j in xrange(test.shape[0])]

    ### draw polygon
    draw.polygon(tp,fill=255)

  ### transpose
  im = im.transpose(Image.FLIP_TOP_BOTTOM)

  ### resize
  #rsize = igrid/5
  #im    = im.resize((rsize,rsize))

  ### check img
  #tname = 'test_'+str(pcenter[0])+'_'+str(pcenter[1])+'.png'
  #im.save(tname)

  ### img2array
  imArray = np.asarray(im)
  del im

  ### calc. dim.
  instep  = 10
  oustep  = 20
  pradius = int(imArray.shape[0])/2
  xdim    = int((dimn/instep+np.floor((pradius-dimn)/oustep))*8+1)

  ### pix vectors
  pix_vecs = np.zeros(xdim,dtype=int)
  xstep    = np.r_[np.arange(0,dimn,instep,dtype=int),np.arange(dimn,pradius,oustep,dtype=int)]
  
  ### concentric square sampling
  zz = 1
  for ii in xstep:
    if ii == 0:
      pix_vecs[0] = imArray[pradius,pradius]
    else:
      pix_vecs[zz]   = imArray[pradius-ii,pradius+ii] ### Left top
      pix_vecs[zz+1] = imArray[pradius,pradius+ii]    ### Center top
      pix_vecs[zz+2] = imArray[pradius+ii,pradius+ii] ### Right top
      pix_vecs[zz+3] = imArray[pradius+ii,pradius]    ### Right center
      pix_vecs[zz+4] = imArray[pradius+ii,pradius-ii] ### Right bottom
      pix_vecs[zz+5] = imArray[pradius,pradius-ii]    ### Center bottom
      pix_vecs[zz+6] = imArray[pradius-ii,pradius-ii] ### Left bottom
      pix_vecs[zz+7] = imArray[pradius-ii,pradius]    ### Left center
      zz += 8
    # endif
  # end for
  pix_vecs[pix_vecs==255] = 1
  
  return pix_vecs

###HLAC
def GetHLAC(xData):
  sgds   = xData[0]
  crange = xData[1]
  pcenter= xData[2]
  iStep  = xData[3]
  rSize  = xData[4]

  ### set calc. area
  spx = float(pcenter[0])-crange/2.0
  spy = float(pcenter[1])-crange/2.0
  epx = spx + crange
  epy = spy + crange

  ### create img array
  igrid = int(crange*1000)
  im    = Image.new('L',(igrid,igrid))
  draw  = ImageDraw.Draw(im)
  lbx   = spx*1000
  lby   = spy*1000
  tdif  = np.array([lbx,lby],dtype=int)

  for pol in sgds.polygons:
    test  = np.array(pol*1000 - tdif, dtype=int)
    test2 = []

    ### grid2pix
    nn = test.shape[0]
    for j in xrange(nn):
      ### cv_lb:1, cv_rb:2, cv_lt:3, cv_rt:4, cc_lt:5, cc_rt:6, cc_rb:7, cc_lb:8
      if j == 0 or j == nn-1:
        test2.append(test[j])
        continue

      if test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] > test[j,1] and test[j,0] < test[j+1,0]:
        test2.append(test[j])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] > test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] < test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append([test[j,0],test[j,1]-1])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      else:
        test2.append(test[j])
    test2 = np.array(test2,dtype=int)

    ### pix_cord2poly
    tp   = [tuple([test2[j,0],test2[j,1]]) for j in xrange(test.shape[0])]

    ### draw polygon
    draw.polygon(tp,fill=255)

  ### transpose
  im = im.transpose(Image.FLIP_TOP_BOTTOM)

  ### resize (x nm/pix)
  imArray = []
  for j in rSize:
    rsize = int(igrid/j)
    imArray.append(im.resize((rsize,rsize)))

  im = np.array(im,dtype=int)
  r1,r2 = im.shape

  ### compute HLAC
  hvec_temp = []
  for j in iStep:
    hvec_temp.extend( CGetHLAC(im,r1,r2,j) )

  if len(rSize) == 1:
    pass
  else:
    for j in xrange(1,len(rSize)):
      im_tmp = np.array(imArray[j],dtype=int)
      r1,r2  = im_tmp.shape
      hvec_temp.extend( CGetHLAC(im_tmp,r1,r2,1) )
  del im,imArray

  hvec = np.array(hvec_temp,dtype=int)
  return hvec

### HOG
def GetHOG(xData):
  sgds   = xData[0]
  crange = xData[1]
  ksize  = xData[2] ### Gaussian filter size
  pcenter= xData[3]

  ### set calc. area
  spx = float(pcenter[0])-crange/2.0
  spy = float(pcenter[1])-crange/2.0
  epx = spx + crange
  epy = spy + crange

  ### create img array
  igrid = int(crange*1000)
  im    = Image.new('L',(igrid,igrid))
  draw  = ImageDraw.Draw(im)
  lbx   = spx*1000
  lby   = spy*1000
  tdif  = np.array([lbx,lby],dtype=int)

  for pol in sgds.polygons:
    test  = np.array(pol*1000 - tdif, dtype=int)
    test2 = []

    ### grid2pix
    nn = test.shape[0]
    for j in xrange(nn):
      ### cv_lb:1, cv_rb:2, cv_lt:3, cv_rt:4, cc_lt:5, cc_rt:6, cc_rb:7, cc_lb:8
      if j == 0 or j == nn-1:
        test2.append(test[j])
        continue

      if test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] > test[j,1] and test[j,0] < test[j+1,0]:
        test2.append(test[j])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] > test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] < test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append([test[j,0],test[j,1]-1])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      else:
        test2.append(test[j])
    test2 = np.array(test2,dtype=int)

    ### pix_cord2poly
    tp   = [tuple([test2[j,0],test2[j,1]]) for j in xrange(test.shape[0])]

    ### draw polygon
    draw.polygon(tp,fill=255)

  ### transpose
  im = im.transpose(Image.FLIP_TOP_BOTTOM)

  ### resize (5nm/pix)
  rsize = igrid/5
  im    = im.resize((rsize,rsize))

  ### Sigma = 0.3*(ksize/2-1)+0.8
  im_gs = cv2.GaussianBlur(np.asarray(im),(ksize,ksize),0)
  #cv2.imshow('test',im_gs)
  #cv2.waitKey()

  ### Window size, Block size(16,16)only, Block stride, Cell size(8,8)only, nbins 9only
  cv_hog = cv2.HOGDescriptor((64,64),(16,16),(8,8),(8,8),9)
  hvecs  = cv_hog.compute(np.asarray(im_gs))
  del im,im_gs

  ### calc. dim.
  hog_dim = len(hvecs)

  ### check img
  #tname = 'test_'+str(pcenter[0])+'_'+str(pcenter[1])+'.png'
  #im.save(tname)

  return hvecs

### Gradient Local Auto Correlation
def GetGLACv(xData):
  sgds   = xData[0]
  crange = xData[1]
  ksize  = xData[2]
  pcenter= xData[3]

  ### set calc. area
  spx = float(pcenter[0])-crange/2.0
  spy = float(pcenter[1])-crange/2.0
  epx = spx + crange
  epy = spy + crange

  ### create img array
  igrid = int(crange*1000)
  im    = Image.new('L',(igrid,igrid))
  draw  = ImageDraw.Draw(im)
  lbx   = spx*1000
  lby   = spy*1000
  tdif  = np.array([lbx,lby],dtype=int)

  for pol in sgds.polygons:
    test  = np.array(pol*1000 - tdif, dtype=int)
    test2 = []

    ### grid2pix
    nn = test.shape[0]
    for j in xrange(nn):
      ### cv_lb:1, cv_rb:2, cv_lt:3, cv_rt:4, cc_lt:5, cc_rt:6, cc_rb:7, cc_lb:8
      if j == 0 or j == nn-1:
        test2.append(test[j])
        continue

      if test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] > test[j,1] and test[j,0] < test[j+1,0]:
        test2.append(test[j])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] > test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] < test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append([test[j,0],test[j,1]-1])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      else:
        test2.append(test[j])
    test2 = np.array(test2,dtype=int)

    ### pix_cord2poly
    tp   = [tuple([test2[j,0],test2[j,1]]) for j in xrange(test.shape[0])]

    ### draw polygon
    draw.polygon(tp,fill=255)

  ### transpose
  im = im.transpose(Image.FLIP_TOP_BOTTOM)

  ### resize (5nm/pix)
  #rsize = igrid/5
  #im    = im.resize((rsize,rsize))

  ### Sigma = 0.3*(ksize/2-1)+0.8
  im_gs = cv2.GaussianBlur(np.asarray(im),(ksize,ksize),0)

  ### Compute GLAC
  #glacvecs = GetGLAC(im_gs.astype(float))
  del im,im_gs

  ### calc. dim.
  #glac_dim = len(glacvecs)

  return glacvecs

###feature extraction for SRAF regression
def GetFeature(pp, gdsii_list, cell_list, ss_tr, data):
  ttime1 = time.time()
  print 'Feature extraction...'
  
  if pp['Feature'] == 'Density':
    fmode = 'dens'
  elif pp['Feature'] == 'Diffr':
    fmode = 'diffr'
  elif pp['Feature'] == 'CSS':
    fmode = 'css'
  elif pp['Feature'] == 'CCSA':
    fmode = 'ccsa'
  elif pp['Feature'] == 'HLAC':
    fmode   = 'hlac'
  elif pp['Feature'] == 'Gauss_HOG':
    fmode = 'hog'
  elif pp['Feature'] == 'Gauss_GLAC':
    fmode = 'glac'
  else:
    print ' feature mode error.'
    quit()

  #### Common parameters
  #if data == 'train':
  #  vecf     = pp['TrainVecs']
  #elif data == 'test':
  #  vecf     = pp['TestVecs']

  #### Check feature vecs (if feature vector exists, skip feature extraction)
  #if os.path.isfile(vecf) and pp['F_Gsearch'] != 'ON':
  #  if fmode == 'dens' or fmode == 'css' or fmode == 'hlac':
  #    fvec = np.genfromtxt(vecf)
  #  else:
  #    fvec = np.load(vecf)

  #  #if pp['Dsize'] != 'All' and data == 'train':
  #  #  nd   = pp['Dindex']
  #  #  if pp['Dsize'] == 'Large':
  #  #    nd = nd[0:10000]
  #  #  elif pp['Dsize'] == 'Middle':
  #  #    nd = nd[0:5000]
  #  #  elif pp['Dsize'] == 'Small':
  #  #    nd = nd[0:1000]
  #  #  fvec = fvec[nd]

  #  print '\tFeature file exists'
  #  print '\tFeature type\t\t: %s' %fmode
  #  print '\t#samples\t\t: %d' %len(fvec)
  #  print '\t#dimensions\t\t: %d' %len(fvec[0])
  #  print '\telapsed time (sec)\t: %.3f' %(time.time()-ttime1)

  #  return fvec

  ###Extract features for each layout clip
  gds_index = 0
  fvec = list()
  #spoints = list()
  #mxlist = [[pp,cell_list[ii],gdsii_list[ii],ss_tr[cell_list[ii]],data] for ii in range(len(cell_list))]
  for item in cell_list:
    print '\t feature extraction for clip %s' %item
    unit_fvec = GetFeature_unit_fast(pp, item, gdsii_list[gds_index], ss_tr[item], data)
    fvec.extend(unit_fvec)
    #spoints.append(ss_tr[item][:,0:2])
    gds_index += 1
  #dict to list  
  #fvec = [vec for item in fvec for vec in item]

  print '  done.'
  print '\tFeature type\t\t: %s' %fmode
  print '\t#samples\t\t: %d' %len(fvec)
  print '\t#dimensions\t\t: %d' %len(fvec[0])
  print '\telapsed time (sec)\t: %.3f' %(time.time()-ttime1)

  ### Output feature vector
  #pp['OutFeature'] = fmode
  #fname = './result/feature.' + data + '.' + fmode + '.init.vec'
  #if pp['F_Gsearch'] == 'OFF':
  #  if fmode == 'dens' or fmode == 'css' or fmode == 'hlac':
  #    np.savetxt(fname,fvec,fmt='%d')
  #  else:
  #    fname = fname+'.npy'
  #    np.save(fname,fvec)
  #else:
  #  pass

  return np.array(fvec)


def GetFeature_unit_fast(pp, cell, gdsii, ss, data):

  if pp['Feature'] == 'Density':
    fmode = 'dens'
    ddim  = pp['Dense_Grid']
    dnum  = int(ddim*ddim)
  elif pp['Feature'] == 'Diffr':
    fmode = 'diffr'
    popt  = [pp['Diffr_na'],pp['Diffr_lambda'],pp['Diffr_lx']]
  elif pp['Feature'] == 'CSS':
    fmode = 'css'
    n_dim = pp['CSS_Rin']
  elif pp['Feature'] == 'CCSA':
    fmode = 'ccsa'
    n_dim = pp['CCS_Rin']
  elif pp['Feature'] == 'HLAC':
    fmode   = 'hlac'
    corrwid = map(int,pp['HLAC_Corr'].split(','))
    pyramid = map(int,pp['HLAC_Pyramid'].split(','))
  elif pp['Feature'] == 'Gauss_HOG':
    ksize = pp['HOG_Gauss']
    fmode = 'hog'
    if ksize <= 0 or ksize %2 != 1:
      print ' Gauss kernel size error'
      quit()
  elif pp['Feature'] == 'Gauss_GLAC':
    ksize = pp['GLAC_Gauss']
    fmode = 'glac'
    if ksize <= 0 or ksize %2 != 1:
      print ' Gauss kernel size error'
      quit()
  else:
    print ' feature mode error.'
    quit()
  
  ### Check feature vecs (if feature vector exists, skip feature extraction)
  vecf = pp['VecsDir'] + '/feature_' + cell + '_' + data + '_vec.npy'
  if os.path.isfile(vecf) and pp['F_Gsearch'] != 'ON' and pp['StoreOption']=='ON':
    if fmode == 'dens' or fmode == 'css' or fmode == 'ccsa' or fmode == 'hlac':
      fvec = np.genfromtxt(vecf)
    else:
      fvec = np.load(vecf)

    print '\tFeature file exists'
    print '\tFeature type\t\t: %s' %fmode
    print '\t#samples\t\t: %d' %len(fvec)
    print '\t#dimensions\t\t: %d' %len(fvec[0])

    return fvec

  debug_function = 0

  ### Common parameters
  if data == 'train':
    layer    = pp['TrainLayer']
    dtype    = pp['TrainDtype']
    #ngds     = pp['TrainGDS']
    ngds     = cell + ".gds"
    vecf     = pp['TrainVecs']
  elif data == 'test':
    layer    = pp['TestLayer']
    dtype    = pp['TestDtype']
    #ngds     = pp['TestGDS']
    ngds     = cell + ".gds"
    vecf     = pp['TestVecs']
  crange   = pp['F_Wx']   ### calc. area
  proc_num = pp['ProcNum'] ### #of cores
  imggrid  = pp['ImgGrid']  ### image grid
  srafgrid = pp['GridSize'] ### sraf grid
  cp       = ss[:,0:2]   ### ss: gdsx, gdsy, weight

  ### Extract features
  if fmode == 'ccsa':
    ### Import polygons
    if data == 'train':
      ingds = gdsii.extract(cell)
    elif data == 'test':
      #ingds = gdsii.extract('IN_TOP_te')
      ingds = gdsii.extract(cell)
    getp  = ingds.get_polygons(True)
    getp  = getp[(layer,dtype)]
  
    ### Clip GDS
    bbox = ingds.get_bounding_box()
    #rounding the bbox
    bbox[0,0] = int(np.floor(bbox[0,0]))
    bbox[0,1] = int(np.floor(bbox[0,1]))
    bbox[1,0] = int(np.ceil(bbox[1,0]))
    bbox[1,1] = int(np.ceil(bbox[1,1]))
    ###bounding box of the target features
    m_llx = np.min([np.min(getp[i][:,0]) for i in xrange(len(getp))]) 
    m_urx = np.max([np.max(getp[i][:,0]) for i in xrange(len(getp))])
    m_lly = np.min([np.min(getp[i][:,1]) for i in xrange(len(getp))])
    m_ury = np.max([np.max(getp[i][:,1]) for i in xrange(len(getp))])
    grid_size = pp['GridSize']
    window_x = pp['Window_x']
    window_y = pp['Window_y']
    ### resize the window
    window_x = np.min([window_x, np.ceil((bbox[1,0] - bbox[0,0])/grid_size)*grid_size + 10*grid_size])
    window_y = np.min([window_y, np.ceil((bbox[1,1] - bbox[0,1])/grid_size)*grid_size + 10*grid_size])
    center_x = int(np.ceil((m_llx+m_urx)/2))
    center_y = int(np.ceil((m_lly+m_ury)/2))
    xmin = int(np.ceil((center_x - window_x / 2.0)/grid_size)*grid_size)
    ymin = int(np.ceil((center_y - window_y / 2.0)/grid_size)*grid_size)
    #xmin = int(np.ceil(center_x - window_x / 2))
    #ymin = int(np.ceil(center_y - window_y / 2))
    xmax = xmin + window_x
    ymax = ymin + window_y
    ### to microns
    xmin = float(xmin/1000.0)
    xmax = float(xmax/1000.0)
    ymin = float(ymin/1000.0)
    ymax = float(ymax/1000.0)
    if(debug_function): print 'GDS window: (%.4f, %.4f, %.4f, %.4f)' %(xmin,ymin,xmax,ymax)
    #xmin = np.min([np.min(getp[i][:,0]) for i in xrange(len(getp))]) / 1000.0 
    #xmax = np.max([np.max(getp[i][:,0]) for i in xrange(len(getp))]) / 1000.0
    #ymin = np.min([np.min(getp[i][:,1]) for i in xrange(len(getp))]) / 1000.0
    #ymax = np.max([np.max(getp[i][:,1]) for i in xrange(len(getp))]) / 1000.0
    
    ### create img array
    lx = len(np.arange(xmin,xmax,imggrid/1000.0))
    ly = len(np.arange(ymin,ymax,imggrid/1000.0))
    
    im   = Image.new('L',(lx,ly))
    draw = ImageDraw.Draw(im)
    lbx  = (xmin*1000)/imggrid
    lby  = (ymin*1000)/imggrid
    tdif = np.array([lbx,lby],dtype=int)

    tt_img = time.time()
    for pol in getp:
        test = np.array(pol/imggrid - tdif, dtype=int)
        test2 = []
    
        nn = test.shape[0]
        for j in xrange(nn):
            test2.append(test[j])
        test2 = np.array(test2,dtype=int)
    
        tp = [tuple([test2[j,0],test2[j,1]]) for j in xrange(test.shape[0])]
        draw.polygon(tp,outline=255,fill=255)
    
    ### check
    #im.save("test.png")
    imArray = np.asarray(im)
    del im
    print "time to generate bit map for image: %.2f" %(time.time()-tt_img)
  
    ### Feature extraction
    spoints = np.array([np.array([int(elem[0]-xmin*1000)/imggrid, int(elem[1]-ymin*1000)/imggrid]) for elem in ss])
    
    instep  = 20
    oustep  = 15
    prad   = int(crange*1000/imggrid)/2
    n_dim  = n_dim/imggrid
    x_range  = int((np.floor((prad-n_dim)/oustep))+1) 
    y_range  = 32
    #print "x_range: %d, y_range: %d" %(x_range, y_range)
    #quit()
    xstep    = np.r_[np.arange(n_dim,prad,oustep,dtype=int)]

    ### concentric circle sampling
    gr    = 3 # (lambda/(2NA(1+sigma)))^2
    ry,rx = np.ogrid[-gr:gr+1,-gr:gr+1]
    mask  = rx*rx+ry*ry <= gr*gr
    #print mask
    ### rotation pattern
    #rotation_lut = np.array([np.array([0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]),
    #               np.array([8,  7,  6,  5,  4,  3,  2,  1,  0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10,  9]),
    #               np.array([24, 25, 26, 27, 28, 29, 30, 31,  0,  1,  2,  3,  4,  5,  6,  7,  8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]),
    #               np.array([ 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10,  9,  8,  7,  6,  5,  4,  3,  2,  1])])
    rotation_lut = np.array([np.array([0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]),
                   np.array([16,  15,  14,  13,  12,  11, 10,  9,  8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18,  17]),
                   np.array([16, 17, 18, 19, 20, 21, 22, 23,  24,  25,  26,  27,  28,  29,  30,  31,  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]),
                   np.array([ 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10,  9,  8,  7,  6,  5,  4,  3,  2,  1])])
    ### feature vector to be extracted
    #pix_vecs = np.zeros((x_range, y_range),dtype=int)
    fvec = np.zeros((spoints.shape[0],(x_range-1)*y_range),dtype=int)
    tt_fvec = time.time()
    ### default
    #fvec   = CCSApy(imArray,spoints,x_range,y_range,xstep,gr,mask)
    #CCSAcpy(imArray,spoints,x_range,y_range,xstep,gr,mask,rotation_lut,fvec)
    #CCSAcpy(imArray,spoints,x_range,y_range,xstep,gr,mask,fvec)
    CCSAcpy(imArray,spoints,x_range,y_range,xstep,gr,fvec)
    print "time to extract features: %.2f" %(time.time()-tt_fvec)

  else:
    print ' Param. file error'
    quit()

  #fvec = np.array(fvec)
  ## Output feature vector
  pp['OutFeature'] = fmode
  fname = pp['VecsDir'] + '/feature_' + cell + '_' + data + '_vec'
  if pp['F_Gsearch'] == 'OFF' and pp['StoreOption']=='ON':
    if fmode == 'dens' or fmode == 'css' or fmode == 'ccsa' or fmode == 'hlac':
      np.savetxt(fname,fvec,fmt='%d')
    else:
      fname = fname+'.npy'
      np.save(fname,fvec)
  else:
    pass
  
  return fvec

### Concentric Circle with Area Sampling
def CCSApy(imArray,spoints,x_range,y_range,xstep,gr,mask):
  debug_function = 0
  fvecs = []
  index = 0
  for sp in spoints:
    index += 1
    #print '%d out of %d' %(index,len(spoints))
    ### pix vectors
    pix_vecs = np.zeros((x_range, y_range),dtype=int)
    
    if(debug_function):
      print "Sampling point: (%d, %d)" %(sp[0],sp[1])
      fname = 'test.gds'
      test_cell = gdspy.Cell('test')
      test_cell.add(gdspy.Polygon([(lbx-gr,lby-gr),(lbx-gr,lby+gr),(lbx+gr,lby+gr),(lbx+gr,lby-gr)],layer=0))
      ### check img
      #np.savetxt("imArray.txt", imArray, fmt='%d')
    shape = imArray.shape
    ###center sampling points
    x_index = 0
    for ii in xstep[1:]:
      x_index += 1
      if(debug_function): circle_list = []
      for jj in range(y_range):
        xdisp = int(np.floor( ii*np.cos(np.pi*(1-2.0*jj/float(y_range))) ))
        ydisp = int(np.floor( ii*np.sin(np.pi*(1-2.0*jj/float(y_range))) ))
        #lc_x = prad+xdisp
        #lc_y = prad+ydisp
        lc_x = sp[0]+xdisp
        lc_y = sp[1]+ydisp
        x_start = lc_x-gr
        x_end = lc_x+gr+1
        y_start = lc_y-gr
        y_end = lc_y+gr+1
        w_flag = True
        if x_start < 0: 
          x_start = 0
          w_flag = False
        if x_start > shape[1]: 
          x_start = shape[1]
          w_flag = False
        if x_end < 0: 
          x_end = 0
          w_flag = False
        if x_end > shape[1]: 
          x_end = shape[1]
          w_flag = False
        if y_start < 0: 
          y_start = 0
          w_flag = False
        if y_start > shape[0]: 
          y_start = shape[0]
          w_flag = False
        if y_end < 0: 
          y_end = 0
          w_flag = False
        if y_end > shape[0]: 
          y_end = shape[0]
          w_flag = False
        if True == w_flag:
          pix_vecs[x_index][jj] = int(np.sum(imArray[y_start:y_end,x_start:x_end][mask])/255) ### sampling center
        else:  
          pix_vecs[x_index][jj] = int(np.sum(imArray[y_start:y_end,x_start:x_end]/255)) ### sampling center
          
        #pix_vecs[x_index][jj] = int(np.sum(imArray[lc_y-gr:lc_y+gr+1,lc_x-gr:lc_x+gr+1][mask])/255) ### sampling center
        if(debug_function):
          lc_x += lbx
          lc_y += lby
          circle_list.append((lc_x,lc_y))
          if(pix_vecs[x_index][jj]): test_cell.add(gdspy.Polygon([(lc_x-gr,lc_y-gr),(lc_x-gr,lc_y+gr),(lc_x+gr,lc_y+gr),(lc_x+gr,lc_y-gr)],layer=10+jj))
      
      if(debug_function): 
        test_cell.add(gdspy.Polygon(circle_list, layer = 1))
    # end for

    if(debug_function):
      print pix_vecs
      for pg in getp: test_cell.add(gdspy.Polygon(pg*1000))
      gds_prt = gdspy.GdsPrint(fname,unit=1.0e-9,precision=1.0e-9)
      gds_prt.write_cell(test_cell)
      gds_prt.close()
      gdspy.Cell.cell_dict.clear()

    ###achieve independent feature matrix with column rotation
    ###trunk the sampling region to 4 or 2 parts
    divideCnt = 4
    divideSize = y_range/divideCnt
    flip_index = 0
    max_sum = 0
    ###scan the quadrant
    for ii in np.arange(0,y_range,divideSize,dtype=int):
      sum = np.sum(pix_vecs[:,ii:ii+divideSize+1])
      ###handle boundary conditions
      if (ii+divideSize == y_range): sum += np.sum(pix_vecs[:,0:1])
      if(sum > max_sum):
        max_sum = sum
        flip_index = ii

    #if(flip_index > 0): debug_function = 1
    if(debug_function):
      print pix_vecs
      print 'debug point'
      print np.arange(0,y_range,divideSize,dtype=int)
      print 'flip_index = %d' %(flip_index)

    ###flip the matrix column
    #pix_vecs = np.roll(pix_vecs, -flip_index)  
    if(flip_index == 0): 
      pass
    elif(flip_index == 1*divideSize):
      ##flip Y
      pix_vecs = np.roll(np.fliplr(np.roll(pix_vecs, -divideSize,axis=1)),divideSize+1,axis=1)
    elif(flip_index == 2*divideSize):
      ##flip X, Y
      pix_vecs = np.roll(np.fliplr(np.roll(np.fliplr(pix_vecs),-divideSize,axis=1)),divideSize,axis=1)
    elif(flip_index == 3*divideSize):
      ##flip X
      pix_vecs = np.fliplr(np.roll(pix_vecs,-1,axis=1))
    else: 
      pass

    if(debug_function):
      print pix_vecs
      #quit()
    
    ###matrix to vector
    fvecs.append((pix_vecs[1:,:]).flatten())
  return np.array(fvecs)

def GetFeature_unit(pp, cell, gdsii, ss, data):

  if pp['Feature'] == 'Density':
    fmode = 'dens'
    ddim  = pp['Dense_Grid']
    dnum  = int(ddim*ddim)
  elif pp['Feature'] == 'Diffr':
    fmode = 'diffr'
    popt  = [pp['Diffr_na'],pp['Diffr_lambda'],pp['Diffr_lx']]
  elif pp['Feature'] == 'CSS':
    fmode = 'css'
    n_dim = pp['CSS_Rin']
  elif pp['Feature'] == 'CCSA':
    fmode = 'ccsa'
    n_dim = pp['CCS_Rin']
  elif pp['Feature'] == 'HLAC':
    fmode   = 'hlac'
    corrwid = map(int,pp['HLAC_Corr'].split(','))
    pyramid = map(int,pp['HLAC_Pyramid'].split(','))
  elif pp['Feature'] == 'Gauss_HOG':
    ksize = pp['HOG_Gauss']
    fmode = 'hog'
    if ksize <= 0 or ksize %2 != 1:
      print ' Gauss kernel size error'
      quit()
  elif pp['Feature'] == 'Gauss_GLAC':
    ksize = pp['GLAC_Gauss']
    fmode = 'glac'
    if ksize <= 0 or ksize %2 != 1:
      print ' Gauss kernel size error'
      quit()
  else:
    print ' feature mode error.'
    quit()
  
  ### Check feature vecs (if feature vector exists, skip feature extraction)
  vecf = pp['VecsDir'] + '/feature_' + cell + '_' + data + '_vec.npy'
  if os.path.isfile(vecf) and pp['F_Gsearch'] != 'ON' and pp['StoreOption']=='ON':
    if fmode == 'dens' or fmode == 'css' or fmode == 'ccsa' or fmode == 'hlac':
      fvec = np.genfromtxt(vecf)
    else:
      fvec = np.load(vecf)

    print '\tFeature file exists'
    print '\tFeature type\t\t: %s' %fmode
    print '\t#samples\t\t: %d' %len(fvec)
    print '\t#dimensions\t\t: %d' %len(fvec[0])

    return fvec

  debug_function = 0

  ### Common parameters
  if data == 'train':
    layer    = pp['TrainLayer']
    dtype    = pp['TrainDtype']
    #ngds     = pp['TrainGDS']
    ngds     = cell + ".gds"
    vecf     = pp['TrainVecs']
  elif data == 'test':
    layer    = pp['TestLayer']
    dtype    = pp['TestDtype']
    #ngds     = pp['TestGDS']
    ngds     = cell + ".gds"
    vecf     = pp['TestVecs']
  crange   = pp['F_Wx']   ### calc. area
  proc_num = pp['ProcNum'] ### #of cores
  cp       = ss[:,0:2]   ### ss: gdsx, gdsy, weight

  ### Extract features
  if fmode == 'diffr' or fmode == 'dens' or fmode == 'css' or fmode == 'ccsa' or fmode == 'hlac' or fmode == 'hog' or fmode == 'glac':
    ### Import polygons
    if data == 'train':
      ingds = gdsii.extract(cell)
    elif data == 'test':
      #ingds = gdsii.extract('IN_TOP_te')
      ingds = gdsii.extract(cell)
    getp  = ingds.get_polygons(True)
    getp  = getp[(layer,dtype)]
  
    ### Set parameters
    centerp = np.array(cp,dtype=float)/1000.0
    tnum    = len(centerp)
    
    if(debug_function): 
      print 'Sampling point:'
      print centerp

    ### Clip GDS
    bbox = ingds.get_bounding_box()
    #rounding the bbox
    bbox[0,0] = int(np.floor(bbox[0,0]))
    bbox[0,1] = int(np.floor(bbox[0,1]))
    bbox[1,0] = int(np.ceil(bbox[1,0]))
    bbox[1,1] = int(np.ceil(bbox[1,1]))
    ###bounding box of the target features
    m_llx = np.min([np.min(getp[i][:,0]) for i in xrange(len(getp))]) 
    m_urx = np.max([np.max(getp[i][:,0]) for i in xrange(len(getp))])
    m_lly = np.min([np.min(getp[i][:,1]) for i in xrange(len(getp))])
    m_ury = np.max([np.max(getp[i][:,1]) for i in xrange(len(getp))])
    grid_size = pp['GridSize']
    window_x = pp['Window_x']
    window_y = pp['Window_y']
    ### resize the window
    window_x = np.min([window_x, np.ceil((bbox[1,0] - bbox[0,0])/grid_size)*grid_size + 10*grid_size])
    window_y = np.min([window_y, np.ceil((bbox[1,1] - bbox[0,1])/grid_size)*grid_size + 10*grid_size])
    center_x = int(np.ceil((m_llx+m_urx)/2))
    center_y = int(np.ceil((m_lly+m_ury)/2))
    xmin = int(np.ceil((center_x - window_x / 2.0)/grid_size)*grid_size)
    ymin = int(np.ceil((center_y - window_y / 2.0)/grid_size)*grid_size)
    #xmin = int(np.ceil(center_x - window_x / 2))
    #ymin = int(np.ceil(center_y - window_y / 2))
    xmax = xmin + window_x
    ymax = ymin + window_y
    ### to microns
    xmin = float(xmin/1000.0)
    xmax = float(xmax/1000.0)
    ymin = float(ymin/1000.0)
    ymax = float(ymax/1000.0)
    if(debug_function): print 'GDS window: (%.4f, %.4f, %.4f, %.4f)' %(xmin,ymin,xmax,ymax)
    #xmin = np.min([np.min(getp[i][:,0]) for i in xrange(len(getp))]) / 1000.0 
    #xmax = np.max([np.max(getp[i][:,0]) for i in xrange(len(getp))]) / 1000.0
    #ymin = np.min([np.min(getp[i][:,1]) for i in xrange(len(getp))]) / 1000.0
    #ymax = np.max([np.max(getp[i][:,1]) for i in xrange(len(getp))]) / 1000.0
    adist = ((xmax-xmin)+(ymax-ymin))/1.0
    anumx = 1
    anumy = 1
    if xmax-xmin > adist: 
      anumx = int(np.ceil((xmax-xmin)/adist))
    if ymax-ymin > adist:
      anumy = int(np.ceil((ymax-ymin)/adist))
    total_anum = int(anumx*anumy)
  
    cntp = []
    for i in xrange(anumy):
      for j in xrange(anumx):
        cntx = xmin+j*adist+adist/2.0
        cnty = ymin+i*adist+adist/2.0
        cntp.append([round(cntx,2),round(cnty,2)])
    
    if(debug_function):
      print 'Sparse grid'
      print cntp

    cntp   = np.array(cntp)
    mp     = Pool(proc_num)
    trange = adist+crange
    ###to microns
    getp = [pg/1000.0 for pg in getp]
    adlist = [[getp,cntp[i],trange,0] for i in xrange(len(cntp))]
    #adlist = [[getp,cntp[i],trange,ss[i,0]] for i in xrange(len(cntp))]
    getp_t = mp.map(MakeList,adlist)
    pdir = np.zeros(ss[:,0].shape, dtype=int)
    mlist  = CExtList(len(centerp),crange,adist,centerp,cntp,getp_t,pdir)
    #mlist  = ExtList(len(centerp),crange,adist,centerp,cntp,getp_t,ss[:,0])
    mplist = mp.map(MakeList,mlist)
    #mplist = []
    #for i in xrange(len(mlist)): 
    #  mplist.append(MakeList(mlist[i]))
    cgds   = mp.map(SliceGDS,mplist)
    #cgds = []
    #for i in xrange(len(mplist)): 
    #  cgds.append(SliceGDS(mplist[i]))

    ### Compute density vector
    if fmode == 'dens':
      mxlist = [[cgds[i],crange,ddim,crange/float(ddim),dnum,centerp[i]] for i in xrange(tnum)]
      fvec   = mp.map(GetArea,mxlist)
    ### Compute css
    elif fmode == 'css':
      mxlist = [[cgds[i],crange,n_dim,centerp[i]] for i in xrange(tnum)]
      fvec   = mp.map(GetCSS,mxlist)
    elif fmode == 'ccsa':
      mxlist = [[cgds[i],crange,n_dim,centerp[i]] for i in xrange(tnum)]
      fvec   = mp.map(GetCCSA,mxlist)
      #fvec = []
      #for i in xrange(tnum): fvec.append(GetCCSA(mxlist[i], getp))
    elif fmode == 'diffr':
      mxlist = [[cgds[i],crange,centerp[i],popt] for i in xrange(tnum)]
      fvec   = mp.map(GetDiffr,mxlist)
    elif fmode == 'hlac':
      mxlist = [[cgds[i],crange,centerp[i],corrwid,pyramid] for i in xrange(tnum)]
      fvec   = mp.map(GetHLAC,mxlist)
    elif fmode == 'hog':
      mxlist = [[cgds[i],crange,ksize,centerp[i]] for i in xrange(tnum)]
      fvec   = mp.map(GetHOG,mxlist)
    elif fmode == 'glac':
      mxlist = [[cgds[i],crange,ksize,centerp[i]] for i in xrange(tnum)]
      fvec   = mp.map(GetGLACv,mxlist)
    else:
      print ' Param. file error'
      quit()
  
    debug_function = 0
    if(debug_function):    
      centerp = AnalyzeCCSA(pp, fvec, ss, [xmin*1000.0,ymin*1000.0,xmax*1000.0,ymax*1000.0])
      tnum    = len(centerp)
      mlist  = CExtList(len(centerp),crange,adist,centerp,cntp,getp_t,pdir)
      mplist = mp.map(MakeList,mlist)
      cgds   = mp.map(SliceGDS,mplist)

      mxlist = [[cgds[i],crange,n_dim,centerp[i]] for i in xrange(tnum)]
      #fvec   = mp.map(GetCCSA,mxlist)
      fname = 'test.gds'
      test_cell = gdspy.Cell('test')
      for i in xrange(tnum): 
        pg_set = GetCCSA_DEBUG(mxlist[i],i+1)
        for pg in pg_set: test_cell.add(pg)
      for pg in getp: test_cell.add(gdspy.Polygon(pg*1000))
      gds_prt = gdspy.GdsPrint(fname,unit=1.0e-9,precision=1.0e-9)
      gds_prt.write_cell(test_cell)
      gds_prt.close()
      gdspy.Cell.cell_dict.clear()
      quit()

    mp.close()
  else:
    print ' Param. file error'
    quit()

  fvec = np.array(fvec)
  ## Output feature vector
  pp['OutFeature'] = fmode
  fname = pp['VecsDir'] + '/feature_' + cell + '_' + data + '_vec'
  if pp['F_Gsearch'] == 'OFF' and pp['StoreOption'] == 'ON':
    if fmode == 'dens' or fmode == 'css' or fmode == 'ccsa' or fmode == 'hlac':
      np.savetxt(fname,fvec,fmt='%d')
    else:
      fname = fname+'.npy'
      np.save(fname,fvec)
  else:
    pass
  
  return fvec

### analysze the symmetry of the feature data
def AnalyzeCCSA(pp, fvec, ss, window):
  print '###Analyze feature...'
  print 'Window'
  print window

  grid_size = pp['GridSize']
  xmin = window[0]
  ymin = window[1]
  xmax = window[2]
  ymax = window[3]
  xcenter = (xmin+xmax)/2.0
  ycenter = (ymin+ymax)/2.0
  
  x_row = int(np.ceil((xmax-xmin)/grid_size))
  y_col = int(np.ceil((ymax-ymin)/grid_size))
  
  centerp = []
  ###check each sampling point
  for pt in range(len(fvec)):
    if(np.sum(fvec[pt])==0): continue;
    centerp = []
    xpt = float(ss[pt][0])
    ypt = float(ss[pt][1])
    xpt_s = 2*xcenter - xpt
    ypt_s = 2*ycenter - ypt
    pt_ridx = int(np.floor((xpt-xmin)/grid_size))
    pt_cidx = int(np.floor((ypt-ymin)/grid_size)) 
    print '\nCheck %d sample point' %pt
    print '(%.1f, %.1f)' %(xpt, ypt)
    centerp.append(np.array([xpt/1000.0, ypt/1000.0]))
    print 'row: %d, col: %d, idx: %d' %(pt_ridx,pt_cidx,pt_ridx*y_col+pt_cidx)
    print ss[pt]
    #print fvec[pt]
    spt_ridx = int(np.floor((xpt_s-xmin)/grid_size))
    spt_cidx = int(np.floor((ypt_s-ymin)/grid_size))
    ## point:(xpt, ypt_s)
    sp_idx1 = pt_ridx*y_col + spt_cidx
    print '(%.1f, %.1f)' %(xpt, ypt_s)
    centerp.append(np.array([xpt/1000.0, ypt_s/1000.0]))
    print 'row: %d, col: %d, idx: %d' %(pt_ridx,spt_cidx,sp_idx1)
    print ss[sp_idx1]
    #print fvec[sp_idx1]
    ## point:(xpt_s, ypt)
    sp_idx2 = spt_ridx*y_col + pt_cidx
    print '(%.1f, %.1f)' %(xpt_s, ypt)
    centerp.append(np.array([xpt_s/1000.0, ypt/1000.0]))
    print 'row: %d, col: %d, idx: %d' %(spt_ridx,pt_cidx,sp_idx2)
    print ss[sp_idx2]
    #print fvec[sp_idx2]
    ## point:(xpt_s, ypt_s)
    sp_idx3 = spt_ridx*y_col + spt_cidx
    print '(%.1f, %.1f)' %(xpt_s, ypt_s)
    centerp.append(np.array([xpt_s/1000.0, ypt_s/1000.0]))
    print 'row: %d, col: %d, idx: %d' %(spt_ridx,spt_cidx,sp_idx3)
    print ss[sp_idx3]
    #print fvec[sp_idx3]
    if(np.sum(np.abs(fvec[pt]-fvec[sp_idx3]))>50 and np.sum(fvec[pt]) > 1000): 
      np.set_printoptions(threshold=np.nan)
      vec_width = len(fvec[pt])/32
      print fvec[pt].reshape(vec_width,32)
      print np.sum(fvec[pt])
      print fvec[sp_idx1].reshape(vec_width,32)
      print np.sum(fvec[sp_idx1])
      print fvec[sp_idx2].reshape(vec_width,32)
      print np.sum(fvec[sp_idx2])
      print fvec[sp_idx3].reshape(vec_width,32)
      print np.sum(fvec[sp_idx3])
      print 'jump point'
      print np.sum(np.abs(fvec[pt]-fvec[sp_idx3]))
      break
  
  return np.array(centerp)
    
def GetCCSA_DEBUG(xData, index=1):
  debug_function = 1

  sgds   = xData[0]
  crange = xData[1]
  dimn   = xData[2]
  pcenter= xData[3]
  pdir   = 0

  ### set calc. area
  spx = float(pcenter[0])-crange/2.0
  spy = float(pcenter[1])-crange/2.0
  epx = spx + crange
  epy = spy + crange

  ### create img array
  igrid = int(crange*1000)
  im    = Image.new('L',(igrid,igrid))
  draw  = ImageDraw.Draw(im)
  lbx   = spx*1000
  lby   = spy*1000
  tdif  = np.array([lbx,lby],dtype=int)

  for pol in sgds.polygons:
    test  = np.array(pol*1000 - tdif, dtype=int)
    test2 = []

    ### grid2pix
    nn = test.shape[0]
    for j in xrange(nn):
      if j == 0 or j == nn-1:
        test2.append(test[j])
        continue
      if test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] > test[j,1] and test[j,0] < test[j+1,0]:
        test2.append(test[j])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] > test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] < test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append([test[j,0],test[j,1]-1])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      else:
        test2.append(test[j])
    test2 = np.array(test2,dtype=int)

    ### pix_cord2poly
    tp   = [tuple([test2[j,0],test2[j,1]]) for j in xrange(test.shape[0])]

    ### draw polygon
    #if(debug_function): print tp
    draw.polygon(tp,outline=255,fill=255)

  ### transpose, do not transpose because after transposal, the imArray origin is top left
  #im = im.transpose(Image.FLIP_TOP_BOTTOM)

  ### resize
  #rsize = igrid/5
  #im    = im.resize((rsize,rsize))

  ### check img
  #tname = 'test_'+str(pcenter[0])+'_'+str(pcenter[1])+'.png'
  #im.save(tname)

  ### img2array
  imArray = np.asarray(im)
  del im

  ### calc. dim.
  instep  = 20
  oustep  = 10
  prad    = int(imArray.shape[0])/2
  xdim    = int((np.floor((prad-dimn)/oustep))*16+1)
  ### 2D matrix dimension
  x_range  = int((np.floor((prad-dimn)/oustep))+1) 
  y_range  = 32

  ### pix vectors
  pix_vecs = np.zeros((x_range, y_range),dtype=int)
  #xstep    = np.r_[np.arange(0,dimn,instep,dtype=int),np.arange(dimn,prad,oustep,dtype=int)]
  xstep    = np.r_[np.arange(dimn,prad,oustep,dtype=int)]
  
  ### concentric circle sampling
  gr    = 3 # (lambda/(2NA(1+sigma)))^2
  ry,rx = np.ogrid[-gr:gr+1,-gr:gr+1]
  mask  = rx*rx+ry*ry <= gr*gr

  #if(len(sgds.polygons) >= 5): debug_function = 1

  test_cell = []
  if(debug_function):
    print "Sampling point: (%.1f, %.1f)" %(pcenter[0]*1000.0,pcenter[1]*1000.0)

  ###center sampling points
  for ii in range(y_range):
    pix_vecs[0][ii] = int(np.sum(imArray[prad-gr:prad+gr+1,prad-gr:prad+gr+1][mask])/255)
  x_index = 0
  for ii in xstep[1:]:
    x_index += 1
    if(debug_function): circle_list = []
    for jj in range(y_range):
      xdisp = int(np.floor( ii*np.cos(np.pi*(1-2.0*jj/float(y_range))) ))
      ydisp = int(np.floor( ii*np.sin(np.pi*(1-2.0*jj/float(y_range))) ))
      lc_x = prad+xdisp
      lc_y = prad+ydisp
      pix_vecs[x_index][jj] = int(np.sum(imArray[lc_y-gr:lc_y+gr+1,lc_x-gr:lc_x+gr+1][mask])/255) ### sampling center
      if(debug_function):
        lc_x += lbx
        lc_y += lby
        circle_list.append((lc_x,lc_y))
        if(pix_vecs[x_index][jj]): test_cell.append(gdspy.Polygon([(lc_x-gr,lc_y-gr),(lc_x-gr,lc_y+gr),(lc_x+gr,lc_y+gr),(lc_x+gr,lc_y-gr)],layer=50*index+jj+1))
    
    if(debug_function): 
      test_cell.append(gdspy.Polygon(circle_list, layer = 50*index))
  # end for

  #if(debug_function):
  #  print pix_vecs.shape
  #  print pix_vecs

  ###achieve independent feature matrix with column rotation
  ###trunk the sampling region to 4 or 2 parts
  divideCnt = 4
  divideSize = y_range/divideCnt
  flip_index = 0
  max_sum = 0
  ###scan the quadrant
  for ii in np.arange(0,y_range,divideSize,dtype=int):
    sum = np.sum(pix_vecs[:,ii:ii+divideSize+1])
    ###handle boundary conditions
    if (ii+divideSize == y_range): sum += np.sum(pix_vecs[:,0:1])
    if(sum > max_sum):
      max_sum = sum
      flip_index = ii

  #if(flip_index > 0): debug_function = 1
  if(debug_function):
    print 'debug point'
    print np.arange(0,y_range,divideSize,dtype=int)
    print 'flip_index = %d' %(flip_index)

  ###flip the matrix column
  #pix_vecs = np.roll(pix_vecs, -flip_index)  
  if(flip_index == 0): 
    pass
  elif(flip_index == 1*divideSize):
    ##flip Y
    pix_vecs = np.roll(np.fliplr(np.roll(pix_vecs, -divideSize, axis=1)),divideSize+1,axis=1)
  elif(flip_index == 2*divideSize):
    ##flip X, Y
    pix_vecs = np.roll(np.fliplr(np.roll(np.fliplr(pix_vecs), -divideSize, axis=1)),divideSize, axis=1)
  elif(flip_index == 3*divideSize):
    ##flip X
    pix_vecs = np.fliplr(np.roll(pix_vecs,-1,axis=1))
  else: 
    pass

  if(debug_function):
    print pix_vecs.shape
    print pix_vecs
    #quit()
  
  ###matrix to vector
  #return (pix_vecs[1:,:]).flatten()
  return test_cell

### Concentric Circle with Area Sampling
def GetCCSA(xData, getp=''):
  debug_function = 0

  sgds   = xData[0]
  crange = xData[1]
  dimn   = xData[2]
  pcenter= xData[3]
  pdir   = 0

  ### set calc. area
  spx = float(pcenter[0])-crange/2.0
  spy = float(pcenter[1])-crange/2.0
  epx = spx + crange
  epy = spy + crange

  ### create img array
  igrid = int(crange*1000)
  im    = Image.new('L',(igrid,igrid))
  draw  = ImageDraw.Draw(im)
  lbx   = spx*1000
  lby   = spy*1000
  tdif  = np.array([lbx,lby],dtype=int)

  for pol in sgds.polygons:
    test  = np.array(pol*1000 - tdif, dtype=int)
    test2 = []

    ### grid2pix
    nn = test.shape[0]
    for j in xrange(nn):
      if j == 0 or j == nn-1:
        test2.append(test[j])
        continue
      if test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] > test[j,1] and test[j,0] < test[j+1,0]:
        test2.append(test[j])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] > test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] < test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append([test[j,0],test[j,1]-1])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      else:
        test2.append(test[j])
    test2 = np.array(test2,dtype=int)

    ### pix_cord2poly
    tp   = [tuple([test2[j,0],test2[j,1]]) for j in xrange(test.shape[0])]

    ### draw polygon
    #if(debug_function): print tp
    draw.polygon(tp,outline=255,fill=255)

  ### transpose, do not transpose because after transposal, the imArray origin is top left
  #im = im.transpose(Image.FLIP_TOP_BOTTOM)

  ### resize
  #rsize = igrid/5
  #im    = im.resize((rsize,rsize))

  ### check img
  #tname = 'test_'+str(pcenter[0])+'_'+str(pcenter[1])+'.png'
  #im.save(tname)

  ### img2array
  imArray = np.asarray(im)
  del im

  ### calc. dim.
  instep  = 20
  oustep  = 10
  prad    = int(imArray.shape[0])/2
  xdim    = int((np.floor((prad-dimn)/oustep))*16+1)
  ### 2D matrix dimension
  x_range  = int((np.floor((prad-dimn)/oustep))+1) 
  y_range  = 32

  ### pix vectors
  pix_vecs = np.zeros((x_range, y_range),dtype=int)
  #xstep    = np.r_[np.arange(0,dimn,instep,dtype=int),np.arange(dimn,prad,oustep,dtype=int)]
  xstep    = np.r_[np.arange(dimn,prad,oustep,dtype=int)]
  
  ### concentric circle sampling
  gr    = 3 # (lambda/(2NA(1+sigma)))^2
  ry,rx = np.ogrid[-gr:gr+1,-gr:gr+1]
  mask  = rx*rx+ry*ry <= gr*gr

  #if(len(sgds.polygons) >= 5): debug_function = 1

  if(debug_function):
    print "Sampling point: (%.1f, %.1f)" %(pcenter[0]*1000.0,pcenter[1]*1000.0)
    fname = 'test.gds'
    test_cell = gdspy.Cell('test')
    for pg in sgds.polygons: test_cell.add(gdspy.Polygon(pg*1000.0))
    text = gdspy.Text(text='spoint',size=20, position=pcenter*1000.0, layer=0, datatype = 1)
    test_cell.add(text)
    test_cell.add(gdspy.Polygon([(lbx-gr,lby-gr),(lbx-gr,lby+gr),(lbx+gr,lby+gr),(lbx+gr,lby-gr)],layer=0))
    ### check img
    #np.savetxt("imArray.txt", imArray, fmt='%d')

  ###center sampling points
  for ii in range(y_range):
    pix_vecs[0][ii] = int(np.sum(imArray[prad-gr:prad+gr+1,prad-gr:prad+gr+1][mask])/255)
  x_index = 0
  for ii in xstep[1:]:
    x_index += 1
    if(debug_function): circle_list = []
    for jj in range(y_range):
      xdisp = int(np.floor( ii*np.cos(np.pi*(1-2.0*jj/float(y_range))) ))
      ydisp = int(np.floor( ii*np.sin(np.pi*(1-2.0*jj/float(y_range))) ))
      lc_x = prad+xdisp
      lc_y = prad+ydisp
      pix_vecs[x_index][jj] = int(np.sum(imArray[lc_y-gr:lc_y+gr+1,lc_x-gr:lc_x+gr+1][mask])/255) ### sampling center
      if(debug_function):
        lc_x += lbx
        lc_y += lby
        circle_list.append((lc_x,lc_y))
        if(pix_vecs[x_index][jj]): test_cell.add(gdspy.Polygon([(lc_x-gr,lc_y-gr),(lc_x-gr,lc_y+gr),(lc_x+gr,lc_y+gr),(lc_x+gr,lc_y-gr)],layer=10+jj))
    
    if(debug_function): 
      test_cell.add(gdspy.Polygon(circle_list, layer = 1))
  # end for

  if(debug_function):
    print pix_vecs
    for pg in getp: test_cell.add(gdspy.Polygon(pg*1000))
    gds_prt = gdspy.GdsPrint(fname,unit=1.0e-9,precision=1.0e-9)
    gds_prt.write_cell(test_cell)
    gds_prt.close()
    gdspy.Cell.cell_dict.clear()

  ###achieve independent feature matrix with column rotation
  ###trunk the sampling region to 4 or 2 parts
  divideCnt = 4
  divideSize = y_range/divideCnt
  flip_index = 0
  max_sum = 0
  ###scan the quadrant
  for ii in np.arange(0,y_range,divideSize,dtype=int):
    sum = np.sum(pix_vecs[:,ii:ii+divideSize+1])
    ###handle boundary conditions
    if (ii+divideSize == y_range): sum += np.sum(pix_vecs[:,0:1])
    if(sum > max_sum):
      max_sum = sum
      flip_index = ii

  #if(flip_index > 0): debug_function = 1
  if(debug_function):
    print pix_vecs
    print 'debug point'
    print np.arange(0,y_range,divideSize,dtype=int)
    print 'flip_index = %d' %(flip_index)

  ###flip the matrix column
  #pix_vecs = np.roll(pix_vecs, -flip_index)  
  if(flip_index == 0): 
    pass
  elif(flip_index == 1*divideSize):
    ##flip Y
    pix_vecs = np.roll(np.fliplr(np.roll(pix_vecs, -divideSize,axis=1)),divideSize+1,axis=1)
  elif(flip_index == 2*divideSize):
    ##flip X, Y
    pix_vecs = np.roll(np.fliplr(np.roll(np.fliplr(pix_vecs),-divideSize,axis=1)),divideSize,axis=1)
  elif(flip_index == 3*divideSize):
    ##flip X
    pix_vecs = np.fliplr(np.roll(pix_vecs,-1,axis=1))
  else: 
    pass

  if(debug_function):
    print pix_vecs
    #quit()
  
  ###matrix to vector
  return (pix_vecs[1:,:]).flatten() 
  #return ((pix_vecs[1:,:]).flatten() != 0)*1
  #return (pix_vecs[1:,:])

### Concentric Circle with Area Sampling
def GetCCSA_16bit_flip(xData, getp=''):
  debug_function = 0

  sgds   = xData[0]
  crange = xData[1]
  dimn   = xData[2]
  pcenter= xData[3]
  pdir   = 0

  ### set calc. area
  spx = float(pcenter[0])-crange/2.0
  spy = float(pcenter[1])-crange/2.0
  epx = spx + crange
  epy = spy + crange

  ### create img array
  igrid = int(crange*1000)
  im    = Image.new('L',(igrid,igrid))
  draw  = ImageDraw.Draw(im)
  lbx   = spx*1000
  lby   = spy*1000
  tdif  = np.array([lbx,lby],dtype=int)

  for pol in sgds.polygons:
    test  = np.array(pol*1000 - tdif, dtype=int)
    test2 = []

    ### grid2pix
    nn = test.shape[0]
    for j in xrange(nn):
      if j == 0 or j == nn-1:
        test2.append(test[j])
        continue
      if test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] > test[j,1] and test[j,0] < test[j+1,0]:
        test2.append(test[j])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] > test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] < test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append([test[j,0],test[j,1]-1])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      else:
        test2.append(test[j])
    test2 = np.array(test2,dtype=int)

    ### pix_cord2poly
    tp   = [tuple([test2[j,0],test2[j,1]]) for j in xrange(test.shape[0])]

    ### draw polygon
    #if(debug_function): print tp
    draw.polygon(tp,fill=255)

  ### transpose, do not transpose because after transposal, the imArray origin is top left
  #im = im.transpose(Image.FLIP_TOP_BOTTOM)

  ### resize
  #rsize = igrid/5
  #im    = im.resize((rsize,rsize))

  ### check img
  #tname = 'test_'+str(pcenter[0])+'_'+str(pcenter[1])+'.png'
  #im.save(tname)

  ### img2array
  imArray = np.asarray(im)
  del im

  ### calc. dim.
  instep  = 15
  oustep  = 15
  prad    = int(imArray.shape[0])/2
  xdim    = int((dimn/instep+np.floor((prad-dimn)/oustep))*16+1)
  ### 2D matrix dimension
  x_range  = int((dimn/instep+np.floor((prad-dimn)/oustep))+1) 
  y_range  = 16

  ### pix vectors
  pix_vecs = np.zeros((x_range, y_range),dtype=int)
  xstep    = np.r_[np.arange(0,dimn,instep,dtype=int),np.arange(dimn,prad,oustep,dtype=int)]
  
  ### concentric circle sampling
  gr    = 3 # (lambda/(2NA(1+sigma)))^2
  ry,rx = np.ogrid[-gr:gr+1,-gr:gr+1]
  mask  = rx*rx+ry*ry <= gr*gr

  #if(len(sgds.polygons) >= 10): debug_function = 1

  if(debug_function):
    print "Sampling point: (%.1f, %.1f)" %(pcenter[0]*1000.0,pcenter[1]*1000.0)
    fname = 'test.gds'
    test_cell = gdspy.Cell('test')
    for pg in sgds.polygons: test_cell.add(gdspy.Polygon(pg*1000.0))
    text = gdspy.Text(text='spoint',size=20, position=pcenter*1000.0, layer=0, datatype = 1)
    test_cell.add(text)
    test_cell.add(gdspy.Polygon([(lbx-gr,lby-gr),(lbx-gr,lby+gr),(lbx+gr,lby+gr),(lbx+gr,lby-gr)],layer=0))
    ### check img
    #np.savetxt("imArray.txt", imArray, fmt='%d')

  ###center sampling points
  for ii in range(y_range):
    pix_vecs[0][ii] = int(np.sum(imArray[prad-gr:prad+gr+1,prad-gr:prad+gr+1][mask])/255)
  x_index = 0
  for ii in xstep[1:]:
    x_index += 1
    xdisp1 = int(np.floor( ii*np.cos(22.5*np.pi/180.0) ))
    ydisp1 = int(np.floor( ii*np.sin(22.5*np.pi/180.0) ))
    xdisp2 = int(np.floor( ii*np.cos(45.0*np.pi/180.0) ))
    ydisp2 = int(np.floor( ii*np.sin(45.0*np.pi/180.0) ))
    xdisp3 = int(np.floor( ii*np.cos(67.5*np.pi/180.0) ))
    ydisp3 = int(np.floor( ii*np.sin(67.5*np.pi/180.0) ))
    lc_x = prad-ii
    lc_y = prad
    #pix_vecs[x_index][0] = int(np.sum(imArray[lc_x-gr:lc_x+gr+1,lc_y-gr:lc_y+gr+1][mask])/255) ### Left center
    pix_vecs[x_index][0] = int(np.sum(imArray[lc_y-gr:lc_y+gr+1,lc_x-gr:lc_x+gr+1][mask])/255) ### Left center
    if(debug_function):
      lc_x += lbx
      lc_y += lby
      if(pix_vecs[x_index][0]): test_cell.add(gdspy.Polygon([(lc_x-gr,lc_y-gr),(lc_x-gr,lc_y+gr),(lc_x+gr,lc_y+gr),(lc_x+gr,lc_y-gr)],layer=10))
    lc_lt_x = prad-xdisp1
    lc_lt_y = prad+ydisp1
    #pix_vecs[x_index][1] = int(np.sum(imArray[lc_lt_x-gr:lc_lt_x+gr+1,lc_lt_y-gr:lc_lt_y+gr+1][mask])/255) ### Left bottom-Left top
    pix_vecs[x_index][1] = int(np.sum(imArray[lc_lt_y-gr:lc_lt_y+gr+1,lc_lt_x-gr:lc_lt_x+gr+1][mask])/255) ### Left bottom-Left top
    if(debug_function):
      lc_lt_x += lbx
      lc_lt_y += lby
      if(pix_vecs[x_index][1]): test_cell.add(gdspy.Polygon([(lc_lt_x-gr,lc_lt_y-gr),(lc_lt_x-gr,lc_lt_y+gr),(lc_lt_x+gr,lc_lt_y+gr),(lc_lt_x+gr,lc_lt_y-gr)],layer=11))
    lt_x = prad-xdisp2
    lt_y = prad+ydisp2
    #pix_vecs[x_index][2]   = int(np.sum(imArray[lt_x-gr:lt_x+gr+1,lt_y-gr:lt_y+gr+1][mask])/255) ### Left top
    pix_vecs[x_index][2]   = int(np.sum(imArray[lt_y-gr:lt_y+gr+1,lt_x-gr:lt_x+gr+1][mask])/255) ### Left top
    if(debug_function):
      lt_x += lbx
      lt_y += lby
      if(pix_vecs[x_index][2]): test_cell.add(gdspy.Polygon([(lt_x-gr,lt_y-gr),(lt_x-gr,lt_y+gr),(lt_x+gr,lt_y+gr),(lt_x+gr,lt_y-gr)],layer=12))
    lt_ct_x = prad-xdisp3
    lt_ct_y = prad+ydisp3
    #pix_vecs[x_index][3] = int(np.sum(imArray[lt_ct_x-gr:lt_ct_x+gr+1,lt_ct_y-gr:lt_ct_y+gr+1][mask])/255) ### Left top-Center top
    pix_vecs[x_index][3] = int(np.sum(imArray[lt_ct_y-gr:lt_ct_y+gr+1,lt_ct_x-gr:lt_ct_x+gr+1][mask])/255) ### Left top-Center top
    if(debug_function):
      lt_ct_x += lbx
      lt_ct_y += lby
      if(pix_vecs[x_index][3]): test_cell.add(gdspy.Polygon([(lt_ct_x-gr,lt_ct_y-gr),(lt_ct_x-gr,lt_ct_y+gr),(lt_ct_x+gr,lt_ct_y+gr),(lt_ct_x+gr,lt_ct_y-gr)],layer=13))
    ct_x = prad
    ct_y = prad+ii
    #pix_vecs[x_index][4] = int(np.sum(imArray[ct_x-gr:ct_x+gr+1,ct_y-gr:ct_y+gr+1][mask])/255) ### Center top
    pix_vecs[x_index][4] = int(np.sum(imArray[ct_y-gr:ct_y+gr+1,ct_x-gr:ct_x+gr+1][mask])/255) ### Center top
    if(debug_function):
      ct_x += lbx
      ct_y += lby
      if(pix_vecs[x_index][4]): test_cell.add(gdspy.Polygon([(ct_x-gr,ct_y-gr),(ct_x-gr,ct_y+gr),(ct_x+gr,ct_y+gr),(ct_x+gr,ct_y-gr)],layer=14))
    ct_rt_x = prad+xdisp3
    ct_rt_y = prad+ydisp3
    #pix_vecs[x_index][5] = int(np.sum(imArray[ct_rt_x-gr:ct_rt_x+gr+1,ct_rt_y-gr:ct_rt_y+gr+1][mask])/255) ### Center top-Right top
    pix_vecs[x_index][5] = int(np.sum(imArray[ct_rt_y-gr:ct_rt_y+gr+1,ct_rt_x-gr:ct_rt_x+gr+1][mask])/255) ### Center top-Right top
    if(debug_function):
      ct_rt_x += lbx
      ct_rt_y += lby
      if(pix_vecs[x_index][5]): test_cell.add(gdspy.Polygon([(ct_rt_x-gr,ct_rt_y-gr),(ct_rt_x-gr,ct_rt_y+gr),(ct_rt_x+gr,ct_rt_y+gr),(ct_rt_x+gr,ct_rt_y-gr)],layer=15))
    rt_x = prad+xdisp2
    rt_y = prad+ydisp2
    #pix_vecs[x_index][6] = int(np.sum(imArray[rt_x-gr:rt_x+gr+1,rt_y-gr:rt_y+gr+1][mask])/255) ### Right top
    pix_vecs[x_index][6] = int(np.sum(imArray[rt_y-gr:rt_y+gr+1,rt_x-gr:rt_x+gr+1][mask])/255) ### Right top
    if(debug_function):
      rt_x += lbx
      rt_y += lby
      if(pix_vecs[x_index][6]): test_cell.add(gdspy.Polygon([(rt_x-gr,rt_y-gr),(rt_x-gr,rt_y+gr),(rt_x+gr,rt_y+gr),(rt_x+gr,rt_y-gr)],layer=16))
    rt_rc_x = prad+xdisp1
    rt_rc_y = prad+ydisp1
    #pix_vecs[x_index][7] = int(np.sum(imArray[rt_rc_x-gr:rt_rc_x+gr+1,rt_rc_y-gr:rt_rc_y+gr+1][mask])/255) ### Right top-Right center
    pix_vecs[x_index][7] = int(np.sum(imArray[rt_rc_y-gr:rt_rc_y+gr+1,rt_rc_x-gr:rt_rc_x+gr+1][mask])/255) ### Right top-Right center
    if(debug_function):
      rt_rc_x += lbx
      rt_rc_y += lby
      if(pix_vecs[x_index][7]): test_cell.add(gdspy.Polygon([(rt_rc_x-gr,rt_rc_y-gr),(rt_rc_x-gr,rt_rc_y+gr),(rt_rc_x+gr,rt_rc_y+gr),(rt_rc_x+gr,rt_rc_y-gr)],layer=17))
    rc_x = prad+ii
    rc_y = prad
    #pix_vecs[x_index][8] = int(np.sum(imArray[rc_x-gr:rc_x+gr+1,rc_y-gr:rc_y+gr+1][mask])/255) ### Right center
    pix_vecs[x_index][8] = int(np.sum(imArray[rc_y-gr:rc_y+gr+1,rc_x-gr:rc_x+gr+1][mask])/255) ### Right center
    if(debug_function):
      rc_x += lbx
      rc_y += lby
      if(pix_vecs[x_index][8]): test_cell.add(gdspy.Polygon([(rc_x-gr,rc_y-gr),(rc_x-gr,rc_y+gr),(rc_x+gr,rc_y+gr),(rc_x+gr,rc_y-gr)],layer=18))
    rc_rb_x=prad+xdisp1
    rc_rb_y=prad-ydisp1
    #pix_vecs[x_index][9] = int(np.sum(imArray[rc_rb_x-gr:rc_rb_x+gr+1,rc_rb_y-gr:rc_rb_y+gr+1][mask])/255) ### Right center-Right bottom
    pix_vecs[x_index][9] = int(np.sum(imArray[rc_rb_y-gr:rc_rb_y+gr+1,rc_rb_x-gr:rc_rb_x+gr+1][mask])/255) ### Right center-Right bottom
    if(debug_function):
      rc_rb_x += lbx
      rc_rb_y += lby
      if(pix_vecs[x_index][9]): test_cell.add(gdspy.Polygon([(rc_rb_x-gr,rc_rb_y-gr),(rc_rb_x-gr,rc_rb_y+gr),(rc_rb_x+gr,rc_rb_y+gr),(rc_rb_x+gr,rc_rb_y-gr)],layer=19))
    rb_x = prad+xdisp2
    rb_y = prad-ydisp2
    #pix_vecs[x_index][10] = int(np.sum(imArray[rb_x-gr:rb_x+gr+1,rb_y-gr:rb_y+gr+1][mask])/255) ### Right bottom
    pix_vecs[x_index][10] = int(np.sum(imArray[rb_y-gr:rb_y+gr+1,rb_x-gr:rb_x+gr+1][mask])/255) ### Right bottom
    if(debug_function):
      rb_x += lbx
      rb_y += lby
      if(pix_vecs[x_index][10]): test_cell.add(gdspy.Polygon([(rb_x-gr,rb_y-gr),(rb_x-gr,rb_y+gr),(rb_x+gr,rb_y+gr),(rb_x+gr,rb_y-gr)],layer=20))
    rb_cb_x = prad+xdisp3
    rb_cb_y = prad-ydisp3
    #pix_vecs[x_index][11] = int(np.sum(imArray[rb_cb_x-gr:rb_cb_x+gr+1,rb_cb_y-gr:rb_cb_y+gr+1][mask])/255) ### Right bottom-Center bottom
    pix_vecs[x_index][11] = int(np.sum(imArray[rb_cb_y-gr:rb_cb_y+gr+1,rb_cb_x-gr:rb_cb_x+gr+1][mask])/255) ### Right bottom-Center bottom
    if(debug_function):
      rb_cb_x += lbx
      rb_cb_y += lby
      if(pix_vecs[x_index][11]): test_cell.add(gdspy.Polygon([(rb_cb_x-gr,rb_cb_y-gr),(rb_cb_x-gr,rb_cb_y+gr),(rb_cb_x+gr,rb_cb_y+gr),(rb_cb_x+gr,rb_cb_y-gr)],layer=21))
    cb_x = prad
    cb_y = prad-ii
    #pix_vecs[x_index][12] = int(np.sum(imArray[cb_x-gr:cb_x+gr+1,cb_y-gr:cb_y+gr+1][mask])/255) ### Right bottom-Center bottom
    pix_vecs[x_index][12] = int(np.sum(imArray[cb_y-gr:cb_y+gr+1,cb_x-gr:cb_x+gr+1][mask])/255) ### Right bottom-Center bottom
    if(debug_function):
      cb_x += lbx
      cb_y += lby
      if(pix_vecs[x_index][12]): test_cell.add(gdspy.Polygon([(cb_x-gr,cb_y-gr),(cb_x-gr,cb_y+gr),(cb_x+gr,cb_y+gr),(cb_x+gr,cb_y-gr)],layer=22))
    cb_lb_x = prad-xdisp3
    cb_lb_y = prad-ydisp3
    #pix_vecs[x_index][13] = int(np.sum(imArray[cb_lb_x-gr:cb_lb_x+gr+1,cb_lb_y-gr:cb_lb_y+gr+1][mask])/255) ### Center bottom-Left bottom
    pix_vecs[x_index][13] = int(np.sum(imArray[cb_lb_y-gr:cb_lb_y+gr+1,cb_lb_x-gr:cb_lb_x+gr+1][mask])/255) ### Center bottom-Left bottom
    if(debug_function):
      cb_lb_x += lbx
      cb_lb_y += lby
      if(pix_vecs[x_index][13]): test_cell.add(gdspy.Polygon([(cb_lb_x-gr,cb_lb_y-gr),(cb_lb_x-gr,cb_lb_y+gr),(cb_lb_x+gr,cb_lb_y+gr),(cb_lb_x+gr,cb_lb_y-gr)],layer=23))
    lb_x = prad-xdisp2
    lb_y = prad-ydisp2
    #pix_vecs[x_index][14] = int(np.sum(imArray[lb_x-gr:lb_x+gr+1,lb_y-gr:lb_y+gr+1][mask])/255) ### Left bottom
    pix_vecs[x_index][14] = int(np.sum(imArray[lb_y-gr:lb_y+gr+1,lb_x-gr:lb_x+gr+1][mask])/255) ### Left bottom
    if(debug_function):
      lb_x += lbx
      lb_y += lby
      if(pix_vecs[x_index][14]): test_cell.add(gdspy.Polygon([(lb_x-gr,lb_y-gr),(lb_x-gr,lb_y+gr),(lb_x+gr,lb_y+gr),(lb_x+gr,lb_y+gr)],layer=24))

    lb_lc_x = prad-xdisp1
    lb_lc_y = prad-ydisp1
    #pix_vecs[x_index][15] = int(np.sum(imArray[lb_lc_x-gr:lb_lc_x+gr+1,lb_lc_y-gr:lb_lc_y+gr+1][mask])/255) ### Left bottom-Left center
    pix_vecs[x_index][15] = int(np.sum(imArray[lb_lc_y-gr:lb_lc_y+gr+1,lb_lc_x-gr:lb_lc_x+gr+1][mask])/255) ### Left bottom-Left center
    if(debug_function):
      lb_lc_x += lbx
      lb_lc_y += lby
      if(pix_vecs[x_index][15]): test_cell.add(gdspy.Polygon([(lb_lc_x-gr,lb_lc_y-gr),(lb_lc_x-gr,lb_lc_y+gr),(lb_lc_x+gr,lb_lc_y+gr),(lb_lc_x+gr,lb_lc_y+gr)],layer=25))
    
    if(debug_function): 
      bbx = 0
      bby = 0
      test_cell.add(gdspy.Polygon([(lt_x+bbx,lt_y+bby),(lt_ct_x+bbx,lt_ct_y+bby),(ct_x+bbx,ct_y+bby),(ct_rt_x+bbx,ct_rt_y+bby),(rt_x+bbx,rt_y+bby),(rt_rc_x+bbx,rt_rc_y+bby),(rc_x+bbx,rc_y+bby),(rc_rb_x+bbx,rc_rb_y+bby),(rb_x+bbx,rb_y+bby),(rb_cb_x+bbx,rb_cb_y+bby),(cb_x+bbx,cb_y+bby),(cb_lb_x+bbx,cb_lb_y+bby),(lb_x+bbx,lb_y+bby),(lb_lc_x+bbx,lb_lc_y+bby),(lc_x+bbx,lc_y+bby),(lc_lt_x+bbx,lc_lt_y+bby)], layer = 1))
  # end for

  if(debug_function):
    print pix_vecs
    for pg in getp: test_cell.add(gdspy.Polygon(pg*1000))
    gds_prt = gdspy.GdsPrint(fname,unit=1.0e-9,precision=1.0e-9)
    gds_prt.write_cell(test_cell)
    gds_prt.close()
    gdspy.Cell.cell_dict.clear()

  ###achieve independent feature matrix with column rotation
  ###trunk the sampling region to 4 or 2 parts
  divideCnt = 4
  divideSize = y_range/divideCnt
  flip_index = 0
  max_sum = 0
  ###scan the quadrant
  for ii in np.arange(0,y_range,divideSize,dtype=int):
    sum = np.sum(pix_vecs[:,ii:ii+divideSize+1])
    ###handle boundary conditions
    if (ii+divideSize == y_range): sum += np.sum(pix_vecs[:,0:1])
    if(sum > max_sum):
      max_sum = sum
      flip_index = ii

  #if(np.sum(pix_vecs[:8:12]) > 0): debug_function = 1
  if(debug_function):
    print 'debug point'
    print np.arange(0,y_range,divideSize,dtype=int)
    print 'flip_index = %d' %(flip_index)

  ###flip the matrix column
  #pix_vecs = np.roll(pix_vecs, -flip_index)  
  if(flip_index == 0): 
    pass
  elif(flip_index == 1*divideSize):
    ##flip Y
    pix_vecs = np.roll(np.fliplr(np.roll(pix_vecs, -divideSize,axis=1)),divideSize+1,axis=1)
  elif(flip_index == 2*divideSize):
    ##flip X, Y
    pix_vecs = np.roll(np.fliplr(np.roll(np.fliplr(pix_vecs), -divideSize,axis=1)),divideSize,axis=1)
  elif(flip_index == 3*divideSize):
    ##flip X
    pix_vecs = np.fliplr(np.roll(pix_vecs,-1,axis=1))
  else: 
    pass

  if(debug_function):
    print pix_vecs
    quit()
  
  ###matrix to vector
  return (pix_vecs[1:,:]).flatten()

### Concentric Circle with Area Sampling
def GetCCSA_16bit(xData, getp=''):
  debug_function = 0

  sgds   = xData[0]
  crange = xData[1]
  dimn   = xData[2]
  pcenter= xData[3]
  pdir   = 0

  ### set calc. area
  spx = float(pcenter[0])-crange/2.0
  spy = float(pcenter[1])-crange/2.0
  epx = spx + crange
  epy = spy + crange

  ### create img array
  igrid = int(crange*1000)
  im    = Image.new('L',(igrid,igrid))
  draw  = ImageDraw.Draw(im)
  lbx   = spx*1000
  lby   = spy*1000
  tdif  = np.array([lbx,lby],dtype=int)

  for pol in sgds.polygons:
    test  = np.array(pol*1000 - tdif, dtype=int)
    test2 = []

    ### grid2pix
    nn = test.shape[0]
    for j in xrange(nn):
      if j == 0 or j == nn-1:
        test2.append(test[j])
        continue
      if test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] > test[j,1] and test[j,0] < test[j+1,0]:
        test2.append(test[j])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] > test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] < test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append([test[j,0],test[j,1]-1])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      else:
        test2.append(test[j])
    test2 = np.array(test2,dtype=int)

    ### pix_cord2poly
    tp   = [tuple([test2[j,0],test2[j,1]]) for j in xrange(test.shape[0])]

    ### draw polygon
    #if(debug_function): print tp
    draw.polygon(tp,fill=255)

  ### transpose
  im = im.transpose(Image.FLIP_TOP_BOTTOM)

  ### resize
  #rsize = igrid/5
  #im    = im.resize((rsize,rsize))

  ### check img
  #tname = 'test_'+str(pcenter[0])+'_'+str(pcenter[1])+'.png'
  #im.save(tname)

  ### img2array
  imArray = np.asarray(im)
  del im

  ### calc. dim.
  instep  = 10
  oustep  = 20
  prad    = int(imArray.shape[0])/2
  xdim    = int((dimn/instep+np.floor((prad-dimn)/oustep))*16+1)

  ### pix vectors
  pix_vecs = np.zeros(xdim,dtype=int)
  xstep    = np.r_[np.arange(0,dimn,instep,dtype=int),np.arange(dimn,prad,oustep,dtype=int)]
  #print xstep
  
  ### concentric circle sampling
  gr    = 3 # (lambda/(2NA(1+sigma)))^2
  ry,rx = np.ogrid[-gr:gr+1,-gr:gr+1]
  mask  = rx*rx+ry*ry <= gr*gr
  zz    = 1

  #if(len(sgds.polygons) > 1): debug_function = 1

  if(debug_function):
    print "Sampling point: (%.1f, %.1f)" %(pcenter[0]*1000.0,pcenter[1]*1000.0)
    fname = 'test.gds'
    test_cell = gdspy.Cell('test')
    for pg in sgds.polygons: test_cell.add(gdspy.Polygon(pg*1000.0))
    text = gdspy.Text(text='spoint',size=20, position=pcenter*1000.0, layer=0, datatype = 1)
    test_cell.add(text)

  pix_vecs[0] = int(np.sum(imArray[prad-gr:prad+gr+1,prad-gr:prad+gr+1][mask])/255)
  for ii in xstep[1:]:
    xdisp1 = int(np.floor( ii*np.cos(22.5*np.pi/180.0) ))
    ydisp1 = int(np.floor( ii*np.sin(22.5*np.pi/180.0) ))
    xdisp2 = int(np.floor( ii*np.cos(45.0*np.pi/180.0) ))
    ydisp2 = int(np.floor( ii*np.sin(45.0*np.pi/180.0) ))
    xdisp3 = int(np.floor( ii*np.cos(67.5*np.pi/180.0) ))
    ydisp3 = int(np.floor( ii*np.sin(67.5*np.pi/180.0) ))
    lt_x = prad-xdisp2
    lt_y = prad+ydisp2
    pix_vecs[zz]   = int(np.sum(imArray[lt_x-gr:lt_x+gr+1,lt_y-gr:lt_y+gr+1][mask])/255) ### Left top
    lt_ct_x = prad-xdisp3
    lt_ct_y = prad+ydisp3
    pix_vecs[zz+1] = int(np.sum(imArray[lt_ct_x-gr:lt_ct_x+gr+1,lt_ct_y-gr:lt_ct_y+gr+1][mask])/255) ### Left top-Center top
    ct_x = prad
    ct_y = prad+ii
    pix_vecs[zz+2] = int(np.sum(imArray[ct_x-gr:ct_x+gr+1,ct_y-gr:ct_y+gr+1][mask])/255) ### Center top
    ct_rt_x = prad+xdisp3
    ct_rt_y = prad+ydisp3
    pix_vecs[zz+3] = int(np.sum(imArray[ct_rt_x-gr:ct_rt_x+gr+1,ct_rt_y-gr:ct_rt_y+gr+1][mask])/255) ### Center top-Right top
    rt_x = prad+xdisp2
    rt_y = prad+ydisp2
    pix_vecs[zz+4] = int(np.sum(imArray[rt_x-gr:rt_x+gr+1,rt_y-gr:rt_y+gr+1][mask])/255) ### Right top
    rt_rc_x = prad+xdisp1
    rt_rc_y = prad+ydisp1
    pix_vecs[zz+5] = int(np.sum(imArray[rt_rc_x-gr:rt_rc_x+gr+1,rt_rc_y-gr:rt_rc_y+gr+1][mask])/255) ### Right top-Right center
    rc_x = prad+ii
    rc_y = prad
    pix_vecs[zz+6] = int(np.sum(imArray[rc_x-gr:rc_x+gr+1,rc_y-gr:rc_y+gr+1][mask])/255) ### Right center
    rc_rb_x=prad+xdisp1
    rc_rb_y=prad-ydisp1
    pix_vecs[zz+7] = int(np.sum(imArray[rc_rb_x-gr:rc_rb_x+gr+1,rc_rb_y-gr:rc_rb_y+gr+1][mask])/255) ### Right center-Right bottom
    rb_x = prad+xdisp2
    rb_y = prad-ydisp2
    pix_vecs[zz+8] = int(np.sum(imArray[rb_x-gr:rb_x+gr+1,rb_y-gr:rb_y+gr+1][mask])/255) ### Right bottom
    rb_cb_x = prad+xdisp3
    rb_cb_y = prad-ydisp3
    pix_vecs[zz+9] = int(np.sum(imArray[rb_cb_x-gr:rb_cb_x+gr+1,rb_cb_y-gr:rb_cb_y+gr+1][mask])/255) ### Right bottom-Center bottom
    cb_x = prad
    cb_y = prad-ii
    pix_vecs[zz+10] = int(np.sum(imArray[cb_x-gr:cb_x+gr+1,cb_y-gr:cb_y+gr+1][mask])/255) ### Right bottom-Center bottom
    cb_lb_x = prad-xdisp3
    cb_lb_y = prad-ydisp3
    pix_vecs[zz+11] = int(np.sum(imArray[cb_lb_x-gr:cb_lb_x+gr+1,cb_lb_y-gr:cb_lb_y+gr+1][mask])/255) ### Center bottom-Left bottom
    lb_x = prad-xdisp2
    lb_y = prad-ydisp2
    pix_vecs[zz+12] = int(np.sum(imArray[lb_x-gr:lb_x+gr+1,lb_y-gr:lb_y+gr+1][mask])/255) ### Left bottom
    lb_lc_x = prad-xdisp1
    lb_lc_y = prad-ydisp1
    pix_vecs[zz+13] = int(np.sum(imArray[lb_lc_x-gr:lb_lc_x+gr+1,lb_lc_y-gr:lb_lc_y+gr+1][mask])/255) ### Left bottom-Left center
    lc_x = prad-ii
    lc_y = prad
    pix_vecs[zz+14] = int(np.sum(imArray[lc_x-gr:lc_x+gr+1,lc_y-gr:lc_y+gr+1][mask])/255) ### Left center
    lc_lt_x = prad-xdisp1
    lc_lt_y = prad+ydisp1
    pix_vecs[zz+15] = int(np.sum(imArray[lc_lt_x-gr:lc_lt_x+gr+1,lc_lt_y-gr:lc_lt_y+gr+1][mask])/255) ### Left bottom-Left top
    
    zz += 16
    #if(debug_function): test_cell.add(gdspy.Polygon([(pmd+lby,ppd+lbx),(prad+lby,ppi+lbx),(ppd+lby,ppd+lbx),(ppi+lby,prad+lbx),(ppd+lby,pmd+lbx),(prad+lby,pmi+lbx),(pmd+lby,pmd+lbx),(pmi+lby,prad+lbx)]))
    if(debug_function): 
      bbx = lbx
      bby = lby
      test_cell.add(gdspy.Polygon([(lt_x+bbx,lt_y+bby),(lt_ct_x+bbx,lt_ct_y+bby),(ct_x+bbx,ct_y+bby),(ct_rt_x+bbx,ct_rt_y+bby),(rt_x+bbx,rt_y+bby),(rt_rc_x+bbx,rt_rc_y+bby),(rc_x+bbx,rc_y+bby),(rc_rb_x+bbx,rc_rb_y+bby),(rb_x+bbx,rb_y+bby),(rb_cb_x+bbx,rb_cb_y+bby),(cb_x+bbx,cb_y+bby),(cb_lb_x+bbx,cb_lb_y+bby),(lb_x+bbx,lb_y+bby),(lb_lc_x+bbx,lb_lc_y+bby),(lc_x+bbx,lc_y+bby),(lc_lt_x+bbx,lc_lt_y+bby)], layer = 1))
  # end for

  if(debug_function):
    print pix_vecs
    for pg in getp: test_cell.add(gdspy.Polygon(pg*1000))
    gds_prt = gdspy.GdsPrint(fname,unit=1.0e-9,precision=1.0e-9)
    gds_prt.write_cell(test_cell)
    gds_prt.close()
    #gdspy.LayoutViewer()
    gdspy.Cell.cell_dict.clear()

  return pix_vecs


### Concentric Circle with Area Sampling
def GetCCSA_8bit(xData):
  debug_function = 0

  sgds   = xData[0]
  crange = xData[1]
  dimn   = xData[2]
  pcenter= xData[3]
  pdir   = 0

  ### set calc. area
  spx = float(pcenter[0])-crange/2.0
  spy = float(pcenter[1])-crange/2.0
  epx = spx + crange
  epy = spy + crange

  ### create img array
  igrid = int(crange*1000)
  im    = Image.new('L',(igrid,igrid))
  draw  = ImageDraw.Draw(im)
  lbx   = spx*1000
  lby   = spy*1000
  tdif  = np.array([lbx,lby],dtype=int)

  for pol in sgds.polygons:
    test  = np.array(pol*1000 - tdif, dtype=int)
    test2 = []

    ### grid2pix
    nn = test.shape[0]
    for j in xrange(nn):
      if j == 0 or j == nn-1:
        test2.append(test[j])
        continue
      if test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] > test[j,1] and test[j,0] < test[j+1,0]:
        test2.append(test[j])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] > test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,1] < test[j,1] and test[j,0] < test[j+1,0]:
        test2.append([test[j,0]-1,test[j,1]])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] < test[j,0] and test[j,1] < test[j+1,1]:
        test2.append(test[j])
      elif test[j-1,0]==test[j,0] and test[j,1]==test[j+1,1] and test[j-1,0] > test[j,0] and test[j,1] > test[j+1,1]:
        test2.append([test[j,0],test[j,1]-1])
      elif test[j-1,1]==test[j,1] and test[j,0]==test[j+1,0] and test[j-1,0] > test[j,0] and test[j,1] < test[j+1,1]:
        test2.append([test[j,0]-1,test[j,1]])
      else:
        test2.append(test[j])
    test2 = np.array(test2,dtype=int)

    ### pix_cord2poly
    tp   = [tuple([test2[j,0],test2[j,1]]) for j in xrange(test.shape[0])]

    ### draw polygon
    #if(debug_function): print tp
    draw.polygon(tp,fill=255)

  ### transpose
  im = im.transpose(Image.FLIP_TOP_BOTTOM)

  ### resize
  #rsize = igrid/5
  #im    = im.resize((rsize,rsize))

  ### check img
  #tname = 'test_'+str(pcenter[0])+'_'+str(pcenter[1])+'.png'
  #im.save(tname)

  ### img2array
  imArray = np.asarray(im)
  del im

  ### calc. dim.
  instep  = 10
  oustep  = 20
  prad    = int(imArray.shape[0])/2
  xdim    = int((dimn/instep+np.floor((prad-dimn)/oustep))*8+1)

  ### pix vectors
  pix_vecs = np.zeros(xdim,dtype=int)
  xstep    = np.r_[np.arange(0,dimn,instep,dtype=int),np.arange(dimn,prad,oustep,dtype=int)]
  #print xstep
  
  ### concentric circle sampling
  gr    = 3 # (lambda/(2NA(1+sigma)))^2
  ry,rx = np.ogrid[-gr:gr+1,-gr:gr+1]
  mask  = rx*rx+ry*ry <= gr*gr
  zz    = 1

  #if(len(sgds.polygons) == 0): debug_function = 0

  if(debug_function):
    print "Sampling point: (%.1f, %.1f)" %(pcenter[0]*1000.0,pcenter[1]*1000.0)
    fname = 'test.gds'
    test_cell = gdspy.Cell('test')
    for pg in sgds.polygons: test_cell.add(gdspy.Polygon(pg*1000.0))
    text = gdspy.Text(text='spoint',size=20, position=pcenter*1000.0, layer=0, datatype = 1)
    test_cell.add(text)

  pix_vecs[0] = int(np.sum(imArray[prad-gr:prad+gr+1,prad-gr:prad+gr+1][mask])/255)
  for ii in xstep[1:]:
    disp = int(np.floor( ii*np.cos(45*np.pi/180.0) ))
    pmd = prad-disp
    ppd = prad+disp
    pmi = prad-ii
    ppi = prad+ii
    pix_vecs[zz]   = int(np.sum(imArray[pmd-gr:pmd+gr+1,ppd-gr:ppd+gr+1][mask])/255) ### Left top
    pix_vecs[zz+1] = int(np.sum(imArray[prad-gr:prad+gr+1,ppi-gr:ppi+gr+1][mask])/255) ### Center top
    pix_vecs[zz+2] = int(np.sum(imArray[ppd-gr:ppd+gr+1,ppd-gr:ppd+gr+1][mask])/255) ### Right top
    pix_vecs[zz+3] = int(np.sum(imArray[ppi-gr:ppi+gr+1,prad-gr:prad+gr+1][mask])/255) ### Right center
    pix_vecs[zz+4] = int(np.sum(imArray[ppd-gr:ppd+gr+1,pmd-gr:pmd+gr+1][mask])/255) ### Right bottom
    pix_vecs[zz+5] = int(np.sum(imArray[prad-gr:prad+gr+1,pmi-gr:pmi+gr+1][mask])/255) ### Center bottom
    pix_vecs[zz+6] = int(np.sum(imArray[pmd-gr:pmd+gr+1,pmd-gr:pmd+gr+1][mask])/255) ### Left bottom
    pix_vecs[zz+7] = int(np.sum(imArray[pmi-gr:pmi+gr+1,prad-gr:prad+gr+1][mask])/255) ### Left center
    zz += 8
    #if(debug_function): test_cell.add(gdspy.Polygon([(pmd+lby,ppd+lbx),(prad+lby,ppi+lbx),(ppd+lby,ppd+lbx),(ppi+lby,prad+lbx),(ppd+lby,pmd+lbx),(prad+lby,pmi+lbx),(pmd+lby,pmd+lbx),(pmi+lby,prad+lbx)]))
    if(debug_function): 
      bbx = lbx
      bby = lby
      test_cell.add(gdspy.Polygon([(pmd+bbx,ppd+bby),(prad+bbx,ppi+bby),(ppd+bbx,ppd+bby),(ppi+bbx,prad+bby),(ppd+bbx,pmd+bby),(prad+bbx,pmi+bby),(pmd+bbx,pmd+bby),(pmi+bbx,prad+bby)]))
  # end for

  if(debug_function):
    print pix_vecs
    gds_prt = gdspy.GdsPrint(fname,unit=1.0e-9,precision=1.0e-9)
    gds_prt.write_cell(test_cell)
    gds_prt.close()
    #gdspy.LayoutViewer()
    gdspy.Cell.cell_dict.clear()

  return pix_vecs

def AnalyzeFeature(pp, f_vec, ss):
  
  print '#Feature analysis...'
  #real_f_vec = f_vec[ss[:,2] != 0]
  #real_ss = ss[f_vec.sum(axis=1) == 0]
  
  ###Feature analysis for logistic regression
  if pp['Model']=='LGR' or pp['Model'] == 'SVC':
    label_0_cnt = np.sum(ss[:,2] == 0.0)
    label_1_cnt = np.sum(ss[:,2] == 1.0)
    print '\tlabel 0 cnt = %d, label 1 cnt = %d, ratio = %.2f' %(label_0_cnt, label_1_cnt, float(label_0_cnt)/float(label_1_cnt))

  return 0

def SampleSizeReduction(pp, f_vec, ss, flg):
  ###Need to predict all sampling points
  #if(pp['F_Gsearch'] == 'OFF' and flg == 'test'): return f_vec, ss
  if(flg == 'test'): return f_vec, ss

  print 'Sample size reduction...'
  ### remove the samples with zero f_vec but not-zero ss
  index = True - ((f_vec.sum(axis=1)==0) & (ss[:,2] != 0))
  _vec = f_vec[index]
  _ss  = ss[index]  

  print '\t#samples\t\t: reduced from  %d  to %d' %(len(f_vec), len(_vec))
  return _vec, _ss

  ### ss: gdsx, gdsy, weight
  rr = 4
  nn = f_vec.shape[0]
  rn = int(nn/rr)

  np.random.seed(0)
  nd = np.random.permutation(rn)
  if flg == 'train':
    pp['Dindex'] = nd

  if (pp['Dsize'] == 'All' and flg == 'train') or flg == 'test':
    _vec = _vec[0:rn]
    _ss  = _ss[0:rn]
  elif pp['Dsize'] == 'Large' and flg == 'train':
    _vec = _vec[nd[0:10000]]
    _ss  = _ss[nd[0:10000]]
  elif pp['Dsize'] == 'Middle' and flg == 'train':
    _vec = _vec[nd[0:5000]]
    _ss  = _ss[nd[0:5000]]
  elif pp['Dsize'] == 'Small' and flg == 'train':
    _vec = _vec[nd[0:1000]]
    _ss  = _ss[nd[0:1000]]

  if pp['F_Gsearch'] == 'ON' and flg == 'test':
    if pp['Dsize'] == 'Small':
      _vec = _vec[nd[0:1000]]
      _ss  = _ss[nd[0:1000]]
    elif pp['Dsize'] == 'Middle':
      _vec = _vec[nd[0:5000]]
      _ss  = _ss[nd[0:5000]]
    elif pp['Dsize'] == 'Large':
      _vec = _vec[nd[0:10000]]
      _ss  = _ss[nd[0:10000]]
    elif pp['Dsize'] == 'All':
      _vec = _vec[0:rn]
      _ss  = _ss[0:rn]

  print '\t#samples\t\t: reduced from  %d  to %d' %(len(f_vec), len(_vec))
  return _vec, _ss
