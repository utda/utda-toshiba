#!/usr/bin/python

import os
import time
import numpy as np
import pylab as pl
import pystan
import pickle

### Generate Stan Model File
def GetStanCodeA(nn):
	ndim = nn+1
	fs   = './log/stan.code.stn'
	fout = open(fs,'w')

	fout.write('data {\n')
	fout.write('	int<lower=0> N;\n')
	fout.write('	int<lower=0> S;\n')
	fout.write('	int<lower=0> Type[N];\n')
	fout.write('	real Y[N];\n')

	for i in range(1,ndim):
		fout.write('	real X%d[N];\n' %i)

	fout.write('}\n')
	fout.write('\n')
	fout.write('parameters {\n')

	for i in range(1,ndim):
		fout.write('	real a_f%d;\n' %i)
	for i in range(1,ndim):
		fout.write('	real a_r%d[S];\n' %i)

	fout.write('	real b_f;\n')
	fout.write('	real b_r[S];\n')
	for i in range(1,ndim):
		fout.write('	real<lower=0> s_af%d;\n' %i)
	for i in range(1,ndim):
		fout.write('	real<lower=0> s_ar%d;\n' %i)
	fout.write('	real<lower=0> s_bf;\n')
	fout.write('	real<lower=0> s_br;\n')
	fout.write('	real<lower=0> s_y;\n')
	fout.write('}\n')

	fout.write('\n')
	fout.write('transformed parameters {\n')
	fout.write('	real yh[N];\n')
	fout.write('	for (i in 1:N){\n')
	fout.write('		yh[i] <- ')
	
	for i in range(1,ndim):
		fout.write('(a_f%d+a_r%d[Type[i]])*X%d[i]+' %(i,i,i))
	fout.write('b_f+b_r[Type[i]];\n')
	fout.write('	}\n')
	fout.write('}\n')
	
	fout.write('\n')
	fout.write('model {\n')
	fout.write('	# hierarchical prior distribution\n')
	for i in range(1,ndim):
		fout.write('	s_af%d ~ uniform(0,1.0e+4);\n' %i)
	for i in range(1,ndim):
		fout.write('	s_ar%d ~ uniform(0,1.0e+4);\n' %i)
	fout.write('	s_bf ~ uniform(0,1.0e+4);\n')
	fout.write('	s_br ~ uniform(0,1.0e+4);\n')
	fout.write('\n')
	fout.write('	# prior distribution\n')
	fout.write('	s_y ~ uniform(0,1.0e+4);\n')
	for i in range(1,ndim):
		fout.write('	a_f%d ~ normal(0,s_af%d);\n' %(i,i))
	for i in range(1,ndim):
		fout.write('	a_r%d ~ normal(0,s_ar%d);\n' %(i,i))
	fout.write('	b_f ~ normal(0,s_bf);\n')
	fout.write('	b_r ~ normal(0,s_br);\n')
	fout.write('\n')
	fout.write('	# regression model with random effects\n')
	fout.write('	Y ~ normal(yh,s_y);\n')
	fout.write('}\n')

	fout.close()

	return 0

### Generate Stan Model File
def GetStanCodeB(nn):
	ndim = nn+1
	fs   = './log/stan.code.stn'
	fout = open(fs,'w')

	fout.write('data {\n')
	fout.write('	int<lower=1> N;\n')
	fout.write('	int<lower=1> S;\n')
	fout.write('	int<lower=1> Type[N];\n')
	fout.write('	real Y[N];\n')

	for i in range(1,ndim):
		fout.write('	real X%d[N];\n' %i)

	fout.write('}\n')
	fout.write('\n')
	fout.write('parameters {\n')

	for i in range(1,ndim):
		fout.write('	real a_f%d;\n' %i)
	for i in range(1,ndim):
		fout.write('	real a_r%d[S];\n' %i)

	fout.write('	real b_f;\n')
	fout.write('	real b_r[S];\n')
	fout.write('	real<lower=0> s_af;\n')
	fout.write('	real<lower=0> s_ar[S];\n')
	fout.write('	real<lower=0> s_af_el;\n')
	fout.write('	real<lower=0> s_ar_el[S];\n')
	fout.write('	real<lower=0> s_bf;\n')
	fout.write('	real<lower=0> s_br[S];\n')
	fout.write('	real<lower=0> s_y;\n')
	fout.write('}\n')
	fout.write('\n')

	fout.write('transformed parameters {\n')
	fout.write('	real yh[N];\n')
	fout.write('	for (i in 1:N){\n')
	fout.write('		yh[i] <- ')
	
	for i in range(1,ndim):
		fout.write('(a_f%d+a_r%d[Type[i]])*X%d[i]+' %(i,i,i))
	fout.write('b_f+b_r[Type[i]];\n')
	fout.write('	}\n')
	fout.write('}\n')
	fout.write('\n')

	fout.write('model {\n')
	fout.write('	# hierarchical prior distribution\n')
	fout.write('	s_af ~ uniform(0,1.0e+4);\n')
	fout.write('	s_af_el ~ uniform(0,1.0e+4);\n')
	fout.write('	for (i in 1:S){\n')
	fout.write('		s_ar[i] ~ uniform(0,1.0e+4);\n')
	fout.write('		s_ar_el[i] ~ uniform(0,1.0e+4);\n')
	fout.write('	}\n')
	fout.write('	s_bf ~ uniform(0,1.0e+4);\n')
	fout.write('	for (i in 1:S){\n')
	fout.write('		s_br[i] ~ uniform(0,1.0e+4);\n')
	fout.write('	}\n')
	fout.write('\n')

	fout.write('	# prior distribution\n')
	fout.write('	s_y ~ uniform(0,1.0e+4);\n')
	for i in range(1,ndim-1):
		fout.write('	a_f%d ~ normal(0,s_af);\n' %i)
	fout.write('	a_f%d ~ normal(0,s_af_el);\n' %(ndim-1))

	for i in range(1,ndim-1):
		fout.write('	for (i in 1:S){\n')
		fout.write('		a_r%d[i] ~ normal(0,s_ar[i]);\n' %i)
		fout.write('	}\n')
	fout.write('	for (i in 1:S){\n')
	fout.write('		a_r%d[i] ~ normal(0,s_ar_el[i]);\n' %(ndim-1))
	fout.write('	}\n')

	fout.write('	b_f ~ normal(0,s_bf);\n')
	fout.write('	for (i in 1:S){\n')
	fout.write('		b_r[i] ~ normal(0,s_br[i]);\n')
	fout.write('	}\n')
	fout.write('\n')

	fout.write('	# regression model with random effects\n')
	fout.write('	Y ~ normal(yh,s_y);\n')
	fout.write('}\n')

	fout.close()

	return 0

### Define Stan params
def SetStanDat(xx,cc,yy):
	ucc = np.unique(cc).shape[0]
	dat = {'N'   :xx.shape[0],
	       'S'   :ucc,
	       'Type':cc+1,
	       'Y'   :np.array(yy,dtype=float)}
	for i in xrange(xx.shape[1]):
		xkey = 'X'+str(i+1)
		dat[xkey] = xx[:,i]
	return dat

### Compute Rhat
def GetRhat(xx,m,n):
	### B: between-chain variance
	### W: within-chain variance
	### m: chains, n: length
	# B = n/(m-1) sum_(j=1)^m (theta_j-theta)^2
	# W = 1/m sum_(j=1)^m [1/(n-1) sum(theta_ij-theta_j)^2]
	# V = (n-1)/n W + 1/n B
	# Rh = sqrt(V/W)

	th_t  = np.zeros(m)
	thijt = np.zeros(m)

	for i in xrange(m):
		thij     = xx[i*m:i*m+n]
		th_t[i]  = (np.mean(thij)-np.mean(xx))**2
		thijt[i] = np.sum([(thij[j]-np.mean(thij))**2 for j in xrange(n)]) / (n-1.0)

	B = n/(m-1.0)*np.sum(th_t)
	W = 1.0/m * np.sum(thijt)
	V = ((n-1.0)/n)*W + (1.0/n)*B
	R = np.sqrt(V/W)

	return R

### Extract model parameters (average of all chains)
def GetModelParams(pp,ndim,ucc,m,n):
	a_f = np.zeros(ndim)		### fixed effects (a)
	a_r = np.zeros((ndim,ucc))	### random effects (a)
	b_f = np.zeros(1)		### fixed effects (b)
	b_r = np.zeros(ucc)		### random effects (b)

	n_iter = pp['yh'].shape[0]
	a_f_s  = np.zeros((n_iter,ndim))
	a_r_s  = np.zeros((n_iter,ndim,ucc))
	saf_s  = np.zeros((n_iter,ndim))
	sar_s  = np.zeros((n_iter,ndim))

	rhat_a_f = np.zeros(ndim)
	rhat_a_r = np.zeros((ndim,ucc))
	#rhat_saf = np.zeros(ndim)

	for i in xrange(ndim):
		fkey = 'a_f'+str(i+1)
		rkey = 'a_r'+str(i+1)
		a_f[i] = np.mean(pp[fkey])
		a_r[i] = np.mean(pp[rkey],axis=0)
		a_f_s[:,i] = pp[fkey]
		a_r_s[:,i] = pp[rkey]
		#saf_s[:,i] = pp['s_af'+str(i+1)]
		#sar_s[:,i] = pp['s_ar'+str(i+1)]

		rhat_a_f[i] = GetRhat(pp[fkey],m,n)
		#rhat_saf[i] = GetRhat(pp['s_af'+str(i+1)],m,n)
		for j in xrange(m):
			rhat_a_r[i,j] = GetRhat(pp[rkey][:,j],m,n)

	b_f = np.mean(pp['b_f'])
	b_r = np.mean(pp['b_r'],axis=0)

	m_pp = []
	for i in xrange(ndim):
		m_pp.append(np.r_[a_f[i],a_r[i]])
	m_pp.append(np.r_[b_f,b_r])

	### Save sampling results
	np.save('./log/re_a_fixed.npy',a_f_s)
	np.save('./log/re_a_random.npy',a_r_s)
	np.save('./log/re_b_fixed.npy',pp['b_f'])
	np.save('./log/re_b_random.npy',pp['b_r'])
	np.save('./log/re_s_a_fixed.npy',pp['s_af'])
	np.save('./log/re_s_a_random.npy',pp['s_ar'])
	np.save('./log/re_s_ael_fixed.npy',pp['s_af_el'])
	np.save('./log/re_s_ael_random.npy',pp['s_ar_el'])
	np.save('./log/re_s_b_fixed.npy',pp['s_bf'])
	np.save('./log/re_s_b_random.npy',pp['s_br'])
	np.save('./log/re_s_y.npy',pp['s_y'])

	### Save Rhat
	fout = open('./log/re_rhat.txt','w')
	for i in xrange(ndim):
		fout.write('%s\t%.3f\n' %('a_f'+str(i+1),rhat_a_f[i]))	### a_f

	for i in xrange(ndim):
		fout.write('%s\t' %('a_r'+str(i+1)))
		for j in xrange(ucc):
			fout.write('%.3f\t' %(rhat_a_r[i,j]))		### a_r
		fout.write('\n')

	fout.write('b_f\t%.3f\n' %(GetRhat(pp['b_f'],m,n)))		### b_f
	fout.write('b_r\t')
	for j in xrange(ucc):
		fout.write('%.3f\t' %GetRhat(pp['b_r'][:,j],m,n))	### b_r
	fout.write('\n')

	fout.write('s_af\t%.3f\n' %(GetRhat(pp['s_af'],m,n)))		### s_af
	fout.write('s_ar\t')
	for j in xrange(ucc):
		fout.write('%.3f\t' %GetRhat(pp['s_ar'][:,j],m,n))	### s_ar
	fout.write('\n')

	fout.write('s_af_el\t%.3f\n' %(GetRhat(pp['s_af_el'],m,n)))	### s_af_el
	fout.write('s_ar_el\t')
	for j in xrange(ucc):
		fout.write('%.3f\t' %GetRhat(pp['s_ar_el'][:,j],m,n))	### s_ar_el
	fout.write('\n')

	fout.write('s_bf\t%.3f\n' %(GetRhat(pp['s_bf'],m,n)))		### s_bf
	fout.write('s_br\t')
	for j in xrange(ucc):
		fout.write('%.3f\t' %GetRhat(pp['s_br'][:,j],m,n))	### s_br
	fout.write('\n')

	fout.close()

	return np.array(m_pp)

### Fit Hierarchical Bayes Model
def FitHBM(pp,xx,cc,yy):
	### Model
	### y = (fixed_wi + random_wi)x + (fixed_ui + random_ui)z + e

	mname  = './stn_model/'+pp['MCMCmodel']
	n_iter = pp['MCMCn']
	n_chain= pp['MCMCc']
	n_thin = pp['MCMCt']
	N      = xx.shape[0]
	ndim   = xx.shape[1]
	ucc    = np.unique(cc).shape[0]

	hbm    = pp['HBMmodel']
	hbmpy  = './result/'+hbm

	### Check calibrated model
	if os.path.isfile(hbmpy):
		print '\tHBM %s found. MCMC is skipped.' %hbm
		model = np.load(hbmpy)
		return [model,9999]

	### Gen. Stan Code
	#GetStanCodeA(ndim)
	GetStanCodeB(ndim)
	sc = open('./log/stan.code.stn')
	stan_code = sc.read()
	sc.close()

	### Set HBM params
	stan_dat = SetStanDat(xx,cc,yy)

	if os.path.isfile(mname):
		### Load model
		print '\tLoading stan model'
		with open(mname,'rb') as f:
			fmodel=pickle.load(f)
	else:
		### Compile model
		print '\tCompiling...'
		ttime = time.time()
		fmodel = pystan.StanModel(model_code=stan_code)
		print '\telapsed time (sec)\t: %.3f' %(time.time()-ttime)

		### Save model
		with open(mname,'wb') as f:
			pickle.dump(fmodel,f)

	### Run MCMC
	print '\tRuning MCMC...'
	ttime = time.time()
	fit   = fmodel.sampling(data=stan_dat,iter=n_iter,chains=n_chain,thin=n_thin,n_jobs=-1)
	print '\telapsed time (sec)\t: %.3f' %(time.time()-ttime)

	### Extract and save parameters
	params = fit.extract(permuted=True)
	model  = GetModelParams(params,ndim,ucc,n_chain,int(n_iter/2)/n_thin)
	np.save(hbmpy,model)

	### Plot
	#fit.traceplot()
	#pl.savefig('mcmc.pdf',format='pdf')

	### Compute R^2
	yh_t  = params['yh']
	yh    = yh_t[-1]
	score = 1.0-((np.sum((yy-yh)**2)/(N-ndim-1))/(np.sum((yy-np.mean(yy))**2)/(N-1)))

	return [model,score]

