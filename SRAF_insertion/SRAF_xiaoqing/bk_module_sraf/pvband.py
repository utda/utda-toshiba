#!/home/intern3/local/bin/python2.7
import os
import sys
import numpy as np
import matplotlib.pyplot as plt

sys.path.insert(0, os.path.join('../lithosim'))
from pvband_cal import PV_band_cal
from pvband_cal import make_calibre_rule

def PVBandSim(pp,cell_list,xy_dict,opc='mb',phase='test'):
    print '\n### PV band simulation'

    sraf = 'mb'
    fpvband = open(('./result/pvband_%s.txt' %sraf),'w')
    fepe_best = open(('./result/epe_best_%s.txt' %sraf),'w')
    fepe_inner = open(('./result/epe_inner_%s.txt' %sraf),'w')
    fepe_outer = open(('./result/epe_outer_%s.txt' %sraf),'w')
    #os.chdir('../lithosim')
    fpvband.write('\nopc: %s, sraf: %s\n' %(opc,sraf))
    fepe_best.write('\nopc: %s, sraf: %s\n' %(opc,sraf))
    fepe_inner.write('\nopc: %s, sraf: %s\n' %(opc,sraf))
    fepe_outer.write('\nopc: %s, sraf: %s\n' %(opc,sraf))
    mb_pvband = []
    mb_epe_best = []
    mb_epe_inner = []
    mb_epe_outer = []
    for item in cell_list:
        #os.system('cp ../SRAF_xiaoqing/result/gds/%s_sraf.gds ./gds' %item)
        os.system('cp ./result/gds/%s_sraf.gds %s/gds' %(item,pp['SimDir']))
        pvb,epes_best,epes_in,epes_out = PV_band_cal(pp['SimDir'],opc,sraf,item+"_sraf",xy_dict[item],True,False)
        mb_pvband.extend(pvb.tolist())
        mb_epe_best.extend([elem[0] for elem in epes_best])
        mb_epe_inner.extend([elem[0] for elem in epes_in])
        mb_epe_outer.extend([elem[0] for elem in epes_out])
        fpvband.write(item)
        for pv in pvb: fpvband.write('\t%.8f' %pv)
        fpvband.write('\nsum: %.6f\n' %(np.sum(pvb)))
        fepe_best.write(item)
        fepe_best.write('epe(nm) gdsx(um) gdsy(um)\n')
        for ix in xrange(len(epes_best)):
            fepe_best.write('%d %.4f %.4f\n' %(epes_best[ix][0],epes_best[ix][1],epes_best[ix][2]))
        fepe_inner.write(item)
        fepe_inner.write('epe(nm) gdsx(um) gdsy(um)\n')
        for ix in xrange(len(epes_best)):
            fepe_inner.write('%d %.4f %.4f\n' %(epes_in[ix][0],epes_in[ix][1],epes_in[ix][2]))
        fepe_outer.write(item)
        fepe_outer.write('epe(nm) gdsx(um) gdsy(um)\n')
        for ix in xrange(len(epes_best)):
            fepe_outer.write('%d %.4f %.4f\n' %(epes_out[ix][0],epes_out[ix][1],epes_out[ix][2]))
    fpvband.close()
    fepe_best.close()
    fepe_inner.close()
    fepe_outer.close()
    #return 0

    #os.chdir('../SRAF_xiaoqing')
    sraf = 'ml'
    fpvband = open(('./result/pvband_%s.txt' %sraf),'w')
    fepe_best = open(('./result/epe_best_%s.txt' %sraf),'w')
    fepe_inner = open(('./result/epe_inner_%s.txt' %sraf),'w')
    fepe_outer = open(('./result/epe_outer_%s.txt' %sraf),'w')
    #os.chdir('../lithosim')
    fpvband.write('\nopc: %s, sraf: %s\n' %(opc,sraf))
    fepe_best.write('opc: %s, sraf: %s\n' %(opc,sraf))
    fepe_inner.write('opc: %s, sraf: %s\n' %(opc,sraf))
    fepe_outer.write('opc: %s, sraf: %s\n' %(opc,sraf))
    ml_pvband = []
    ml_epe_best = []
    ml_epe_inner = []
    ml_epe_outer = []
    for item in cell_list:
        #os.system('cp ../SRAF_xiaoqing/result/gds/%s_sraf.gds ./gds' %item)
        os.system('cp ./result/gds/%s_sraf.gds %s/gds' %(item,pp['SimDir']))
        pvb,epes_best,epes_in,epes_out = PV_band_cal(pp['SimDir'],opc,sraf,item+"_sraf",xy_dict[item],True,False)
        ml_pvband.extend(pvb.tolist())
        ml_epe_best.extend([elem[0] for elem in epes_best])
        ml_epe_inner.extend([elem[0] for elem in epes_in])
        ml_epe_outer.extend([elem[0] for elem in epes_out])
        fpvband.write(item)
        for pv in pvb: fpvband.write('\t%.8f' %pv)
        fpvband.write('\nsum: %.6f\n' %(np.sum(pvb)))
        fepe_best.write(item)
        fepe_best.write('epe(nm) gdsx(um) gdsy(um)\n')
        for ix in xrange(len(epes_best)):
            fepe_best.write('%d %.4f %.4f\n' %(epes_best[ix][0],epes_best[ix][1],epes_best[ix][2]))
        fepe_inner.write('epe(nm) gdsx(um) gdsy(um)\n')
        for ix in xrange(len(epes_best)):
            fepe_inner.write('%d %.4f %.4f\n' %(epes_in[ix][0],epes_in[ix][1],epes_in[ix][2]))
        fepe_outer.write(item)
        fepe_outer.write('epe(nm) gdsx(um) gdsy(um)\n')
        for ix in xrange(len(epes_best)):
            fepe_outer.write('%d %.4f %.4f\n' %(epes_out[ix][0],epes_out[ix][1],epes_out[ix][2]))
    fpvband.close()
    fepe_best.close()
    fepe_inner.close()
    fepe_outer.close()
    
    ##plot histogram
    mb_epe_best = np.array(mb_epe_best)
    mb_epe_inner = np.array(mb_epe_inner)
    mb_epe_outer = np.array(mb_epe_outer)
    ml_epe_best = np.array(ml_epe_best)
    ml_epe_inner = np.array(ml_epe_inner)
    ml_epe_outer = np.array(ml_epe_outer)
    mb_pvband = np.array(mb_pvband)
    ml_pvband = np.array(ml_pvband)
    fmodel = pp['Model']
    f_handler = open('./result/mb_epe_best.txt','a')
    np.savetxt(f_handler,mb_epe_best)
    f_handler.close()
    f_handler = open('./result/mb_epe_inner.txt','a')
    np.savetxt(f_handler,mb_epe_inner)
    f_handler.close()
    f_handler = open('./result/mb_epe_outer.txt','a')
    np.savetxt(f_handler,mb_epe_outer)
    f_handler.close()
    f_handler = open('./result/%s_epe_best.txt' %fmodel, 'a')
    np.savetxt(f_handler,ml_epe_best)
    f_handler.close()
    f_handler = open('./result/%s_epe_inner.txt' %fmodel, 'a')
    np.savetxt(f_handler,ml_epe_inner)
    f_handler.close()
    f_handler = open('./result/%s_epe_outer.txt' %fmodel, 'a')
    np.savetxt(f_handler,ml_epe_outer)
    f_handler = open('./result/mb_pvband.txt', 'a')
    np.savetxt(f_handler,mb_pvband)
    f_handler.close()
    f_handler = open('./result/%s_pvband.txt' %fmodel, 'a')
    np.savetxt(f_handler,ml_pvband)
    f_handler.close()
    #np.savetxt('./result/mb_epe_best.txt',mb_epe_best)
    #np.savetxt('./result/mb_epe_inner.txt',mb_epe_inner)
    #np.savetxt('./result/mb_epe_outer.txt',mb_epe_outer)
    #np.savetxt(('./result/%s_epe_best.txt' %fmodel),ml_epe_best)
    #np.savetxt(('./result/%s_epe_inner.txt' %fmodel),ml_epe_inner)
    #np.savetxt(('./result/%s_epe_outer.txt' %fmodel),ml_epe_outer)
    #np.savetxt('./result/mb_pvband.txt',mb_pvband)
    #np.savetxt(('./result/%s_pvband.txt' %fmodel),ml_pvband)
    
    #os.chdir('../SRAF_xiaoqing')
    return 0

def PVBandClip(clip,wsraf=True):
    SimDir = '../lithosim'
    #os.chdir('../lithosim')
    #os.system('cp ../SRAF_xiaoqing/result/clips/%s.gds ./gds' %clip)
    os.system('cp ./result/clips/%s.gds %s/gds' %(clip,SimDir))
    pvb,epe_best,epe_in,epe_out = PV_band_cal(SimDir,'mb','ml',clip,np.array([]),wsraf,fclip=True)
    #os.chdir('../SRAF_xiaoqing')
    return pvb[0]
