#!/usr/bin/python

import numpy as np
cimport numpy as np
cimport cython

cdef CGetHLAC(np.ndarray[np.float64_t, ndim=1] imgmat, int w, int h, int iStep):
	cdef int nMask = 25
	cdef int pixv  = 255
	cdef np.ndarray[np.int64_t, ndim=1] feature
	cdef int iy,ix
	cdef int mc,m0,m1,m2,m3,m4,m5,m6,m7

	feature = np.zeros(nMask,dtype=int)
	
	### HLAC mask image
	# |6 7 8|    |5 6 7|
	# |3 4 5| => |3 * 4|
	# |0 1 2|    |0 1 2|

	for iy in xrange(iStep,h-iStep):
		for ix in xrange(iStep,w-iStep):
			mc = imgmat[iy,ix]
			if mc == 0: continue
			
			m0 = imgmat[iy-iStep,ix-iStep]
			m1 = imgmat[iy-iStep,ix]
			m2 = imgmat[iy-iStep,ix+iStep]
			m3 = imgmat[iy,ix-iStep]
			m4 = imgmat[iy,ix+iStep]
			m5 = imgmat[iy+iStep,ix-iStep]
			m6 = imgmat[iy+iStep,ix]
			m7 = imgmat[iy+iStep,ix+iStep]
			
			feature[0] += 1
			if m4 == pixv: feature[1] += 1
			if m7 == pixv: feature[2] += 1
			if m6 == pixv: feature[3] += 1
			if m5 == pixv: feature[4] += 1
			if m3 == pixv and m4 == pixv: feature[5]  += 1
			if m0 == pixv and m7 == pixv: feature[6]  += 1
			if m1 == pixv and m6 == pixv: feature[7]  += 1
			if m2 == pixv and m5 == pixv: feature[8]  += 1
			if m3 == pixv and m7 == pixv: feature[9]  += 1
			if m0 == pixv and m6 == pixv: feature[10] += 1
			if m1 == pixv and m5 == pixv: feature[11] += 1
			if m2 == pixv and m3 == pixv: feature[12] += 1
			if m0 == pixv and m4 == pixv: feature[13] += 1
			if m1 == pixv and m7 == pixv: feature[14] += 1
			if m2 == pixv and m6 == pixv: feature[15] += 1
			if m4 == pixv and m5 == pixv: feature[16] += 1
			if m3 == pixv and m6 == pixv: feature[17] += 1
			if m0 == pixv and m5 == pixv: feature[18] += 1
			if m1 == pixv and m3 == pixv: feature[19] += 1
			if m0 == pixv and m2 == pixv: feature[20] += 1
			if m1 == pixv and m4 == pixv: feature[21] += 1
			if m2 == pixv and m7 == pixv: feature[22] += 1
			if m4 == pixv and m6 == pixv: feature[23] += 1
			if m4 == pixv and m7 == pixv: feature[24] += 1
	return feature

def CRhat(dict pp, int m, int n):
	return rhat
