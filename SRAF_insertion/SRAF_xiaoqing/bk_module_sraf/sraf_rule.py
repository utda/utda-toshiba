#!/home/intern3/local/bin/python2.7

import os
import gdspy
from PIL import Image, ImageDraw
import numpy as np
from multiprocessing import Pool
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from pvband import PVBandClip
from insertion import cinsertion
import time

###SRAF data based on coherence map
def GetSrafData(pp,gdsii_list,cell_list,phase):
  n_cell = len(cell_list)
  if n_cell != len(gdsii_list) :
    print '###ERROR: input gdsii count should be equal to each other in GetSrafData(pp, gdsii_list, cell_list, data).'
    quit()
  
  ss_tr = {}
  gds_index = 0
  fm = pp['FeatureMode']
  if fm == 'CM': 
    print '###Collect SRAF data based on coherence map'
    for item in cell_list:
      #unit_ss_tr = GetSrafData_CM_unit(pp,gdsii_list[ii],cell_list[ii],phase)
      unit_ss_tr = GetSrafData_CM_unit(pp,gdsii_list[gds_index],item,phase)
      #ss_tr.append((cell_list[ii], unit_ss_tr))
      #ss_tr[cell_list[ii]] = unit_ss_tr
      ss_tr[item] = unit_ss_tr
      gds_index += 1
  elif fm == 'PV':
    print '###Collect SRAF data based on PV band simulation'
    for item in cell_list:
      unit_ss_tr = GetSrafData_PV_unit(pp,gdsii_list[gds_index],item,phase)
      ss_tr[item] = unit_ss_tr
      gds_index += 1
  else:
    print '###ERROR: the FeatureMode should be PV or CM instead of %s' %fm
    quit()

  print '###SRAF data collected\n'
  return ss_tr

###SRAF data based on coherence map for each unit
def GetSrafData_CM_unit(pp,gdsii,cell,phase):
  debug_function = 0
  print '\t#SRAF data extraction for cell - %s' %cell

  if phase == 'train':
    fmode = 'train'
    fss = "./result/train_feature/" + cell + "_gauge_train_CM.ss"
  elif phase == 'test':
    fmode = 'test'
    fss = "./result/test_feature/" + cell + "_gauge_test_CM.ss"
  else:
    print "### ini file error"
    quit()

  ### Check gauge (if exists, skip SRAF data extraction)
  if(pp['F_Gsearch']=='OFF') and (pp['StoreOption']=='ON'):
    if os.path.isfile(fss):
      ### Read gauge file
      a_t = np.genfromtxt(fss,skip_header=1)
      att = [[a_t[i,2],a_t[i,3],a_t[i,4],a_t[i,5],a_t[i,6]] for i in xrange(len(a_t))]
      a_data = np.array(att,dtype=int)
      #print '\tdata size: %d' %len(a_data)
      print '\t#Gauge file for %s exists (SRAF data extraction skipped)' %cell
      return a_data
    else:
      pass
  else:
    pass
  
  ### Check coherence map level set look-up-table
  if(pp['ToolMode']=='Toshiba'):
    fcp_lut = "./gds/train/cm_level_lut.txt"
    if(os.path.isfile(fcp_lut)):
      a_t = np.genfromtxt(fcp_lut)
      #500 is the normalization parameter for the coherence map intensity
      cp_lut = {int(a_t[i,0]):a_t[i,1]*5000.0 for i in xrange(len(a_t))}
      #print cp_lut
    else:
      print "###ERROR: coherence map level set look-up-table in %s is missing" %fcp_lut
      quit()

  cell_inst = gdsii.extract(cell)
  bbox = cell_inst.get_bounding_box()
  #rounding the bbox
  bbox[0,0] = int(np.floor(bbox[0,0]))
  bbox[0,1] = int(np.floor(bbox[0,1]))
  bbox[1,0] = int(np.ceil(bbox[1,0]))
  bbox[1,1] = int(np.ceil(bbox[1,1]))
  #if(debug_function): print "bounding box - (%.1f, %.1f, %.1f, %.1f)" %(bbox[0,0],bbox[0,1],bbox[1,0],bbox[1,1])
  #print "bounding box - (%.1f, %.1f, %.1f, %.1f)" %(bbox[0,0],bbox[0,1],bbox[1,0],bbox[1,1])
  ### polygons
  polygon_set = cell_inst.get_polygons(True)
  
  ####hard coding the layers to be parsed
  ####target feature layer
  #target_layer = (pp['TrainLayer'], pp['TrainDtype'])
  #getp = polygon_set[target_layer]
  ####get target feature bounding box
  #llx = getp[0][0][0]
  #urx = llx
  #lly = getp[0][0][1]
  #ury = lly
  #for pg in getp:
  #  for point in pg:
  #    if(point[0] < llx): llx = point[0]
  #    if(point[0] > urx): urx = point[0]
  #    if(point[1] < lly): lly = point[1]
  #    if(point[1] > ury): ury = point[1]
  #if(debug_function): print "target feature bbox - (%.1f, %.1f, %.1f, %.1f)" %(llx, lly, urx, ury)
  #center_x = int(np.ceil((llx + urx)/2))
  #center_y = int(np.ceil((lly + ury)/2))
  
  ###create bitmap for the coherence map 
  if(pp['ToolMode']=='Toshiba'):
    first_flag = True
    for layer in range(150, 180):
      map_layer = (layer, 0)
      if not (map_layer in polygon_set): continue
      l_img = Image.new('L', ( int(np.ceil(bbox[1,0]-bbox[0,0])), int(np.ceil(bbox[1,1]-bbox[0,1])) ))
      getp = polygon_set[map_layer]
      ##transfer getp[ii] for tuple format, account for the offset
      for ii in xrange(len(getp)):
        tp = [tuple([ getp[ii][jj][0]-bbox[0,0], getp[ii][jj][1]-bbox[0,1] ]) for jj in xrange(len(getp[ii]))]
        ImageDraw.Draw(l_img).polygon(tp, outline=1, fill=1)
      #l_img = l_img.transpose(Image.FLIP_TOP_BOTTOM)
      if first_flag == True:
        ### the usefulness based on the coherence map level, (cp_lut)
        cmap = np.asarray(l_img)*cp_lut[layer]
        #cmap = np.asarray(l_img)*1.0
        first_flag = False
      else:
        ### the usefulness based on the coherence map level, (cp_lut)
        tmp = np.asarray(l_img)*cp_lut[layer]
        #tmp = np.asarray(l_img)*1.0
        np.maximum(cmap, tmp, cmap)
      del l_img
      #if(debug_function): np.savetxt("cmap.txt", cmap, fmt='%.1d' )
  
  ###create bitmap for the sraf features
  if(debug_function): print 'create bitmap for sraf features'
  l_img = Image.new('L', ( int(np.ceil(bbox[1,0]-bbox[0,0])), int(np.ceil(bbox[1,1]-bbox[0,1])) ))
  getp = polygon_set[(20,0)]
  ##transfer getp[ii] for tuple format, account for the offset
  for ii in xrange(len(getp)):
    #print getp[ii]
    tp = [tuple([ np.ceil(getp[ii][jj][0]-bbox[0,0]), np.ceil(getp[ii][jj][1]-bbox[0,1]) ]) for jj in xrange(len(getp[ii]))]
    #print tp
    ImageDraw.Draw(l_img).polygon(tp, outline=1, fill=1)
  #l_img = l_img.transpose(Image.FLIP_TOP_BOTTOM)
  sraf_map = np.asarray(l_img)
  del l_img
  if(debug_function): np.savetxt("sraf_map.txt", sraf_map, fmt='%.1d')

  ###create bitmap for the mask features
  if(debug_function): print 'create bitmap for mask features'
  mask_layer = (pp['TrainLayer'], pp['TrainDtype'])
  l_img = Image.new('L', ( int(np.ceil(bbox[1,0]-bbox[0,0])), int(np.ceil(bbox[1,1]-bbox[0,1])) ))
  getp = polygon_set[mask_layer]
  ##transfer getp[ii] for tuple format, account for the offset
  for ii in xrange(len(getp)):
    #print getp[ii]
    tp = [tuple([ getp[ii][jj][0]-bbox[0,0], getp[ii][jj][1]-bbox[0,1] ]) for jj in xrange(len(getp[ii]))]
    ImageDraw.Draw(l_img).polygon(tp, outline=1, fill=1)
    #print tp
  #l_img = l_img.transpose(Image.FLIP_TOP_BOTTOM)
  mask = np.asarray(l_img)
  del l_img  
  #if(debug_function): np.savetxt("mask.txt", mask, fmt='%.1d' )
  ###bounding box for the mask layer  
  m_llx = np.min([np.min([getp[i][:,0]]) for i in xrange(len(getp))])
  m_urx = np.max([np.max([getp[i][:,0]]) for i in xrange(len(getp))])
  m_lly = np.min([np.min([getp[i][:,1]]) for i in xrange(len(getp))])
  m_ury = np.max([np.max([getp[i][:,1]]) for i in xrange(len(getp))])

  ###create bitmap for reserved opc regions
  l_img = Image.new('L', ( int(np.ceil(bbox[1,0]-bbox[0,0])), int(np.ceil(bbox[1,1]-bbox[0,1])) ))
  getp = polygon_set[(21,0)]
  ##transfer getp[ii] for tuple format, account for the offset
  for ii in xrange(len(getp)):
    #print getp[ii]
    tp = [tuple([ np.ceil(getp[ii][jj][0]-bbox[0,0]), np.ceil(getp[ii][jj][1]-bbox[0,1]) ]) for jj in xrange(len(getp[ii]))]
    #print tp
    ImageDraw.Draw(l_img).polygon(tp, outline=1, fill=1)
  #l_img = l_img.transpose(Image.FLIP_TOP_BOTTOM)
  opc_region = np.asarray(l_img)
  del l_img

  ###create bitmap for reserved sraf regions
  l_img = Image.new('L', ( int(np.ceil(bbox[1,0]-bbox[0,0])), int(np.ceil(bbox[1,1]-bbox[0,1])) ))
  getp = polygon_set[(22,0)]
  ##transfer getp[ii] for tuple format, account for the offset
  for ii in xrange(len(getp)):
    #print getp[ii]
    tp = [tuple([ np.ceil(getp[ii][jj][0]-bbox[0,0]), np.ceil(getp[ii][jj][1]-bbox[0,1]) ]) for jj in xrange(len(getp[ii]))]
    #print tp
    ImageDraw.Draw(l_img).polygon(tp, outline=1, fill=1)
  #l_img = l_img.transpose(Image.FLIP_TOP_BOTTOM)
  sraf_region = np.asarray(l_img)
  del l_img

  ### create meshgrid
  if (pp['ToolMode'] == 'Toshiba'):
    sraf_size = pp['SrafSizeToshiba']
  elif (pp['ToolMode'] == 'Calibre'):
    sraf_size = pp['SrafSizeCalibre']
  else:
    print '###ERROR: the tool mode should be Toshiba or Calibre instead of %s' %(pp['ToolMode'])
  grid_size = pp['GridSize']
  window_x = pp['Window_x']
  window_y = pp['Window_y']
  ### resize the window
  window_x = np.min([window_x, np.ceil((bbox[1,0] - bbox[0,0])/grid_size)*grid_size + 10*grid_size])
  window_y = np.min([window_y, np.ceil((bbox[1,1] - bbox[0,1])/grid_size)*grid_size + 10*grid_size])
  center_x = int(np.ceil((m_llx+m_urx)/2))
  #center_x = int(np.ceil((bbox[0,0]+bbox[1,0])/2))
  center_y = int(np.ceil((m_lly+m_ury)/2))
  #center_y = int(np.ceil((bbox[1,1]+bbox[0,1])/2))
  min_x = int(np.ceil((center_x - window_x / 2.0)/grid_size)*grid_size)
  min_y = int(np.ceil((center_y - window_y / 2.0)/grid_size)*grid_size)
  if(phase=='train'):
    #x_row = int(np.ceil(window_x/grid_size))
    #y_col = int(np.ceil(window_y/grid_size))
    x_row = int(np.ceil(window_x/grid_size/2.0))
    y_col = int(np.ceil(window_y/grid_size/2.0))
  else:
    x_row = int(np.ceil(window_x/grid_size))
    y_col = int(np.ceil(window_y/grid_size))
    
  #print 'window_x = %.1f, window_y = %.1f, center_x = %.1f, center_y = %.1f' %(window_x, window_y, center_x, center_y)
  #print 'mask bounding box: (%.1f, %.1f, %.1f, %.1f)' %(m_llx, m_lly, m_urx, m_ury)
  #print 'min_x = %.1f, min_y = %.1f, x_row = %d, y_col = %d' %(min_x, min_y, x_row, y_col)
  ###extract feature point
  if(debug_function): debug_cell = cell_inst.copy('TOP', exclude_from_global=True)
  #offset_x = bbox[0,0]
  #offset_y = bbox[0,1]
  sraf_data = list()
  usefulness_map = np.zeros((x_row, y_col),dtype=float)
  for ii in xrange(x_row):
    for jj in xrange(y_col):
      llx = ii * grid_size + min_x
      lly = jj * grid_size + min_y
      urx = llx + sraf_size
      ury = lly + sraf_size
      if(debug_function): 
        pg = gdspy.Polygon([(llx, lly),(llx, ury),(urx, ury),(urx, lly)], layer=0, datatype=0)
        debug_cell.add(pg)
      
      ###query mask features
      b_llx = int(llx - bbox[0,0])
      b_urx = int(urx - bbox[0,0])
      b_lly = int(lly - bbox[0,1])
      b_ury = int(ury - bbox[0,1])
      mb_lly = b_lly
      mb_ury = b_ury
      mb_llx = b_llx
      mb_urx = b_urx
      if(b_lly < 0): mb_lly = 0
      if(b_lly > mask.shape[0]): mb_lly = mask.shape[0]
      if(b_ury < 0): mb_ury = 0
      if(b_ury > mask.shape[0]): mb_ury = mask.shape[0]
      if(b_llx < 0): mb_llx = 0
      if(b_llx > mask.shape[1]): mb_llx = mask.shape[1]
      if(b_urx < 0): mb_urx = 0
      if(b_urx > mask.shape[1]): mb_urx = mask.shape[1]
      #mb_lly = np.max([b_lly, 0.0])
      #mb_lly = np.min([b_lly, mask.shape[0]])
      #mb_ury = np.max([b_ury, 0.0])
      #mb_ury = np.min([b_ury, mask.shape[0]])
      #mb_llx = np.max([b_llx, 0.0])
      #mb_llx = np.min([b_llx, mask.shape[1]])
      #mb_urx = np.max([b_urx, 0.0])
      #mb_urx = np.min([b_urx, mask.shape[1]])
      #if(debug_function): 
      #  print mask.shape
      #  print 'from (%d, %d, %d, %d)' %(b_llx, b_lly, b_urx, b_ury)
      #  print 'to (%d, %d, %d, %d)' %(mb_llx, mb_lly, mb_urx, mb_ury)
      ###skip feature point overlapping reserved opc region or not overlapping reserved sraf region
      opc_region_area = np.sum(opc_region[mb_lly:mb_ury+1,:][:,mb_llx:mb_urx+1])
      sraf_region_area = np.sum(sraf_region[mb_lly:mb_ury+1,:][:,mb_llx:mb_urx+1])
      if(opc_region_area > 0 or sraf_region_area == 0): continue
      mask_area = np.sum(mask[mb_lly:mb_ury+1,:][:,mb_llx:mb_urx+1])
      #if(debug_function & (mask_area > 0)):
      #  print 'mask-%d' %mask_area
      #  print mask[mb_lly:mb_ury+1,:][:,mb_llx:mb_urx+1]
      ###feature point determination, account for the offset 
      #mid_x = int(np.ceil(llx+grid_size/2.0)) 
      #mid_y = int(np.ceil(lly+grid_size/2.0)) 
      mid_x = int(np.ceil((llx+urx)/2)) 
      mid_y = int(np.ceil((lly+ury)/2)) 
      ###skip feature point intersection target features
      #if(mask_area > 0): continue
      if(mask_area > 0): 
        weight = 1.0
        continue
      else:
        ###query sraf features
        sraf_area = np.sum(sraf_map[mb_lly:mb_ury+1,:][:,mb_llx:mb_urx+1])
        #if(sraf_area < (sraf_size**2)/4): weight = 0.0
        #if(sraf_area < ((sraf_size)**2/2.0)): weight = 0.0
        if(sraf_area < ((sraf_size-grid_size)**2)): weight = 0.0
        else: 
          if pp['Model']=='LGR' or pp['Model']=='LGR_AdaBoost' or pp['Model']=='SVC': weight = 1.0
          else: weight = np.max(cmap[mb_lly:mb_ury+1,:][:,mb_llx:mb_urx+1])
      sraf_data.append([mid_x, mid_y, weight, ii, jj])
      #print 'feature point (%.1f, %.1f) - %.1f' %(mid_x, mid_y, weight)
      if(debug_function): 
          print 'feature point (%.1f, %.1f) - %.1f' %(mid_x, mid_y, weight)
          #print 'from (%d, %d, %d, %d)' %(b_llx, b_lly, b_urx, b_ury)
          #print 'to (%d, %d, %d, %d)' %(mb_llx, mb_lly, mb_urx, mb_ury)
          #if(weight): print sraf_map[mb_lly:mb_ury+1,:][:,mb_llx:mb_urx+1]
          text = gdspy.Text(text=str(weight),size=5, position=(mid_x, mid_y), layer=0, datatype = 1)
          #print str(text)
          debug_cell.add(text)
      usefulness_map[ii][jj] = weight
  
  if(pp['F_Gsearch']=='OFF'):
    ###output usefulness map
    X, Y = np.meshgrid(range(y_col), range(x_row))
    fig = plt.figure()
    ax1 = fig.add_subplot(121,projection='3d')
    ax1.plot_surface(X, Y, usefulness_map)
    ax2 = fig.add_subplot(122)
    ax2.imshow(usefulness_map)
    fig.savefig(("./result/gds/%s_train.pdf" %cell))
  else: 
    pass

  if(debug_function):
    for ii in xrange(x_row):
      for jj in xrange(y_col):
        if(usefulness_map[ii][jj] <= 0.0): continue
        llx = ii * grid_size + min_x
        lly = jj * grid_size + min_y
        urx = llx + sraf_size
        ury = lly + sraf_size
        down = np.max([ii-1,0])
        up = np.min([ii+1,usefulness_map.shape[0]-1])
        left = np.max([jj-1,0])
        right = np.min([jj+1,usefulness_map.shape[1]-1])
        if (usefulness_map[ii][jj] >= usefulness_map[down][jj] and 
            usefulness_map[ii][jj] >= usefulness_map[up][jj] and 
            usefulness_map[ii][jj] >= usefulness_map[ii][left] and 
            usefulness_map[ii][jj] >= usefulness_map[ii][right]):
          pg = gdspy.Polygon([(llx, lly),(llx, ury),(urx, ury),(urx, lly)], layer=0, datatype=2)
          debug_cell.add(pg)
    gds_prt = gdspy.GdsPrint('debug_cell.gds', unit=1.0e-9, precision=1.0e-9)
    gds_prt.write_cell(debug_cell)
    gds_prt.close()
    quit()

  ###re-arrange the sraf_data
  ###make gauge file 
  ###-> id, gdsx, gdsy, usefulness, index_x, index_y
  ###list2array
  n_data = len(sraf_data)
  a_data = np.zeros((n_data,5), dtype=float)
  if(pp['F_Gsearch']=='OFF'):
    fout = open(fss, 'w')
    fout.write('gauge\t\t\tGDSX\tGDSY\tweight\n')
    for ii in xrange(n_data):
      a_data[ii] = sraf_data[ii]
      fout.write('test\t%d\t%d\t%d\t%.4f\t%d\t%d\n' %(ii+1,a_data[ii,0],a_data[ii,1],a_data[ii,2],a_data[ii,3],a_data[ii,4]))
    fout.close()
  else:
    pass
  
  #print '\tdata size: %d' %len(a_data)
  return a_data


###SRAF data based on pv band simulation
def GetSrafData_PV_unit(pp,gdsii,cell,phase):
  debug_function = 1
  print '\t#SRAF data extraction for cell - %s' %cell

  if(pp['ToolMode']=='Toshiba'):
    print '###ERROR: when using GetSrafData_PV_unit(...), the tool mode should be Calibre instead of %s' %(pp['ToolMode'])
    quit()

  if phase == 'train':
    fmode = 'train'
    fss = "./result/train_feature/" + cell + "_gauge_train_PV.ss"
  elif phase == 'test':
    fmode = 'test'
    fss = "./result/test_feature/" + cell + "_gauge_test_PV.ss"
  else:
    print "### ini file error"
    quit()

  ### Check gauge (if exists, skip SRAF data extraction)
  if(pp['F_Gsearch']=='OFF') and (pp['StoreOption']=='ON'):
    if os.path.isfile(fss):
      ### Read gauge file
      a_t = np.genfromtxt(fss,skip_header=1)
      att = [[a_t[i,2],a_t[i,3],a_t[i,4]] for i in xrange(len(a_t))]
      a_data = np.array(att,dtype=int)
      print '\t#Gauge file for %s exists (SRAF data extraction skipped)' %cell
      return a_data
    else:
      pass
  else:
    pass
  
  cell_inst = gdsii.extract(cell)
  bbox = cell_inst.get_bounding_box()
  #rounding the bbox
  bbox[0,0] = int(np.floor(bbox[0,0]))
  bbox[0,1] = int(np.floor(bbox[0,1]))
  bbox[1,0] = int(np.ceil(bbox[1,0]))
  bbox[1,1] = int(np.ceil(bbox[1,1]))
  #if(debug_function): print "bounding box - (%.1f, %.1f, %.1f, %.1f)" %(bbox[0,0],bbox[0,1],bbox[1,0],bbox[1,1])
  #print "bounding box - (%.1f, %.1f, %.1f, %.1f)" %(bbox[0,0],bbox[0,1],bbox[1,0],bbox[1,1])
  ### polygons
  polygon_set = cell_inst.get_polygons(True)
  
  ###create bitmap for the mask features
  if(debug_function): print 'create bitmap for mask features'
  mask_layer = (pp['TrainLayer'], pp['TrainDtype'])
  l_img = Image.new('L', ( int(np.ceil(bbox[1,0]-bbox[0,0])), int(np.ceil(bbox[1,1]-bbox[0,1])) ))
  getp = polygon_set[mask_layer]
  ##transfer getp[ii] for tuple format, account for the offset
  for ii in xrange(len(getp)):
    #print getp[ii]
    tp = [tuple([ getp[ii][jj][0]-bbox[0,0], getp[ii][jj][1]-bbox[0,1] ]) for jj in xrange(len(getp[ii]))]
    ImageDraw.Draw(l_img).polygon(tp, outline=1, fill=1)
    #print tp
  #l_img = l_img.transpose(Image.FLIP_TOP_BOTTOM)
  mask = np.asarray(l_img)
  del l_img  
  #if(debug_function): np.savetxt("mask.txt", mask, fmt='%.1d' )
  ###bounding box for the mask layer  
  m_llx = np.min([np.min([getp[i][:,0]]) for i in xrange(len(getp))])
  m_urx = np.max([np.max([getp[i][:,0]]) for i in xrange(len(getp))])
  m_lly = np.min([np.min([getp[i][:,1]]) for i in xrange(len(getp))])
  m_ury = np.max([np.max([getp[i][:,1]]) for i in xrange(len(getp))])

  ###create bitmap for reserved opc regions
  l_img = Image.new('L', ( int(np.ceil(bbox[1,0]-bbox[0,0])), int(np.ceil(bbox[1,1]-bbox[0,1])) ))
  getp = polygon_set[(21,0)]
  ##transfer getp[ii] for tuple format, account for the offset
  for ii in xrange(len(getp)):
    #print getp[ii]
    tp = [tuple([ np.ceil(getp[ii][jj][0]-bbox[0,0]), np.ceil(getp[ii][jj][1]-bbox[0,1]) ]) for jj in xrange(len(getp[ii]))]
    #print tp
    ImageDraw.Draw(l_img).polygon(tp, outline=1, fill=1)
  #l_img = l_img.transpose(Image.FLIP_TOP_BOTTOM)
  opc_region = np.asarray(l_img)
  del l_img

  ###create bitmap for reserved sraf regions
  l_img = Image.new('L', ( int(np.ceil(bbox[1,0]-bbox[0,0])), int(np.ceil(bbox[1,1]-bbox[0,1])) ))
  getp = polygon_set[(22,0)]
  ##transfer getp[ii] for tuple format, account for the offset
  for ii in xrange(len(getp)):
    #print getp[ii]
    tp = [tuple([ np.ceil(getp[ii][jj][0]-bbox[0,0]), np.ceil(getp[ii][jj][1]-bbox[0,1]) ]) for jj in xrange(len(getp[ii]))]
    #print tp
    ImageDraw.Draw(l_img).polygon(tp, outline=1, fill=1)
  #l_img = l_img.transpose(Image.FLIP_TOP_BOTTOM)
  sraf_region = np.asarray(l_img)
  del l_img

  ### create meshgrid
  if (pp['ToolMode'] == 'Toshiba'):
    sraf_size = pp['SrafSizeToshiba']
  elif (pp['ToolMode'] == 'Calibre'):
    sraf_size = pp['SrafSizeCalibre']
  else:
    print '###ERROR: the tool mode should be Toshiba or Calibre instead of %s' %(pp['ToolMode'])
  grid_size = pp['GridSize']
  window_x = pp['Window_x']
  window_y = pp['Window_y']
  ### resize the window
  window_x = np.min([window_x, np.ceil((bbox[1,0] - bbox[0,0])/grid_size)*grid_size + 10*grid_size])
  window_y = np.min([window_y, np.ceil((bbox[1,1] - bbox[0,1])/grid_size)*grid_size + 10*grid_size])
  center_x = int(np.ceil((m_llx+m_urx)/2))
  #center_x = int(np.ceil((bbox[0,0]+bbox[1,0])/2))
  center_y = int(np.ceil((m_lly+m_ury)/2))
  #center_y = int(np.ceil((bbox[1,1]+bbox[0,1])/2))
  min_x = int(np.ceil((center_x - window_x / 2.0)/grid_size)*grid_size)
  min_y = int(np.ceil((center_y - window_y / 2.0)/grid_size)*grid_size)
  #min_x = int(np.ceil(center_x - window_x / 2))
  #min_y = int(np.ceil(center_y - window_y / 2))
  if(phase=='train'):
    x_row = int(np.ceil(window_x/grid_size))
    y_col = int(np.ceil(window_y/grid_size))
    #x_row = int(np.ceil(window_x/grid_size/2.0))
    #y_col = int(np.ceil(window_y/grid_size/2.0))
  else:
    x_row = int(np.ceil(window_x/grid_size))
    y_col = int(np.ceil(window_y/grid_size))
    
  #print 'window_x = %.1f, window_y = %.1f, center_x = %.1f, center_y = %.1f' %(window_x, window_y, center_x, center_y)
  #print 'mask bounding box: (%.1f, %.1f, %.1f, %.1f)' %(m_llx, m_lly, m_urx, m_ury)
  #print 'min_x = %.1f, min_y = %.1f, x_row = %d, y_col = %d' %(min_x, min_y, x_row, y_col)
  ###extract feature point
  if(debug_function): debug_cell = cell_inst.copy('TOP', exclude_from_global=True)
  #offset_x = bbox[0,0]
  #offset_y = bbox[0,1]
  sraf_data = list()
  usefulness_map = np.zeros((x_row, y_col),dtype=float)
  clip_index = 0
  clip_list = []
  index_list = []
  for ii in xrange(x_row):
    for jj in xrange(y_col):
      clip_index += 1
      llx = ii * grid_size + min_x
      lly = jj * grid_size + min_y
      urx = llx + sraf_size
      ury = lly + sraf_size
      if(debug_function): 
        pg = gdspy.Polygon([(llx, lly),(llx, ury),(urx, ury),(urx, lly)], layer=0, datatype=0)
        debug_cell.add(pg)
      
      ###query mask features
      b_llx = int(llx - bbox[0,0])
      b_urx = int(urx - bbox[0,0])
      b_lly = int(lly - bbox[0,1])
      b_ury = int(ury - bbox[0,1])
      mb_lly = b_lly
      mb_ury = b_ury
      mb_llx = b_llx
      mb_urx = b_urx
      if(b_lly < 0): mb_lly = 0
      if(b_lly > mask.shape[0]): mb_lly = mask.shape[0]
      if(b_ury < 0): mb_ury = 0
      if(b_ury > mask.shape[0]): mb_ury = mask.shape[0]
      if(b_llx < 0): mb_llx = 0
      if(b_llx > mask.shape[1]): mb_llx = mask.shape[1]
      if(b_urx < 0): mb_urx = 0
      if(b_urx > mask.shape[1]): mb_urx = mask.shape[1]
      #if(debug_function): 
      #  print mask.shape
      #  print 'from (%d, %d, %d, %d)' %(b_llx, b_lly, b_urx, b_ury)
      #  print 'to (%d, %d, %d, %d)' %(mb_llx, mb_lly, mb_urx, mb_ury)
      ###skip feature point overlapping reserved opc region or not overlapping reserved sraf region
      opc_region_area = np.sum(opc_region[mb_lly:mb_ury+1,:][:,mb_llx:mb_urx+1])
      sraf_region_area = np.sum(sraf_region[mb_lly:mb_ury+1,:][:,mb_llx:mb_urx+1])
      if(opc_region_area > 0 or sraf_region_area == 0): continue
      mask_area = np.sum(mask[mb_lly:mb_ury+1,:][:,mb_llx:mb_urx+1])
      #if(debug_function & (mask_area > 0)):
      #  print 'mask-%d' %mask_area
      #  print mask[mb_lly:mb_ury+1,:][:,mb_llx:mb_urx+1]
      ###feature point determination, account for the offset 
      #mid_x = int(np.ceil(llx+grid_size/2.0)) 
      #mid_y = int(np.ceil(lly+grid_size/2.0)) 
      mid_x = int(np.ceil((llx+urx)/2)) 
      mid_y = int(np.ceil((lly+ury)/2)) 

      ###Generate clip with sraf inserted 
      clip_cell = gdspy.Cell('TOP',exclude_from_global=True)
      for layer_t in polygon_set:
        for pg in polygon_set[layer_t]:
          clip_cell.add(gdspy.Polygon(pg/1000.0,layer=layer_t[0],datatype=layer_t[1]))
      clip_cell.add( gdspy.Polygon([(llx/1000.0, lly/1000.0),(llx/1000.0, ury/1000.0),(urx/1000.0, ury/1000.0),(urx/1000.0, lly/1000.0)], layer=0, datatype=0))
      ###output SRAFed clip
      clip = cell + "_clip" + str(clip_index)
      clip_list.append(clip)
      index_list.append([ii, jj])
      gds_prt = gdspy.GdsPrint(('./result/clips/%s.gds' %clip), unit=1.0e-6, precision=1.0e-9)
      gds_prt.write_cell(clip_cell)
      gdspy.Cell.cell_dict.clear()
      gds_prt.close()
      weight = 0.0
      sraf_data.append([mid_x, mid_y, weight, ii, jj])
      #print 'feature point (%.1f, %.1f) - %.1f' %(mid_x, mid_y, weight)
      #if(debug_function): 
          #print 'feature point (%.1f, %.1f) - %.1f' %(mid_x, mid_y, weight)
          #print 'from (%d, %d, %d, %d)' %(b_llx, b_lly, b_urx, b_ury)
          #print 'to (%d, %d, %d, %d)' %(mb_llx, mb_lly, mb_urx, mb_ury)
          #if(weight): print sraf_map[mb_lly:mb_ury+1,:][:,mb_llx:mb_urx+1]
          #text = gdspy.Text(text=str(weight),size=5, position=(mid_x, mid_y), layer=0, datatype = 1)
          #print str(text)
          #debug_cell.add(text)
      #usefulness_map[ii][jj] = weight
  
  clip = cell + "_org"
  clip_cell = gdspy.Cell('TOP',exclude_from_global=True)
  for layer_t in polygon_set:
    for pg in polygon_set[layer_t]:
      clip_cell.add(gdspy.Polygon(pg/1000.0,layer=layer_t[0],datatype=layer_t[1]))
  gds_prt = gdspy.GdsPrint(('./result/clips/%s.gds' %clip), unit=1.0e-6, precision=1.0e-9)
  gds_prt.write_cell(clip_cell)
  gdspy.Cell.cell_dict.clear()
  gds_prt.close()
  weight_0 = PVBandClip(clip,wsraf=False)

  ###Parallel processing the PV band simulation
  #mp = Pool(4)
  #weight_list = mp.map(PVBandClip,clip_list)
  weight_list = []
  for ii in range(len(clip_list)): 
    _weight = PVBandClip(clip_list[ii],wsraf=True)
    weight_list.append(_weight)
    print '%d out of %d - %.4f' %(ii,len(clip_list),(weight_0 - _weight)/weight_0*100.0)
  for ii in range(len(clip_list)):
    sraf_data[ii][2] = (weight_0 - weight_list[ii])/weight_0*100.0
    usefulness_map[index_list[ii][0]][index_list[ii][1]] = sraf_data[ii][2]
  
  if(pp['F_Gsearch']=='OFF'):
    ###output usefulness map
    X, Y = np.meshgrid(range(y_col), range(x_row))
    fig = plt.figure()
    ax1 = fig.add_subplot(121,projection='3d')
    ax1.plot_surface(X, Y, usefulness_map)
    ax2 = fig.add_subplot(122)
    ax2.imshow(usefulness_map)
    fig.savefig(("./result/gds/%s_train.pdf" %cell))
  else: 
    pass

  if(debug_function):
    for ii in xrange(x_row):
      for jj in xrange(y_col):
        if(usefulness_map[ii][jj] <= 0.0): continue
        llx = ii * grid_size + min_x
        lly = jj * grid_size + min_y
        urx = llx + sraf_size
        ury = lly + sraf_size
        if (usefulness_map[ii][jj] <= 0): continue 
        down = np.max([ii-1,0])
        up = np.min([ii+1,usefulness_map.shape[0]-1])
        left = np.max([jj-1,0])
        right = np.min([jj+1,usefulness_map.shape[1]-1])
        if (usefulness_map[ii][jj] >= usefulness_map[down][jj] and 
            usefulness_map[ii][jj] >= usefulness_map[up][jj] and 
            usefulness_map[ii][jj] >= usefulness_map[ii][left] and 
            usefulness_map[ii][jj] >= usefulness_map[ii][right]):
          pg = gdspy.Polygon([(llx, lly),(llx, ury),(urx, ury),(urx, lly)], layer=0, datatype=2)
          debug_cell.add(pg)
    gds_prt = gdspy.GdsPrint('debug_cell.gds', unit=1.0e-9, precision=1.0e-9)
    gds_prt.write_cell(debug_cell)
    gds_prt.close()
    #quit()

  ###re-arrange the sraf_data
  ###make gauge file 
  ###-> id, gdsx, gdsy, usefulness
  ###list2array
  n_data = len(sraf_data)
  a_data = np.zeros((n_data,5), dtype=float)
  if(pp['F_Gsearch']=='OFF'):
    fout = open(fss, 'w')
    fout.write('gauge\t\t\tGDSX\tGDSY\tweight\n')
    for ii in xrange(n_data):
      a_data[ii] = sraf_data[ii]
      fout.write('test\t%d\t%d\t%d\t%.4f\t%d\t%d\n' %(ii+1,a_data[ii,0],a_data[ii,1],a_data[ii,2],a_data[ii,3],a_data[ii,4]))
    fout.close()
  else:
    pass
  
  if(debug_function): quit()

  return a_data


###SRAF insertion based on coherence map prediction
def InsertSraf(pp,gdsii_list,cell_list,ss_te_dict,fvec_te,yh_te,th,phase):
  ###loop cell_list
  gds_index = 0
  fp_index = 0
  xy_dict = {}
  for item in cell_list:
    print '\tInsert SRAF features for cell %s' %item
    fp_index,xy = InsertSraf_unit_fast(pp,gdsii_list[gds_index],item,ss_te_dict[item],fvec_te,yh_te,fp_index,th,phase)
    xy_dict[item] = xy
    #for next gds
    gds_index += 1

  return xy_dict

def InsertSraf_unit(pp,gdsii,cell,ss_te,fvec_te,yh_te,fp_index,th,phase):
  if phase == 'train':
    mask_layer = (pp['TrainLayer'], pp['TrainDtype'])
  elif phase == 'test':
    mask_layer = (pp['TestLayer'], pp['TestDtype'])
  else:
    print '\t #Error: InsertSraf(...) should be used in train or test phase instead of %s' %phase
    quit()  

  debug_function = 0

  if (pp['ToolMode'] == 'Toshiba'):
    sraf_size = pp['SrafSizeToshiba']
  elif (pp['ToolMode'] == 'Calibre'):
    sraf_size = pp['SrafSizeCalibre']
  else:
    print '###ERROR: the tool mode should be Toshiba or Calibre instead of %s' %(pp['ToolMode'])
  grid_size = pp['GridSize']
  cell_inst = gdsii.extract(cell)
  bbox = cell_inst.get_bounding_box()
  #rounding the bbox
  bbox[0,0] = int(np.floor(bbox[0,0]))
  bbox[0,1] = int(np.floor(bbox[0,1]))
  bbox[1,0] = int(np.ceil(bbox[1,0]))
  bbox[1,1] = int(np.ceil(bbox[1,1]))
  #print "bounding box - (%.1f, %.1f, %.1f, %.1f)" %(bbox[0,0],bbox[0,1],bbox[1,0],bbox[1,1])
  ### polygons
  polygon_set = cell_inst.get_polygons(True)
  getp = polygon_set[mask_layer]

  l_img = Image.new('L', ( int(np.ceil(bbox[1,0]-bbox[0,0])), int(np.ceil(bbox[1,1]-bbox[0,1])) ))
  ##transfer getp[ii] for tuple format, account for the offset
  for ii in xrange(len(getp)):
    #print getp[ii]
    tp = [tuple([ getp[ii][jj][0]-bbox[0,0], getp[ii][jj][1]-bbox[0,1] ]) for jj in xrange(len(getp[ii]))]
    ImageDraw.Draw(l_img).polygon(tp, outline=1, fill=1)
    #print tp
  #l_img = l_img.transpose(Image.FLIP_TOP_BOTTOM)
  mask = np.asarray(l_img)
  del l_img  

  ###mask layer (target feature) center points - for PV band simulation
  centers_x = np.array([np.min([getp[i][:,0]]) for i in xrange(len(getp))]) + np.array([np.max([getp[i][:,0]]) for i in xrange(len(getp))])
  centers_y = np.array([np.min([getp[i][:,1]]) for i in xrange(len(getp))]) + np.array([np.max([getp[i][:,1]]) for i in xrange(len(getp))])
  xy = np.array([np.array([centers_x[ii]/2000.0,centers_y[ii]/2000.0]) for ii in range(len(centers_x))])
  fout = open(('./result/sample/%s_sample.txt' %cell),'w')
  fout.write('X\tY\n')
  for ii in range(len(centers_x)): fout.write('%.3f\t%.3f\n' %(centers_x[ii]/2000.0, centers_y[ii]/2000.0))
  fout.close()

  ###create bitmap for reserved opc regions
  l_img = Image.new('L', ( int(np.ceil(bbox[1,0]-bbox[0,0])), int(np.ceil(bbox[1,1]-bbox[0,1])) ))
  getp = polygon_set[(21,0)]
  ##transfer getp[ii] for tuple format, account for the offset
  for ii in xrange(len(getp)):
    #print getp[ii]
    tp = [tuple([ np.ceil(getp[ii][jj][0]-bbox[0,0]), np.ceil(getp[ii][jj][1]-bbox[0,1]) ]) for jj in xrange(len(getp[ii]))]
    #print tp
    ImageDraw.Draw(l_img).polygon(tp, outline=1, fill=1)
  #l_img = l_img.transpose(Image.FLIP_TOP_BOTTOM)
  opc_region = np.asarray(l_img)
  del l_img

  ###create bitmap for reserved sraf regions
  l_img = Image.new('L', ( int(np.ceil(bbox[1,0]-bbox[0,0])), int(np.ceil(bbox[1,1]-bbox[0,1])) ))
  getp = polygon_set[(22,0)]
  ##transfer getp[ii] for tuple format, account for the offset
  for ii in xrange(len(getp)):
    #print getp[ii]
    tp = [tuple([ np.ceil(getp[ii][jj][0]-bbox[0,0]), np.ceil(getp[ii][jj][1]-bbox[0,1]) ]) for jj in xrange(len(getp[ii]))]
    #print tp
    ImageDraw.Draw(l_img).polygon(tp, outline=1, fill=1)
  #l_img = l_img.transpose(Image.FLIP_TOP_BOTTOM)
  sraf_region = np.asarray(l_img)
  del l_img
  
  ###bounding box for the mask layer  
  m_llx = np.min([np.min([getp[i][:,0]]) for i in xrange(len(getp))])
  m_urx = np.max([np.max([getp[i][:,0]]) for i in xrange(len(getp))])
  m_lly = np.min([np.min([getp[i][:,1]]) for i in xrange(len(getp))])
  m_ury = np.max([np.max([getp[i][:,1]]) for i in xrange(len(getp))])

  ### resize the window
  window_x = pp['Window_x']
  window_y = pp['Window_y']
  window_x = np.min([window_x, np.ceil((bbox[1,0] - bbox[0,0])/grid_size)*grid_size + 10*grid_size])
  window_y = np.min([window_y, np.ceil((bbox[1,1] - bbox[0,1])/grid_size)*grid_size + 10*grid_size])
  center_x = int(np.ceil((m_llx+m_urx)/2))
  center_y = int(np.ceil((m_lly+m_ury)/2))
  min_x = int(np.ceil((center_x - window_x / 2.0)/grid_size)*grid_size)
  min_y = int(np.ceil((center_y - window_y / 2.0)/grid_size)*grid_size)
  #min_x = int(np.ceil(center_x - window_x / 2))
  #min_y = int(np.ceil(center_y - window_y / 2))
  x_row = int(np.ceil(window_x/grid_size))
  y_col = int(np.ceil(window_y/grid_size))

  #print 'window_x = %.1f, window_y = %.1f, center_x = %.1f, center_y = %.1f' %(window_x, window_y, center_x, center_y)
  #print 'mask bounding box: (%.1f, %.1f, %.1f, %.1f)' %(m_llx, m_lly, m_urx, m_ury)
  #print 'min_x = %.1f, min_y = %.1f, x_row = %d, y_col = %d' %(min_x, min_y, x_row, y_col)
  #sraf_cell = cell_inst.copy('TOP', exclude_from_global=True)
  sraf_cell = gdspy.Cell('TOP',exclude_from_global=True)
  for layer_t in polygon_set:
    for pg in polygon_set[layer_t]:
      sraf_cell.add(gdspy.Polygon(pg/1000.0,layer=layer_t[0],datatype=layer_t[1]))
  ### create meshgrid
  ss_index = 0
  usefulness_map = np.zeros((x_row,y_col),dtype=float)
  label_map = np.zeros((x_row,y_col),dtype=int)
  for ii in xrange(x_row):
    for jj in xrange(y_col):
      llx = ii * grid_size + min_x
      lly = jj * grid_size + min_y
      urx = llx + sraf_size
      ury = lly + sraf_size
      ###query mask features
      b_llx = int(llx - bbox[0,0])
      b_urx = int(urx - bbox[0,0])
      b_lly = int(lly - bbox[0,1])
      b_ury = int(ury - bbox[0,1])
      mb_lly = b_lly
      mb_ury = b_ury
      mb_llx = b_llx
      mb_urx = b_urx
      if(b_lly < 0): mb_lly = 0
      if(b_lly > mask.shape[0]): mb_lly = mask.shape[0]
      if(b_ury < 0): mb_ury = 0
      if(b_ury > mask.shape[0]): mb_ury = mask.shape[0]
      if(b_llx < 0): mb_llx = 0
      if(b_llx > mask.shape[1]): mb_llx = mask.shape[1]
      if(b_urx < 0): mb_urx = 0
      if(b_urx > mask.shape[1]): mb_urx = mask.shape[1]
      ###skip feature point overlapping reserved opc region or not overlapping reserved sraf region
      opc_region_area = np.sum(opc_region[mb_lly:mb_ury+1,:][:,mb_llx:mb_urx+1])
      sraf_region_area = np.sum(sraf_region[mb_lly:mb_ury+1,:][:,mb_llx:mb_urx+1])
      if(opc_region_area > 0 or sraf_region_area == 0): continue
      ###skip feature point intersection target features
      mask_area = np.sum(mask[mb_lly:mb_ury+1,:][:,mb_llx:mb_urx+1])
      if(mask_area > 0): continue
      ### feature point
      #mid_x = int(np.ceil(llx+grid_size/2.0)) 
      #mid_y = int(np.ceil(lly+grid_size/2.0)) 
      mid_x = int(np.ceil((llx+urx)/2)) 
      mid_y = int(np.ceil((lly+ury)/2)) 
      if pp['Model'] == 'LGR' or pp['Model'] == 'LGR_AdaBoost' or pp['Model'] == 'SVC':
        ###For logistic regression model, extract the probability as the usefulness
        usefulness_map[ii][jj] = th[fp_index][1]
        label_map[ii][jj] = yh_te[0][fp_index]
      else:
        usefulness_map[ii][jj] = yh_te[0][fp_index]
      #if(yh_te[0][fp_index] > th and np.sum(fvec_te[fp_index]) > 0):
      #  pg = gdspy.Polygon([(llx, lly),(llx, ury),(urx, ury),(urx, lly)], layer=0, datatype=0)
      #  sraf_cell.add(pg)
      #if(debug_function): 
      #    print 'test feature point    (%.1f, %.1f) - %.4f' %(ss_te[ss_index][0], ss_te[ss_index][1], ss_te[ss_index][2])
      #    print 'predict feature point (%.1f, %.1f) - %.4f\n' %(mid_x, mid_y, yh_te[0][fp_index])
      #    #text = gdspy.Text(text=str(yh_te[0][fp_index]),size=5, position=(mid_x, mid_y), layer=0, datatype = 1)
      #    #sraf_cell.add(text)
      ss_index += 1
      fp_index += 1

  ###insert sraf features
  for ii in xrange(x_row):
    for jj in xrange(y_col):
      llx = ii * grid_size + min_x
      lly = jj * grid_size + min_y
      urx = llx + sraf_size
      ury = lly + sraf_size
      ###ignore none sraf features
      if pp['Model'] == 'LGR' or pp['Model'] == 'LGR_AdaBoost' or pp['Model'] == 'SVC':
        if label_map[ii][jj] == 0: continue
      else:
        if (usefulness_map[ii][jj] < th): continue
      down = np.max([ii-1,0])
      up = np.min([ii+1,usefulness_map.shape[0]-1])
      left = np.max([jj-1,0])
      right = np.min([jj+1,usefulness_map.shape[1]-1])
      if (usefulness_map[ii][jj] >= usefulness_map[down][jj] and 
          usefulness_map[ii][jj] >= usefulness_map[up][jj] and 
          usefulness_map[ii][jj] >= usefulness_map[ii][left] and 
          usefulness_map[ii][jj] >= usefulness_map[ii][right]):
        llx = llx/1000.0
        lly = lly/1000.0
        urx = urx/1000.0
        ury = ury/1000.0
        pg = gdspy.Polygon([(llx, lly),(llx, ury),(urx, ury),(urx, lly)], layer=0, datatype=0)
        sraf_cell.add(pg)
      else: 
        pass
        #pg = gdspy.Polygon([(llx, lly),(llx, ury),(urx, ury),(urx, lly)], layer=0, datatype=0)
        #sraf_cell.add(pg)
          
  ###output usefulness map
  X, Y = np.meshgrid(range(y_col), range(x_row))
  fig = plt.figure()
  ax1 = fig.add_subplot(121,projection='3d')
  ax1.plot_surface(X, Y, usefulness_map)
  ax2 = fig.add_subplot(122)
  ax2.imshow(usefulness_map)
  fig.savefig(("./result/gds/%s_test.pdf" %cell))

  ###output SRAFed layout
  gds_prt = gdspy.GdsPrint(('./result/gds/%s_sraf.gds' %cell), unit=1.0e-6, precision=1.0e-9)
  gds_prt.write_cell(sraf_cell)
  gdspy.Cell.cell_dict.clear()
  gds_prt.close()
  
  return fp_index,xy

def InsertSraf_unit_fast(pp,gdsii,cell,ss_te,fvec_te,yh_te,fp_index,th,phase):
  output = True
  if phase == 'train':
    mask_layer = (pp['TrainLayer'], pp['TrainDtype'])
  elif phase == 'test':
    mask_layer = (pp['TestLayer'], pp['TestDtype'])
  else:
    print '\t #Error: InsertSraf(...) should be used in train or test phase instead of %s' %phase
    quit()  

  if (pp['ToolMode'] == 'Toshiba'):
    sraf_size = pp['SrafSizeToshiba']
  elif (pp['ToolMode'] == 'Calibre'):
    sraf_size = pp['SrafSizeCalibre']
  else:
    print '###ERROR: the tool mode should be Toshiba or Calibre instead of %s' %(pp['ToolMode'])
  grid_size = pp['GridSize']
  cell_inst = gdsii.extract(cell)
  bbox = cell_inst.get_bounding_box()
  #rounding the bbox
  bbox[0,0] = int(np.floor(bbox[0,0]))
  bbox[0,1] = int(np.floor(bbox[0,1]))
  bbox[1,0] = int(np.ceil(bbox[1,0]))
  bbox[1,1] = int(np.ceil(bbox[1,1]))
  #print "bounding box - (%.1f, %.1f, %.1f, %.1f)" %(bbox[0,0],bbox[0,1],bbox[1,0],bbox[1,1])
  ### polygons
  polygon_set = cell_inst.get_polygons(True)
  getp = polygon_set[mask_layer]

  ###bounding box for the mask layer  
  m_llx = np.min([np.min([getp[i][:,0]]) for i in xrange(len(getp))])
  m_urx = np.max([np.max([getp[i][:,0]]) for i in xrange(len(getp))])
  m_lly = np.min([np.min([getp[i][:,1]]) for i in xrange(len(getp))])
  m_ury = np.max([np.max([getp[i][:,1]]) for i in xrange(len(getp))])

  ### resize the window
  window_x = pp['Window_x']
  window_y = pp['Window_y']
  window_x = np.min([window_x, np.ceil((bbox[1,0] - bbox[0,0])/grid_size)*grid_size + 10*grid_size])
  window_y = np.min([window_y, np.ceil((bbox[1,1] - bbox[0,1])/grid_size)*grid_size + 10*grid_size])
  center_x = int(np.ceil((m_llx+m_urx)/2))
  center_y = int(np.ceil((m_lly+m_ury)/2))
  min_x = int(np.ceil((center_x - window_x / 2.0)/grid_size)*grid_size)
  min_y = int(np.ceil((center_y - window_y / 2.0)/grid_size)*grid_size)
  #min_x = int(np.ceil(center_x - window_x / 2))
  #min_y = int(np.ceil(center_y - window_y / 2))
  x_row = int(np.ceil(window_x/grid_size))
  y_col = int(np.ceil(window_y/grid_size))

  ###mask layer (target feature) center points - for PV band simulation
  centers_x = np.array([np.min([getp[i][:,0]]) for i in xrange(len(getp))]) + np.array([np.max([getp[i][:,0]]) for i in xrange(len(getp))])
  centers_y = np.array([np.min([getp[i][:,1]]) for i in xrange(len(getp))]) + np.array([np.max([getp[i][:,1]]) for i in xrange(len(getp))])
  xy = np.array([np.array([centers_x[ii]/2000.0,centers_y[ii]/2000.0]) for ii in range(len(centers_x))])

  ###create sraf  
  #fp_index, srafs = insertion(ss_te,fp_index,yh_te,th,x_row,y_col,grid_size,sraf_size,min_x,min_y,pp['Model'])
  srafs = []
  tt_insertion = time.time()
  fp_index = cinsertion(ss_te,len(ss_te),fp_index,yh_te,th,x_row,y_col,grid_size,sraf_size,min_x,min_y,pp['Model'],srafs)
  print "SRAF insertion time: %.4f" %(time.time() - tt_insertion)

  if output:
    ###create srafed cell
    sraf_cell = gdspy.Cell('TOP',exclude_from_global=True)
    for layer_t in polygon_set:
      for pg in polygon_set[layer_t]:
        sraf_cell.add(gdspy.Polygon(pg/1000.0,layer=layer_t[0],datatype=layer_t[1]))

    for pg in srafs:
      sraf_cell.add(gdspy.Polygon(pg,layer=0,datatype=0))

    ###output usefulness map
    #X, Y = np.meshgrid(range(y_col), range(x_row))
    #fig = plt.figure()
    #ax1 = fig.add_subplot(121,projection='3d')
    #ax1.plot_surface(X, Y, usefulness_map)
    #ax2 = fig.add_subplot(122)
    #ax2.imshow(usefulness_map)
    #fig.savefig(("./result/gds/%s_test.pdf" %cell))

    ###output SRAFed layout
    gds_prt = gdspy.GdsPrint(('./result/gds/%s_sraf.gds' %cell), unit=1.0e-6, precision=1.0e-9)
    gds_prt.write_cell(sraf_cell)
    gdspy.Cell.cell_dict.clear()
    gds_prt.close()
    
  return fp_index,xy

### seperate for loop for SRAF feature insertion
def insertion(ss_te,fp_index,yh_te,th,x_row,y_col,grid_size,sraf_size,min_x,min_y,model):
  ### create meshgrid
  srafs = []
  ss_index = 0
  usefulness_map = np.zeros((x_row,y_col),dtype=float)
  label_map = np.zeros((x_row,y_col),dtype=int)
  for elem in ss_te:
    ii = int(elem[3])
    jj = int(elem[4])
    llx = ii * grid_size + min_x
    lly = jj * grid_size + min_y
    urx = llx + sraf_size
    ury = lly + sraf_size
    ### feature point
    #mid_x = int(np.ceil(llx+grid_size/2.0)) 
    #mid_y = int(np.ceil(lly+grid_size/2.0)) 
    mid_x = int(np.ceil((llx+urx)/2)) 
    mid_y = int(np.ceil((lly+ury)/2)) 
    if model == 'LGR' or model == 'LGR_AdaBoost' or model == 'SVC':
      ###For logistic regression model, extract the probability as the usefulness
      usefulness_map[ii,jj] = th[fp_index][1]
      label_map[ii,jj] = yh_te[0][fp_index]
    else:
      usefulness_map[ii,jj] = yh_te[0][fp_index]
    ss_index += 1
    fp_index += 1

  ###insert sraf features
  for ii in xrange(x_row):
    for jj in xrange(y_col):
      llx = ii * grid_size + min_x
      lly = jj * grid_size + min_y
      urx = llx + sraf_size
      ury = lly + sraf_size
      ###ignore none sraf features
      if model == 'LGR' or model == 'LGR_AdaBoost' or model == 'SVC':
        if label_map[ii,jj] == 0: continue
      else:
        if (usefulness_map[ii,jj] < th): continue
      down = np.max([ii-1,0])
      up = np.min([ii+1,usefulness_map.shape[0]-1])
      left = np.max([jj-1,0])
      right = np.min([jj+1,usefulness_map.shape[1]-1])
      if (usefulness_map[ii,jj] >= usefulness_map[down,jj] and 
          usefulness_map[ii,jj] >= usefulness_map[up,jj] and 
          usefulness_map[ii,jj] >= usefulness_map[ii,left] and 
          usefulness_map[ii,jj] >= usefulness_map[ii,right]):
        llx = llx/1000.0
        lly = lly/1000.0
        urx = urx/1000.0
        ury = ury/1000.0
        #pg = gdspy.Polygon([(llx, lly),(llx, ury),(urx, ury),(urx, lly)], layer=0, datatype=0)
        #sraf_cell.add(pg)
        srafs.append([(llx, lly),(llx, ury),(urx, ury),(urx, lly)])
      else: 
        pass
  return fp_index, srafs

###SRAF insertion based on coherence map prediction
def InsertSraf_CM_all(pp,gdsii_list,cell_list,ss_te_dict,fvec_te,yh_te,th,phase):
  if phase == 'train':
    mask_layer = (pp['TrainLayer'], pp['TrainDtype'])
  elif phase == 'test':
    mask_layer = (pp['TestLayer'], pp['TestDtype'])
  else:
    print '\t #Error: InsertSraf(...) should be used in train or test phase instead of %s' %phase
    quit()  

  debug_function = 0

  if (pp['ToolMode'] == 'Toshiba'):
    sraf_size = pp['SrafSizeToshiba']
  elif (pp['ToolMode'] == 'Calibre'):
    sraf_size = pp['SrafSizeCalibre']
  else:
    print '###ERROR: the tool mode should be Toshiba or Calibre instead of %s' %(pp['ToolMode'])
  grid_size = pp['GridSize']
  ###loop cell_list
  gds_index = 0
  fp_index = 0
  for item in cell_list:
    print 'Insert SRAF features for cell %s.' %item
    cell_inst = gdsii_list[gds_index].extract(item)
    #for next gds
    gds_index += 1
    bbox = cell_inst.get_bounding_box()
    #rounding the bbox
    bbox[0,0] = int(np.floor(bbox[0,0]))
    bbox[0,1] = int(np.floor(bbox[0,1]))
    bbox[1,0] = int(np.ceil(bbox[1,0]))
    bbox[1,1] = int(np.ceil(bbox[1,1]))
    #print "bounding box - (%.1f, %.1f, %.1f, %.1f)" %(bbox[0,0],bbox[0,1],bbox[1,0],bbox[1,1])
    ### polygons
    polygon_set = cell_inst.get_polygons(True)
    getp = polygon_set[mask_layer]
  
    l_img = Image.new('L', ( int(np.ceil(bbox[1,0]-bbox[0,0])), int(np.ceil(bbox[1,1]-bbox[0,1])) ))
    ##transfer getp[ii] for tuple format, account for the offset
    for ii in xrange(len(getp)):
      #print getp[ii]
      tp = [tuple([ getp[ii][jj][0]-bbox[0,0], getp[ii][jj][1]-bbox[0,1] ]) for jj in xrange(len(getp[ii]))]
      ImageDraw.Draw(l_img).polygon(tp, outline=1, fill=1)
      #print tp
    #l_img = l_img.transpose(Image.FLIP_TOP_BOTTOM)
    mask = np.asarray(l_img)
    del l_img  

    ###bounding box for the mask layer  
    m_llx = np.min([np.min([getp[i][:,0]]) for i in xrange(len(getp))])
    m_urx = np.max([np.max([getp[i][:,0]]) for i in xrange(len(getp))])
    m_lly = np.min([np.min([getp[i][:,1]]) for i in xrange(len(getp))])
    m_ury = np.max([np.max([getp[i][:,1]]) for i in xrange(len(getp))])

    ### resize the window
    window_x = pp['Window_x']
    window_y = pp['Window_y']
    window_x = np.min([window_x, np.ceil((bbox[1,0] - bbox[0,0])/grid_size)*grid_size + 10*grid_size])
    window_y = np.min([window_y, np.ceil((bbox[1,1] - bbox[0,1])/grid_size)*grid_size + 10*grid_size])
    center_x = int(np.ceil((m_llx+m_urx)/2))
    center_y = int(np.ceil((m_lly+m_ury)/2))
    min_x = int(np.ceil((center_x - window_x / 2.0)/grid_size)*grid_size)
    min_y = int(np.ceil((center_y - window_y / 2.0)/grid_size)*grid_size)
    #min_x = int(np.ceil(center_x - window_x / 2))
    #min_y = int(np.ceil(center_y - window_y / 2))
    x_row = int(np.ceil(window_x/grid_size))
    y_col = int(np.ceil(window_y/grid_size))

    #print 'window_x = %.1f, window_y = %.1f, center_x = %.1f, center_y = %.1f' %(window_x, window_y, center_x, center_y)
    #print 'mask bounding box: (%.1f, %.1f, %.1f, %.1f)' %(m_llx, m_lly, m_urx, m_ury)
    #print 'min_x = %.1f, min_y = %.1f, x_row = %d, y_col = %d' %(min_x, min_y, x_row, y_col)
    sraf_cell = cell_inst.copy('TOP', exclude_from_global=True)
    ### create meshgrid
    ss_index = 0
    ss_te = ss_te_dict[item]
    usefulness_map = np.zeros((x_row, y_col), dtype=float)
    for ii in xrange(x_row):
      for jj in xrange(y_col):
        llx = ii * grid_size + min_x
        lly = jj * grid_size + min_y
        urx = llx + sraf_size
        ury = lly + sraf_size
        ###query mask features
        b_llx = int(llx - bbox[0,0])
        b_urx = int(urx - bbox[0,0])
        b_lly = int(lly - bbox[0,1])
        b_ury = int(ury - bbox[0,1])
        mb_lly = b_lly
        mb_ury = b_ury
        mb_llx = b_llx
        mb_urx = b_urx
        if(b_lly < 0): mb_lly = 0
        if(b_lly > mask.shape[0]): mb_lly = mask.shape[0]
        if(b_ury < 0): mb_ury = 0
        if(b_ury > mask.shape[0]): mb_ury = mask.shape[0]
        if(b_llx < 0): mb_llx = 0
        if(b_llx > mask.shape[1]): mb_llx = mask.shape[1]
        if(b_urx < 0): mb_urx = 0
        if(b_urx > mask.shape[1]): mb_urx = mask.shape[1]
        ###skip feature point intersection target features
        mask_area = np.sum(mask[mb_lly:mb_ury+1,:][:,mb_llx:mb_urx+1])
        if(mask_area > 0): continue
        ### feature point
        #mid_x = int(np.ceil(llx+grid_size/2.0)) 
        #mid_y = int(np.ceil(lly+grid_size/2.0)) 
        mid_x = int(np.ceil((llx+urx)/2)) 
        mid_y = int(np.ceil((lly+ury)/2)) 
        usefulness_map[ii][jj] = yh_te[0][fp_index]
        if(yh_te[0][fp_index] > th and np.sum(fvec_te[fp_index]) > 0):
          pg = gdspy.Polygon([(llx, lly),(llx, ury),(urx, ury),(urx, lly)], layer=0, datatype=0)
          sraf_cell.add(pg)
        if(debug_function): 
            print 'test feature point    (%.1f, %.1f) - %.4f' %(ss_te[ss_index][0], ss_te[ss_index][1], ss_te[ss_index][2])
            print 'predict feature point (%.1f, %.1f) - %.4f\n' %(mid_x, mid_y, yh_te[0][fp_index])
            #text = gdspy.Text(text=str(yh_te[0][fp_index]),size=5, position=(mid_x, mid_y), layer=0, datatype = 1)
            #sraf_cell.add(text)
        ss_index += 1
        fp_index += 1
  
    ###output usefulness map
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.imshow(usefulness_map)
    fig.savefig(("%s_test.pdf" %item))

    ###output SRAFed layout
    gds_prt = gdspy.GdsPrint(('%s_sraf.gds' %item), unit=1.0e-9, precision=1.0e-9)
    gds_prt.write_cell(sraf_cell)
    gdspy.Cell.cell_dict.clear()
    gds_prt.close()

  return 0

### Extract calculation points (1/10)
def RedCalcPoint(pp,data,flg):
  ### data: gdsx, gdsy, weight

  rr = 10
  nn = data.shape[0]
  rn = int(nn/rr)

  np.random.seed(0)
  nd = np.random.permutation(rn)
  if flg == 'train':
    pp['Dindex'] = nd

  #nn_norm = data[data[:,6]==0].shape[0]
  #nn_conv = data[data[:,6]==1].shape[0]
  #nn_conc = data[data[:,6]==2].shape[0]
  #nn_lend = data[data[:,6]==3].shape[0]

  if (pp['Dsize'] == 'All' and flg == 'train') or flg == 'test':
    a_data = data[0:rn]
  elif pp['Dsize'] == 'Large' and flg == 'train':
    a_data = data[nd[0:10000]]
  elif pp['Dsize'] == 'Middle' and flg == 'train':
    a_data = data[nd[0:5000]]
  elif pp['Dsize'] == 'Small' and flg == 'train':
    a_data = data[nd[0:1000]]

  if pp['F_Gsearch'] == 'ON' and flg == 'test':
    if pp['Dsize'] == 'Small':
      a_data = data[nd[0:1000]]
    elif pp['Dsize'] == 'Middle':
      a_data = data[nd[0:5000]]
    elif pp['Dsize'] == 'Large':
      a_data = data[nd[0:10000]]
    elif pp['Dsize'] == 'All':
      a_data = data[0:rn]

  #nd_norm = a_data[a_data[:,6]==0].shape[0]
  #nd_conv = a_data[a_data[:,6]==1].shape[0]
  #nd_conc = a_data[a_data[:,6]==2].shape[0]
  #nd_lend = a_data[a_data[:,6]==3].shape[0]

  print '\t#samples(init.)\t\t: %d ' %(nn)
  #print '\t#samples(init.)\t\t: %d %d %d %d %d' %(nn,nn_norm,nn_conv,nn_conc,nn_lend)
  print '\tSample size\t\t: %s' %pp['Dsize']
  print '\t#samples\t\t: %d ' %(a_data.shape[0])
  #print '\t#samples\t\t: %d %d %d %d %d' %(a_data.shape[0],nd_norm,nd_conv,nd_conc,nd_lend)

  return a_data

