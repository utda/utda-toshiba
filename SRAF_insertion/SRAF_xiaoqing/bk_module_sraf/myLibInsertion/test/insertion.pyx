#!/work/lpkopt/python/2.7.3/bin/python

import numpy as np
cimport numpy as np
cimport cython
from libc.math cimport ceil,floor
from libc.stdlib cimport malloc, free
from cython.parallel cimport prange

### seperate for loop for SRAF feature insertion
cpdef int insertion(ss_te,int fp_index,yh_te,th, int x_row, int y_col, int grid_size, int sraf_size, int min_x, int min_y,str model):
  cdef ii, jj, llx, lly, urx, ury, mid_x, mid_y
  ### create meshgrid
  cdef ss_index = 0
  cdef np.ndarray[np.float,ndim=2] usefulness_map = np.zeros((x_row,y_col),dtype=float)
  cdef np.ndarray[np.int,ndim=2] label_map = np.zeros((x_row,y_col),dtype=int)
  for elem in ss_te:
    ii = int(elem[3])
    jj = int(elem[4])
    llx = ii * grid_size + min_x
    lly = jj * grid_size + min_y
    urx = llx + sraf_size
    ury = lly + sraf_size
    ### feature point
    mid_x = int(np.ceil((llx+urx)/2)) 
    mid_y = int(np.ceil((lly+ury)/2)) 
    if model == 'LGR' or model == 'SVC':
      ###For logistic regression model, extract the probability as the usefulness
      usefulness_map[ii,jj] = th[fp_index][1]
      label_map[ii,jj] = yh_te[0][fp_index]
    else:
      usefulness_map[ii,jj] = yh_te[0][fp_index]
    ss_index += 1
    fp_index += 1

  ###insert sraf features
  for ii in xrange(x_row):
    for jj in xrange(y_col):
      llx = ii * grid_size + min_x
      lly = jj * grid_size + min_y
      urx = llx + sraf_size
      ury = lly + sraf_size
      ###ignore none sraf features
      if model == 'LGR' or model == 'SVC':
        if label_map[ii,jj] == 0: continue
      else:
        if (usefulness_map[ii,jj] < th): continue
      down = np.max([ii-1,0])
      up = np.min([ii+1,usefulness_map.shape[0]-1])
      left = np.max([jj-1,0])
      right = np.min([jj+1,usefulness_map.shape[1]-1])
      if (usefulness_map[ii,jj] >= usefulness_map[down,jj] and 
          usefulness_map[ii,jj] >= usefulness_map[up,jj] and 
          usefulness_map[ii,jj] >= usefulness_map[ii,left] and 
          usefulness_map[ii,jj] >= usefulness_map[ii,right]):
        llx = llx/1000.0
        lly = lly/1000.0
        urx = urx/1000.0
        ury = ury/1000.0
        srafs.append([(llx, lly),(llx, ury),(urx, ury),(urx, lly)])
      else: 
        pass
  return fp_index
