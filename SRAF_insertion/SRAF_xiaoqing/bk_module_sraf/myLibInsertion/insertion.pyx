#!/work/lpkopt/python/2.7.3/bin/python

import numpy as np
cimport numpy as np
cimport cython
from libc.math cimport floor
from libc.stdlib cimport malloc, free
from cython.parallel cimport prange

cdef inline int int_max(int a, int b): return a if a >= b else b
cdef inline int int_min(int a, int b): return a if a <= b else b

### seperate for loop for SRAF feature insertion
cpdef int cinsertion(ss_te, int ss_te_len, int fp_index, yh_te, th, int x_row, int y_col, int grid_size, int sraf_size, int min_x, int min_y, str model, srafs):
  cdef int ii, jj, llx, lly, urx, ury
  cdef double f_llx, f_lly, f_urx, f_ury
  cdef int up, down, left, right
  ### create meshgrid
  cdef int ss_index = 0
  cdef bint map_peak = 1
  #cdef int ss_te_len = len(ss_te_len)
  #cdef np.ndarray[np.float,ndim=2] usefulness_map = np.zeros((x_row,y_col),dtype=float)
  #cdef np.ndarray[np.int,ndim=2] label_map = np.zeros((x_row,y_col),dtype=int)
  cdef float** usefulness_map
  cdef int** label_map
  usefulness_map = <float**> malloc(sizeof(float*)*x_row)
  for ii in xrange(x_row):
    usefulness_map[ii] = <float*> malloc(sizeof(float)*y_col)
    for jj in xrange(y_col):
      usefulness_map[ii][jj] = 0.0
  label_map = <int**> malloc(sizeof(int*)*x_row)
  for ii in xrange(x_row):
    label_map[ii] = <int*> malloc(sizeof(int)*y_col)
    for jj in xrange(y_col):
      label_map[ii][jj] = 0

  for index in xrange(ss_te_len):
    ii = int(ss_te[index][3])
    jj = int(ss_te[index][4])
    llx = ii * grid_size + min_x
    lly = jj * grid_size + min_y
    urx = llx + sraf_size
    ury = lly + sraf_size
    ### feature point
    if model == 'LGR' or model == 'LGR_AdaBoost' or model == 'SVC':
      ###For logistic regression model, extract the probability as the usefulness
      usefulness_map[ii][jj] = th[fp_index][1]
      label_map[ii][jj] = yh_te[0][fp_index]
      #if (usefulness_map[ii][jj] >= 0.5):
      #  assert(label_map[ii][jj] == 1)
      #if (label_map[ii][jj] == 1):
      #  assert(usefulness_map[ii][jj] >= 0.5)
    else:
      usefulness_map[ii][jj] = yh_te[0][fp_index]
    ss_index += 1
    fp_index += 1

  ###insert sraf features
  for ii in xrange(x_row):
    for jj in xrange(y_col):
      llx = ii * grid_size + min_x
      lly = jj * grid_size + min_y
      urx = llx + sraf_size
      ury = lly + sraf_size
      ###ignore none sraf features
      if model == 'LGR' or model == 'LGR_AdaBoost' or model == 'SVC':
        if label_map[ii][jj] == 0: continue
      else:
        if (usefulness_map[ii][jj] < th): continue
      if map_peak:
        #down = np.max([ii-1,0])
        down = int_max(ii-1,0)
        #up = np.min([ii+1,usefulness_map.shape[0]-1])
        up = int_min(ii+1,x_row-1)
        #left = np.max([jj-1,0])
        left = int_max(jj-1,0)
        #right = np.min([jj+1,usefulness_map.shape[1]-1])
        right = int_min(jj+1,y_col-1)
        if (usefulness_map[ii][jj] >= usefulness_map[down][jj] and 
            usefulness_map[ii][jj] >= usefulness_map[up][jj] and 
            usefulness_map[ii][jj] >= usefulness_map[ii][left] and 
            usefulness_map[ii][jj] >= usefulness_map[ii][right]):
          f_llx = llx/1000.0
          f_lly = lly/1000.0
          f_urx = urx/1000.0
          f_ury = ury/1000.0
          srafs.append([(f_llx, f_lly),(f_llx, f_ury),(f_urx, f_ury),(f_urx, f_lly)])
        else: 
          pass
      else: 
        f_llx = llx/1000.0
        f_lly = lly/1000.0
        f_urx = urx/1000.0
        f_ury = ury/1000.0
        srafs.append([(f_llx, f_lly),(f_llx, f_ury),(f_urx, f_ury),(f_urx, f_lly)])
  
  for ii in xrange(x_row): 
    free(usefulness_map[ii])
    free(label_map[ii])
  free(usefulness_map)
  free(label_map)

  return fp_index
