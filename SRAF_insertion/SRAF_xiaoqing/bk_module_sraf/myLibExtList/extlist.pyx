#!/usr/bin/python

#import os
import numpy as np
cimport numpy as np
cimport cython

def CExtList(int tnum, float crange, float adist, np.ndarray[np.float64_t, ndim=2] cpoints, np.ndarray[np.float64_t,ndim=2] cntp,list getp_t,np.ndarray[np.int64_t,ndim=1] pdir):
	cdef list mlist = []
	cdef int i = 0
	cdef int j = 0
	cdef int k = 0
	cdef float tbx = 0.0
	cdef float tby = 0.0
	cdef float ttx = 0.0
	cdef float tty = 0.0
	cdef int ncntp = cntp.shape[0]
	
	for i in xrange(tnum):
		k = 0
		for j in xrange(ncntp):
			tbx = cntp[j,0]-adist/2.0
			tby = cntp[j,1]-adist/2.0
			ttx = cntp[j,0]+adist/2.0
			tty = cntp[j,1]+adist/2.0
			if cpoints[i,0] >= tbx and cpoints[i,0] <= ttx and cpoints[i,1] >= tby and cpoints[i,1] <= tty:
				k = j
			else:
				pass
		mlist.append([getp_t[k][0],cpoints[i],crange,pdir[i]])
	return mlist
