#!/bin/csh -f 

#clean sraf data
/bin/rm result/train_feature/*
/bin/rm result/test_feature/*
/bin/rm result/*.pdf
/bin/rm result/*.txt
/bin/rm result/*.npy

#clean feature vector
/bin/rm result/fvec/*

#clean log file
/bin/rm ./log/*.log

#clean results
/bin/rm ./result/gds/*gds
/bin/rm ./result/gds/*pdf
