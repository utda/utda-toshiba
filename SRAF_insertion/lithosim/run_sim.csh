#!/bin/csh -f

set cpu_num = 8

### num = 1 w sraf, num =2 wo sraf
set num = 1
set opcdeck = ./cali/opc${num}.rule 
set lccdeck = ./cali/lcc${num}.rule 

### OPC
#calibre -drc -hier -turbo -turbo_litho ${cpu_num} -64 -hyper ${opcdeck} >& ./log/opc.log 
calibre -drc -hier -turbo -turbo_litho ${cpu_num} -64 -hyper ${opcdeck} 

### LCC
#calibre -drc -hier -turbo -turbo_litho ${cpu_num} -64 -hyper ${lccdeck} >& ./log/lcc.log
calibre -drc -hier -turbo -turbo_litho ${cpu_num} -64 -hyper ${lccdeck} 

### PV band calculation
#python2.7 pvband.py
