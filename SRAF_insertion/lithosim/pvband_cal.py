#!/work/lpkopt/python/2.7.3/bin/python

import os
import re
import sys
import time
import numpy as np

### PV band simulation 
def PV_band_cal(dir,opc,sraf,cell,xy=np.array([]),wsraf=True,fclip=False):
	if False == fclip:
		sraf_code = make_srafcheck_rule(dir,cell)
		fs = open(('%s/cali/%s_srafcheck.rule' %(dir,cell)),'w')
		fs.write(sraf_code)
		fs.close()
	else: 
		pass

	drc_code = make_calibre_rule(dir,opc,sraf,cell,wsraf,fclip)
  ### read xy list (um)
	if len(xy) != 0:
		if(len(xy.shape) == 1): xy = np.array([xy])

		### anchor hole size : 0.1(um)
		### calc. window : 0.15(um)
		ws = 0.10/2.
		for ix in xrange(len(xy)):
			outx = 'out'+str(ix+1)
			outt = outx+'.txt'
			tx,ty = xy[ix,0],xy[ix,1]
			lbx = tx-ws
			lby = ty-ws
			rtx = tx+ws
			rty = ty+ws
			drc_code+="""
%s {DENSITY PV_BAND [AREA(PV_BAND)] >= 0 INSIDE OF (%s) (%s) (%s) (%s) PRINT ONLY '%s/%s_tmp/%s'}
""" %(outx,lbx,lby,rtx,rty,dir,cell,outt)
		
	else:
		### compute total area
		#print " File sample.txt not found."
		xy = [0]
		drc_code+="""
out1 {DENSITY PV_BAND [AREA(PV_BAND)] >= 0 PRINT ONLY '%s/%s_tmp/out1.txt'}
""" %(dir,cell)

	drc_code += """
lay_opc_new = NOT lay_opc target
OUT_target     {COPY target    } DRC CHECK MAP OUT_target      0  0
OUT_opc        {COPY lay_opc_new  } DRC CHECK MAP OUT_opc         1  0
OUT_sraf       {COPY lay_sraf    } DRC CHECK MAP OUT_sraf        2  0
OUT_simBEST    {COPY SIM_BEST  } DRC CHECK MAP OUT_simBEST     100   0
//OUT_simUNDER   {COPY EPEin } DRC CHECK MAP OUT_simUNDER    101   0
//OUT_simOVER    {COPY EPEout  } DRC CHECK MAP OUT_simOVER     102   0
OUT_simPVBAND  {COPY PV_BAND } DRC CHECK MAP OUT_simPVBAND   103   0
"""

	fs = open(('%s/cali/%s_pvb.cali' %(dir,cell)),'w')
	fs.write(drc_code)
	fs.close()

	if not os.path.isdir('%s/%s_tmp' %(dir,cell)):
		os.system('mkdir %s/%s_tmp' %(dir,cell))

  ### run calibre
	if False == fclip:
		### SRAF post processing
		os.system('calibre -drc -hier -turbo -turbo_litho 8 -64 -hyper %s/cali/%s_srafcheck.rule >& %s/log/%s_sraf.log' %(dir,cell,dir,cell))
	else:
		pass
	### opc lcc and pv band simlulation 
	os.system('calibre -drc -hier -64 %s/cali/%s_pvb.cali >& %s/log/%s_cali.log' %(dir,cell,dir,cell))
        #return np.zeros(len(xy)), [], [], []

        ### read pvband files
	pvb = np.zeros(len(xy))
	for ix in xrange(len(xy)):
		outt = dir+'/'+cell+'_tmp/out'+str(ix+1)+'.txt' 
		if os.path.exists(outt):
			dat  = np.genfromtxt(outt)
			pvb[ix] = dat[4]
		else:
			pvb[ix] = 'nan'

	if False == fclip:
		### read epe files
		if os.path.isfile(('%s/%s_epeout.db' %(dir,cell))):
			fs = open(('%s/%s_epeout.db' %(dir,cell)))
			fsall = fs.read().split('\n')
			fs.close()
			### get inex
			id_epein   = fsall.index('epein')
			id_epeout  = fsall.index('epeout')
			id_epebin  = fsall.index('epebestin')
			id_epebout = fsall.index('epebestout')
			id_target  = fsall.index('ntarget')

			### epes from inner contour
			nepein = int(fsall[id_epein+1].split()[0])
			tmp = fsall[id_epein+3:id_epein+3+nepein*5]
			ts_in = tmp[1::5]
			te_in = tmp[3::5]

			### epes from outer contour
			nepeout = int(fsall[id_epeout+1].split()[0])
			tmp = fsall[id_epeout+3:id_epeout+3+nepeout*5]
			ts_out = tmp[1::5]
			te_out = tmp[3::5]

			### epes from nominal contour
			nepebestin  = int(fsall[id_epebin+1].split()[0])
			nepebestout = int(fsall[id_epebout+1].split()[0])
			tmp = fsall[id_epebin+3:id_epebin+3+nepebestin*5]
			ts_bin = tmp[1::5]
			te_bin = tmp[3::5]
			tmp = fsall[id_epebout+3:id_epebout+3+nepebestout*5]
			ts_bout = tmp[1::5]
			te_bout = tmp[3::5]

			### check total #target
			ntarget = int(fsall[id_target+1].split()[0])*4

			### extract epes	
			epes_in   = []
			epes_out  = []
			epes_best = []
			for ix in xrange(len(ts_in)):
				txy1 = ts_in[ix].split()
				txy2 = te_in[ix].split()
				tmpx = np.abs(int(txy2[0])-int(txy1[0]))
				tmpy = np.abs(int(txy2[1])-int(txy1[1]))
				tmpd = tmpx if tmpx < 60 else tmpy
				cntx = (int(txy2[0])+int(txy1[0]))/2000.
				cnty = (int(txy2[1])+int(txy1[1]))/2000.
				epes_in.append([-1.*tmpd,cntx,cnty])
			if len(epes_in) != ntarget:
				tnum = np.abs(ntarget-len(epes_in))
				[epes_in.append([0,0.0,0.0]) for i in xrange(tnum)]

			for ix in xrange(len(ts_out)):
				txy1 = ts_out[ix].split()
				txy2 = te_out[ix].split()
				tmpx = np.abs(int(txy2[0])-int(txy1[0]))
				tmpy = np.abs(int(txy2[1])-int(txy1[1]))
				tmpd = tmpx if tmpx < 60 else tmpy
				cntx = (int(txy2[0])+int(txy1[0]))/2000.
				cnty = (int(txy2[1])+int(txy1[1]))/2000.
				epes_out.append([tmpd,cntx,cnty])
			if len(epes_out) != ntarget:
				tnum = np.abs(ntarget-len(epes_out))
				[epes_out.append([0,0.0,0.0]) for i in xrange(tnum)]

			for ix in xrange(len(ts_bin)):
				txy1 = ts_bin[ix].split()
				txy2 = te_bin[ix].split()
				tmpx = np.abs(int(txy2[0])-int(txy1[0]))
				tmpy = np.abs(int(txy2[1])-int(txy1[1]))
				tmpd = tmpx if tmpx < 30 else tmpy
				cntx = (int(txy2[0])+int(txy1[0]))/2000.
				cnty = (int(txy2[1])+int(txy1[1]))/2000.
				epes_best.append([-1.*tmpd,cntx,cnty])
			for ix in xrange(len(ts_bout)):
				txy1 = ts_bout[ix].split()
				txy2 = te_bout[ix].split()
				tmpx = np.abs(int(txy2[0])-int(txy1[0]))
				tmpy = np.abs(int(txy2[1])-int(txy1[1]))
				tmpd = tmpx if tmpx < 30 else tmpy
				cntx = (int(txy2[0])+int(txy1[0]))/2000.
				cnty = (int(txy2[1])+int(txy1[1]))/2000.
				epes_best.append([tmpd,cntx,cnty])
			if len(epes_best) != ntarget:
				tnum = np.abs(ntarget-len(epes_best))
				[epes_best.append([0,0.0,0.0]) for i in xrange(tnum)]

			#### output
			#fs = open('epeout_inner.txt','w')
			#fs.write('epe(nm) gdsx(um) gdsy(um)\n')
			#for ix in xrange(len(epes_in)):
			#	fs.write('%d %.4f %.4f\n' %(epes_in[ix][0],epes_in[ix][1],epes_in[ix][2]))
			#fs.close()

			#fs = open('epeout_outer.txt','w')
			#fs.write('epe(nm) gdsx(um) gdsy(um)\n')
			#for ix in xrange(len(epes_out)):
			#	fs.write('%d %.4f %.4f\n' %(epes_out[ix][0],epes_out[ix][1],epes_out[ix][2]))
			#fs.close()

			#fs = open('epeout_best.txt','w')
			#fs.write('epe(nm) gdsx(um) gdsy(um)\n')
			#for ix in xrange(len(epes_best)):
			#	fs.write('%d %.4f %.4f\n' %(epes_best[ix][0],epes_best[ix][1],epes_best[ix][2]))
			#fs.close()
		else:
			print '###ERROR: the EPE check file %s/%s_epeout.db does not exist' %(dir,cell)
	else: 
		epes_in   = []
		epes_out  = []
		epes_best = []

	os.system('rm -rf %s/%s_tmp' %(dir,cell))
	#if 'mb' == sraf:
	#	os.system('rm %s/results/%s.oas' %(dir,cell))
	#	os.system('rm %s/results/%s_lccout.oas' %(dir,cell))
	#else:
	#	os.system('mv %s/results/%s.oas %s/results/%s_%s_%s.oas' %(dir,cell,dir,cell,opc,sraf))
	#	os.system('mv %s/results/%s_lccout.oas %s/results/%s_%s_%s_lccout.oas' %(dir,cell,dir,cell,opc,sraf))
	os.system('mv %s/results/%s.oas %s/results/%s_%s_%s.oas' %(dir,cell,dir,cell,opc,sraf))
	os.system('mv %s/results/%s_lccout.oas %s/results/%s_%s_%s_lccout.oas' %(dir,cell,dir,cell,opc,sraf))
	if False == fclip:
		os.system('rm %s/cali/%s_srafcheck.rule' %(dir,cell))
		#os.system('rm %s/log/%s_sraf.log' %(dir,cell))
	else: 
		pass
	os.system('rm  %s/%s_epeout.db' %(dir,cell))
	os.system('rm %s/cali/%s_pvb.cali' %(dir,cell))
	#os.system('rm %s/log/%s_cali.log' %(dir,cell))
  
	return pvb, epes_best, epes_in, epes_out
	### End

### SRAF post processing rule
def make_srafcheck_rule(dir,cell):
	drc_code = """ 
LAYOUT PATH    '%s/gds/%s.gds'
""" %(dir,cell)
	drc_code += """
LAYOUT PRIMARY '*'
"""
	drc_code += """
DRC RESULTS DATABASE '%s/results/%s.oas' OASIS
""" %(dir,cell)
	drc_code += """
PRECISION 1000
LAYOUT SYSTEM GDSII
DRC MAXIMUM RESULTS ALL
DRC MAXIMUM VERTEX 4000

LAYER MAP 0   datatype 0 1000 LAYER lay_sraf   1000
LAYER MAP 2   datatype 0 1001 LAYER target     1001
LAYER MAP 4   datatype 0 1002 LAYER lay_bias   1002
LAYER MAP 20  datatype 0 1003 LAYER lay_mbsraf 1003
LAYER MAP 21  datatype 0 1004 LAYER dcare      1004
LAYER MAP 22  datatype 0 1005 LAYER sgenarea0  1005
LAYER MAP 23  datatype 0 1006 LAYER sgenarea   1006
LAYER MAP 101 datatype 0 1007 LAYER calsraf    1007

// area: 0.040 x 0.040
VARIABLE min_area 0.0016
VARIABLE hole_area 0.6
VARIABLE check_area 0.10
VARIABLE min_size 0.040
VARIABLE min_size2 0.041
VARIABLE min_size3 0.080
VARIABLE hminsize 0.02
VARIABLE max_length 0.09
// max_length x 3
VARIABLE max_length2 0.27
// area: 0.04 x 0.09:min_size x max_length
VARIABLE min_area2 0.0036
VARIABLE min_space 0.06
VARIABLE min_space2 0.04
 
// #001: read original ml_sraf layout
// #002: flatten
lay_sraf0 = SIZE lay_sraf BY 0.001
lay_sraf2 = FLATTEN lay_sraf0

// #003: make seeds (center in each sraf)
tl0a   = EXTENTS lay_sraf2 CENTERS min_size
tl0b   = EXTENTS tl0a CENTERS min_size
tl0_m1 = EXT tl0b <= min_space2 REGION EXTENTS
tl0_m2 = OR tl0_m1 (INTERACT tl0b tl0_m1)
tl0_m3 = EXTENTS tl0_m2 CENTERS min_size
tl0    = OR tl0_m3 (NOT INTERACT tl0b tl0_m2)

// #004: extend seeds along the vertical axis
tl1 = ENC (ANGLE tl0 == 0) (SIZE lay_sraf2 BY -0.001)  < 0.2 OPPOSITE REGION
tl2a = EXTENTS (OR tl0 tl1)
tl2b = (EXTENTS tl2a) WITH WIDTH > min_size 
tl2b2 = CONVEX EDGE tl2b ANGLE1 >= 0 ANGLE2 >= 0 WITH LENGTH > 0.035 < 0.10
tl2b3 = NOT tl2b (EXPAND EDGE tl2b2 INSIDE BY 0.001 EXTEND BY 0.001)
tl2c = INTERNAL tl2b3 > min_size <= 0.1 REGION CENTERLINE min_size
tl2d = EXTENTS (NOT RECTANGLE tl2c) CENTERS min_size
tl2 = OR (OR tl2d (NOT INTERACT tl2c tl2d)) (NOT INTERACT tl2a tl2b3)

// #005: extend seeds along the horizontal axis
tl3 = ENC (ANGLE tl0 == 90) (SIZE lay_sraf2 BY -0.001) < 0.2 OPPOSITE REGION
tl4a = EXTENTS (OR tl0 tl3)
tl4b = (EXTENTS tl4a) WITH WIDTH > min_size 
tl4b2 = CONVEX EDGE tl4b ANGLE1 >= 0 ANGLE2 >= 0 WITH LENGTH > 0.035 < 0.10
tl4b3 = NOT tl4b (EXPAND EDGE tl4b2 INSIDE BY 0.001 EXTEND BY 0.001)
tl4c = INTERNAL tl4b3 > min_size <= 0.1 REGION CENTERLINE min_size
tl4d = EXTENTS (NOT RECTANGLE tl4c) CENTERS min_size
tl4 = OR (OR tl4d (NOT INTERACT tl4c tl4d)) (NOT INTERACT tl4a tl4b3)

// #006: define xy area
starget = SIZE target BY 0.10
H_EDGE:0 = (EXPAND EDGE (ANGLE starget == 0) OUTSIDE BY hole_area) OR starget
V_EDGE:0 = (EXPAND EDGE (ANGLE starget == 90) OUTSIDE BY hole_area) OR starget
H_SIDE = EXPAND EDGE (ANGLE starget == 0) OUTSIDE BY 0.05
V_SIDE = EXPAND EDGE (ANGLE starget == 90) OUTSIDE BY 0.05
H_EDGE = (EXTENTS (NOT H_EDGE:0 V_EDGE:0)) OR H_SIDE
V_EDGE = (EXTENTS (NOT V_EDGE:0 H_EDGE:0)) OR V_SIDE

// #007: extact extended seeds srafs based on xy area
H_SRAF = INTERACT tl4 H_EDGE
V_SRAF = NOT INTERACT (INTERACT tl2 V_EDGE) H_SRAF

// #008: extract srafs in the area except for the xy_area
R_SRAF:0 = AND tl2 tl4
R_SRAF:1 = NOT INTERACT R_SRAF:0 H_SRAF
R_SRAF   = NOT INTERACT R_SRAF:1 V_SRAF

// #009: merge all srafs
ML_SRAF = OR R_SRAF (OR H_SRAF V_SRAF)

// #010: space check (identify adjacent small srafs based on min_area2 nm^2 and min_space)
sraf_tmp:0 = AREA ML_SRAF <= min_area2
sraf_tmp:1 = EXT sraf_tmp:0 <= min_space REGION EXTENTS
sraf_tmp:2 = OR sraf_tmp:1 (INTERACT sraf_tmp:0 sraf_tmp:1)
sraf_tmp:2a = (EXTENTS sraf_tmp:2) LENGTH >= max_length2
sraf_tmp:2b = INTERACT sraf_tmp:2 (EXPAND EDGE sraf_tmp:2a INSIDE BY 0.001)
sraf_tmp:2c = NOT sraf_tmp:2b (EXTENTS sraf_tmp:2b CENTERS min_size3)
sraf_tmp:2d = OR sraf_tmp:2c (NOT INTERACT sraf_tmp:2 sraf_tmp:2c)

// #011: repeat the above processing(#003-#009) for small srafs
sraf_tmp:3 = EXTENTS sraf_tmp:2d CENTERS min_size
sraf_tmp:4 = ENC (ANGLE sraf_tmp:3 == 0) sraf_tmp:2d < 0.2 OPPOSITE REGION
sraf_tmp:5 = EXTENTS (OR sraf_tmp:3 sraf_tmp:4)
sraf_tmp:6 = ENC (ANGLE sraf_tmp:3 == 90) sraf_tmp:2d < 0.2 OPPOSITE REGION
sraf_tmp:7 = EXTENTS (OR sraf_tmp:3 sraf_tmp:6)
H_SRAF:2  = INTERACT sraf_tmp:7 H_EDGE
V_SRAF:2  = NOT INTERACT (INTERACT sraf_tmp:5 V_EDGE) H_SRAF:2
R_SRAF:2  = OR H_SRAF:2 V_SRAF:2

// #012: merge all srafs
ML_SRAF:2 = OR R_SRAF:2 (NOT INTERACT ML_SRAF R_SRAF:2)

// #013: cut down long rectangles
lsraf0      = AREA ML_SRAF:2 >= 0.008
lsraf1      = INTERACT ML_SRAF:2 (EXPAND EDGE (LENGTH ML_SRAF:2 > max_length) INSIDE BY 0.001)
lsraf_cent  = EXTENTS lsraf1 CENTERS max_length
lay_sraf_f1 = (AND lsraf1 lsraf_cent) OR (NOT INTERACT ML_SRAF:2 lsraf_cent)

// #014: space check2
spc01 = EXT lay_sraf_f1 <= min_space REGION EXTENTS

// #015: identify adjacent srafs
spc02 = TOUCH EDGE lay_sraf_f1 spc01
spc03 = EXPAND EDGE spc02 INSIDE BY hminsize EXTEND BY 0.01

// #016: cut the center part of adjacent srafs
lay_sraf_f2 = NOT lay_sraf_f1 spc03
lay_sraf_f3 = EXTENTS (INTERNAL lay_sraf_f2 < min_size REGION CENTERLINE min_size)

// #017: merge all srafs
lay_sraf_fin = OR (NOT INTERACT lay_sraf_f2 lay_sraf_f3) lay_sraf_f3

//lay_check1 = NOT RECTANGLE lay_sraf_fin

// #018: final MRC
mrc:1 = EXT lay_sraf_fin < min_space REGION EXTENTS
mrc:2 = EXTENTS (OR mrc:1 (INTERACT lay_sraf_fin mrc:1))
//mrc:3 = ENC (SIZE mrc:2 BY 0.001) mrc:2 <= 0.001 INSIDE ALSO REGION CENTERLINE min_size
mrc:3 = EXTENTS mrc:2 CENTERS min_size
mrc:4 = ENC (ANGLE mrc:3 == 0) mrc:2 < 0.2 OPPOSITE REGION
mrc:5 = EXTENTS (OR mrc:3 mrc:4)
mrc:6 = ENC (ANGLE mrc:3 == 90) mrc:2 < 0.2 OPPOSITE REGION
mrc:7 = EXTENTS (OR mrc:3 mrc:6)
H_SRAF:3  = INTERACT mrc:7 H_EDGE
V_SRAF:3  = NOT INTERACT (INTERACT mrc:5 V_EDGE) H_SRAF:3
R_SRAF:3  = OR H_SRAF:3 V_SRAF:3
mrc:8 = AND (EXTENTS R_SRAF:3 CENTERS max_length) R_SRAF:3
lay_sraf_fin2 = OR mrc:8 (NOT INTERACT lay_sraf_fin mrc:2)

// #019: output results
lay_bias_offset = NOT lay_bias target
OUT_target     {COPY target     } DRC CHECK MAP OUT_target    0     0
OUT_bias       {COPY lay_bias_offset   } DRC CHECK MAP OUT_bias      1     0
OUT_sraf       {SIZE lay_sraf2 BY -0.001  } DRC CHECK MAP OUT_sraf      2     0
OUT_sraf2      {COPY lay_sraf_fin2 } DRC CHECK MAP OUT_sraf2   3     0
OUT_mbsraf     {COPY lay_mbsraf } DRC CHECK MAP OUT_mbsraf    4     0
dcare          {COPY dcare      } DRC CHECK MAP dcare         21    0
sraf0          {COPY sgenarea0  } DRC CHECK MAP sraf0         22    0
sraf1          {COPY sgenarea   } DRC CHECK MAP sraf1         23    0
"""
	return drc_code

### option based opc rule generation 
### opc: rb or mb, sraf: ml or mb
def make_calibre_rule(dir,opc,sraf,cell,wsraf,fclip):
	### make rule
	drc_code = """
//Calibre rule for opc
//edited by Nojima
"""
	if False == fclip:
		drc_code += """
LAYOUT PATH    '%s/results/%s.oas' 
LAYOUT SYSTEM OASIS
LAYOUT PRIMARY 'TOP'
""" %(dir,cell)
	else:
		drc_code += """
LAYOUT PATH    '%s/gds/%s.gds' 
LAYOUT SYSTEM GDSII
LAYOUT PRIMARY 'TOP'
""" %(dir,cell)
	

	drc_code += """
DRC RESULTS DATABASE '%s/results/%s_lccout.oas' OASIS APPEND _new
//DRC SUMMARY REPORT   "%s/results/opcout1.rep"
PRECISION 1000
DRC MAXIMUM RESULTS ALL
DRC MAXIMUM VERTEX 4000
""" %(dir,cell,dir)
	
	if False == fclip:
		drc_code += """
LAYER MAP 0  datatype 0 1000 LAYER target     1000
LAYER MAP 1  datatype 0 1001 LAYER lay_biased       1001
"""
	else:
		drc_code += """
LAYER MAP 2  datatype 0 1002 LAYER target     1002
LAYER MAP 4  datatype 0 1004 LAYER lay_biased       1004
"""
	
	### sraf option
	if False == fclip:
		if sraf == 'ml':
			drc_code += """
LAYER MAP 3  datatype 0 1003 LAYER lay_sraf     1003
//LAYER MAP 4  datatype 0 1004 LAYER lay_sraf     1004
"""
		elif sraf == 'mb':
			drc_code += """
//LAYER MAP 3  datatype 0 1003 LAYER lay_sraf     1003
LAYER MAP 4  datatype 0 1004 LAYER lay_sraf     1004
"""
		else:
			print '###ERROR: sraf option should be ml or mb instead of %s' %sraf
			quit()
	else:
		if sraf == 'ml':
			drc_code += """
LAYER MAP 0  datatype 0 1000 LAYER lay_sraf     1000
//LAYER MAP 20  datatype 0 1020 LAYER lay_sraf     1020
"""
		elif sraf == 'mb':
			drc_code += """
//LAYER MAP 0  datatype 0 1000 LAYER lay_sraf     1000
LAYER MAP 20  datatype 0 1020 LAYER lay_sraf     1020
"""
		else:
			print '###ERROR: sraf option should be ml or mb instead of %s' %sraf
			quit()
		

	drc_code += """
//Perform model based OPC or simple lay_biased addition similar to rule based OPC
"""

	### opc option
	if opc == 'rb':
		drc_code += """
lay_OPC      = OR target lay_biased
//lay_OPC    = LITHO DENSEOPC FILE "nmopc.in" target lay_sraf MAP lay_OPC
"""
	elif opc == 'mb':
		drc_code += """
//lay_OPC      = OR target lay_biased
lay_OPC    = LITHO DENSEOPC FILE "nmopc.in" target lay_sraf MAP lay_OPC
"""
	else:
		print '###ERROR: opc option should be rb or mb instead of %s' %opc
		quit()

	drc_code += """

LITHO FILE nmopc.in [/*

#progress_meter off
tilemicrons 40

# model definition
modelpath %s:models

layer target
layer lay_sraf

simulation_consistency 2

denseopc_options nmopc_opt {

    version 1

    layer target        opc   mask_layer 0
    layer lay_sraf      sraf  mask_layer 0

    image models

    fragment_min 0.060
    fragment_max 0.500
    fragment_corner
    fragment_inter OFF

    algorithm          2
    controller         off
    step_size          0.001
    max_iter_movement  0.004
    max_opc_move       0.080
    feedback          -0.5 -0.4 -0.3 

    mrc_rule external target target { use 0.025 euclidean }
    mrc_rule internal target target { use 0.045 opposite  }

    NEWTAG all target -out tag_all
    OPC_ITERATION 20
}

setlayer lay_OPC  = denseopc target lay_sraf MAP target OPTIONS nmopc_opt

*/]

""" %dir

	drc_code += """

//Perform lcc (pv band simulation)

SIM_BEST = LITHO OPCVERIFY FILE opcv.in target lay_OPC lay_sraf MAP SIM_BEST
PV_BAND  = LITHO OPCVERIFY FILE opcv.in target lay_OPC lay_sraf MAP PV_BAND
EPEin    = LITHO OPCVERIFY FILE opcv.in target lay_OPC lay_sraf MAP EPEin
EPEout   = LITHO OPCVERIFY FILE opcv.in target lay_OPC lay_sraf MAP EPEout
EPEbest  = LITHO OPCVERIFY FILE opcv.in target lay_OPC lay_sraf MAP EPEbest

tmp:0 = EXTENTS target CENTERS 0.030
tmp:1 = EXPAND EDGE (ANGLE tmp:0 == 0) OUTSIDE BY 0.05
tmp:2 = EXPAND EDGE (ANGLE tmp:0 == 90) OUTSIDE BY 0.05
tmp:3 = AND EPEbest (OR tmp:1 tmp:2)
EPEbin  = AND tmp:3 target
EPEbout = NOT tmp:3 target

nominal_contour { copy SIM_BEST } DRC CHECK MAP nominal_contour 200 0 APPEND _new
pv_band         { copy PV_BAND  } DRC CHECK MAP pv_band         201 0 APPEND _new
epein            { copy EPEin     } DRC CHECK MAP epein         ASCII '%s/%s_epeout.db'
epeout           { not EPEout target    } DRC CHECK MAP epeout  ASCII '%s/%s_epeout.db'
epebestin        { copy EPEbin  } DRC CHECK MAP epebestin  ASCII '%s/%s_epeout.db'
epebestout       { copy EPEbout } DRC CHECK MAP epebestout ASCII '%s/%s_epeout.db'
ntarget          { copy target } DRC CHECK MAP ntarget ASCII '%s/%s_epeout.db'

DRC PRINT AREA pv_band

LITHO FILE opcv.in [/*

progress_meter off
tilemicrons 40

# model definition
modelpath %s:models

layer target
layer lay_OPC
layer lay_sraf

image_options LccSimBest {
        litho_model models
        layer lay_OPC  visible mask_layer 0
""" %(dir,cell,dir,cell,dir,cell,dir,cell,dir,cell,dir)
	if wsraf == True: 
		drc_code += """
				layer lay_sraf visible mask_layer 0
"""

	drc_code += """
}

simulation_consistency 2

setlayer SIM_BEST        = image LccSimBest dose 1
setlayer contour1        = image LccSimBest dose 1.03 focus 30nm
setlayer contour2        = image LccSimBest dose 0.97 focus 30nm
setlayer contour3        = image LccSimBest dose 1.03 
setlayer contour4        = image LccSimBest dose 0.97 
setlayer contour5        = image LccSimBest dose 1.03 focus -30nm
setlayer contour6        = image LccSimBest dose 0.97 focus -30nm

image_set contours = list SIM_BEST contour1 contour2 contour3 contour4 contour5 contour6
setlayer PV_BAND = not contours.outer contours.inner
setlayer EPEin = measure_epe contours.inner target function min magnitude epe_spacing .001 min_featsize .035 max_edgelen .1
setlayer EPEout = measure_epe contours.outer target function max epe_spacing .001 min_featsize .035 max_edgelen .1
setlayer EPEbest = measure_epe contours.nominal target function max epe_spacing .001 min_featsize .035 max_edgelen .1

*/]

"""
	
	return drc_code
