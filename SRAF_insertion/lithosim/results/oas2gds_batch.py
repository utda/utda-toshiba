#!/usr/bin/python

import os

target_dir = "./sparse_dense_train"

for eachfile in os.listdir(target_dir):
    if eachfile.split('.')[-1] !='oas': continue
    fname = eachfile.split('.oas')[0]
    cal_tcl = open('oas2gds.tcl', 'w')
    cal_tcl.write("set l_src [layout create \"%s/%s\"]\n" %(target_dir,eachfile))
    cal_tcl.write("$l_src gdsout %s.gds" %fname)
    cal_tcl.close()
    os.system("calibredrv oas2gds.tcl")
    os.system("rm oas2gds.tcl")
