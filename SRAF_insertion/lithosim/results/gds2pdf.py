#!/usr/bin/python

import os
import gdspy
from PIL import Image, ImageDraw 
import numpy as np
import math,cairo

target_dir = "./oas_dir"
gds_dir = "./gds_dir"
contour = False
mb_sraf = False

if os.path.exists(gds_dir):
    pass
else:
    os.system("mkdir %s" %gds_dir)

###format coversion to gds using calibre
if False:
    for eachfile in os.listdir(target_dir):
        #handling file format, convert oas to gds
        fformat = eachfile.split('.')[-1]
        if fformat =='oas':
            fname = eachfile.split('.oas')[0]
            cal_tcl = open('oas2gds.tcl', 'w')
            cal_tcl.write("set l_src [layout create \"%s/%s\"]\n" %(target_dir,eachfile))
            cal_tcl.write("$l_src gdsout %s/%s.gds" %(gds_dir,fname))
            cal_tcl.close()
            os.system("calibredrv oas2gds.tcl")
            os.system("rm oas2gds.tcl")
        elif fformat == 'gds':
            os.system("cp %s/%s %s/%s" %(target_dir,eachfile,gds_dir,eachfile))
        else: 
            continue

###create pdf image for each gds 
layer_color_dict_lcc = {0: (0,1,0), 1: (0,0,1), 2: (1,0,0), 201: (0,1,1)}
#layer_color_dict_lcc = {0: (0,1,0), 1: (0,0,1), 201: (0,1,1)}
layer_color_dict_contour = {0: (0,1,0), 100: (1,0,0), 201: (0,1,1)}
if mb_sraf:
    layer_color_dict_sraf = {0: (0,1,0), 1: (0,0,1), 4: (1,0,0)}
else:
    #layer_color_dict_sraf = {0: (0,1,0), 1: (0,0,1), 2: (1,1,0), 3: (1,0,0)}
    layer_color_dict_sraf = {0: (0,1,0), 1: (0,0,1), 2: (1,1,0)}
    #layer_color_dict_sraf = {0: (0,1,0), 1: (0,0,1), 3: (1,0,0)}
for eachfile in os.listdir(gds_dir):
    #ignore none gds file
    if eachfile.split('.')[-1] != 'gds': continue
    #if 'lccout' in eachfile: print eachfile
    fname = eachfile.split('.gds')[0]
    gname = gds_dir + '/' + eachfile
    gdsii = gdspy.GdsImport(gname,unit=1e-9)
    #currently only support one cell_name
    for cell_name in gdsii.cell_dict:
        #plot gds
        cell_inst = gdsii.extract(cell_name)
        bbox = cell_inst.get_bounding_box()
        #rounding the bbox
        bbox[0,0] = int(np.floor(bbox[0,0]))
        bbox[0,1] = int(np.floor(bbox[0,1]))
        bbox[1,0] = int(np.ceil(bbox[1,0]))
        bbox[1,1] = int(np.ceil(bbox[1,1]))
        #print bbox
        polygon_set = cell_inst.get_polygons(True)
        if 'lccout' in eachfile:
            layer_dict = layer_color_dict_lcc
        else:
            layer_dict = layer_color_dict_sraf
        if contour:
            layer_dict = layer_color_dict_contour
        #Draw each layer 
        width, height = int(np.ceil(bbox[1,0]-bbox[0,0])), int(np.ceil(bbox[1,1]-bbox[0,1]))
        if mb_sraf:
            surface = cairo.PDFSurface('./mb_pdf/%s_mb.pdf' %fname, width, height)
        else:
            surface = cairo.PDFSurface('./ml_pdf/%s_ml.pdf' %fname, width, height)
        ctx = cairo.Context(surface)
        #background
        ctx.set_source_rgb(1,1,1)
        ctx.rectangle(0,0,width,height)
        ctx.fill()
        if mb_sraf:
        #if False:
            #only use the lower left part as the training data
            ctx.set_source_rgb(0,0,0)
            ctx.rectangle(10,height/2,width/2-10,height/2-10)
            ctx.set_line_width(5)
            ctx.stroke()
        for layer in layer_dict:
            #set source color
            rgb = layer_dict[layer]
            #set drawing layer
            map_layer = (layer, 0)
            #if not (map_layer in polygon_set): print "Error in handling ", eachfile
            if not (map_layer in polygon_set): continue
            getp = polygon_set[map_layer]
            #drawing each polygon
            for ii in xrange(len(getp)):
                tp = [tuple([ getp[ii][jj][0]-bbox[0,0], getp[ii][jj][1]-bbox[0,1] ]) for jj in xrange(len(getp[ii]))]
                if layer == 1:
                    ctx.set_source_rgba(rgb[0],rgb[1],rgb[2])
                else:
                    ctx.set_source_rgb(rgb[0],rgb[1],rgb[2])
                ctx.move_to(tp[0][0],tp[0][1])
                for pt in tp:
                    ctx.line_to(pt[0],pt[1])
                ctx.close_path()
                if contour:
                    ctx.set_line_width(1)
                    ctx.stroke()
                else:
                    ctx.fill()
            #draw the bbox
            if False == contour:
                for ii in xrange(len(getp)):
                    tp = [tuple([ getp[ii][jj][0]-bbox[0,0], getp[ii][jj][1]-bbox[0,1] ]) for jj in xrange(len(getp[ii]))]
                    ctx.set_source_rgb(0,0,0)
                    ctx.move_to(tp[0][0],tp[0][1])
                    for pt in tp:
                        ctx.line_to(pt[0],pt[1])
                    ctx.close_path()
                    ctx.set_line_width(2)
                    ctx.stroke()
        #output pdf figure
        ctx.show_page()

    gdspy.Cell.cell_dict.clear()

###draw legend
surface = cairo.PDFSurface('label_train.pdf',4200,200)
ctx = cairo.Context(surface)
#background
ctx.set_source_rgb(1,1,1)
ctx.rectangle(0,0,2000,200)
ctx.fill()
#target
ctx.set_source_rgb(0,1,0)
ctx.rectangle(50,50,200,100)
ctx.fill()
ctx.set_source_rgb(0,0,0)
ctx.rectangle(50,50,200,100)
ctx.set_line_width(2)
ctx.stroke()
#opc
ctx.set_source_rgb(0,0,1)
ctx.rectangle(1150,50,200,100)
ctx.fill()
ctx.set_source_rgb(0,0,0)
ctx.rectangle(1150,50,200,100)
ctx.set_line_width(2)
ctx.stroke()
#model based sraf
ctx.set_source_rgb(1,0,0)
ctx.rectangle(2150,50,200,100)
ctx.fill()
ctx.set_source_rgb(0,0,0)
ctx.rectangle(2150,50,200,100)
ctx.set_line_width(2)
ctx.stroke()
#box
ctx.set_source_rgb(0,0,0)
ctx.rectangle(3050,50,200,100)
ctx.set_line_width(5)
ctx.stroke()
#text
ctx.set_source_rgb(0,0,0)
ctx.select_font_face('Calibri')
ctx.set_font_size(100)
ctx.move_to(300,140)
ctx.show_text('Target pattern')
ctx.move_to(1400,140)
ctx.show_text('OPC pattern')
ctx.move_to(2400,140)
ctx.show_text('MB SRAF')
ctx.move_to(3300,140)
ctx.show_text('Sampling region')
ctx.show_page()

###draw legend
surface = cairo.PDFSurface('label_test.pdf',3800,200)
ctx = cairo.Context(surface)
#background
ctx.set_source_rgb(1,1,1)
ctx.rectangle(0,0,2000,200)
ctx.fill()
#target
ctx.set_source_rgb(0,1,0)
ctx.rectangle(50,50,200,100)
ctx.fill()
ctx.set_source_rgb(0,0,0)
ctx.rectangle(50,50,200,100)
ctx.set_line_width(2)
ctx.stroke()
#opc
ctx.set_source_rgb(0,0,1)
ctx.rectangle(1150,50,200,100)
ctx.fill()
ctx.set_source_rgb(0,0,0)
ctx.rectangle(1150,50,200,100)
ctx.set_line_width(2)
ctx.stroke()
#machine learning output
ctx.set_source_rgb(1,1,0)
ctx.rectangle(2150,50,200,100)
ctx.fill()
ctx.set_source_rgb(0,0,0)
ctx.rectangle(2150,50,200,100)
ctx.set_line_width(2)
ctx.stroke()
#mask rule check
ctx.set_source_rgb(1,0,0)
ctx.rectangle(3150,50,200,100)
ctx.fill()
ctx.set_source_rgb(0,0,0)
ctx.rectangle(3150,50,200,100)
ctx.set_line_width(2)
ctx.stroke()
#text
ctx.set_source_rgb(0,0,0)
ctx.select_font_face('Calibri')
ctx.set_font_size(100)
ctx.move_to(300,140)
ctx.show_text('Target pattern')
ctx.move_to(1400,140)
ctx.show_text('OPC pattern')
ctx.move_to(2400,140)
ctx.show_text('ML prediction')
ctx.move_to(3400,140)
ctx.show_text('SRAF')
ctx.show_page()

###draw legend
surface = cairo.PDFSurface('label_lcc.pdf',3600,200)
ctx = cairo.Context(surface)
#background
ctx.set_source_rgb(1,1,1)
ctx.rectangle(0,0,2000,200)
ctx.fill()
#target
ctx.set_source_rgb(0,1,0)
ctx.rectangle(50,50,200,100)
ctx.fill()
ctx.set_source_rgb(0,0,0)
ctx.rectangle(50,50,200,100)
ctx.set_line_width(2)
ctx.stroke()
#opc
ctx.set_source_rgb(0,0,1)
ctx.rectangle(1150,50,200,100)
ctx.fill()
ctx.set_source_rgb(0,0,0)
ctx.rectangle(1150,50,200,100)
ctx.set_line_width(2)
ctx.stroke()
#SRAF
ctx.set_source_rgb(1,0,0)
ctx.rectangle(2150,50,200,100)
ctx.fill()
ctx.set_source_rgb(0,0,0)
ctx.rectangle(2150,50,200,100)
ctx.set_line_width(2)
ctx.stroke()
#pv band
ctx.set_source_rgb(0,1,1)
ctx.rectangle(2800,50,200,100)
ctx.fill()
ctx.set_source_rgb(0,0,0)
ctx.rectangle(2800,50,200,100)
ctx.set_line_width(2)
ctx.stroke()
#text
ctx.set_source_rgb(0,0,0)
ctx.select_font_face('Calibri')
ctx.set_font_size(100)
ctx.move_to(300,140)
ctx.show_text('Target pattern')
ctx.move_to(1400,140)
ctx.show_text('OPC pattern')
ctx.move_to(2400,140)
ctx.show_text('SRAF')
ctx.move_to(3050,140)
ctx.show_text('PV band')
ctx.show_page()
