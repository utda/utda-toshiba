#!/bin/csh -f

### make tcc
cd model
calibrewb -a opticsgen -opticalfile nominal -o na135sig90sigin60 -nolookuptables
calibrewb -a opticsgen -opticalfile defm30 -o na135sig90sigin60def-30 -nolookuptables
calibrewb -a opticsgen -opticalfile defm60 -o na135sig90sigin60def-60 -nolookuptables
calibrewb -a opticsgen -opticalfile defp30 -o na135sig90sigin60defp30 -nolookuptables
calibrewb -a opticsgen -opticalfile defp60 -o na135sig90sigin60defp60 -nolookuptables
cd ../

