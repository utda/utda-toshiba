#!/home/mlitho/opt/6.6/python/2.7.3/bin/python

import os
import sys
import numpy as np

def main():
	epeout = 'epeout.db'

	fs = open(epeout)
	fsall = fs.read().split('\n')
	fs.close()

	### get inex
	id_epein   = fsall.index('epein')
	id_epeout  = fsall.index('epeout')
	id_epebin  = fsall.index('epebestin')
	id_epebout = fsall.index('epebestout')
	id_target  = fsall.index('ntarget')

	### epes from inner contour
	nepein = int(fsall[id_epein+1].split()[0])
	tmp = fsall[id_epein+3:id_epein+3+nepein*5]
	ts_in = tmp[1::5]
	te_in = tmp[3::5]

	### epes from outer contour
	nepeout = int(fsall[id_epeout+1].split()[0])
	tmp = fsall[id_epeout+3:id_epeout+3+nepeout*5]
	ts_out = tmp[1::5]
	te_out = tmp[3::5]

	### epes from nominal contour
	nepebestin  = int(fsall[id_epebin+1].split()[0])
	nepebestout = int(fsall[id_epebout+1].split()[0])
	tmp = fsall[id_epebin+3:id_epebin+3+nepebestin*5]
	ts_bin = tmp[1::5]
	te_bin = tmp[3::5]
	tmp = fsall[id_epebout+3:id_epebout+3+nepebestout*5]
	ts_bout = tmp[1::5]
	te_bout = tmp[3::5]

	### check total #target
	ntarget = int(fsall[id_target+1].split()[0])*4

	### extract epes	
	epes_in   = []
	epes_out  = []
	epes_best = []
	for ix in xrange(len(ts_in)):
		txy1 = ts_in[ix].split()
		txy2 = te_in[ix].split()
		tmpx = np.abs(int(txy2[0])-int(txy1[0]))
		tmpy = np.abs(int(txy2[1])-int(txy1[1]))
		tmpd = tmpx if tmpx < 60 else tmpy
		cntx = (int(txy2[0])+int(txy1[0]))/2000.
		cnty = (int(txy2[1])+int(txy1[1]))/2000.
		epes_in.append([-1.*tmpd,cntx,cnty])
	if len(epes_in) != ntarget:
		tnum = np.abs(ntarget-len(epes_in))
		[epes_in.append([0,0.0,0.0]) for i in xrange(tnum)]

	for ix in xrange(len(ts_out)):
		txy1 = ts_out[ix].split()
		txy2 = te_out[ix].split()
		tmpx = np.abs(int(txy2[0])-int(txy1[0]))
		tmpy = np.abs(int(txy2[1])-int(txy1[1]))
		tmpd = tmpx if tmpx < 60 else tmpy
		cntx = (int(txy2[0])+int(txy1[0]))/2000.
		cnty = (int(txy2[1])+int(txy1[1]))/2000.
		epes_out.append([tmpd,cntx,cnty])
	if len(epes_out) != ntarget:
		tnum = np.abs(ntarget-len(epes_out))
		[epes_out.append([0,0.0,0.0]) for i in xrange(tnum)]

	for ix in xrange(len(ts_bin)):
		txy1 = ts_bin[ix].split()
		txy2 = te_bin[ix].split()
		tmpx = np.abs(int(txy2[0])-int(txy1[0]))
		tmpy = np.abs(int(txy2[1])-int(txy1[1]))
		tmpd = tmpx if tmpx < 30 else tmpy
		cntx = (int(txy2[0])+int(txy1[0]))/2000.
		cnty = (int(txy2[1])+int(txy1[1]))/2000.
		epes_best.append([-1.*tmpd,cntx,cnty])
	for ix in xrange(len(ts_bout)):
		txy1 = ts_bout[ix].split()
		txy2 = te_bout[ix].split()
		tmpx = np.abs(int(txy2[0])-int(txy1[0]))
		tmpy = np.abs(int(txy2[1])-int(txy1[1]))
		tmpd = tmpx if tmpx < 30 else tmpy
		cntx = (int(txy2[0])+int(txy1[0]))/2000.
		cnty = (int(txy2[1])+int(txy1[1]))/2000.
		epes_best.append([tmpd,cntx,cnty])
	if len(epes_best) != ntarget:
		tnum = np.abs(ntarget-len(epes_best))
		[epes_best.append([0,0.0,0.0]) for i in xrange(tnum)]

	### output
	fs = open('epeout_inner.txt','w')
	fs.write('epe(nm) gdsx(um) gdsy(um)\n')
	for ix in xrange(len(epes_in)):
		fs.write('%d %.4f %.4f\n' %(epes_in[ix][0],epes_in[ix][1],epes_in[ix][2]))
	fs.close()

	fs = open('epeout_outer.txt','w')
	fs.write('epe(nm) gdsx(um) gdsy(um)\n')
	for ix in xrange(len(epes_out)):
		fs.write('%d %.4f %.4f\n' %(epes_out[ix][0],epes_out[ix][1],epes_out[ix][2]))
	fs.close()

	fs = open('epeout_best.txt','w')
	fs.write('epe(nm) gdsx(um) gdsy(um)\n')
	for ix in xrange(len(epes_best)):
		fs.write('%d %.4f %.4f\n' %(epes_best[ix][0],epes_best[ix][1],epes_best[ix][2]))
	fs.close()

	return 0

if __name__=='__main__':
	main()
