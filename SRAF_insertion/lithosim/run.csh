#!/bin/csh -f

set cpu_num = 8

### make tcc
cd model
calibrewb -a opticsgen -opticalfile nominal -o na135sig90sigin60 -nolookuptables
calibrewb -a opticsgen -opticalfile defm30 -o na135sig90sigin60def-30 -nolookuptables
calibrewb -a opticsgen -opticalfile defm60 -o na135sig90sigin60def-60 -nolookuptables
calibrewb -a opticsgen -opticalfile defp30 -o na135sig90sigin60defp30 -nolookuptables
calibrewb -a opticsgen -opticalfile defp60 -o na135sig90sigin60defp60 -nolookuptables
cd ../

### num = 1 w sraf, num =2 wo sraf
set num = 2
set opcdeck = ./cali/opc${num}.rule 
set lccdeck = ./cali/lcc${num}.rule 

### OPC
calibre -drc -hier -turbo -turbo_litho ${cpu_num} -64 -hyper ${opcdeck} >& ./log/opc.log 

### LCC
calibre -drc -hier -turbo -turbo_litho ${cpu_num} -64 -hyper ${lccdeck} >& ./log/lcc.log

### PV band calculation
python2.7 pvband.py
