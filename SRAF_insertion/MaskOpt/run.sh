##
# @file   run.sh
# @author Xiaoqing Xu
# @date   Mar 2017
#

exe="./bin/mask_opt"

commands="\
    -param_json ./test/param_sraf_ispd15.json \
    "

echo "$exe $commands"
# run SRAF generation 
time ($exe $commands) > SRAF.rpt 2>&1

