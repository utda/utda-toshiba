#!/usr/bin/python
##
# @file   scripts/search.py
# @author Xiaoqing Xu
# @date   Mar 2017
#

import os
import sys
import numpy as np
import matplotlib.pyplot as plt

import json


json_file = open("./test/param_sraf_ispd15.json")
params = json.load(json_file)

#param search for model SVC
params["SEARCH"] = 1
params["MODEL"]["Type"] = "SVC"
params["MODEL"]["ParamScale"] = 1000

for gamma in xrange(200, 500, 200):
    for coef in xrange(-20, 10, 10):
        for c in xrange(1, 500, 100):
            for w1 in xrange(1000, 5000, 2000):
                #if coef > -5: continue
                #if c > 1: continue
                #if w1 > 1000: continue
                params["MODEL"]["SVC"]["GAMMA"] = gamma
                params["MODEL"]["SVC"]["COEF0"] = coef * gamma
                params["MODEL"]["SVC"]["C"] = c 
                params["MODEL"]["SVC"]["W1"] = w1
                jname = "./result_debug/"+"_gamma"+str(gamma)+"_coef"+str(coef*gamma)+"_C"+str(c)+"_w1"+str(w1)
                print jname
                fp = open(jname+".json", 'w')
                json.dump(params, fp, indent=True)
                fp.close()
            
                #perform search
                os.system('time (./bin/mask_opt -param_json %s.json) > %s.log 2>&1' %(jname, jname))

