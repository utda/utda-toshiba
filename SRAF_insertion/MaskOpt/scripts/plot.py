#!/home/intern3/local/bin/python2.7

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter

import matplotlib
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['text.usetex'] = True

def plot_histogram():

    fmodes = ['mb', 'LGR', 'SVC']
    colors = ['red','tan','blue']
    labels = ['Model-based', 'LGR', 'SVC']

    #plot epe at nominal contour
    min_epe = 100
    max_epe = 0
    epe_best_dist = []
    for fmode in fmodes:
        epe_best = np.loadtxt('./result/%s/%s_epe_best.txt' %(fmode, fmode))
        print "%s SRAF - epe nominal: %.4f" %(fmode, np.sum(np.absolute(epe_best)*1.0)/len(epe_best))
        min_epe = int(np.min([min_epe, np.min(epe_best)]))
        max_epe = int(np.max([max_epe, np.max(epe_best)]))
        epe_best_dist.append(epe_best)

    fmode = "ns"
    epe_best = np.loadtxt('./result/%s/%s_epe_best.txt' %(fmode, fmode))
    print "%s SRAF - epe nominal: %.4f" %(fmode, np.sum(np.absolute(epe_best)*1.0)/len(epe_best))

    fig_epe,ax_epe = plt.subplots()
    bins = np.array(range(min_epe-1,max_epe+2))
    plt.ylim([0,len(epe_best)])
    n,bins,patches = ax_epe.hist(epe_best_dist,bins=bins,histtype='bar',color=colors,label=labels,normed=0)
    ax_epe.legend(prop={'size':18},loc='upper left',frameon=False)
    #ax_epe.set_title('EPE distributions')
    ax_epe.set_xlabel('EPE at nominal contour (nm)',fontsize=25)
    ax_epe.set_ylabel('Normalized frequency',fontsize=25)
    for tick in ax_epe.xaxis.get_major_ticks():
        tick.label.set_fontsize(20)
    for tick in ax_epe.yaxis.get_major_ticks():
        tick.label.set_fontsize(20)
    plt.xticks(bins)
    to_percentage = lambda y, pos: str(round( ( y / float(len(epe_best)) ), 2))
    plt.gca().yaxis.set_major_formatter(FuncFormatter(to_percentage))
    plt.gcf().subplots_adjust(bottom=0.15)
    plt.gcf().subplots_adjust(left=0.15)
    fig_epe.savefig('./result/epe_best_dist.pdf')

    #plot epe at inner contour
    min_epe = 100 
    max_epe = 0
    epe_inner_dist = []
    for fmode in fmodes:
        epe_inner = np.loadtxt('./result/%s/%s_epe_inner.txt' %(fmode, fmode))
        print "%s SRAF - epe inner: %.4f" %(fmode, np.sum(np.absolute(epe_inner)*1.0)/len(epe_inner))
        min_epe = int(np.min([min_epe, np.min(epe_inner)]))
        max_epe = int(np.max([max_epe, np.max(epe_inner)]))
        epe_inner_dist.append(epe_inner)

    fmode = 'ns'
    epe_inner = np.loadtxt('./result/%s/%s_epe_inner.txt' %(fmode, fmode))
    print "%s SRAF - epe inner: %.4f" %(fmode, np.sum(np.absolute(epe_inner)*1.0)/len(epe_inner))

    fig_epe,ax_epe = plt.subplots()
    bins = np.array(range(min_epe-1,max_epe+2))
    plt.ylim([0,len(epe_inner)])
    n,bins,patches = ax_epe.hist(epe_inner_dist,bins=bins,histtype='bar',color=colors,label=labels,normed=0)
    ax_epe.legend(prop={'size':18},loc='upper right',frameon=False)
    #ax_epe.set_title('EPE distributions')
    ax_epe.set_xlabel('EPE at inner contour (nm)',fontsize=25)
    ax_epe.set_ylabel('Normalized Frequency',fontsize=25)
    for tick in ax_epe.xaxis.get_major_ticks():
        tick.label.set_fontsize(20)
    for tick in ax_epe.yaxis.get_major_ticks():
        tick.label.set_fontsize(20)
    plt.xticks(bins)
    to_percentage = lambda y, pos: str(round( ( y / float(len(epe_inner)) ), 2)) 
    plt.gca().yaxis.set_major_formatter(FuncFormatter(to_percentage))
    plt.gcf().subplots_adjust(bottom=0.15)
    plt.gcf().subplots_adjust(left=0.15)
    fig_epe.savefig('./result/epe_inner_dist.pdf')


    #plot epe at outer contour
    min_epe = 100 
    max_epe = 0
    epe_outer_dist = []
    for fmode in fmodes:
        epe_outer = np.loadtxt('./result/%s/%s_epe_outer.txt' %(fmode, fmode))
        print "%s SRAF - epe outer: %.4f" %(fmode, np.sum(np.absolute(epe_outer)*1.0)/len(epe_outer))
        min_epe = int(np.min([min_epe, np.min(epe_outer)]))
        max_epe = int(np.max([max_epe, np.max(epe_outer)]))
        epe_outer_dist.append(epe_outer)

    fmode = 'ns'
    epe_outer = np.loadtxt('./result/%s/%s_epe_outer.txt' %(fmode, fmode))
    print "%s SRAF - epe outer: %.4f" %(fmode, np.sum(np.absolute(epe_outer)*1.0)/len(epe_outer))

    fig_epe,ax_epe = plt.subplots()
    bins = np.array(range(min_epe-1,max_epe+2))
    plt.ylim([0,len(epe_outer)])
    n,bins,patches = ax_epe.hist(epe_outer_dist,bins=bins,histtype='bar',color=colors,label=labels,normed=0)
    ax_epe.legend(prop={'size':18},loc='upper right',frameon=False)
    #ax_epe.set_title('EPE distributions')
    ax_epe.set_xlabel('EPE at outer contour (nm)',fontsize=25)
    ax_epe.set_ylabel('Normalized Frequency',fontsize=25)
    for tick in ax_epe.xaxis.get_major_ticks():
        tick.label.set_fontsize(20)
    for tick in ax_epe.yaxis.get_major_ticks():
        tick.label.set_fontsize(20)
    plt.xticks(bins)
    to_percentage = lambda y, pos: str(round( ( y / float(len(epe_outer)) ), 2)) 
    plt.gca().yaxis.set_major_formatter(FuncFormatter(to_percentage))
    plt.gcf().subplots_adjust(bottom=0.15)
    plt.gcf().subplots_adjust(left=0.15)
    fig_epe.savefig('./result/epe_outer_dist.pdf')

    #plot pvband 
    fmodes=['mb', 'LGR', 'SVC', 'ns']
    labels = ['Model-based', 'LGR', 'SVC', 'No SRAF']
    colors = ['red','tan','blue','black']
    pv_band_dist = []
    for fmode in fmodes:
        pvband = np.loadtxt('./result/%s/%s_pvband.txt' %(fmode, fmode))
        print "%s SRAF - pv band mean: %.4f" %(fmode, np.sum(pvband*1000.0)/len(pvband))
        pv_band_dist.append(pvband*1000.0)

    fig_pvb,ax_pvb = plt.subplots()
    plt.ylim([0,len(pvband)])
    n,bins,patches = ax_pvb.hist(pv_band_dist,bins=8,histtype='bar',color=colors,label=labels,normed=0)
    ax_pvb.legend(prop={'size':18},loc='upper left',frameon=False)
    #ax_pvb.set_title('PV band distributions')
    ax_pvb.set_xlabel('PV band area ($*0.001 um^2$)',fontsize=25)
    ax_pvb.set_ylabel('Normalized Frequency',fontsize=25)
    for tick in ax_pvb.xaxis.get_major_ticks():
        tick.label.set_fontsize(20)
    for tick in ax_pvb.yaxis.get_major_ticks():
        tick.label.set_fontsize(20)
    plt.xticks(bins)
    to_percentage = lambda y, pos: str(round( ( y / float(len(pvband)) ), 2))
    plt.gca().yaxis.set_major_formatter(FuncFormatter(to_percentage))
    digit_rounding = lambda y, pos: str(round( ( y ), 2))
    plt.gca().xaxis.set_major_formatter(FuncFormatter(digit_rounding))
    plt.gcf().subplots_adjust(bottom=0.15)
    plt.gcf().subplots_adjust(left=0.15)
    fig_pvb.savefig('./result/pvb_dist.pdf')

    #plot runtime
    fig_rt,ax_rt = plt.subplots()
    axis_x = ['3x1', '3x3', '1x1', '2x1', '4x1', '2x2', '2x3']
    #index_x = np.arange(11)
    index_x = np.arange(7)
    bar_width = 0.2
    axis_y_mb = [5.0, 5.0, 5.0, 5.0, 6.0, 5.0, 5.0]
    axis_y_svm = [10.4, 12.2, 9.1, 9.3, 10.9, 10.4, 10.9]
    axis_y_dtree = [0.39, 0.52, 0.34, 0.35, 0.46, 0.38, 0.42]
    axis_y_lgr = [0.44, 0.48, 0.33, 0.36, 0.46, 0.39, 0.42]
    print "model based SRAF - runtime mean: %.4f" %(np.sum(axis_y_mb)/len(axis_y_mb))
    print "DTree SRAF w/ feature optimization - runtime mean: %.4f" %(np.sum(axis_y_dtree)/len(axis_y_dtree))
    #print "SVC SRAF w/ feature optimization - runtime mean: %.4f" %(np.sum(axis_y_svm)/len(axis_y_svm))
    print "LGR SRAF w/ feature optimization - runtime mean: %.4f" %(np.sum(axis_y_lgr)/len(axis_y_lgr))
    plt.ylim([0,8])
    ax_rt.bar(index_x,axis_y_mb,bar_width,color=colors[0],label='Calibre')
    #ax_rt.bar(index_x+bar_width,axis_y_svm,bar_width,color=colors[1],label='SVM')
    ax_rt.bar(index_x+bar_width,axis_y_dtree,bar_width,color=colors[1],label='DTree')
    ax_rt.bar(index_x+2*bar_width,axis_y_lgr,bar_width,color=colors[2],label='LGR')
    ax_rt.legend(prop={'size':18},loc='upper right',frameon=False)
    ax_rt.set_xlabel('Layout windows with dense contact patterns (m x n)',fontsize=20)
    ax_rt.set_ylabel('Runtime (s)',fontsize=25)
    for tick in ax_rt.xaxis.get_major_ticks():
        tick.label.set_fontsize(20)
    for tick in ax_rt.yaxis.get_major_ticks():
        tick.label.set_fontsize(20)
    plt.xticks(index_x+bar_width,axis_x)
    plt.gcf().subplots_adjust(bottom=0.15)
    plt.gcf().subplots_adjust(left=0.15)
    fig_rt.savefig('./result/runtime.pdf')

    #plot runtime
    fig_rt,ax_rt = plt.subplots()
    axis_x = ['$1*1$', '$2*2$', '$3*3$', '$4*4$', '$5*5$', '$6*6$', '$7*7$', '$8*8$', '$9*9$', '$10*10$']
    #index_x = np.arange(11)
    index_x = np.arange(10)
    bar_width = 0.2
    axis_y_mb = [3, 4, 5, 8, 16, 23, 31, 38, 42, 46]
    axis_y_lgr = [0.378947, 1.063898, 2.129623, 3.539934, 4.390526, 6.369322, 8.622571, 11.477952, 14.607127, 16.381109]
    axis_y_svm = [0.318160, 1.023980, 1.918080, 3.349988, 4.291499, 5.969570, 8.366796, 11.192263, 13.615556, 15.229888]
    print "model based SRAF - runtime mean: %.4f" %(np.sum(axis_y_mb)/len(axis_y_mb))
    print "SVC SRAF w/ feature optimization - runtime mean: %.4f" %(np.sum(axis_y_svm)/len(axis_y_svm))
    print "LGR SRAF w/ feature optimization - runtime mean: %.4f" %(np.sum(axis_y_lgr)/len(axis_y_lgr))
    plt.ylim([0,50])
    ax_rt.bar(index_x,axis_y_mb,bar_width,color=colors[0],label='Calibre')
    ax_rt.bar(index_x+2*bar_width,axis_y_lgr,bar_width,color=colors[2],label='LGR')
    ax_rt.bar(index_x+bar_width,axis_y_svm,bar_width,color=colors[1],label='SVC')
    ax_rt.legend(prop={'size':18},loc='upper left',frameon=False)
    ax_rt.set_xlabel('Layout size ($um*um$)',fontsize=20)
    ax_rt.set_ylabel('Runtime (s)',fontsize=25)
    for tick in ax_rt.xaxis.get_major_ticks():
        tick.label.set_fontsize(18)
    for tick in ax_rt.yaxis.get_major_ticks():
        tick.label.set_fontsize(20)
    plt.xticks(index_x+bar_width,axis_x)
    plt.gcf().subplots_adjust(bottom=0.15)
    plt.gcf().subplots_adjust(left=0.15)
    fig_rt.savefig('./result/runtime_1.pdf')

    fig_rt,ax_rt = plt.subplots()
    axis_x = ['$1*1$', '$2*2$', '$3*3$', '$4*4$', '$5*5$', '$6*6$', '$7*7$', '$8*8$', '$9*9$', '$10*10$']
    #index_x = np.arange(11)
    index_x = np.arange(10)
    bar_width = 0.2
    axis_y_mb = [3, 4, 5, 8, 16, 23, 31, 38, 42, 46]
    axis_y_lgr = [0.320830, 1.018900, 2.065791, 3.553834, 4.334820, 6.351836, 8.751027, 11.461194, 14.787478, 16.336626]
    axis_y_svm = [0.354090, 1.037202, 2.075233, 3.616757, 4.440044, 6.262499, 9.065291, 11.465377, 14.597726, 16.494963]
    print "model based SRAF - runtime mean: %.4f" %(np.sum(axis_y_mb)/len(axis_y_mb))
    print "SVC SRAF w/ feature optimization - runtime mean: %.4f" %(np.sum(axis_y_svm)/len(axis_y_svm))
    print "LGR SRAF w/ feature optimization - runtime mean: %.4f" %(np.sum(axis_y_lgr)/len(axis_y_lgr))
    plt.ylim([0,50])
    ax_rt.bar(index_x,axis_y_mb,bar_width,color=colors[0],label='Calibre')
    ax_rt.bar(index_x+2*bar_width,axis_y_lgr,bar_width,color=colors[2],label='LGR')
    ax_rt.bar(index_x+bar_width,axis_y_svm,bar_width,color=colors[1],label='SVC')
    ax_rt.legend(prop={'size':18},loc='upper left',frameon=False)
    ax_rt.set_xlabel('Layout size ($um*um$)',fontsize=20)
    ax_rt.set_ylabel('Runtime (s)',fontsize=25)
    for tick in ax_rt.xaxis.get_major_ticks():
        tick.label.set_fontsize(18)
    for tick in ax_rt.yaxis.get_major_ticks():
        tick.label.set_fontsize(20)
    plt.xticks(index_x+bar_width,axis_x)
    plt.gcf().subplots_adjust(bottom=0.15)
    plt.gcf().subplots_adjust(left=0.15)
    fig_rt.savefig('./result/runtime_2.pdf')

if __name__ == "__main__":
    plot_histogram()

