#########################################################################
# File Name: gdb.sh
# Author: Yibo Lin
# mail: yibolin@utexas.edu
# Created Time: Sat Sep 26 13:10:20 2015
#########################################################################
#!/bin/bash

exe="mask_opt"
#exe="flatten_layout"
valgrind=false

if [[ $exe == "mask_opt" ]]; then 

# under lldb, I have to use \\(\\) for bbox argument 
benchmark=Via1_small
commands="\
    -param_json ./test/param_sraf_ispd15.json \
    "

elif [[ $exe == "flatten_layout" ]]; then 

commands="\
    -input_gds ./test/simple_nest.gds \
    -output_gds ./simple_nest-flat.gds \
    -keep_top_only true \
    "

fi 

if [[ $valgrind == "true" ]]; then 

echo "run valgrind for memory check"
valgrind --leak-check=yes \
    --log-file="valgrind.txt" \
    "bin/${exe}" \
    $commands
return 

fi

uname_str=$(uname)

# run placement 
if [[ $uname_str == "Linux" ]]; then 

gdb -ex "source $LIBRARIES_DIR/gdb_container.sh" \
	--args "bin/${exe}" \
    $commands

elif [[ $uname_str == "Darwin" ]]; then 

lldb \
    -- "bin/${exe}" \
    $commands

#    "bin/${exe}" \
#    $commands
fi

