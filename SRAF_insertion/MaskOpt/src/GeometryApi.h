/*************************************************************************
    > File Name: GeometryApi.h
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Tue 18 Aug 2015 11:01:34 PM CDT
 ************************************************************************/

#ifndef MASKOPT_GEOMETRYAPI_H
#define MASKOPT_GEOMETRYAPI_H

// for Polygon2Rectangle
#include <limbo/geometry/api/BoostPolygonApi.h>
#include <limbo/geometry/Polygon2Rectangle.h>
// use adapted boost.polygon in boost.geometry, which is compatible to rtree
#include <boost/geometry/geometries/adapted/boost_polygon.hpp>
#include <boost/geometry/index/rtree.hpp>

MASKOPT_BEGIN_NAMESPACE

class Shape;

MASKOPT_END_NAMESPACE

namespace gtl = boost::polygon;

/// API for Boost.Geometry 
namespace boost { namespace geometry { namespace index {

template <typename Box>
struct indexable< boost::shared_ptr<Box> >
{
    typedef boost::shared_ptr<Box> V;

    typedef Box const& result_type;
    result_type operator()(V const& v) const { return *v; }
};

template <typename Box>
struct indexable< Box* >
{
    typedef Box* V;

    typedef Box const& result_type;
    result_type operator()(V const& v) const { return *v; }
};

template <>
struct indexable< MaskOpt::Shape* >
{
    typedef MaskOpt::Shape shape_base_type;
    typedef gtl::rectangle_data<shape_base_type::coordinate_type> result_type;

    result_type operator()(shape_base_type* shape) const {return shape->bbox();}
};

}}} // namespace boost // namespace geometry // namespace index

namespace boost { namespace geometry { namespace traits {

//////// for Boost.rectangle_data ////////

//////// for rectangles ////////
template <>
struct tag<MaskOpt::Rectangle > : public tag<MaskOpt::Rectangle::base_type>
{};


template <>
struct point_type<MaskOpt::Rectangle >
{
    typedef MaskOpt::Rectangle::point_type type;
};


template <>
struct indexed_access
<
    MaskOpt::Rectangle,
    min_corner, 0
> : public indexed_access<MaskOpt::Rectangle::base_type, min_corner, 0>
{};


template <>
struct indexed_access
<
    MaskOpt::Rectangle,
    min_corner, 1
> : public indexed_access<MaskOpt::Rectangle::base_type, min_corner, 1>
{};


template <>
struct indexed_access
<
    MaskOpt::Rectangle,
    max_corner, 0
> : public indexed_access<MaskOpt::Rectangle::base_type, max_corner, 0>
{};


template <>
struct indexed_access
<
    MaskOpt::Rectangle,
    max_corner, 1
> : public indexed_access<MaskOpt::Rectangle::base_type, max_corner, 1>
{};

}}} // namespace boost // namespace geometry // namespace traits

/// API for Boost.Polygon
namespace boost { namespace polygon {

/// necessary for customized rectangle types 
template <>
struct geometry_concept<MaskOpt::Rectangle > 
{
	typedef rectangle_concept type;
};
template <>
struct geometry_concept<MaskOpt::Polygon90 > 
{
	typedef polygon_90_concept type;
};
template <>
struct geometry_concept<MaskOpt::Polygon > 
{
	typedef polygon_concept type;
};

/// bug in boost library in the following function
/// function intersects and intersect do not always return the same results (when consider_touch = false)
/// create a specialization to resolve it 
  template <>
  inline typename enable_if< typename gtl_and_3<y_r_b_intersect3, typename is_mutable_rectangle_concept<typename geometry_concept<MaskOpt::Rectangle >::type>::type,
                                         typename is_rectangle_concept<typename geometry_concept<MaskOpt::Rectangle >::type>::type>::type,
                       bool>::type
  intersect(MaskOpt::Rectangle& rectangle, const MaskOpt::Rectangle& b, bool consider_touch) {
	  // the original version is "intersects(rectangle, b)" without consider_touch 
    if(intersects(rectangle, b, consider_touch)) {
      intersect(rectangle, horizontal(b), HORIZONTAL, consider_touch);
      intersect(rectangle, vertical(b), VERTICAL, consider_touch);
      return true;
    }
    return false;
  }

/// bug in boost library
  template <>
  inline typename enable_if< typename gtl_and_3<y_r_edist2, typename is_rectangle_concept<typename geometry_concept<MaskOpt::Rectangle >::type>::type,
                                                          typename is_rectangle_concept<typename geometry_concept<MaskOpt::Rectangle >::type>::type>::type,
                       typename rectangle_distance_type<MaskOpt::Rectangle >::type>::type
  euclidean_distance(const MaskOpt::Rectangle & lvalue, const MaskOpt::Rectangle& rvalue) {
    typename rectangle_distance_type<MaskOpt::Rectangle >::type val = square_euclidean_distance(lvalue, rvalue); // originally the result is cast to int, which causes overflow 
    return std::sqrt(val);
  }

}} // namespace boost // namespace polygon

/// API for limbo::geometry
namespace limbo { namespace geometry {

/// \brief specialization for MaskOpt::Rectangle
template <>
struct rectangle_traits<MaskOpt::Rectangle >// : public rectangle_traits<boost::polygon::rectangle_data<T> >
{
	typedef MaskOpt::Rectangle rectangle_type;
    typedef rectangle_type::coordinate_type coordinate_type;

	static coordinate_type get(const typename rectangle_type::base_type& rect, direction_2d const& dir) 
	{
		switch (dir)
		{
			case LEFT: return boost::polygon::xl(rect);
			case BOTTOM: return boost::polygon::yl(rect);
			case RIGHT: return boost::polygon::xh(rect);
			case TOP: return boost::polygon::yh(rect);
			default: assert_msg(0, "unknown direction_2d type");
		}
	}
	static void set(typename rectangle_type::base_type& rect, direction_2d const& dir, coordinate_type const& value) 
	{
		switch (dir)
		{
			case LEFT: boost::polygon::xl(rect, value); break;
			case BOTTOM: boost::polygon::yl(rect, value); break;
			case RIGHT: boost::polygon::xh(rect, value); break;
			case TOP: boost::polygon::yh(rect, value); break;
			default: assert_msg(0, "unknown direction_2d type");
		}
	}
	static rectangle_type construct(coordinate_type const& xl, coordinate_type const& yl, 
			coordinate_type const& xh, coordinate_type const& yh) 
	{
		return rectangle_type(xl, yl, xh, yh); 
	}
};

}} // namespace limbo // namespace geometry

#endif
