/*************************************************************************
    > File Name: FlattenLayoutMain.cpp
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Tue Oct  6 17:55:11 2015
 ************************************************************************/

#include "FlattenLayout.h"

int main(int argc, char** argv)
{
    MaskOpt::FlattenLayout fl;
    fl(argc, argv);

    return 0;
}
