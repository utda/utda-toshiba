/*************************************************************************
    > File Name: msg.h
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Fri 31 Jul 2015 03:18:29 PM CDT
 ************************************************************************/

#ifndef MASKOPT_MSG_H
#define MASKOPT_MSG_H

#include <cstdarg>
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include "Namespace.h"

MASKOPT_BEGIN_NAMESPACE

/// message type for print functions 
enum MessageType {
	kNONE = 0, 
	kINFO = 1, 
	kWARN = 2, 
	kERROR = 3, 
	kDEBUG = 4, 
    kASSERT = 5
};

/// print to screen (stdout)
int maskPrint(MessageType m, const char* format, ...);
/// print to stream 
int maskPrintStream(MessageType m, FILE* stream, const char* format, ...);
/// core function to print formatted data from variable argument list 
int maskVPrintStream(MessageType m, FILE* stream, const char* format, va_list args);
/// format to a buffer 
int maskSPrint(MessageType m, char* buf, const char* format, ...);
/// core function to format a buffer 
int maskVSPrint(MessageType m, char* buf, const char* format, va_list args);
/// format prefix 
int maskSPrintPrefix(MessageType m, char* buf);

/// assertion 
void maskPrintAssertMsg(const char* expr, const char* fileName, unsigned lineNum, const char* funcName, const char* format, ...);
void maskPrintAssertMsg(const char* expr, const char* fileName, unsigned lineNum, const char* funcName);

#define maskAssertMsg(condition, args...) do {\
    if (!(condition)) \
    {\
        ::MASKOPT_NAMESPACE::maskPrintAssertMsg(#condition, __FILE__, __LINE__, __PRETTY_FUNCTION__, args); \
        abort(); \
    }\
} while (false)
#define maskAssert(condition) do {\
    if (!(condition)) \
    {\
        ::MASKOPT_NAMESPACE::maskPrintAssertMsg(#condition, __FILE__, __LINE__, __PRETTY_FUNCTION__); \
        abort(); \
    }\
} while (false)

/// static assertion 
template <bool>
struct maskStaticAssert;
template <>
struct maskStaticAssert<true> 
{
    maskStaticAssert(const char* = NULL) {}
};


MASKOPT_END_NAMESPACE

#endif
