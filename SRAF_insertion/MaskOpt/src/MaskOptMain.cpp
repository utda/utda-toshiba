/*************************************************************************
    > File Name: AerialImageMain.cpp
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Fri 25 Sep 2015 03:58:53 PM CDT
 ************************************************************************/

#include "MaskImage.h"

int main(int argc, char** argv)
{
    MaskOpt::MaskImage mask_data;
    mask_data(argc, argv);

    return 0;
}
