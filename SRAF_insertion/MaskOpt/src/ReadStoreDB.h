/*************************************************************************
    > File Name: ReadStoreDB.h
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Thu 24 Sep 2015 09:42:57 PM CDT
 ************************************************************************/

#ifndef MASKOPT_READSTOREDB_H
#define MASKOPT_READSTOREDB_H

#include "LayoutDB.h"
#include "Cell.h"

MASKOPT_BEGIN_NAMESPACE

/// read/store layout database class 
/// this is designed for very complex manipulation to the layout
/// it is necessary to store all the information in the layout 
class ReadStoreDB : public LayoutDB
{
    public:
        typedef LayoutDB base_type;
        typedef gtl::coordinate_traits<coordinate_type>::manhattan_area_type area_type;
        typedef gtl::coordinate_traits<coordinate_type>::coordinate_difference coordinate_difference;
        typedef Rectangle rectangle_type;
        /// rectilinear polygon type 
        typedef base_type::polygon_90_type polygon_90_type;
        /// general polygon type 
        typedef base_type::polygon_type polygon_type;
        typedef base_type::path_type path_type;
        typedef base_type::text_type text_type;
        typedef Cell cell_type;
        typedef cell_type::shape_base_type shape_base_type;

        /// constructor 
        ReadStoreDB() {}
        /// forbidden 
        ReadStoreDB(ReadStoreDB const& rhs);
        //clear all data
        void clear();
        virtual ~ReadStoreDB();
        /// forbidden 
        ReadStoreDB& operator=(ReadStoreDB const& rhs);

        /// begin lib 
        virtual void begin_lib();
        /// end lib 
        virtual void end_lib();
        /// set strname 
        virtual void add_cell(std::string const& str);
        /// add a box 
        virtual void add_box(int32_t layer, std::vector<point_type> const& vPoint);
        /// add a boundary 
        virtual void add_boundary(int32_t layer, std::vector<point_type> const& vPoint);
        /// add paths 
        virtual void add_path(int32_t layer, std::vector<point_type> const& vPoint, coordinate_type width);
        /// add text 
        virtual void add_text(int32_t layer, std::string const& str, point_type const& pos, int32_t width);
        /// add sref 
        virtual void add_sref(std::string const& str, point_type const& pos, double mag, double angle);

        std::vector<cell_type*> const& cells() const {return m_vCells;}
        std::vector<cell_type*>& cells() {return m_vCells;}

        std::size_t cell_index(std::string const& cellName) const;

        /// \return parent list of a cell, immutable 
        std::vector<std::size_t> const& parents(std::size_t cell_id) const {return m_vParents[cell_id];}

    protected:
        /// initialize cell hierarchy, set m_vParents
        /// must be called after finishing reading layout, currently called in end_lib() function
        void init_cell_hierarchy(); 

        std::vector<cell_type*> m_vCells; ///< cells 
        std::map<std::string, std::size_t> m_mCellName2Idx; ///< retrieve cell index with cell name 
        std::vector<std::vector<std::size_t> > m_vParents; ///< parents for each cell, used to record cell hierarchy, only valid after finishing reading layout  
};

MASKOPT_END_NAMESPACE

#endif
