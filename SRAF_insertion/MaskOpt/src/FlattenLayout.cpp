/*************************************************************************
    > File Name: FlattenLayout.cpp
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Tue Oct  6 17:10:14 2015
 ************************************************************************/

#include "FlattenLayout.h"
#include "GdsiiIO.h"
#include <limbo/programoptions/ProgramOptions.h>

MASKOPT_BEGIN_NAMESPACE

FlattenLayoutParams::FlattenLayoutParams()
    : input_gds()
    , output_gds()
    , keep_top_only(true)
{
}

void FlattenLayout::operator()(int32_t argc, char** argv) 
{
    maskAssert(read_cmd(argc, argv));
    maskAssert(read_layout(m_params.input_gds));
    maskAssert(write_output(m_params.output_gds));
}

bool FlattenLayout::read_cmd(int32_t argc, char** argv)
{
    maskPrint(kINFO, "commands =");
    for (int i = 0; i < argc; ++i)
        maskPrint(kNONE, " %s", argv[i]);
    maskPrint(kNONE, "\n");

    FlattenLayoutParams default_params; // get default value from default constructor 
    bool help = false;
    // append options here 
    typedef limbo::programoptions::ProgramOptions po_type;
    using limbo::programoptions::Value;
    po_type desc (std::string("Flatten Layout Usage"));
    desc.add_option(Value<bool>("-help", &help, "print help message").toggle(true).default_value(false).toggle_value(true).help(true))
        .add_option(Value<std::string>("-input_gds", &m_params.input_gds, "input gds file name").required(true))
        .add_option(Value<std::string>("-output_gds", &m_params.output_gds, "output gds file name").required(true))
        .add_option(Value<bool>("-keep_top_only", &m_params.keep_top_only, "only write flattened top cells to output gdsii file").default_value(default_params.keep_top_only))
        ;
    try
    {
        desc.parse(argc, argv);

        // print help message 
        if (help)
        {
            std::cout << desc << "\n";
            exit(1);
        }
    }
    catch (std::exception& e)
    {
        // print help message and error message 
        std::cout << desc << "\n";
        maskPrint(kERROR, "%s\n", e.what());
        return false;
    }
    return true;
}
bool FlattenLayout::read_layout(std::string const& filename)
{
    maskPrint(kINFO, "reading input gdsii %s\n", filename.c_str());
    GdsReader gdsReader (m_db);
    return gdsReader(filename);
}
bool FlattenLayout::write_output(std::string const& filename) const
{
    maskPrint(kINFO, "write output gdsii %s\n", filename.c_str());
    GdsWriter gw;
    gw.open(filename);
    gw.begin_lib(m_db.libname(), m_db.unit()*1.0e+6);

    // write cells 
    for (std::vector<cell_type*>::const_iterator it1 = m_db.cells().begin(), it1e = m_db.cells().end(); it1 != it1e; ++it1)
    {
        cell_type const& cell = **it1;
        // if keep_top_only set, skip child cells 
        if (m_params.keep_top_only && !m_db.parents(cell.cell_id()).empty())
            continue;
        write_cell(gw, cell, gtl::construct<point_type>(0, 0), 0);
    }

    gw.end_lib();
    gw.close();
    return true;
}
void FlattenLayout::write_cell(GdsWriter& gw, FlattenLayout::cell_type const& cell, FlattenLayout::point_type const& offset, int32_t level) const
{
    if (level == 0)
        gw.begin_cell(cell.name());
    for (std::map<int32_t, std::vector<shape_base_type*> >::const_iterator it1 = cell.shapes().begin(), it1e = cell.shapes().end(); it1 != it1e; ++it1)
    {
        std::vector<shape_base_type*> const& vShapes = it1->second;
        for (std::vector<shape_base_type*>::const_iterator it2 = vShapes.begin(), it2e = vShapes.end(); it2 != it2e; ++it2)
        {
            shape_base_type* shape = *it2;
            if (shape->tag() & ShapeTag::SREF) 
            {
                sref_type* cshape = static_cast<sref_type*>(shape);
                if ((cshape->has_mag() && cshape->mag() != 1.0) || (cshape->has_angle() && cshape->angle() != 0.0))
                {
                    maskPrint(kERROR, "unsupported (mag,angle) = (%g,%g)\n", cshape->mag(), cshape->angle());
                    exit(-1);
                }
                std::size_t cell_index = m_db.cell_index(cshape->sname());
                maskAssertMsg(cell_index < std::numeric_limits<std::size_t>::max(), "cell name %s not found, cell_index = %lu", cshape->sname().c_str(), cell_index);
                cell_type const* sub_cell = m_db.cells()[cell_index];
                // recursively write cells 
                write_cell(gw, *sub_cell, gtl::construct<point_type>(offset.x()+cshape->pos().x(), offset.y()+cshape->pos().y()), level+1);
            }
            else gw.write_shape(shape, ShapeTag::RECTANGLE | ShapeTag::POLYGON90 | ShapeTag::POLYGON | ShapeTag::PATH | ShapeTag::TEXT, offset);
        }
    }
    if (level == 0)
        gw.end_cell();
}


MASKOPT_END_NAMESPACE
