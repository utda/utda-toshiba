/**
 * @file   MaskFeature.h
 * @author Xiaoqing Xu
 * @date   Feb 2017
 */

#ifndef MASKOPT_MASKFEATURE_H
#define MASKOPT_MASKFEATURE_H

#include <boost/numeric/ublas/vector_sparse.hpp>
#include <boost/numeric/ublas/io.hpp>

#include "GdsiiIO.h"
#include "Namespace.h"

MASKOPT_BEGIN_NAMESPACE

// forward declaration 
class MaskImage;

class FeatureDB
{
  public:
    typedef boost::numeric::ublas::compressed_vector<float> feature_vec_type; ///< 
    typedef std::vector<float> flatten_feature_vec_type;

    virtual feature_vec_type& get_feature_vec(unsigned int idx) = 0;
    virtual feature_vec_type const& get_feature_vec(unsigned int idx) const = 0;
    virtual unsigned int feature_vec_non_zeros(unsigned int idx) const = 0;
    virtual std::vector<feature_vec_type>& get_feature_vecs() = 0;
    virtual std::vector<feature_vec_type> const& get_feature_vecs() const = 0;
    virtual size_t size() const = 0; 
    virtual size_t featureDim() const = 0;
    virtual bool feature_extraction(MaskImage const& image) = 0;
    
    //for debug
    virtual void write_feature_shapes(GdsWriter& gw, MaskImage const& image) const = 0;
 
    FeatureDB() {};
    virtual ~FeatureDB() {};
};

class CCAS : public FeatureDB
{
  public:
    CCAS(int mr, int rs, int rn, int cs);
    virtual ~CCAS() {};

    //index featureVecs
    virtual feature_vec_type& get_feature_vec(unsigned int idx) {return featureVecs.at(idx);}
    virtual feature_vec_type const& get_feature_vec(unsigned int idx) const {return featureVecs.at(idx);}
    virtual unsigned int feature_vec_non_zeros(unsigned int idx) const;
    virtual std::vector<feature_vec_type>& get_feature_vecs() {return featureVecs;}
    virtual std::vector<feature_vec_type> const& get_feature_vecs() const {return featureVecs;}
    size_t size() const {return featureVecs.size();}
    size_t featureDim() const {return ringNum*circleSR;}

    //change featureVecs
    void resize(unsigned int _size) {featureVecs.resize(_size);}
    void clear() {featureVecs.clear();}

    virtual bool feature_extraction(MaskImage const& image);
    //sampling at a SRAF grid 
    void feature_sampling(MaskImage const& image, unsigned int sGridIdx, std::vector<float>& fVec);
    //feature compaction based on symmetry
    void feature_compaction(std::vector<float>& fVec, feature_vec_type& kVec);
    
    //index each feature vector
    unsigned int idxy2index(unsigned int idx, unsigned int idy) const {return circleSR*idx + idy;}
    std::pair<unsigned int, unsigned int> index2idxy(unsigned int index) const {return std::make_pair(index/circleSR, index%circleSR);}

    //debug 
    void print(feature_vec_type const& vec);
    bool empty_vec(feature_vec_type const& vec) const;
    virtual void write_feature_shapes(GdsWriter& gw, MaskImage const& image) const;

  protected:
    //feature data 
    std::vector<feature_vec_type> featureVecs;

    int minRadius; ///< minimum radius of the circle
    int ringSpace; ///< ring space in nm 
    int ringNum; ///< number of rings 
    int circleSR; ///< sampling rate on each circle
    std::vector<int> xdisp_lut; ///< flattened 2D table for x displacement
    std::vector<int> ydisp_lut; ///< flattened 2D table for y displacement

};

MASKOPT_END_NAMESPACE

#endif
