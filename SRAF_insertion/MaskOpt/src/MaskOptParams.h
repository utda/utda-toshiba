/*************************************************************************
    > File Name: aerial_image_energy/MaskOptParams.h
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Fri 25 Sep 2015 01:23:48 PM CDT
 ************************************************************************/

#ifndef MASKOPT_PARAMS_H
#define MASKOPT_PARAMS_H

MASKOPT_BEGIN_NAMESPACE

enum LayerType {
    TARGET_LAYER = 0, ///< it can be final target or litho target, depends on what regression data you want
    SRAF_LAYER = 1, ///< SRAF layer index 
    NUM_LAYERS = 2
};

struct MaskOptParams
{
    MaskOptParams();
    void print() const;

    std::string param_json; ///< input json parameters
    int search; ///< search model or not
    //litho parameters
    double Lambda; ///< lithography wavelenght in micron
    double NA; ///< numeric aperture
    double Lx; ///< optical parameter
    int opc_region; ///< OPC region 
    int sraf_region; ///< SRAF region 
    int sraf_size; ///< SRAF sampling grid size
    int pixel_size[2]; ///< pixel size x/y for pixel_data, in nm
    int sraf_grid[2]; ///< SRAF size x/y for sraf_data, in nm
    //training data
    std::string training_gds_dir; ///< training gds folder
    std::set<std::string> training_gds; ///< training gdsii files
    std::string training_gds_cell; ///< training cell name in training_gds
    int32_t training_layers[2]; ///< target, SRAF layer in a training gds file 
    std::string output_training_data; ///< pixel based training feature/label data 
    //testing data
    std::string testing_gds_dir; /// testing gds folder
    std::set<std::string> testing_gds; ///< testing gdsii files
    std::string testing_gds_cell; ///< testing cell name in testing_gds
    int32_t testing_layers[2]; ///< target, SRAF layer in a testing gds file 
    std::string output_testing_data; ///< pixel based testing feature data
    int output_gds; ///< if not zero, generate SRAF for each layout clips
    //cell names in training/testing data
    std::set<std::string> sCellNames; ///< cell names
    //feature extraction options
    double effective_distance; ///< lithography simulation window size in micron
    std::string feature_mode;  ///< feature mode [CCAS]
    //for CCAS feature
    int rinSpace;   ///< the space between each rin
    int circleSR; ///< the sampling rate on each circle
    //feature optimization options
    std::string dim_reduction; ///< feature optimization option [None, "PCA", "LPP"]
    //learning model options
    std::string learning_model; ///< calibration model, [LGR, LGR_Adaboost, SVC, DTree]
    //model parameter scaling factor 
    int model_sf; ///< the scaling factor for model parameters
    //probability threshold 
    int prob_threshold; ///< probability threshold for label 1
    //parameters for LGR with liblinear
    int lgr_eps;
    int lgr_c; 
    int lgr_w0;
    int lgr_w1;
    //parameters for SVC with libsvm 
    int svc_degree;
    int svc_gamma;
    int svc_coef0;
    int svc_eps;
    int svc_c;
    int svc_w0;
    int svc_w1;
    //learning model path, pre-calibrated 
    std::string model_path; ///< pre-calibrated model file path 
    //lithosim
    std::string simulation_dir; ///< lithosim folder
    //result dir 
    std::string result_dir; ///< result folder
    //debug dir 
    std::string debug_dir; ///< debug folder
};

inline MaskOptParams::MaskOptParams()
{
    param_json = "";
    search = 0;
    Lambda = 0.193;
    NA = 1.35;
    Lx = 0.5;
    opc_region = 0;
    sraf_region = 0;
    sraf_size = 0;
    pixel_size[0] = pixel_size[1] = 0;
    sraf_grid[0] = sraf_grid[1] = 0;
    training_gds_dir = "";
    training_gds = std::set<std::string>();
    training_gds_cell = "";
    training_layers[0] = training_layers[1] = -1;
    output_training_data = "";
    testing_gds_dir = "";
    testing_gds = std::set<std::string>();
    testing_gds_cell = "";
    testing_layers[0] = testing_layers[1] = -1;
    output_testing_data = "";
    output_gds = 0;
    sCellNames = std::set<std::string>();
    effective_distance = 0;
    feature_mode = "";
    rinSpace = 0;
    circleSR = 0;
    dim_reduction = "None";
    learning_model = "";
    model_sf = 1000;
    prob_threshold = 500;
    lgr_eps = 10000;
    lgr_c = model_sf;
    lgr_w0 = model_sf;
    lgr_w1 = model_sf;
    svc_degree = 1;
    svc_gamma = model_sf;
    svc_coef0 = model_sf;
    svc_eps = 10000;
    svc_c = model_sf;
    svc_w0 = model_sf;
    svc_w1 = model_sf;
    model_path = "";
    simulation_dir = "";
    result_dir = "";
    debug_dir = "";
}

inline void MaskOptParams::print() const
{

    #include<iostream>
    using namespace std;
    cout << "Lambda: " << Lambda << endl;
    cout << "NA: " << NA << endl;
    cout << "Lx: " << Lx << endl;
    cout << "OPC_REGION: " << opc_region << endl;
    cout << "SRAF_REGION: " << sraf_region << endl;
    cout << "GRIDSIZE: " << sraf_grid[0] << endl;
    cout << "IMGGRID: " << pixel_size[0] << endl;
    cout << "SRAFSIZE: " << sraf_size << endl;
    cout << "LAYOUT_LEARN: " << endl;
    cout << "\t MSGDSDIR: " << training_gds_dir << endl;
    cout << "\t GDS:";
    for(std::set<std::string>::const_iterator it = training_gds.begin(), ite = training_gds.end(); it != ite; ++it)
    {
      cout << " "<< *it;
    }
    cout << endl;
    cout << "\t TOPCELL: " << training_gds_cell << endl;
    cout << "\t TARGET_LAYER: " << training_layers[0] << endl;
    cout << "\t SRAFLayer: " << training_layers[1] << endl;
    cout << "\t OUTPUT_TRAINING_DATA: " << output_training_data << endl;

    cout << "LAYOUT_TEST: " << endl;
    cout << "\t MSGDSDIR: " << testing_gds_dir << endl;
    cout << "\t GDS:";
    for(std::set<std::string>::const_iterator it = testing_gds.begin(), ite = testing_gds.end(); it != ite; ++it)
    {
      cout << " "<< *it;
    }
    cout << endl;
    cout << "\t TOPCELL: " << testing_gds_cell << endl;
    cout << "\t TARGET_LAYER: " << testing_layers[0] << endl;
    cout << "\t SRAFLayer: " << testing_layers[1] << endl;
    cout << "\t OUTPUT_TESTING_DATA: " << output_testing_data << endl;
    cout << "\t SIMDIR: " << simulation_dir << endl;

    cout << "\t RESULT_DIR: " << result_dir << endl;
    cout << "\t DEBUG_DIR: " << debug_dir << endl;

    cout << "FEATURE: " << endl;
    cout << "\t Wx: " << effective_distance << endl;
    cout << "\t Mode: " << feature_mode << endl;
    cout << "\t CCAS: " << endl;
    cout << "\t \t Rin: " << rinSpace << endl;
    cout << "\t \t CircleSampleRate: " << circleSR << endl;
    cout << "\t DIM_REDUCTION: " << endl;
    cout << "\t \t Mode: " << dim_reduction << endl;
    cout << "MODEL: " << learning_model << endl;
    cout << "ProbabilityThreshold: " << prob_threshold << endl;
}

MASKOPT_END_NAMESPACE

#endif
