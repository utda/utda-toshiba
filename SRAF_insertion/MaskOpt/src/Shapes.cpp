/*************************************************************************
    > File Name: Shapes.cpp
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Sat Sep 26 10:38:27 2015
 ************************************************************************/

#include "Shapes.h"

MASKOPT_BEGIN_NAMESPACE

const int32_t DummyLayer::SREF_LAYER = std::numeric_limits<int32_t>::max()-1;

Shape::Shape() {this->initialize();}
Shape::Shape(Shape const& rhs) {this->copy(rhs);}
Shape& Shape::operator=(Shape const& rhs)
{
    if (this != &rhs)
        this->copy(rhs);
    return *this;
}
Shape::~Shape() {}

Shape::operator std::string() const 
{
    std::ostringstream oss;
    this->print(oss);
    return oss.str();
}

void Shape::print(std::ostream& os) const 
{
    os << m_pattern_id << ": " << m_layer << " " << m_tag << " ";
}

void Shape::initialize()
{
    m_layer = -1;
    m_pattern_id = std::numeric_limits<uint32_t>::max();
    m_tag = ShapeTag::UNKNOWN;
#ifdef DEBUG
#ifdef _OPENMP
#pragma omp critical 
#endif
    m_internal_id = generate_id();
#endif
}
void Shape::copy(Shape const& rhs)
{
    this->m_layer = rhs.m_layer;
    this->m_pattern_id = rhs.m_pattern_id;
    this->m_tag = rhs.m_tag;
#ifdef DEBUG
    this->m_internal_id = rhs.m_internal_id;
#endif
}
int64_t Shape::generate_id()
{
    static int64_t cnt = -1;
    maskAssert(cnt < std::numeric_limits<int64_t>::max());
#ifdef _OPENMP
#pragma omp atomic 
#endif
    cnt += 1;
    return cnt;
}


Rectangle::Rectangle() 
    : Rectangle::shape_base_type(), Rectangle::base_type() 
{}
Rectangle::Rectangle(Rectangle::interval_type const& hor, Rectangle::interval_type const& ver) 
    : Rectangle::shape_base_type(), Rectangle::base_type(hor, ver) 
{}
Rectangle::Rectangle(Rectangle::coordinate_type xl, Rectangle::coordinate_type yl, Rectangle::coordinate_type xh, Rectangle::coordinate_type yh) 
    : Rectangle::shape_base_type(), Rectangle::base_type(xl, yl, xh, yh) 
{}
Rectangle::Rectangle(Rectangle const& rhs) 
    : Rectangle::shape_base_type(rhs), Rectangle::base_type(rhs) 
{}
Rectangle& Rectangle::operator=(Rectangle const& rhs)
{
    if (this != &rhs)
    {
        this->base_type::operator=(rhs);
        this->shape_base_type::operator=(rhs);
    }
    return *this;
}
Rectangle::~Rectangle() 
{}

Rectangle::shape_base_type* Rectangle::bloat(coordinate_type width) const 
{
  Rectangle* rectPtr = new Rectangle(*this);
  gtl::bloat(*rectPtr, width);
  return rectPtr;
}

void Rectangle::print(std::ostream& os) const 
{
    this->shape_base_type::print(os);
    os << "(" << gtl::xl(*this) << ", " << gtl::yl(*this) << ", " << gtl::xh(*this) << ", " << gtl::yh(*this) << ")";
}
gtl::rectangle_data<Rectangle::coordinate_type> Rectangle::bbox() const 
{
    return *this;
}

Polygon90::Polygon90() 
    : Polygon90::shape_base_type(), Polygon90::base_type() 
{}
Polygon90::Polygon90(Polygon90 const& rhs) 
    : Polygon90::shape_base_type(rhs), Polygon90::base_type(rhs) 
{}
Polygon90& Polygon90::operator=(Polygon90 const& rhs)
{
    if (this != &rhs)
    {
        this->base_type::operator=(rhs);
        this->shape_base_type::operator=(rhs);
    }
    return *this;
}

Polygon90::shape_base_type* Polygon90::bloat(coordinate_type width) const 
{
  Polygon90* pgPtr = new Polygon90(*this);
  //The implmentation to bloat Polygon90 needs to be added later 
  maskAssertMsg(width == 0, "bloat polygon90 is not supported yet\n");
  //gtl::bloat(*pgPtr, (unsigned_area_type)width, (unsigned_area_type)width, (unsigned_area_type)width, (unsigned_area_type)width);
  return pgPtr;
}

Polygon90::operator std::string() const 
{
    std::ostringstream oss;
    print(oss);
    return oss.str();
}
Polygon90::~Polygon90() 
{}
void Polygon90::print(std::ostream& os) const
{
    this->shape_base_type::print(os);
    os << "(";
    for (iterator_type it = this->begin(), ite = this->end(); it != ite; ++it)
        os << "(" << (*it).x() << ", " << (*it).y() << ")";
    os << ")";
}
gtl::rectangle_data<Polygon90::coordinate_type> Polygon90::bbox() const 
{
    gtl::rectangle_data<coordinate_type> rect;
    maskAssert(gtl::extents(rect, *this));
    return rect;
}

Path::Path() 
    : Path::shape_base_type(), Path::base_type() 
    , m_width(0)
{}
Path::Path(Path const& rhs) 
    : Path::shape_base_type(rhs), Path::base_type(rhs) 
{
    this->copy(rhs);
}
Path& Path::operator=(Path const& rhs)
{
    if (this != &rhs)
    {
        this->base_type::operator=(rhs);
        this->shape_base_type::operator=(rhs);
        this->copy(rhs);
    }
    return *this;
}
Path::~Path() 
{}
void Path::print(std::ostream& os) const
{
    this->shape_base_type::print(os);
    os << "(";
    for (const_iterator_type it = this->begin(), ite = this->end(); it != ite; ++it)
        os << "(" << it->x() << ", " << it->y() << ")";
    os << ")";
}
gtl::rectangle_data<Path::coordinate_type> Path::bbox() const 
{
    gtl::rectangle_data<coordinate_type> rect; 
    maskAssert(!this->empty());
    for (const_iterator_type it = this->begin(), ite = this->end(); it != ite; ++it)
    {
        if (it == this->begin())
            gtl::set_points(rect, *it, *it);
        else 
            gtl::encompass(rect, *it);
    }
    return rect;
}
void Path::copy(Path const& rhs)
{
    m_width = rhs.m_width;
}

Text::Text() 
    : Text::shape_base_type(), Text::base_type()
{
    m_pos = gtl::construct<point_type>(0, 0); 
    m_width = 5;
}
Text::Text(Text const& rhs) 
    : Text::shape_base_type(rhs), Text::base_type(rhs)
{
    copy(rhs);
}
Text& Text::operator=(Text const& rhs)
{
    if (this != &rhs)
    {
        this->base_type::operator=(rhs);
        this->shape_base_type::operator=(rhs);
        copy(rhs);
    }
    return *this;
}
Text::~Text() 
{}
void Text::print(std::ostream& os) const
{
    this->shape_base_type::print(os);
    os << this->c_str();
}
gtl::rectangle_data<Text::coordinate_type> Text::bbox() const 
{
    // this is just an estimated bounding box 
    // actually it is meaningless to aquire the bounding box a text 
    return gtl::rectangle_data<coordinate_type>(m_pos.x(), m_pos.y(), m_pos.x()+m_width*this->size(), m_pos.y()+m_width*this->size());
}
void Text::copy(Text const& rhs)
{
    m_pos = rhs.m_pos;
    m_width = rhs.m_width;
}

Sref::Sref() 
    : Sref::shape_base_type()
    , m_sname()
    , m_pos(gtl::construct<point_type>(0, 0))
    , m_mag(std::numeric_limits<double>::max())
    , m_angle(std::numeric_limits<double>::max())
{
}
Sref::Sref(Sref const& rhs) 
    : Sref::shape_base_type(rhs)
{
    copy(rhs);
}
Sref& Sref::operator=(Sref const& rhs)
{
    if (this != &rhs)
    {
        this->shape_base_type::operator=(rhs);
        copy(rhs);
    }
    return *this;
}
Sref::~Sref() 
{}
void Sref::print(std::ostream& os) const
{
    this->shape_base_type::print(os);
    os << m_sname << " (" << m_pos.x() << "," << m_pos.y() << ")";
}
gtl::rectangle_data<Sref::coordinate_type> Sref::bbox() const 
{
    // this is just an estimated bounding box 
    // actually it is meaningless to aquire the bounding box a SREF  
    maskAssertMsg(0, "bounding box of SREF is not valid");
    return gtl::rectangle_data<coordinate_type>(m_pos.x(), m_pos.y(), m_pos.x(), m_pos.y());
}
void Sref::copy(Sref const& rhs)
{
    m_sname = rhs.m_sname;
    m_pos = rhs.m_pos;
    m_mag = rhs.m_mag;
    m_angle = rhs.m_angle;
}

Polygon::Polygon() 
    : Polygon::shape_base_type(), Polygon::base_type() 
{}
Polygon::Polygon(Polygon const& rhs) 
    : Polygon::shape_base_type(rhs), Polygon::base_type(rhs) 
{}
Polygon& Polygon::operator=(Polygon const& rhs)
{
    if (this != &rhs)
    {
        this->base_type::operator=(rhs);
        this->shape_base_type::operator=(rhs);
    }
    return *this;
}
Polygon::~Polygon() 
{}
void Polygon::print(std::ostream& os) const
{
    this->shape_base_type::print(os);
    os << "(";
    for (iterator_type it = this->begin(), ite = this->end(); it != ite; ++it)
        os << "(" << (*it).x() << ", " << (*it).y() << ")";
    os << ")";
}
gtl::rectangle_data<Polygon::coordinate_type> Polygon::bbox() const 
{
    gtl::rectangle_data<coordinate_type> rect; 
    maskAssert(gtl::extents(rect, *this));
    return rect;
}

MASKOPT_END_NAMESPACE
