/*************************************************************************
    > File Name: ReadWriteDB.cpp
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Sat 29 Aug 2015 11:45:41 AM CDT
 ************************************************************************/

#include "ReadWriteDB.h"

MASKOPT_BEGIN_NAMESPACE

namespace gtl = boost::polygon;

ReadWriteDB::ReadWriteDB(std::string const& output_gds) 
    : ReadWriteDB::base_type()
    , m_output_gds(output_gds)
{
}
ReadWriteDB::~ReadWriteDB()
{
    check_headers_and_footers_ends();
}

void ReadWriteDB::add_cell(std::string const& str)
{
    m_vCells.push_back(str);
    // need to check cell previous cell ends 
    if (!m_gw.is_cell_end())
        m_gw.end_cell();
}
void ReadWriteDB::add_box(int32_t layer, std::vector<ReadWriteDB::point_type> const& vPoint)
{
    maskAssert(vPoint.size() == 4 || vPoint.size() == 5);
    rectangle_type* shape = create_rectangle(layer, vPoint);

    check_headers_and_footers_begins();
    m_gw.write_rectangle(layer, *shape, gtl::construct<point_type>(0, 0));

    delete shape;
}
void ReadWriteDB::add_boundary(int32_t layer, std::vector<ReadWriteDB::point_type> const& vPoint)
{
    polygon_type* shape = create_polygon(layer, vPoint);

    check_headers_and_footers_begins();
    m_gw.write_polygon(layer, *shape, gtl::construct<point_type>(0, 0));

    delete shape;
}
void ReadWriteDB::add_path(int32_t layer, std::vector<ReadWriteDB::point_type> const& vPoint, ReadWriteDB::coordinate_type width)
{
    maskAssert(vPoint.size() > 1);
    path_type* shape = new path_type;
    shape->assign(vPoint.begin(), vPoint.end());
    shape->width(width);

    check_headers_and_footers_begins();
    m_gw.write_path(layer, *shape, gtl::construct<point_type>(0, 0));

    delete shape;
}
void ReadWriteDB::add_text(int32_t layer, std::string const& str, ReadWriteDB::point_type const& pos, int32_t width)
{
    text_type* shape = new text_type;
    shape->assign(str.begin(), str.end());
    shape->pos(pos);
    shape->width(width);

    check_headers_and_footers_begins();
    m_gw.write_text(layer, *shape, gtl::construct<point_type>(0, 0));

    delete shape;
}
void ReadWriteDB::add_sref(std::string const& str, ReadWriteDB::point_type const& pos, double mag, double angle) 
{
    sref_type* shape = new sref_type;
    shape->sname(str);
    shape->pos(pos);
    shape->mag(mag);
    shape->angle(angle);

    check_headers_and_footers_begins();
    m_gw.write_sref(*shape, gtl::construct<point_type>(0, 0));

    delete shape;
}

void ReadWriteDB::check_headers_and_footers_begins()
{
    if (!m_gw.is_open())
        m_gw.open(m_output_gds);
    if (m_gw.is_lib_end())
        m_gw.begin_lib(m_libname, m_unit);
    if (m_gw.is_cell_end())
        m_gw.begin_cell(m_vCells.back());
}

void ReadWriteDB::check_headers_and_footers_ends()
{
    if (m_gw.is_open())
        m_gw.close();
    if (!m_gw.is_lib_end())
        m_gw.end_lib();
    if (!m_gw.is_cell_end())
        m_gw.end_cell();
}

MASKOPT_END_NAMESPACE
