/*************************************************************************
    > File Name: Cell.h
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Thu 24 Sep 2015 11:09:02 PM CDT
 ************************************************************************/

#ifndef MASKOPT_CELL_H
#define MASKOPT_CELL_H

#include "Shapes.h"

MASKOPT_BEGIN_NAMESPACE

/// cell type 
class Cell 
{
    public:
        typedef coordinate_traits::coordinate_type coordinate_type;
        typedef Shape shape_base_type;

        Cell(std::string const& name = "TOP", uint32_t id = std::numeric_limits<uint32_t>::max());
        /// copy constructor, forbidden
        Cell(Cell const& rhs);
        /// destructor 
        virtual ~Cell();

        uint32_t cell_id() const {return m_cell_id;}
        void cell_id(uint32_t id) {m_cell_id = id;}

        std::string const& name() const {return m_name;}
        void name(std::string const& name) {m_name = name;}

        void add(shape_base_type* shape);
        std::size_t layers() const {return m_mShapes.size();}
        std::map<int32_t, std::vector<shape_base_type*> > const& shapes() const {return m_mShapes;}
        std::map<int32_t, std::vector<shape_base_type*> >& shapes() {return m_mShapes;}
        std::vector<shape_base_type*> const& shapes(int32_t layer) const {return m_mShapes.at(layer);}
        std::vector<shape_base_type*>& shapes(int32_t layer) {return m_mShapes.at(layer);}

        std::size_t num_shapes(int32_t layer) const;
    protected:
        std::string m_name; 
        uint32_t m_cell_id; ///< id of cell 
        std::map<int32_t, std::vector<shape_base_type*> > m_mShapes; ///< all shapes 
};

MASKOPT_END_NAMESPACE

#endif
