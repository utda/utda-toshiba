/*************************************************************************
    > File Name: FlattenLayout.h
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Tue Oct  6 17:06:04 2015
 ************************************************************************/

#ifndef MASKOPT_FLATTENLAYOUT_H
#define MASKOPT_FLATTENLAYOUT_H

#include "ReadStoreDB.h"

MASKOPT_BEGIN_NAMESPACE

class GdsWriter;

struct FlattenLayoutParams
{
    FlattenLayoutParams();

    std::string input_gds; ///< input gdsii file 
    std::string output_gds; ///< output gdsii file 
    bool keep_top_only; ///< only write flattened top cells to output, default is true  
};

class FlattenLayout
{
    public:
        typedef ReadStoreDB::coordinate_type coordinate_type;
        typedef ReadStoreDB::cell_type cell_type;
        typedef ReadStoreDB::shape_base_type shape_base_type;
        typedef ReadStoreDB::point_type point_type;
        typedef ReadStoreDB::rectangle_type rectangle_type;
        typedef ReadStoreDB::polygon_90_type polygon_90_type;
        typedef ReadStoreDB::polygon_type polygon_type;
        typedef ReadStoreDB::text_type text_type;
        typedef ReadStoreDB::sref_type sref_type;

        void operator()(int32_t argc, char** argv); 

    protected: 
        bool read_cmd(int32_t argc, char** argv);
        bool read_layout(std::string const& filename);
        bool write_output(std::string const& filename) const;
        /// recursively write cells 
        /// \param level denotes the position of nested cell in the hierarchy, level of root is 0
        void write_cell(GdsWriter& gw, cell_type const& cell, point_type const& offset, int32_t level) const;

        ReadStoreDB m_db; ///< layout database 
        FlattenLayoutParams m_params; ///< input parameters 
};

MASKOPT_END_NAMESPACE

#endif
