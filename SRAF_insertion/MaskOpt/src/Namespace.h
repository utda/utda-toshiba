/*************************************************************************
    > File Name: Namespace.h
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Fri 19 Jun 2015 04:45:49 PM CDT
 ************************************************************************/

#ifndef MASKOPT_NAMESPACE_H
#define MASKOPT_NAMESPACE_H

#define MASKOPT_NAMESPACE MaskOpt
#define MASKOPT_BEGIN_NAMESPACE namespace MaskOpt {
#define MASKOPT_END_NAMESPACE }

#endif
