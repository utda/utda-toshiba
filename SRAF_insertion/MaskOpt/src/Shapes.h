/*************************************************************************
    > File Name: Shapes.h
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Thu 06 Nov 2014 09:04:57 AM CST
 ************************************************************************/

#ifndef MASKOPT_SHAPES_H
#define MASKOPT_SHAPES_H

#include <iostream>
#include <string>
#include <vector>
#include <boost/version.hpp>

#if (BOOST_VERSION/100)%1000 > 55
// this is to fix the problem in boost 1.57.0 (1.55.0 works fine)
// it reports problem to find abs 
namespace boost { namespace polygon {
	using std::abs;
}} // namespace boost // namespace polygon
#endif

#include <boost/cstdint.hpp>
//#include <boost/polygon/polygon.hpp>
#include <boost/geometry.hpp>
// use adapted boost.polygon in boost.geometry, which is compatible to rtree
#include <boost/geometry/geometries/adapted/boost_polygon.hpp>
#include <boost/geometry/index/rtree.hpp>

#include "Msg.h"

MASKOPT_BEGIN_NAMESPACE

namespace gtl = boost::polygon;
using boost::int32_t;
using boost::int64_t;

using namespace gtl::operators;

struct ShapeTag
{
    enum EnumType {
        UNKNOWN = 0x0, 
        //// following shape type tags ////
        RECTANGLE = 0x1, 
        POLYGON90 = 0x2, 
        POLYGON = 0x4, 
        //// following are GDSII tags ////
        BOX = 0x8, 
        BOUNDARY = 0x10, 
        PATH = 0x20, /// as path and text share the same shape and gdsii tag 
        TEXT = 0x40, 
        SREF = 0x80 /// gdsii with hierarchy uses SREF for cell nesting 
    };
    friend std::ostream& operator<<(std::ostream& os, EnumType const& tag)
    {
        if (tag & RECTANGLE) os << "RECTANGLE";
        else if (tag & POLYGON90) os << "POLYGON90";
        else if (tag & POLYGON) os << "POLYGON";
        else os << "UNKNOWN";

        if (tag & BOX) os << "BOX";
        else if (tag & BOUNDARY) os << "BOUNDARY";
        else if (tag & PATH) os << "PATH";
        else if (tag & TEXT) os << "TEXT";
        else os << "UNKNOWN";

        return os;
    }
};

/// dummy layers for some shapes 
struct DummyLayer
{
    static const int32_t SREF_LAYER;
};

struct coordinate_traits : public gtl::coordinate_traits<int32_t>
{
    typedef gtl::coordinate_traits<int32_t> base_type;
    typedef base_type::coordinate_type coordinate_type;
    typedef base_type::area_type area_type;
    typedef base_type::manhattan_area_type manhattan_area_type;
    typedef base_type::unsigned_area_type unsigned_area_type;
    typedef base_type::coordinate_difference coordinate_difference;
    typedef base_type::coordinate_distance coordinate_distance;
    typedef double grid_coordinate_type;
};

class Shape 
{
    public:
        typedef coordinate_traits::coordinate_type coordinate_type;

		/// default constructor 
		Shape(); 
		/// copy constructor
		Shape(Shape const& rhs); 
		/// assignment 
		Shape& operator=(Shape const& rhs);
		virtual ~Shape(); 

        /// convertion to std::string 
        operator std::string() const; 

#ifdef DEBUG
		int64_t internal_id() {return m_internal_id;}
#endif

		int32_t layer() const {return m_layer;}
		void layer(int32_t l) {m_layer = l;}

		uint32_t pattern_id() const {return m_pattern_id;}
		void pattern_id(uint32_t p) {m_pattern_id = p;}

        int32_t tag() const {return m_tag;}
        void tag(int32_t t) {m_tag = t;}


        virtual Shape* bloat(coordinate_type /*width*/) const {return NULL;};

        virtual void print(std::ostream& os) const; 

		friend std::ostream& operator<<(std::ostream& os, Shape const& rhs) { rhs.print(os); return os; }

        virtual gtl::rectangle_data<coordinate_type> bbox() const = 0;

    private:
		void initialize();
		void copy(Shape const& rhs);
		static int64_t generate_id();

#ifdef DEBUG
		int64_t m_internal_id; ///< internal id 
#endif
	protected:
		int32_t m_layer; ///< input layer
		uint32_t m_pattern_id; ///< index in the pattern array 
        int32_t m_tag; ///< indicate what kind of shapes, use integer because we need and/or operations  
};

class Rectangle : public Shape, public gtl::rectangle_data<coordinate_traits::coordinate_type>
{
	public:
		typedef coordinate_traits::coordinate_type coordinate_type;
		typedef gtl::rectangle_data<coordinate_type> base_type;
        typedef Shape shape_base_type;
		typedef gtl::rectangle_concept geometry_type; // important 
		typedef gtl::point_data<coordinate_type> point_type;
		typedef base_type::interval_type interval_type;

		/// default constructor 
		Rectangle();
		Rectangle(interval_type const& hor, interval_type const& ver);
		Rectangle(coordinate_type xl, coordinate_type yl, coordinate_type xh, coordinate_type yh);
		/// copy constructor
		Rectangle(Rectangle const& rhs);
		/// assignment 
		Rectangle& operator=(Rectangle const& rhs);
		virtual ~Rectangle();

    virtual shape_base_type* bloat(coordinate_type width) const;

        virtual void print(std::ostream& os) const;
        virtual gtl::rectangle_data<coordinate_type> bbox() const;
};

class Polygon90 : public Shape, public gtl::polygon_90_data<coordinate_traits::coordinate_type>
{
	public:
		typedef coordinate_traits::coordinate_type coordinate_type;
    typedef coordinate_traits::unsigned_area_type unsigned_area_type;
		typedef gtl::polygon_90_data<coordinate_type> base_type;
        typedef Shape shape_base_type;
		typedef Rectangle rectangle_type;
		typedef gtl::point_data<coordinate_type> point_type;
		typedef base_type::geometry_type geometry_type;
		typedef base_type::compact_iterator_type compact_iterator_type;
		typedef base_type::iterator_type iterator_type;
		typedef base_type::area_type area_type;

		/// default constructor 
		Polygon90();
		/// copy constructor
		Polygon90(Polygon90 const& rhs);
		/// assignment 
		Polygon90& operator=(Polygon90 const& rhs);
        /// convertion to std::string 
        operator std::string() const;
		virtual ~Polygon90();

    shape_base_type* bloat(coordinate_type width) const;

        virtual void print(std::ostream& os) const;
        virtual gtl::rectangle_data<coordinate_type> bbox() const; 
};

class Path : public Shape, public std::vector<gtl::point_data<coordinate_traits::coordinate_type> >
{
    public:
		typedef coordinate_traits::coordinate_type coordinate_type;
        typedef Shape shape_base_type;
        typedef std::vector<gtl::point_data<coordinate_type> > base_type;
		typedef gtl::point_data<coordinate_type> point_type;
		typedef base_type::iterator iterator_type;
        typedef base_type::const_iterator const_iterator_type;

		/// default constructor 
		Path();
		/// copy constructor
		Path(Path const& rhs);
		/// assignment 
		Path& operator=(Path const& rhs);
		virtual ~Path();

        void width(coordinate_type w) {m_width = w;}
        coordinate_type width() const {return m_width;}

        virtual void print(std::ostream& os) const;
        virtual gtl::rectangle_data<coordinate_type> bbox() const;
    protected:
        void copy(Path const& rhs);
        coordinate_type m_width;
};

class Text : public Shape, public std::string
{
    public:
		typedef coordinate_traits::coordinate_type coordinate_type;
        typedef Shape shape_base_type;
        typedef std::string base_type;
		typedef gtl::point_data<coordinate_type> point_type;
		typedef base_type::iterator iterator_type;

		/// default constructor 
		Text();
		/// copy constructor
		Text(Text const& rhs);
		/// assignment 
		Text& operator=(Text const& rhs);
		virtual ~Text();

        void pos(point_type const& p) {m_pos = p;}
        point_type pos() const {return m_pos;}

        void width(int32_t w) {m_width = w;}
        int32_t width() const {return m_width;}

        virtual void print(std::ostream& os) const;
        virtual gtl::rectangle_data<coordinate_type> bbox() const;
    protected:
        void copy(Text const& rhs);

        /// attributes for text 
        point_type m_pos; ///< position 
        int32_t m_width;
};

class Sref : public Shape 
{
    public:
		typedef coordinate_traits::coordinate_type coordinate_type;
        typedef Shape shape_base_type;
		typedef gtl::point_data<coordinate_type> point_type;

		/// default constructor 
		Sref();
		/// copy constructor
		Sref(Sref const& rhs);
		/// assignment 
		Sref& operator=(Sref const& rhs);
		virtual ~Sref();

        void sname(std::string const& str) {m_sname = str;}
        std::string const& sname() const {return m_sname;}

        void pos(point_type const& p) {m_pos = p;}
        point_type pos() const {return m_pos;}

        void mag(double m) {m_mag = m;}
        double mag() const {return m_mag;}
        bool has_mag() const {return m_mag != std::numeric_limits<double>::max();}

        void angle(double a) {m_angle = a;}
        double angle() const {return m_angle;}
        bool has_angle() const {return m_angle != std::numeric_limits<double>::max();}

        virtual void print(std::ostream& os) const;
        virtual gtl::rectangle_data<coordinate_type> bbox() const;
    protected:
        void copy(Sref const& rhs);

        /// attributes for SREF 
        std::string m_sname; 
        point_type m_pos; 
        double m_mag; ///< how many times larger 
        double m_angle; ///< angle in degree 
};

/// general polygon from Boost.Geometry 
class Polygon : public Shape, public gtl::polygon_data<coordinate_traits::coordinate_type>
{
    public:
		typedef coordinate_traits::coordinate_type coordinate_type;
		typedef gtl::polygon_data<coordinate_type> base_type;
        typedef Shape shape_base_type;
		typedef gtl::point_data<coordinate_type> point_type;
        typedef base_type::iterator_type iterator_type;

		/// default constructor 
		Polygon();
		/// copy constructor
		Polygon(Polygon const& rhs);
		/// assignment 
		Polygon& operator=(Polygon const& rhs);
		virtual ~Polygon();

        virtual void print(std::ostream& os) const;
        virtual gtl::rectangle_data<coordinate_type> bbox() const; 
};

/// check whether a shape forms rectangle 
/// check the bounding box and the area of a polygon
template <typename T>
inline bool is_rectangle(std::vector<gtl::point_data<T> > const& vPoint)
{
    typedef T coordinate_type;
    typedef gtl::point_data<coordinate_type> point_type;
    if (vPoint.size() == 4 || vPoint.size() == 5)
    {
        gtl::rectangle_data<coordinate_type> rect;
        for (typename std::vector<point_type>::const_iterator it = vPoint.begin(), ite = vPoint.end(); it != ite; ++it)
        {
            if (it == vPoint.begin())
                gtl::set_points(rect, *it, *it);
            else 
                gtl::encompass(rect, *it);
        }
        gtl::polygon_data<coordinate_type> polygon;
        polygon.set(vPoint.begin(), vPoint.end());
        typename gtl::coordinate_traits<coordinate_type>::area_type area = gtl::area(polygon);
        // if the bounding box and polygon have the same area 
        if (gtl::area(rect) == area) 
            return true;
    }
    return false;
}

/// check whether a shape forms rectilinear polygon 
/// check the angle of each corner 
template <typename T>
inline bool is_polygon_90(std::vector<gtl::point_data<T> > const& vPoint)
{
    typedef T coordinate_type;
    typedef gtl::point_data<coordinate_type> point_type;

    if (vPoint.size() > 3)
    {
        for (std::size_t i = 0, ie = (vPoint.front() == vPoint.back())? vPoint.size()-1 : vPoint.size(); i != ie; ++i)
        {
            point_type const& p1 = vPoint[i%ie];
            point_type const& p2 = vPoint[(i+1)%ie]; // corner point 
            point_type const& p3 = vPoint[(i+2)%ie];

            if (!((p1.x() == p2.x() && p2.y() == p3.y())
                    || (p1.y() == p2.y() && p2.x() == p3.x()))
                    )
                return false;
        }
        return true;
    }
    return false;
}

MASKOPT_END_NAMESPACE

#include "GeometryApi.h"

#endif 
