/*************************************************************************
    > File Name: GdsiiIO.h
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Thu 06 Nov 2014 08:53:46 AM CST
 ************************************************************************/

#ifndef MASKOPT_GDSIIIO_H
#define MASKOPT_GDSIIIO_H

#include <fstream>
#include <limits>
#include <limbo/parsers/gdsii/stream/GdsReader.h>
#include <limbo/parsers/gdsii/stream/GdsWriter.h>
#include <limbo/programoptions/ProgramOptions.h>

#include "LayoutDB.h"

MASKOPT_BEGIN_NAMESPACE

namespace gtl = boost::polygon;
using boost::int32_t;
using boost::int64_t;
using boost::array;
using gtl::point_concept;
using gtl::rectangle_concept;
using gtl::polygon_90_concept;
using gtl::polygon_90_set_concept;
using gtl::point_data;
using gtl::rectangle_data;
using gtl::polygon_90_data;
using gtl::polygon_90_set_data;

using namespace gtl::operators;

/// read gds file 
struct GdsReader : GdsParser::GdsDataBaseKernel
{
	typedef LayoutDB layoutdb_type;
	typedef layoutdb_type::coordinate_type coordinate_type;
	typedef layoutdb_type::point_type point_type;
    typedef layoutdb_type::shape_base_type shape_base_type;

	//string strname; // TOPCELL name, useful for dump out gds files 
	//double unit;
    int32_t datatype; ///< store datatype here, but not used actually
    int32_t pathtype; ///< store pathtype here, but not used actually
	int32_t layer;
    ShapeTag::EnumType status; 
    std::vector<point_type> vPoint;
    std::string text; // record text string 
    int32_t width; // record width of text 
    std::string sname; ///< record sname of sref 
    double mag; ///< record mag of sref, default is 1 
    double angle; ///< record angle of sref, default is 0
	int64_t file_size; // in bytes 

	layoutdb_type& db;  

	GdsReader(layoutdb_type& _db) : db(_db) {}

	bool operator() (std::string const& filename)  
	{
		// calculate file size 
		std::ifstream in (filename.c_str());
		if (!in.good()) return false;
		std::streampos begin = in.tellg();
		in.seekg(0, std::ios::end);
		std::streampos end = in.tellg();
		file_size = (end-begin);
		in.close();
		// read gds 
		return GdsParser::read(*this, filename);
	}

	/// required callbacks in parser 
	virtual void bit_array_cbk(GdsParser::GdsRecords::EnumType record_type, GdsParser::GdsData::EnumType data_type, std::vector<int> const& vBitArray);
	virtual void integer_2_cbk(GdsParser::GdsRecords::EnumType record_type, GdsParser::GdsData::EnumType data_type, std::vector<int> const& vInteger);
	virtual void integer_4_cbk(GdsParser::GdsRecords::EnumType record_type, GdsParser::GdsData::EnumType data_type, std::vector<int> const& vInteger);
	virtual void real_4_cbk(GdsParser::GdsRecords::EnumType record_type, GdsParser::GdsData::EnumType data_type, std::vector<double> const& vFloat);
	virtual void real_8_cbk(GdsParser::GdsRecords::EnumType record_type, GdsParser::GdsData::EnumType data_type, std::vector<double> const& vFloat);
	virtual void string_cbk(GdsParser::GdsRecords::EnumType record_type, GdsParser::GdsData::EnumType data_type, std::string const& str);
	virtual void begin_end_cbk(GdsParser::GdsRecords::EnumType record_type);

    /// helper functions 
    void integer_cbk(GdsParser::GdsRecords::EnumType record_type, GdsParser::GdsData::EnumType data_type, std::vector<int> const& vData);
    void float_cbk(GdsParser::GdsRecords::EnumType record_type, GdsParser::GdsData::EnumType data_type, std::vector<double> const& vData);
};

/// write gds file 
/// I provide plenty of writing functions here for different usage 
/// some functions are efficient but specific 
/// while some are generic but slower 
/// user can choose different functions for convenience 
class GdsWriter
{
    public:
        typedef LayoutDB layoutdb_type;
        typedef layoutdb_type::coordinate_type coordinate_type;
        typedef layoutdb_type::point_type point_type;
        typedef layoutdb_type::rectangle_type::base_type rectangle_type;
        typedef layoutdb_type::polygon_90_type::base_type polygon_90_type;
        typedef layoutdb_type::polygon_type::base_type polygon_type;
        typedef layoutdb_type::path_type path_type;
        typedef layoutdb_type::text_type text_type;
        typedef layoutdb_type::sref_type sref_type;
        typedef layoutdb_type::shape_base_type shape_base_type;

        GdsWriter();
        ~GdsWriter();

        void open(std::string const& filename);
        void close();
        void begin_lib(std::string const& libname, double unit);
        void end_lib();
        void begin_cell(std::string const& str);
        void end_cell();
        /// write rectangle 
        void write_rectangle(int32_t layer, rectangle_type const& rect, point_type const& offset/* = point_type(0, 0)*/);
        /// write polygon 90 
        void write_polygon(int32_t layer, polygon_90_type const& poly, point_type const& offset/* = point_type(0, 0)*/);
        /// write polygon 
        void write_polygon(int32_t layer, polygon_type const& poly, point_type const& offset/* = point_type(0, 0)*/);
        /// write path 
        void write_path(int32_t layer, path_type const& path, point_type const& offset/* = point_type(0, 0)*/);
        /// write text 
        void write_text(int32_t layer, text_type const& text, point_type const& offset/* = point_type(0, 0)*/);
        /// write sref 
        void write_sref(sref_type const& sref, point_type const& offset/* = point_type(0, 0)*/);
        /// write boundary 
        template <typename PointIterator>
        void write_boundary(int32_t layer, PointIterator first, PointIterator last, point_type const& offset/* = point_type(0, 0)*/);

        bool is_open() const {return m_gw != NULL;}
        bool is_lib_end() const {return m_lib_end;}
        bool is_cell_end() const {return m_cell_end;}

        /// generalized shape writer 
        /// \param valid_tags denotes all valid shapes supported, it will perform logic or operation to check validity 
        void write_shape(shape_base_type* shape, int32_t valid_tags, point_type const& offset/* = point_type(0, 0)*/);
    protected:
        GdsParser::GdsWriter* m_gw;
        unsigned char m_lib_end : 1;
        unsigned char m_cell_end : 1;
};

template <typename PointIterator>
void GdsWriter::write_boundary(int32_t layer, PointIterator first, PointIterator last, GdsWriter::point_type const& offset) 
{
    std::size_t num = std::distance(first, last);
    std::vector<int32_t> vx (num);
    std::vector<int32_t> vy (num);

    uint32_t count = 0;
    for (PointIterator it = first; it != last; ++it, ++count)
    {
        vx[count] = (*it).x() + offset.x();
        vy[count] = (*it).y() + offset.y();
    }

    m_gw->write_boundary(layer, 0, vx, vy, false);
}

MASKOPT_END_NAMESPACE

#endif 
