/*************************************************************************
    > File Name: MaskImage.h
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Fri 25 Sep 2015 11:44:37 AM CDT
 ************************************************************************/

#ifndef MASKOPT_MASKIMAGE_H
#define MASKOPT_MASKIMAGE_H

#include "GdsiiIO.h"
#include "ReadStoreDB.h"
#include "MaskOptParams.h"
#include "MaskFeature.h"

#include <boost/numeric/ublas/vector_sparse.hpp>
#include <boost/numeric/ublas/io.hpp>

MASKOPT_BEGIN_NAMESPACE

//forward declaration
class LearningModel;

namespace bg = boost::geometry;
namespace bgi = bg::index;

class MaskImage
{
    friend class FeatureDB;
    friend class CCAS;
    public:
        typedef ReadStoreDB::coordinate_type coordinate_type;
        typedef ReadStoreDB::cell_type cell_type;
        typedef ReadStoreDB::shape_base_type shape_base_type;
        typedef ReadStoreDB::point_type point_type;
        typedef ReadStoreDB::rectangle_type rectangle_type;
        typedef ReadStoreDB::polygon_90_type polygon_90_type;
        typedef ReadStoreDB::polygon_type polygon_type;
        typedef ReadStoreDB::text_type text_type;
        typedef ReadStoreDB::sref_type sref_type;
        typedef boost::numeric::ublas::compressed_vector<float> label_vec_type;
        //typedef boost::numeric::ublas::compressed_vector<bool> pixel_img_type;
        typedef std::vector<bool> pixel_img_type;
        /// use double for grid_coordinate_type because grid is too small for integer 
        typedef coordinate_traits::grid_coordinate_type grid_coordinate_type;
        typedef bgi::rtree<shape_base_type*, bgi::rstar<16> > rtree_type;

        enum TrainingDataType {
            PIXEL_DATA,
            DISTANCE_DATA
        };

        enum EnergyMetricType {
            ASML_ILS = 0x1, 
            ASML_PVBAND = 0x2, 
            CALIBRE_INTENSITY = 0x4
        };

        enum OutputLayerType {
            REGION_BBOX = 2,  ///< start from 2, leave out 1
            ENERGY_INTENSITY = 3, 
            ENERGY_ILS = 4, 
            ENERGY_PVBAND = 5
        };

        //constructor
        MaskImage();
        //destructor
        ~MaskImage();

        void operator()(int32_t argc, char** argv); 

        static int32_t get_index(grid_coordinate_type x, grid_coordinate_type xl, grid_coordinate_type stepx);
        static grid_coordinate_type get_coordinate(int32_t idx, grid_coordinate_type xl, grid_coordinate_type stepx);

    protected: 
        bool read_cmd(int32_t argc, char** argv);
        bool read_param_json();
        bool read_layout(std::string const& filename, ReadStoreDB& db);
        bool write_layout(std::string const& filename, ReadStoreDB const& db, bool training_phase = true, bool sraf = false) const;
        bool write_training_data(std::string const& filename) const;
        void write_pixel_data_header(std::ofstream& out) const;
        void write_pixel_data(std::ofstream& out, shape_base_type const* shape) const;
        void init_rtrees(ReadStoreDB const& db, std::vector<std::map<int32_t, rtree_type> >& tShapes);
        std::vector<MaskImage::shape_base_type*> const* get_target_shapes(ReadStoreDB const& db, bool training_phase = true) const;
        std::vector<MaskImage::shape_base_type*> const* get_sraf_shapes(ReadStoreDB const& db, bool training_phase = true) const;
        /// \initialize mask optimization region 
        void init_opt_region(ReadStoreDB const& db, std::vector<shape_base_type*>& opc_region, std::vector<shape_base_type*>& sraf_region, bool training_phase = true);
        /// \initialize mask image bounding box
        void init_img_bbox(ReadStoreDB const& db, bool training_phase = true);
        /// \return the xl, xh, yl, yh of the m_img_bbox 
        coordinate_type xl() const {return gtl::xl(m_img_bbox);}
        coordinate_type xh() const {return gtl::xh(m_img_bbox);}
        coordinate_type yl() const {return gtl::yl(m_img_bbox);}
        coordinate_type yh() const {return gtl::yh(m_img_bbox);}
        /// \initialize pixel data: m_pixel_img
        void init_img_pixel(ReadStoreDB const& db, bool training_phase = true);
        /// \initialize sraf region: m_sraf_region, must be called after init_img_pixel(db, training_phase)
        void init_sraf_region(ReadStoreDB const& db, std::vector<shape_base_type*> const& opc_region, std::vector<shape_base_type*> const& sraf_region, bool training_phase = true, bool symmetric_data = true);
        void init_label_data(ReadStoreDB const& db, bool training_phase = true);
        void init_feature_vectors(bool training_phase = true);
        bool get_learning_data(bool training_phase = true);
        bool read_learning_data(bool training_phase = true);
        bool learning_model_construction();
        bool learning_model_prediction(bool training_data = false);
        void prediction_error_analysis(bool training_data = false);

        /// \param ll denotes left lower point of the system 
        bool check_grid_within_shape(int32_t idxx, int32_t idxy, shape_base_type const* shape, 
                point_type const& ll, grid_coordinate_type stepx, grid_coordinate_type stepy) const;
        bool check_point_within_shape(point_type p, shape_base_type const* shape) const;
        /// \return a effective bbox centered by point \param center  
        gtl::rectangle_data<coordinate_type> get_effective_bbox(point_type const& center) const; 
        /// \return image pixel size in corresponding direction
        coordinate_type get_img_pixel_size(int32_t idx) const;
        /// \return SRAF pixel size in corresponding direction
        coordinate_type get_sraf_pixel_size(int32_t idx) const;
        /// \return minimum SRAF size 
        coordinate_type get_sraf_size() const;
        /// \index among image grids 
        uint32_t idxy2index_img(uint32_t gx, uint32_t gy) const {return gy * m_gridx + gx;}
        std::pair<uint32_t, uint32_t> index2idxy_img(uint32_t gxy) const {return std::make_pair(gxy%m_gridx, gxy/m_gridx);}
        /// \index among sraf grids 
        uint32_t idxy2index_sraf(uint32_t sx, uint32_t sy) const {return sy * m_srafx + sx;}
        std::pair<uint32_t, uint32_t> index2idxy_sraf(uint32_t sxy) const {return std::make_pair(sxy%m_srafx, sxy/m_srafx);}
        point_type grid_center_sraf(std::pair<uint32_t, uint32_t> const& pair) const;
        point_type grid_center_sraf(uint32_t sxy) const;
        point_type grid_ll_sraf(std::pair<uint32_t, uint32_t> const& pair) const;
        point_type grid_ll_sraf(uint32_t sxy) const;

        unsigned int sraf_area_threshold() const;
        uint32_t sraf_grid_size() const {return m_sraf_region.size();}
        bool valid_sraf_region(uint32_t idx) const {return m_sraf_region[idx];}

        //generate SRAFs to one layer on gds 
        void sraf_generation(GdsWriter& gw, int layer=25) const;
        //generate the centers of contacts 
        void center_generation(ReadStoreDB const& db, std::string const& cell) const;

        MaskOptParams m_params; ///< input parameters 
        gtl::rectangle_data<coordinate_type> m_img_bbox; //mask image bounding box

        uint32_t m_gridx; ///< number of image grids in x direction within m_img_bbox
        uint32_t m_gridy; ///< number of image grids in y direction within m_img_bbox 
        pixel_img_type m_pixel_img; ///< pixel image in the size of m_gridx * m_gridy

        uint32_t m_srafx; ///< number of sraf grids in x direction within m_img_bbox
        uint32_t m_srafy; ///< number of sraf grids in y direction within m_img_bbox
        pixel_img_type m_sraf_region; ///< SRAF region in the size of m_srafx * m_srafy, data sample is valid when corresponding index stores "true" 
        pixel_img_type m_golden_sraf; ///< SRAFs from model-based approaches

        FeatureDB* m_train_feature; ///< training data feature 
        label_vec_type m_train_label; ///< training data label from model-based approaches 
        label_vec_type m_train_predictions; ///< prediction on training data

        LearningModel* m_model; ///< learning model
        
        FeatureDB* m_test_feature;  ///< testing data feature 
        label_vec_type m_test_label; ///< testing data label from model-based approaches
        label_vec_type m_test_predictions; ///< testing data predicted labels from learning model
        float m_prob_th;
};

struct GridTraversal
{
    int32_t idxx, idxy;
    int32_t idxxl, idxyl, idxxh, idxyh;

    GridTraversal(gtl::rectangle_data<MaskImage::coordinate_type> const& bbox, MaskImage::point_type const& origin, 
            MaskImage::grid_coordinate_type stepx, MaskImage::grid_coordinate_type stepy)
    {
        idxx = idxxl = MaskImage::get_index(gtl::xl(bbox), origin.x(), stepx);
        idxxh = MaskImage::get_index(gtl::xh(bbox), origin.x(), stepx);
        idxy = idxyl = MaskImage::get_index(gtl::yl(bbox), origin.y(), stepy);
        idxyh = MaskImage::get_index(gtl::yh(bbox), origin.y(), stepy);
    }
    bool has_next() const 
    {
        return idxx < idxxh && idxy < idxyh;
    }
    void next()
    {
        idxy += 1;
        if (idxy >= idxyh)
        {
            idxy = idxyl;
            idxx += 1;
        }
    }
    int64_t count() const 
    {
        return (idxx-idxxl)*(idxyh-idxyl) + (idxy-idxyl);
    }
    void print(std::ostream& os) const 
    {
      os << "(" << idxx << ", " << idxy << ") in (" << idxxl << ", " << idxyl << ", " << idxxh << ", " << idxyh << ") ";
    }
    operator std::string() const 
    {
      std::ostringstream oss;
      this->print(oss);
      return oss.str();
    }
};

MASKOPT_END_NAMESPACE

#endif
