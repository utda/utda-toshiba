/**
 * @file   LearningModel.h
 * @author Xiaoqing Xu
 * @date   Feb 2017
 */

#ifndef MASKOPT_LEARNINGMODEL_H
#define MASKOPT_LEARNINGMODEL_H

//liblinear
#include "linear.h"
//libsvm
#include "svm.h"

#include "MaskImage.h"
#include "MaskFeature.h"

MASKOPT_BEGIN_NAMESPACE

class LearningModel
{
  public:
    typedef FeatureDB::feature_vec_type feature_vec_type;
    typedef FeatureDB::flatten_feature_vec_type flatten_feature_vec_type;
    typedef MaskImage::label_vec_type label_vec_type;

    virtual bool model_construction(MaskOptParams const& params, FeatureDB const* train_feature, label_vec_type const& train_label) = 0;
    virtual bool prediction(FeatureDB const* test_feature, label_vec_type& predict_label) = 0;
    virtual void solver_type(int _type) = 0;
    virtual int solver_type() const = 0;

    LearningModel() {};
    virtual ~LearningModel() {};
};

class LGR : public LearningModel
{
  public:
    LGR(int solver_type) : LearningModel(), m_model(NULL), m_solver_type(solver_type) {};

    virtual bool model_construction(MaskOptParams const& params, FeatureDB const* train_feature, label_vec_type const& train_label);
    virtual bool prediction(FeatureDB const* test_feature, label_vec_type& predict_label);
    virtual void solver_type(int _type) {m_solver_type = _type;}
    virtual int solver_type() const {return m_solver_type;}

    virtual ~LGR();

  protected:
    model* m_model;
    int m_solver_type;
};


class SVC : public LearningModel 
{
  public:
    SVC(int solver_type) : LearningModel(), m_model(NULL), m_solver_type(solver_type) {};
    
    virtual bool model_construction(MaskOptParams const& params, FeatureDB const* train_feature, label_vec_type const& train_label);
    virtual bool prediction(FeatureDB const* test_feature, label_vec_type& predict_label);
    virtual void solver_type(int _type) {m_solver_type = _type;}
    virtual int solver_type() const {return m_solver_type;}

    virtual ~SVC();

  protected:
    svm_model* m_model;
    int m_solver_type;
};

MASKOPT_END_NAMESPACE

#endif
