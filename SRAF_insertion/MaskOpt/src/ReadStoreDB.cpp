/*************************************************************************
    > File Name: ReadStoreDB.cpp
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Thu 24 Sep 2015 11:46:48 PM CDT
 ************************************************************************/

#include "ReadStoreDB.h"

MASKOPT_BEGIN_NAMESPACE

ReadStoreDB::~ReadStoreDB()
{
  clear();
}
void ReadStoreDB::clear()
{
    for (std::vector<cell_type*>::const_iterator it = m_vCells.begin(), ite = m_vCells.end(); it != ite; ++it)
        delete *it;
    m_vCells.clear();
    m_mCellName2Idx.clear();
    m_vParents.clear();
}
void ReadStoreDB::begin_lib()
{
}
void ReadStoreDB::end_lib()
{
    init_cell_hierarchy();
}
void ReadStoreDB::add_cell(std::string const& str)
{
    maskAssertMsg(m_mCellName2Idx.insert(std::make_pair(str, m_vCells.size())).second, "duplicate cell name %s", str.c_str());
    m_vCells.push_back(new cell_type(str, m_vCells.size()));
}
void ReadStoreDB::add_box(int32_t layer, std::vector<ReadStoreDB::point_type> const& vPoint)
{
    maskAssert(vPoint.size() == 4 || vPoint.size() == 5);
    shape_base_type* shape = create_rectangle(layer, vPoint);
    shape->tag(ShapeTag::RECTANGLE | ShapeTag::BOX);
    shape->pattern_id(m_vCells.back()->num_shapes(layer));
    m_vCells.back()->add(shape);
    // update layout bbox 
    update_bbox(shape->bbox());
}
void ReadStoreDB::add_boundary(int32_t layer, std::vector<ReadStoreDB::point_type> const& vPoint)
{
    // here we detect whether BOUNDARY is RECTANGLE, POLYGON90, or POLYGON 
    // create suitable object for the shape 
    shape_base_type* shape;
    if (is_rectangle(vPoint))
    {
        shape = create_rectangle(layer, vPoint);
        shape->tag(ShapeTag::RECTANGLE | ShapeTag::BOUNDARY);
    }
    else if (is_polygon_90(vPoint))
    {
        shape = create_polygon_90(layer, vPoint);
        shape->tag(ShapeTag::POLYGON90 | ShapeTag::BOUNDARY);
    }
    else 
    {
        shape = create_polygon(layer, vPoint);
        shape->tag(ShapeTag::POLYGON | ShapeTag::BOUNDARY);
    }
    shape->pattern_id(m_vCells.back()->num_shapes(layer));
    m_vCells.back()->add(shape);
    // update layout bbox 
    update_bbox(shape->bbox());
}
void ReadStoreDB::add_path(int32_t layer, std::vector<ReadStoreDB::point_type> const& vPoint, ReadStoreDB::coordinate_type width)
{
    maskAssert(vPoint.size() > 1);
    path_type* shape = new path_type;
    shape->assign(vPoint.begin(), vPoint.end());
    shape->layer(layer);
    shape->width(width);
    shape->pattern_id(m_vCells.back()->num_shapes(layer));
    shape->tag(ShapeTag::PATH);
    m_vCells.back()->add(shape);
    // update layout bbox 
    update_bbox(shape->bbox());
}
void ReadStoreDB::add_text(int32_t layer, std::string const& str, ReadStoreDB::point_type const& pos, int32_t width)
{
    text_type* shape = new text_type;
    shape->assign(str.begin(), str.end());
    shape->pos(pos);
    shape->width(width);
    shape->layer(layer);
    shape->pattern_id(m_vCells.back()->num_shapes(layer));
    shape->tag(ShapeTag::TEXT);
    m_vCells.back()->add(shape);
}
void ReadStoreDB::add_sref(std::string const& str, ReadStoreDB::point_type const& pos, double mag, double angle) 
{
    if (str.empty())
    {
        maskPrint(kWARN, "empty SNAME for SREF, ignored\n");
        return;
    }
    sref_type* shape = new sref_type;
    shape->sname(str);
    shape->pos(pos);
    shape->mag(mag);
    shape->angle(angle);
    shape->layer(DummyLayer::SREF_LAYER);
    shape->pattern_id(m_vCells.back()->num_shapes(DummyLayer::SREF_LAYER));
    shape->tag(ShapeTag::SREF);
    m_vCells.back()->add(shape);
}

std::size_t ReadStoreDB::cell_index(std::string const& cellName) const 
{
    std::map<std::string, std::size_t>::const_iterator found = m_mCellName2Idx.find(cellName);
    if (found != m_mCellName2Idx.end())
        return found->second;
    else return std::numeric_limits<std::size_t>::max();
}

void ReadStoreDB::init_cell_hierarchy() 
{
    // initialize cell hierarchy 
    m_vParents.assign(m_vCells.size(), std::vector<std::size_t>(0));
    for (std::vector<cell_type*>::const_iterator itc = m_vCells.begin(), itce = m_vCells.end(); itc != itce; ++itc)
    {
        cell_type const& cell = **itc;
        // check every SREF shape in the cell 
        for (std::map<int32_t, std::vector<shape_base_type*> >::const_iterator it1 = cell.shapes().begin(), it1e = cell.shapes().end(); it1 != it1e; ++it1)
        {
            std::vector<shape_base_type*> const& vShapes = it1->second;
            for (std::vector<shape_base_type*>::const_iterator it2 = vShapes.begin(), it2e = vShapes.end(); it2 != it2e; ++it2)
            {
                shape_base_type* shape = *it2;
                if (shape->tag() & ShapeTag::SREF) 
                {
                    sref_type* cshape = static_cast<sref_type*>(shape);
                    // check child cell 
                    std::size_t cell_idx = cell_index(cshape->sname());
                    maskAssertMsg(cell_idx < std::numeric_limits<std::size_t>::max(), "cell name %s not found, cell_index = %lu", cshape->sname().c_str(), cell_idx);
                    cell_type const& sub_cell = *m_vCells[cell_idx];
                    m_vParents[sub_cell.cell_id()].push_back(cell.cell_id());
                }
            }
        }
    }
    // remove duplicates 
    // it is possible that a parent cell uses a child cell for multiple times 
    for (std::vector<std::vector<std::size_t> >::iterator it1 = m_vParents.begin(), it1e = m_vParents.end(); it1 != it1e; ++it1)
    {
        std::set<std::size_t> sParents (it1->begin(), it1->end());
        it1->assign(sParents.begin(), sParents.end());
    }
}

MASKOPT_END_NAMESPACE
