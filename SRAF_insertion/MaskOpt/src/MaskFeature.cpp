/**
 * @file   MaskFeature.cpp
 * @author Xiaoqing Xu
 * @date   Feb 2017
 */

#include "GdsiiIO.h"
#include "MaskFeature.h"
#include "MaskImage.h"

#include <omp.h>

MASKOPT_BEGIN_NAMESPACE

CCAS::CCAS(int mr, int rs, int rn, int cs) 
  : FeatureDB(), 
    minRadius(mr), 
    ringSpace(rs), 
    ringNum(rn), 
    circleSR(cs)
{
  //initialize x/y displacement LUT for CCAS
  xdisp_lut = std::vector<int>(ringNum*circleSR, 0);
  ydisp_lut = std::vector<int>(ringNum*circleSR, 0);
  //maskPrint(kINFO, "xstep:");
  for(int ii = 0; ii != ringNum; ++ii)
  {
    //maskPrint(kNONE, " %d", minRadius+(ii+1)*ringSpace);
    for(int jj = 0; jj != circleSR; ++jj)
    {
      xdisp_lut.at(idxy2index(ii, jj)) = std::floor( (minRadius+(ii+1)*ringSpace) * std::cos(M_PI*(1-2.0*jj/circleSR)) );
      ydisp_lut.at(idxy2index(ii, jj)) = std::floor( (minRadius+(ii+1)*ringSpace) * std::sin(M_PI*(1-2.0*jj/circleSR)) );
    }
  }
  //maskPrint(kNONE, "\n");
  /*
#ifdef DEBUG
  maskPrint(kINFO, "\t x_displacement: \n");
  for(int ii = 0; ii != ringNum; ++ii)
  {
    maskPrint(kNONE, "\t\t(");
    for(int jj = 0; jj != circleSR; ++jj)
    {
      maskPrint(kNONE, " %d", xdisp_lut.at(idxy2index(ii, jj)));
    }
    maskPrint(kNONE, " )\n");
  }
  maskPrint(kINFO, "\t y_displacement: \n");
  for(int ii = 0; ii != ringNum; ++ii)
  {
    maskPrint(kNONE, "\t\t(");
    for(int jj = 0; jj != circleSR; ++jj)
    {
      maskPrint(kNONE, " %d", ydisp_lut.at(idxy2index(ii, jj)));
    }
    maskPrint(kNONE, " )\n");
  }
#endif 
  */
}

unsigned int CCAS::feature_vec_non_zeros(unsigned int idx) const 
{
  feature_vec_type const& fVec = get_feature_vec(idx);
  unsigned int nonZeroCnt = 0;
  for(feature_vec_type::const_iterator it = fVec.begin(), ite = fVec.end(); it != ite; ++it)
  {
    if(0 != *it)
    {
      nonZeroCnt += 1;
    }
  }
  return nonZeroCnt;
}

bool CCAS::feature_extraction(MaskImage const& image)
{
  typedef boost::numeric::ublas::compressed_vector<unsigned int> idx_vec_type;
  maskPrint(kINFO, "\tfeature extaction in mode CCAS: minRadius %d, ringSpace: %d, ringNum %d, circleSR: %d\n", minRadius, ringSpace, ringNum, circleSR);
  unsigned int offset = featureVecs.size();
  unsigned int fSize = 0;
  //determine the index map from SRAF index to feature vector index
  idx_vec_type srafIdx2fvecIdx = idx_vec_type(image.sraf_grid_size(), 0);
  for(unsigned int k = 0, ke = image.sraf_grid_size(); k != ke; ++k)
  {
    if(image.valid_sraf_region(k))
    {
      srafIdx2fvecIdx(k) = fSize+offset;
      fSize += 1;
    }
  }
  
  //move feature vecs to a resized temporary vector
  std::vector<feature_vec_type> tFeatureVecs = std::vector<feature_vec_type>(fSize+offset, feature_vec_type());
  for(unsigned int k = 0; k != offset; ++k)
  {
    tFeatureVecs.at(k).swap(featureVecs.at(k));
  }

  unsigned int gSize = image.sraf_grid_size();
  //thread-safe implementation of feature extraction
#ifdef _OPENMP
#pragma omp parallel for num_threads(8)
#endif
  for(unsigned int k = 0; k < gSize; ++k)
  {
    //ignore invalid regions
    if(false == image.valid_sraf_region(k))
    {
      continue;
    }

    //collect sampled feature vector 
    feature_vec_type kVec; 
    std::vector<float> fVec; // temporary storage of feature vector, no need to be sparse for efficiency  
    feature_sampling(image, k, fVec);

    //feature compaction
    feature_compaction(fVec, kVec);
    //kVec.swap(fVec);
    tFeatureVecs.at(srafIdx2fvecIdx(k)).swap(kVec);
/*
#ifdef DEBUG
    if(!empty_vec(fVec))
    {
      MaskImage::point_type gCenter = image.grid_center_sraf(k);
      maskPrint(kINFO, "\t sampling feature vec at (%d, %d)\n", gtl::x(gCenter), gtl::y(gCenter));
      print(fVec);
      //maskPrint(kINFO, "\t compaction results\n");
      //print(kVec);
    }
#endif
*/
  }

  //swap back to member vector 
  featureVecs.swap(tFeatureVecs);
  return true;
}

void CCAS::feature_sampling(MaskImage const& image, unsigned int sGridIdx, std::vector<float>& fVec)
{
  typedef MaskImage::coordinate_type coordinate_type;
  typedef MaskImage::point_type point_type;

  fVec.resize(ringNum*circleSR);
  //constant parameters for sampling
  int gr = 3; //(lambda/2NA(1+sigma))^2
  bool mask[7][7] = {{0, 0, 0, 1, 0, 0, 0},
                     {0, 1, 1, 1, 1, 1, 0},
                     {0, 1, 1, 1, 1, 1, 0},
                     {1, 1, 1, 1, 1, 1, 1},
                     {0, 1, 1, 1, 1, 1, 0},
                     {0, 1, 1, 1, 1, 1, 0},
                     {0, 0, 0, 1, 0, 0, 0}};

  //the center of sampling SRAF grid
  point_type gCenter = image.grid_center_sraf(sGridIdx);
  //start sampling
  for (int ii = 0; ii != ringNum; ++ii)
  {
    for (int jj = 0; jj != circleSR; ++jj)
    {
      int idxy = idxy2index(ii, jj);
      //sampling window
      coordinate_type xc = gtl::x(gCenter) + xdisp_lut.at(idxy);
      coordinate_type yc = gtl::y(gCenter) + ydisp_lut.at(idxy);
      gtl::rectangle_data<coordinate_type> sRect = gtl::rectangle_data<coordinate_type>(xc-gr, yc-gr, xc+gr+1, yc+gr+1);

      //ignore sampling window out of image box 
      if(!gtl::contains(image.m_img_bbox, sRect))
      {
        continue;
      }

      //m_img_bbox grid traversal 
      int idxxl = MaskImage::get_index(gtl::xl(sRect), gtl::xl(image.m_img_bbox), image.get_img_pixel_size(0));
      int idxyl = MaskImage::get_index(gtl::yl(sRect), gtl::yl(image.m_img_bbox), image.get_img_pixel_size(1));
      float value = 0; 
      for (int j = 0; j < 7; ++j)
      {
          for (int i = 0; i < 7; ++i)
          {
              if( mask[j][i] // assume mask is symmetric
                      //mask[i][j] 
                      && image.m_pixel_img[image.idxy2index_img(idxxl+i, idxyl+j)] )
              {
                  value += 1; 
              }
          }
      }

      fVec[idxy] = value; 
    }//end for jj
  }//end for ii
  
}

void CCAS::feature_compaction(std::vector<float>& fVec, feature_vec_type& kVec)
{
  kVec.clear();
  kVec.resize(ringNum*circleSR);

  int rotation_lut[4][32] = {{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31},
                             {16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17},
                             {16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
                             {0, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1}};
  
  ;

  int divideCnt = 4; ///number of quadrant
  int divideSize = circleSR / divideCnt; ///< size of each quadrant
  float max_sum = 0.0; ///< the bit sum for main quadrant
  int flip_index = 0; ///< the symmetry index
  //detect main quadrant
  for (int mm = 0; mm != 4; ++mm)
  {
    int sIdx = mm*divideSize;
    float sum = 0.0;
    for (int ii = 0; ii != ringNum; ++ii)
    {
      for (int jj = 0; jj != divideSize; ++jj)
      {
        sum += fVec[idxy2index(ii, jj+sIdx)];
      }
    }
    //handle boundary condition
    if(sIdx + divideSize == circleSR)
    {
      for (int ii = 0; ii != ringNum; ++ii)
      {
        sum += fVec[idxy2index(ii, 0)];
      }
    }
    //check larger quadrant
    if(sum > max_sum)
    {
      max_sum = sum;
      flip_index = mm;
    }
  }//end for mm
  
  //rearrange the feature vector 
  for (int ii = 0; ii != ringNum; ++ii)
  {
    for (int jj = 0; jj != circleSR; ++jj)
    {
      float value = fVec[idxy2index(ii, rotation_lut[flip_index][jj])]; 
      if(value != 0.0)
      {
        kVec(idxy2index(ii, jj)) = value;
      }
    }
  }
}

void CCAS::print(feature_vec_type const& vec)
{
  for(int ii = 0; ii != ringNum; ++ii)
  {
    maskPrint(kNONE, "\t\t[");
    for(int jj = 0; jj != circleSR; ++jj)
    {
      maskPrint(kNONE, " %.0f", vec(idxy2index(ii, jj)));
    }
    maskPrint(kNONE, " ]\n");
  }
}

bool CCAS::empty_vec(feature_vec_type const& vec) const
{
  for(unsigned int k = 0, ke = vec.size(); k != ke; ++k)
  {
    if(vec(k) != 0)
    {
      return false;
    }
  }
  return true;
}

void CCAS::write_feature_shapes(GdsWriter& gw, MaskImage const& image) const 
{
  typedef MaskImage::coordinate_type coordinate_type;
  typedef MaskImage::point_type point_type;
  typedef MaskImage::rectangle_type rectangle_type;
  typedef MaskImage::shape_base_type shape_base_type;
  //write non-empty feature vector 
  uint32_t fVecIdx = 0;
  uint32_t shapeLayer = 150;
  for(uint32_t k = 0, ke = image.sraf_grid_size(); k != ke; ++k)
  {
    if(shapeLayer > 300)
    {
      break;
    }
    //ignore non-SRAF grids 
    if(false == image.valid_sraf_region(k))
    {
      continue;
    }
    
    if(empty_vec(featureVecs.at(fVecIdx)))
    {
      fVecIdx += 1;
      continue;
    }
    else
    {
      //the center of sampling SRAF grid
      point_type gCenter = image.grid_center_sraf(k);
      //write SRAF grid 
      rectangle_type rect(gtl::x(gCenter)-5, gtl::y(gCenter)-5, gtl::x(gCenter)+5, gtl::y(gCenter)+5);
      rect.layer(shapeLayer);
      rect.tag(ShapeTag::RECTANGLE);
      shape_base_type* shape = &rect;
      gw.write_shape(shape, ShapeTag::RECTANGLE | ShapeTag::POLYGON90 | ShapeTag::POLYGON, gtl::construct<point_type>(0, 0));
      
      feature_vec_type const& fVec = featureVecs.at(fVecIdx);
      //index feature vector
      for (int ii = 0; ii != ringNum; ++ii)
      {
        for (int jj = 0; jj != circleSR; ++jj)
        {
          int idxy = idxy2index(ii, jj);
          coordinate_type sx = gtl::x(gCenter) + xdisp_lut.at(idxy);
          coordinate_type sy = gtl::y(gCenter) + ydisp_lut.at(idxy);
          if(fVec(idxy) == 0.0)
          {
            rectangle_type rect(sx-1, sy-1, sx+1, sy+1);
            rect.layer(shapeLayer);
            rect.tag(ShapeTag::RECTANGLE);
            shape_base_type* shape = &rect;
            gw.write_shape(shape, ShapeTag::RECTANGLE | ShapeTag::POLYGON90 | ShapeTag::POLYGON, gtl::construct<point_type>(0, 0));
          }
          else
          {
            rectangle_type rect(sx-5, sy-5, sx+5, sy+5);
            rect.layer(shapeLayer);
            rect.tag(ShapeTag::RECTANGLE);
            shape_base_type* shape = &rect;
            gw.write_shape(shape, ShapeTag::RECTANGLE | ShapeTag::POLYGON90 | ShapeTag::POLYGON, gtl::construct<point_type>(0, 0));
          }
        }//end for jj
      }//end for ii
  
      fVecIdx += 1;
      shapeLayer += 1;
    }//end if 
  }//end for k
}

MASKOPT_END_NAMESPACE
