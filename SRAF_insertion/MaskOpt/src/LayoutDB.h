/*************************************************************************
    > File Name: LayoutDB.h
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Sat 29 Aug 2015 11:44:46 AM CDT
 ************************************************************************/

#ifndef MASKOPT_LAYOUTDB_H
#define MASKOPT_LAYOUTDB_H

#include <sstream>
#include "Cell.h"

MASKOPT_BEGIN_NAMESPACE

/// base layout database class 
class LayoutDB : public Rectangle
{
    public:
        typedef Rectangle base_type;
        typedef Shape shape_base_type;
        typedef shape_base_type::coordinate_type coordinate_type;
        typedef gtl::point_data<coordinate_type> point_type;
        typedef Rectangle rectangle_type;
        /// rectilinear polygon type 
        typedef Polygon90 polygon_90_type;
        /// general polygon type 
        typedef Polygon polygon_type;
        typedef Path path_type;
        typedef Text text_type;
        typedef Sref sref_type;
        typedef base_type::geometry_type geometry_type;

        LayoutDB();
        LayoutDB(LayoutDB const& rhs);
        virtual ~LayoutDB();
        LayoutDB& operator=(LayoutDB const& rhs);

        void initialize();
        void copy(LayoutDB const& rhs);
        /// begin lib 
        virtual void begin_lib() {};
        /// end lib 
        virtual void end_lib() {};
        /// set libname 
        virtual void set_libname(std::string const& str) {m_libname = str;}
        /// set units 
        virtual void set_unit(double u) {m_unit = u;}
        /// set strname 
        virtual void add_cell(std::string const& str) = 0;
        /// add a box 
        virtual void add_box(int32_t layer, std::vector<point_type> const& vPoint) = 0;
        /// add a boundary 
        virtual void add_boundary(int32_t layer, std::vector<point_type> const& vPoint) = 0;
        /// add paths 
        virtual void add_path(int32_t layer, std::vector<point_type> const& vPoint, coordinate_type width) = 0;
        /// add text 
        virtual void add_text(int32_t layer, std::string const& str, point_type const& pos, int32_t width) = 0;
        /// add sref 
        virtual void add_sref(std::string const& str, point_type const& pos, double mag, double angle) = 0;
        /// helper functions 
        /// update bounding box of layout 
        virtual void update_bbox(base_type::base_type const& box);

        /// accessors 
        double unit() const {return m_unit;}
        std::string const& libname() const {return m_libname;}

    protected:
        rectangle_type* create_rectangle(int32_t layer, std::vector<point_type> const& vPoint) const;
        polygon_90_type* create_polygon_90(int32_t layer, std::vector<point_type> const& vPoint) const;
        polygon_type* create_polygon(int32_t layer, std::vector<point_type> const& vPoint) const;

        std::string m_libname;                       ///< lib name, useful for dump out gds files 
        double m_unit;                               ///< keep output gdsii file has the same unit as input gdsii file 
};

MASKOPT_END_NAMESPACE

#endif
