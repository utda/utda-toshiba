/*************************************************************************
    > File Name: GdsiiIO.cpp
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Wed 26 Aug 2015 10:59:58 AM CDT
 ************************************************************************/

#include "GdsiiIO.h"

MASKOPT_BEGIN_NAMESPACE

namespace gtl = boost::polygon;

/// required callbacks in parser 
void GdsReader::bit_array_cbk(GdsParser::GdsRecords::EnumType record_type, GdsParser::GdsData::EnumType data_type, std::vector<int> const& vBitArray)
{
    this->integer_cbk(record_type, data_type, vBitArray);
}
void GdsReader::integer_2_cbk(GdsParser::GdsRecords::EnumType record_type, GdsParser::GdsData::EnumType data_type, std::vector<int> const& vInteger)
{
    this->integer_cbk(record_type, data_type, vInteger);
}
void GdsReader::integer_4_cbk(GdsParser::GdsRecords::EnumType record_type, GdsParser::GdsData::EnumType data_type, std::vector<int> const& vInteger)
{
    this->integer_cbk(record_type, data_type, vInteger);
}
void GdsReader::real_4_cbk(GdsParser::GdsRecords::EnumType record_type, GdsParser::GdsData::EnumType data_type, std::vector<double> const& vFloat) 
{
    this->float_cbk(record_type, data_type, vFloat);
}
void GdsReader::real_8_cbk(GdsParser::GdsRecords::EnumType record_type, GdsParser::GdsData::EnumType data_type, std::vector<double> const& vFloat) 
{
    this->float_cbk(record_type, data_type, vFloat);
}
void GdsReader::string_cbk(GdsParser::GdsRecords::EnumType record_type, GdsParser::GdsData::EnumType data_type, std::string const& str) 
{
    maskAssert(data_type == GdsParser::GdsData::STRING);
    switch (record_type)
    {
        case GdsParser::GdsRecords::STRNAME:
            db.add_cell(str);
            break;
        case GdsParser::GdsRecords::LIBNAME:
            db.set_libname(str);
            break;
        case GdsParser::GdsRecords::STRING:
            text.assign(str);
            break;
        case GdsParser::GdsRecords::SNAME:
            sname.assign(str);
            break;
        default: break;
    }
}
void GdsReader::begin_end_cbk(GdsParser::GdsRecords::EnumType record_type)
{
    switch (record_type)
    {
        case GdsParser::GdsRecords::BOX:
            vPoint.clear();
            layer = 0;
            status = ShapeTag::BOX;
            break;
        case GdsParser::GdsRecords::BOUNDARY:
            vPoint.clear();
            layer = 0;
            status = ShapeTag::BOUNDARY;
            break;
        case GdsParser::GdsRecords::PATH:
            vPoint.clear();
            layer = 0;
            width = datatype = pathtype = 0;
            status = ShapeTag::PATH;
            break;
        case GdsParser::GdsRecords::TEXT:
            vPoint.clear();
            layer = width = 0;
            status = ShapeTag::TEXT;
            break;
        case GdsParser::GdsRecords::SREF:
            // reset to default values 
            vPoint.clear();
            sname = "";
            mag = std::numeric_limits<double>::max(); 
            angle = std::numeric_limits<double>::max();
            status = ShapeTag::SREF;
            break;
        case GdsParser::GdsRecords::ENDEL:
            {
                switch (status)
                {
                    case ShapeTag::BOX: db.add_box(layer, vPoint); break;
                    case ShapeTag::BOUNDARY: db.add_boundary(layer, vPoint); break;
                    case ShapeTag::PATH: db.add_path(layer, vPoint, width); break;
                    case ShapeTag::TEXT: db.add_text(layer, text, vPoint.front(), width); break;
                    case ShapeTag::SREF: db.add_sref(sname, ((vPoint.empty())? gtl::construct<point_type>(0, 0) : vPoint.front()), mag, angle); break;
                    default: maskAssertMsg(0, "invalid status");
                }
                status = ShapeTag::UNKNOWN;
            }
            break;
        case GdsParser::GdsRecords::ENDLIB:
            // notify database on the end of lib 
            db.end_lib();
            break;
        case GdsParser::GdsRecords::ENDSTR:
            // currently not interested, add stuff here if needed 
            break;
        default:
            maskPrint(kERROR, "%s() invalid record_type = %s", __func__, GdsParser::gds_record_ascii(record_type));
            break;
    }
}
/// helper functions 
void GdsReader::integer_cbk(GdsParser::GdsRecords::EnumType record_type, GdsParser::GdsData::EnumType data_type, std::vector<int> const& vData)
{
    switch (record_type)
    {
        case GdsParser::GdsRecords::DATATYPE:
            datatype = vData[0];
            break;
        case GdsParser::GdsRecords::PATHTYPE:
            pathtype = vData[0];
            break;
        case GdsParser::GdsRecords::LAYER:
            layer = vData[0];
            break;
        case GdsParser::GdsRecords::WIDTH:
            width = vData[0];
            break;
        case GdsParser::GdsRecords::PRESENTATION: // these are text attributes, but not interested 
        case GdsParser::GdsRecords::STRANS:
            break;
        case GdsParser::GdsRecords::XY:
            {
                vPoint.clear();
                uint32_t end = vData.size();
                for (uint32_t i = 0; i < end; i += 2)
                    vPoint.push_back(gtl::construct<point_type>(vData[i], vData[i+1]));
                // skip last point for BOX and BOUNDARY
                if ((status == ShapeTag::BOX || status == ShapeTag::BOUNDARY) && vPoint.front() == vPoint.back()) vPoint.pop_back();
            }
            break;
        case GdsParser::GdsRecords::BGNLIB:
            // notify database on the begin of lib 
            db.begin_lib();
            break;
        case GdsParser::GdsRecords::HEADER:
        case GdsParser::GdsRecords::BGNSTR: 
            // just date of creation, not interesting
            break;
        default:
            maskPrint(kERROR, "%s() invalid record_type = %s, data_type = %s", __func__, GdsParser::gds_record_ascii(record_type), GdsParser::gds_data_ascii(data_type));
            break;
    }
}
void GdsReader::float_cbk(GdsParser::GdsRecords::EnumType record_type, GdsParser::GdsData::EnumType data_type, std::vector<double> const& vData)
{
    switch (record_type)
    {
        case GdsParser::GdsRecords::UNITS:
            db.set_unit(vData[1]);
            break;
        case GdsParser::GdsRecords::MAG:
            mag = vData[0];
            break;
        case GdsParser::GdsRecords::ANGLE:
            angle = vData[0];
            break;
        default:
            maskPrint(kERROR, "%s() invalid record_type = %s, data_type = %s", __func__, GdsParser::gds_record_ascii(record_type), GdsParser::gds_data_ascii(data_type));
            break;
    }
}

GdsWriter::GdsWriter()
    : m_gw(NULL)
    , m_lib_end(true)
    , m_cell_end(true)
{
}
GdsWriter::~GdsWriter()
{
    close();
}

void GdsWriter::open(std::string const& filename)
{
    m_gw = new GdsParser::GdsWriter(filename.c_str());
}

void GdsWriter::close()
{
    if (m_gw)
        delete m_gw;
    m_gw = NULL;
}

void GdsWriter::begin_lib(std::string const& libname, double unit)
{
    maskAssert(is_lib_end());
    m_gw->gds_create_lib(libname.c_str(), unit /* um per bit */ );
    m_lib_end = false; // begin lib 
}
void GdsWriter::end_lib()
{
    maskAssert(!is_lib_end());
    m_gw->gds_write_endlib(); 
    m_lib_end = true; // end lib 
}
void GdsWriter::begin_cell(std::string const& str)
{
    maskAssert(is_cell_end());
    m_gw->gds_write_bgnstr();
    m_gw->gds_write_strname(str.c_str());
    m_cell_end = false; // begin cell 
}
void GdsWriter::end_cell()
{
    maskAssert(!is_cell_end());
    m_gw->gds_write_endstr();
    m_cell_end = true; // end cell 
}

void GdsWriter::write_rectangle(int32_t layer, GdsWriter::rectangle_type const& rect, GdsWriter::point_type const& offset) 
{
    m_gw->write_box(layer, 0, 
            gtl::xl(rect)+offset.x(), gtl::yl(rect)+offset.y(), 
            gtl::xh(rect)+offset.x(), gtl::yh(rect)+offset.y());
}

void GdsWriter::write_polygon(int32_t layer, GdsWriter::polygon_90_type const& poly, GdsWriter::point_type const& offset)
{
    write_boundary(layer, poly.begin(), poly.end(), offset);
}

void GdsWriter::write_polygon(int32_t layer, GdsWriter::polygon_type const& poly, GdsWriter::point_type const& offset)
{
    write_boundary(layer, poly.begin(), poly.end(), offset);
}

void GdsWriter::write_path(int32_t layer, GdsWriter::path_type const& path, GdsWriter::point_type const& offset)
{
    // create a path
    m_gw->gds_write_path();
    m_gw->gds_write_layer(layer);
    m_gw->gds_write_datatype(0);
    m_gw->gds_write_pathtype(2); // extended square ends
    m_gw->gds_write_width(5); // 5 nm wide

    int32_t* vx = new int32_t [path.size()];
    int32_t* vy = new int32_t [path.size()];
    uint32_t count = 0;
    for (path_type::const_iterator_type it = path.begin(), ite = path.end(); it != ite; ++it, ++count)
    {
        vx[count] = it->x()+offset.x();
        vy[count] = it->y()+offset.y();
    }
    if (path.width() != 0)
        m_gw->gds_write_width(path.width());
    m_gw->gds_write_xy(vx, vy, count);
    m_gw->gds_write_endel();

    delete [] vx;
    delete [] vy;
}

void GdsWriter::write_text(int32_t layer, GdsWriter::text_type const& text, GdsWriter::point_type const& offset)
{
    m_gw->gds_create_text(text.c_str(), text.pos().x()+offset.x(), text.pos().y()+offset.y(), layer, text.width());
}

void GdsWriter::write_sref(GdsWriter::sref_type const& sref, GdsWriter::point_type const& offset)
{
    m_gw->gds_write_sref();                    // contains an instance of...
    m_gw->gds_write_sname(sref.sname().c_str());
    if (sref.has_mag())
        m_gw->gds_write_mag(sref.mag()); 
    if (sref.has_angle())
        m_gw->gds_write_angle(sref.angle()); 

    int32_t vx[1] = {sref.pos().x()+offset.x()};
    int32_t vy[1] = {sref.pos().y()+offset.y()};

    m_gw->gds_write_xy(vx, vy, 1); 
    m_gw->gds_write_endel();
}

void GdsWriter::write_shape(GdsWriter::shape_base_type* shape, int32_t valid_tags, GdsWriter::point_type const& offset) 
{
    if (shape->tag() & ShapeTag::RECTANGLE)
    {
        maskAssertMsg(valid_tags & ShapeTag::RECTANGLE, "ShapeTag::RECTANGLE is set to invalid shape");
        layoutdb_type::rectangle_type* cshape = static_cast<layoutdb_type::rectangle_type*>(shape);
        write_rectangle(cshape->layer(), *cshape, offset);
    }
    else if (shape->tag() & ShapeTag::POLYGON90)
    {
        maskAssertMsg(valid_tags & ShapeTag::POLYGON90, "ShapeTag::POLYGON90 is set to invalid shape");
        layoutdb_type::polygon_90_type* cshape = static_cast<layoutdb_type::polygon_90_type*>(shape);
        write_polygon(cshape->layer(), *cshape, offset);
    }
    else if (shape->tag() & ShapeTag::POLYGON)
    {
        maskAssertMsg(valid_tags & ShapeTag::POLYGON, "ShapeTag::POLYGON is set to invalid shape");
        layoutdb_type::polygon_type* cshape = static_cast<layoutdb_type::polygon_type*>(shape);
        write_polygon(cshape->layer(), *cshape, offset);
    }
    else if (shape->tag() & ShapeTag::PATH)
    {
        maskAssertMsg(valid_tags & ShapeTag::PATH, "ShapeTag::PATH is set to invalid shape");
        path_type* cshape = static_cast<path_type*>(shape);
        write_path(cshape->layer(), *cshape, offset);
    }
    else if (shape->tag() & ShapeTag::TEXT)
    {
        maskAssertMsg(valid_tags & ShapeTag::TEXT, "ShapeTag::TEXT is set to invalid shape");
        text_type* cshape = static_cast<text_type*>(shape);
        write_text(cshape->layer(), *cshape, offset);
    }
    else if (shape->tag() & ShapeTag::SREF)
    {
        maskAssertMsg(valid_tags & ShapeTag::SREF, "ShapeTag::SREF is set to invalid shape");
        sref_type* cshape = static_cast<sref_type*>(shape);
        write_sref(*cshape, offset);
    }
    else 
    {
        maskPrint(kERROR, "unknown tag %d\n", shape->tag());
        exit(-1);
    }
}

MASKOPT_END_NAMESPACE
