/*************************************************************************
    > File Name: ReadWriteDB.h
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Sat 29 Aug 2015 11:44:46 AM CDT
 ************************************************************************/

#ifndef MASKOPT_READWRITEDB_H
#define MASKOPT_READWRITEDB_H

#include "LayoutDB.h"
#include "GdsiiIO.h"

MASKOPT_BEGIN_NAMESPACE

/// read/write layout database class 
/// this is designed for very simple manipulation to each shape 
/// so there is no need to store all the shapes 
/// simply read and then write with new shapes 
class ReadWriteDB : public LayoutDB
{
    public:
        typedef LayoutDB base_type;
        typedef gtl::coordinate_traits<coordinate_type>::manhattan_area_type area_type;
        typedef gtl::coordinate_traits<coordinate_type>::coordinate_difference coordinate_difference;
        typedef Rectangle rectangle_type;
        /// rectilinear polygon type 
        typedef base_type::polygon_90_type polygon_90_type;
        /// general polygon type 
        typedef base_type::polygon_type polygon_type;
        typedef base_type::path_type path_type;
        typedef base_type::text_type text_type;

        /// constructor 
        /// assume ControlParameter has already been initialized 
        ReadWriteDB(std::string const& output_gds);
        /// forbidden 
        ReadWriteDB(ReadWriteDB const& rhs);
        virtual ~ReadWriteDB();
        /// forbidden 
        ReadWriteDB& operator=(ReadWriteDB const& rhs);

        /// set strname 
        virtual void add_cell(std::string const& str);
        /// add a box 
        virtual void add_box(int32_t layer, std::vector<point_type> const& vPoint);
        /// add a boundary 
        virtual void add_boundary(int32_t layer, std::vector<point_type> const& vPoint);
        /// add paths 
        virtual void add_path(int32_t layer, std::vector<point_type> const& vPoint, coordinate_type width);
        /// add text 
        virtual void add_text(int32_t layer, std::string const& str, point_type const& pos, int32_t width);
        /// add sref 
        virtual void add_sref(std::string const& str, point_type const& pos, double mag, double angle);

    protected:
        void check_headers_and_footers_begins(); 
        void check_headers_and_footers_ends();

        GdsWriter m_gw; ///< a gdsii writer object 
        std::string m_output_gds; ///< output gds file 
        std::vector<std::string> m_vCells; ///< record cell names 
};

MASKOPT_END_NAMESPACE

#endif
