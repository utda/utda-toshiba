/**
 * @file   LearningModel.cpp
 * @author Xiaoqing Xu
 * @date   Feb 2017
 */

#include "LearningModel.h"

#include <omp.h>

#define Malloc(type,n) (type *)malloc((n)*sizeof(type))

MASKOPT_BEGIN_NAMESPACE
//destroy problem instance from liblinear
void destroy_prob(problem* prob)
{
  for(int k = 0; k != prob->l; ++k)
  {
    free(prob->x[k]);
  }
  free(prob->x);
  free(prob->y);
}

LGR::~LGR()
{
  if(m_model)
  {
    free_model_content(m_model);
    m_model = NULL;
  }
}

bool LGR::model_construction(MaskOptParams const& params, FeatureDB const* train_feature, label_vec_type const& train_label)
{
  maskPrint(kINFO, "Learning model with logistic regression\n");
  maskAssertMsg(train_feature->size()==train_label.size(), "\t the featureVecs size should be the same as the label size\n");

  maskPrint(kNONE, "\t model construction");
  if(params.model_path != "")
  {
    maskPrint(kNONE, " from model file\n");
    std::string model_file = params.model_path+"/LGR.model";
    m_model = load_model(model_file.c_str());
    return true;
  }
  maskPrint(kNONE, " from data training\n");
  //construct liblinear problem
  problem* prob = new problem();
  prob->l = train_feature->size()+1;
  prob->n = train_feature->featureDim();
  prob->y = Malloc(double, prob->l+1);
  unsigned int lSize = train_label.size();
  unsigned int s0 = 0;
  unsigned int s1 = 0;

#ifdef _OPENMP
#pragma omp parallel for reduction(+:s1,s0)
#endif
  for(unsigned int k = 0; k <lSize; ++k)
  {
    if(0.0 != train_label(k))
    {
      prob->y[k] = 1;
      s1 += 1;
    }
    else
    {
      prob->y[k] = 0;
      s0 += 1;
    }
  }
  //add {feature, label} data {0s, 0}
  prob->y[lSize] = 0;
  lSize += 1, s0 += 1;

  //init feature vectors 
  prob->x = Malloc(feature_node*, prob->l+1);
  unsigned int fSize = train_feature->size();

#ifdef _OPENMP
#pragma omp parallel for
#endif
  for(unsigned int k = 0; k < fSize; ++k)
  {
    feature_vec_type const& fVec = train_feature->get_feature_vec(k);
    unsigned int nonZeroCnt = train_feature->feature_vec_non_zeros(k);
    prob->x[k] = Malloc(feature_node, nonZeroCnt+1);
    unsigned int nonZeroIdx = 0;
    for(unsigned int idx = 0, idxe = fVec.size(); idx != idxe; ++idx)
    {
      //ingore zeros
      if(0 != fVec(idx))
      {
        //feature index for liblinear starts with 1 not 0
        prob->x[k][nonZeroIdx].index = idx+1;
        prob->x[k][nonZeroIdx].value = fVec(idx);
        nonZeroIdx += 1;
      }
    }
    maskAssertMsg(nonZeroIdx == nonZeroCnt, "\t failed to traverse feature vec\t");
    //denote the end of feature vec 
    prob->x[k][nonZeroIdx].index = -1;
    prob->x[k][nonZeroIdx].value = -1;
  }
  
  //add {feature, label} data {0s, 0}
  prob->x[fSize] = Malloc(feature_node, 1);
  prob->x[fSize][0].index = -1;
  prob->x[fSize][0].value = -1;

  //no bias in our model 
  prob->bias = 1.0;

  //construct liblinear parameter 
  parameter* param = new parameter();
  param->solver_type = m_solver_type;
  param->eps = 1.0 / float(params.lgr_eps);
  param->C = float(params.lgr_c)/float(params.model_sf);
  //param->nr_weight = 0;
  param->nr_weight = 2;
  param->weight_label = Malloc(int, 2);
  param->weight_label[0] = 0;
  param->weight_label[1] = 1;
  param->weight = Malloc(double, 2);
  param->weight[0] = float(lSize) / 2.0 / s0 * (float(params.lgr_w0)/float(params.model_sf));
  param->weight[1] = float(lSize) / 2.0 / s1 * (float(params.lgr_w1)/float(params.model_sf));

  //check parameter 
  maskAssert(NULL == check_parameter(prob, param));

  //model calibration
  maskPrint(kNONE, "\t model calibration\n");
  m_model = train(prob, param);

  //cross validation
  maskPrint(kNONE, "\t cross validation\n");
  double* target = Malloc(double, prob->l+1);
  //cross_validation(prob, param, 4, target);

  maskPrint(kINFO, "Logistic regression model, bias: %.4f, nr_feature: %d, nr_class %d ( ", m_model->bias, m_model->nr_feature, m_model->nr_class);
  for(int k = 0; k != m_model->nr_class; ++k)
  {
    maskPrint(kNONE, "%d ", m_model->label[k]);
  }
  maskPrint(kNONE, ")\n");

  //test at x=0s
  feature_node* x0 = Malloc(feature_node, 1);
  double* pEstimates = Malloc(double, m_model->nr_class);
  x0[0].index = -1;
  x0[0].value = -1;
  predict_probability(m_model, x0, pEstimates);
  maskPrint(kINFO, "prediction at 0s with label: %.1f, with probability %.5f(%d), %.5f(%d)\n", predict(m_model, x0), pEstimates[0], m_model->label[0], pEstimates[1], m_model->label[1]);
  free(x0);
  free(pEstimates);

  if(0 == params.search)
  {
    //save model 
    std::string model_file = "./result/" + params.learning_model + ".model";
    save_model(model_file.c_str(), m_model);
  }

  //memory control
  destroy_param(param);
  destroy_prob(prob);
  free(target);
  return (m_model != NULL);
}

bool LGR::prediction(FeatureDB const* test_feature, label_vec_type& predict_label)
{
  if(NULL == test_feature)
  {
    return false;
  }
  unsigned int fSize = test_feature->size(); 
  //thread-safe temporary vector
  std::vector<float> predictions(fSize, 0.0);

#ifdef _OPENMP
#pragma omp parallel for num_threads(8)
#endif
  for(unsigned int k = 0; k < fSize; ++k)
  {
    feature_vec_type const& fVec = test_feature->get_feature_vec(k);
    unsigned int nonZeroCnt = test_feature->feature_vec_non_zeros(k);
    //ignore zeros feature vector
    if(0 == nonZeroCnt)
    {
      predictions.at(k) = 0;
      continue;
    }
    feature_node* x = Malloc(feature_node, nonZeroCnt+1);
    unsigned int nonZeroIdx = 0;
    for(unsigned int idx = 0, idxe = fVec.size(); idx != idxe; ++idx)
    {
      //ingore zeros
      if(0 != fVec(idx))
      {
        //feature index for liblinear starts with 1 not 0
        x[nonZeroIdx].index = idx+1;
        x[nonZeroIdx].value = fVec(idx);
        nonZeroIdx += 1;
      }
    }
    maskAssertMsg(nonZeroIdx == nonZeroCnt, "\t failed to traverse feature vec\t");
    //denote the end of feature vec 
    x[nonZeroIdx].index = -1;
    x[nonZeroIdx].value = -1;

    //get probability/label
    double* pEstimates = Malloc(double, m_model->nr_class);
    predict_probability(m_model, x, pEstimates);
    predictions.at(k) = pEstimates[1];
    //predict_label(k) = predict(m_model, x);
    //memory control
    free(x);
    free(pEstimates);
  }

  //assign back  
  predict_label.clear();
  predict_label.resize(fSize);
  for(unsigned int k = 0; k != fSize; ++k)
  {
    predict_label(k) = predictions.at(k);
  }
  
  return true;
}

SVC::~SVC()
{
  if(m_model)
  {
    svm_free_model_content(m_model);
    m_model = NULL;
  }
}

bool SVC::model_construction(MaskOptParams const& params, FeatureDB const* train_feature, label_vec_type const& train_label)
{

  maskPrint(kINFO, "Learning model with support vector machine\n");
  maskAssertMsg(train_feature->size()==train_label.size(), "\t the featureVecs size should be the same as the label size\n");

  maskPrint(kNONE, "\t model construction");
  if(params.model_path != "")
  {
    maskPrint(kNONE, " from model file\n");
    std::string model_file = params.model_path+"/SVC.model";
    m_model = svm_load_model(model_file.c_str());
    return true;
  }
  maskPrint(kNONE, " from data training\n");
  //construct liblinear problem
  svm_problem* prob = new svm_problem();
  prob->l = train_feature->size()+1;
  prob->y = Malloc(double, prob->l+1);
  unsigned int lSize = train_label.size();
  unsigned int s1 = 0;
  unsigned int s0 = 0;

#ifdef _OPENMP
#pragma omp parallel for reduction(+:s1,s0)
#endif
  for(unsigned int k = 0; k < lSize; ++k)
  {
    if(0.0 != train_label(k))
    {
      prob->y[k] = 1;
      s1 += 1;
    }
    else
    {
      prob->y[k] = 0;
      s0 += 1;
    }
  }
  //add {feature, label} data {0s, 0}
  prob->y[lSize] = 0;

  lSize += 1, s0 += 1;

  //init feature vectors 
  prob->x = Malloc(svm_node*, prob->l+1);
  unsigned int fSize = train_feature->size();

#ifdef _OPENMP
#pragma omp parallel for
#endif
  for(unsigned int k = 0; k < fSize; ++k)
  {
    feature_vec_type const& fVec = train_feature->get_feature_vec(k);
    unsigned int nonZeroCnt = train_feature->feature_vec_non_zeros(k);
    prob->x[k] = Malloc(svm_node, nonZeroCnt+1);
    unsigned int nonZeroIdx = 0;
    for(unsigned int idx = 0, idxe = fVec.size(); idx != idxe; ++idx)
    {
      //ingore zeros
      if(0 != fVec(idx))
      {
        //feature index for liblinear starts with 1 not 0
        prob->x[k][nonZeroIdx].index = idx+1;
        prob->x[k][nonZeroIdx].value = fVec(idx);
        nonZeroIdx += 1;
      }
    }
    maskAssertMsg(nonZeroIdx == nonZeroCnt, "\t failed to traverse feature vec\t");
    //denote the end of feature vec 
    prob->x[k][nonZeroIdx].index = -1;
    prob->x[k][nonZeroIdx].value = -1;
  }

  //add {feature, label} data {0s, 0}
  prob->x[fSize] = Malloc(svm_node, 1);
  prob->x[fSize][0].index = -1;
  prob->x[fSize][0].value = -1;

  //construct liblinear parameter 
  svm_parameter* param = new svm_parameter();
  param->svm_type = C_SVC;
  param->kernel_type = m_solver_type;
  param->degree = params.svc_degree;
  param->gamma = float(params.svc_gamma)/float(params.model_sf);
  param->coef0 = float(params.svc_coef0)/float(params.model_sf);
  param->cache_size = 16;
  param->eps = 1.0 / float(params.svc_eps);
  param->C = float(params.svc_c)/float(params.model_sf);
  param->nr_weight = 2;
  param->weight_label = Malloc(int, 2);
  param->weight_label[0] = 0;
  param->weight_label[1] = 1;
  param->weight = Malloc(double, 2);
  param->weight[0] = float(lSize) / 2.0 / s0 * (float(params.svc_w0)/float(params.model_sf));
  param->weight[1] = float(lSize) / 2.0 / s1 * (float(params.svc_w1)/float(params.model_sf));
  param->nu = 0.0;
  param->p = 0.0;
  param->shrinking = 1;
  param->probability = 1;

  //check parameter 
  svm_check_parameter(prob, param);

  //model calibration
  maskPrint(kNONE, "\t model calibration\n");
  m_model = svm_train(prob, param);

  //cross validation
  maskPrint(kNONE, "\t cross validation\n");
  double* target = Malloc(double, prob->l+1);
  //svm_cross_validation(prob, param, 4, target);

  maskAssertMsg(m_model->nr_class == 2, "the number of classes is %d\n", m_model->nr_class);

  //test at x=0s
  svm_node* x0 = Malloc(svm_node, 1);
  double* pEstimates = Malloc(double, m_model->nr_class);
  x0[0].index = -1;
  x0[0].value = -1;
  svm_predict_probability(m_model, x0, pEstimates);
  maskPrint(kINFO, "prediction at 0s with label: %.1f, with probability %.5f(%d), %.5f(%d)\n", svm_predict(m_model, x0), pEstimates[0], m_model->label[0], pEstimates[1], m_model->label[1]);
  free(x0);
  free(pEstimates);

  if(0 == params.search)
  {
    //save model 
    std::string model_file = "./result/" + params.learning_model + ".model";
    svm_save_model(model_file.c_str(), m_model);
  }

  //memory control
  svm_destroy_param(param);
  //destroy_prob(prob);
  free(target);
  return (m_model != NULL);
}

bool SVC::prediction(FeatureDB const* test_feature, label_vec_type& predict_label)
{
  if(NULL == test_feature)
  {
    return false;
  }
  unsigned int fSize = test_feature->size(); 
  //thread-safe temporary vector
  std::vector<float> predictions(fSize, 0.0);

#ifdef _OPENMP
#pragma omp parallel for num_threads(8)
#endif
  for(unsigned int k = 0; k < fSize; ++k)
  {
    feature_vec_type const& fVec = test_feature->get_feature_vec(k);
    unsigned int nonZeroCnt = test_feature->feature_vec_non_zeros(k);
    //ignore zeros feature vector
    if(0 == nonZeroCnt)
    {
      predictions.at(k) = 0;
      continue;
    }
    svm_node* x = Malloc(svm_node, nonZeroCnt+1);
    unsigned int nonZeroIdx = 0;
    for(feature_vec_type::const_iterator it = fVec.begin(), ite = fVec.end(); it != ite; ++it, ++nonZeroIdx)
    {
      maskAssert(*it != 0);
      //feature index for liblinear starts with 1 not 0
      x[nonZeroIdx].index = it.index()+1;
      x[nonZeroIdx].value = *it;
    }
    maskAssertMsg(nonZeroIdx == nonZeroCnt, "\t failed to traverse feature vec\t");
    //denote the end of feature vec 
    x[nonZeroIdx].index = -1;
    x[nonZeroIdx].value = -1;

    //get probability/label
    double* pEstimates = Malloc(double, m_model->nr_class);
    svm_predict_probability(m_model, x, pEstimates);
    predictions.at(k) = pEstimates[1];
    //memory control
    free(x);
    free(pEstimates);
  }

  //assign back  
  predict_label.clear();
  predict_label.resize(fSize);
  for(unsigned int k = 0; k != fSize; ++k)
  {
    predict_label(k) = predictions.at(k);
  }
  
  return true;
}

MASKOPT_END_NAMESPACE
