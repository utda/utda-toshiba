/*************************************************************************
    > File Name: Cell.cpp
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Thu 24 Sep 2015 11:32:26 PM CDT
 ************************************************************************/

#include "Cell.h"

MASKOPT_BEGIN_NAMESPACE

Cell::Cell(std::string const& name, uint32_t id) 
    : m_name(name)
    , m_cell_id(id)
{}

Cell::~Cell()
{
    for (std::map<int32_t, std::vector<shape_base_type*> >::const_iterator it1 = m_mShapes.begin(), it1e = m_mShapes.end(); it1 != it1e; ++it1)
        for (std::vector<shape_base_type*>::const_iterator it2 = it1->second.begin(), it2e = it1->second.end(); it2 != it2e; ++it2)
            delete *it2;
}

void Cell::add(shape_base_type* shape)
{
    m_mShapes[shape->layer()].push_back(shape);
}

std::size_t Cell::num_shapes(int32_t layer) const 
{
    std::map<int32_t, std::vector<shape_base_type*> >::const_iterator found = m_mShapes.find(layer);
    if (found != m_mShapes.end())
        return found->second.size();
    else return 0;
}

MASKOPT_END_NAMESPACE

