/*************************************************************************
    > File Name: MaskImage.cpp
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Fri 25 Sep 2015 11:50:59 AM CDT
 ************************************************************************/

#include "MaskImage.h"
#include "LearningModel.h"
#include <fstream>
#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <limbo/programoptions/ProgramOptions.h>
#include <limbo/string/String.h>
#include <limbo/math/Math.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/timer/timer.hpp>

//liblinear
#include "linear.h"
//libsvm
#include "svm.h"

MASKOPT_BEGIN_NAMESPACE

MaskImage::MaskImage()
{
  m_train_feature = NULL;
  m_test_feature = NULL;
  m_img_bbox = gtl::rectangle_data<coordinate_type>(std::numeric_limits<coordinate_type>::max(), std::numeric_limits<coordinate_type>::max(), std::numeric_limits<coordinate_type>::min(), std::numeric_limits<coordinate_type>::min());
  m_gridx = 0;
  m_gridy = 0;
  m_pixel_img.clear();
  m_srafx = 0;
  m_srafy = 0;
  m_sraf_region.clear();
  m_golden_sraf.clear();
  m_train_label.clear();
  m_model = NULL;
  m_test_label.clear();
  m_test_predictions.clear();
  m_prob_th = 0.5;
}

MaskImage::~MaskImage()
{
  if(m_train_feature)
  {
    delete m_train_feature;
    m_train_feature = NULL;
  }

  if(m_test_feature)
  {
    delete m_test_feature;
    m_test_feature = NULL;
  }
}

void MaskImage::operator()(int32_t argc, char** argv) 
{
    maskAssert(read_cmd(argc, argv));
    //Get training data
    if (!m_params.output_training_data.empty())
    {
      //read from m_params.output_training_data
      //maskAssert(read_learning_data(true));
    }
    else
    {
      //grid-based sampling to collect training data
      maskAssert(get_learning_data(true));
    }

    //model construction
    learning_model_construction();

    //model prediction 
    learning_model_prediction(true);

    //prediction error analysis
    prediction_error_analysis(true);

    //Get testing data 
    if (!m_params.output_testing_data.empty())
    {
      //read from m_params.output_testing_data
      //maskAssert(read_learning_data(false));
    }
    else
    {
      //grid-based sampling to collect testing data 
      char buf[256];
      maskSPrint(kINFO, buf, "testing SRAF generation takes %%t seconds CPU, %%w seconds real\n");
      boost::timer::auto_cpu_timer timer (buf);

      maskAssert(get_learning_data(false));
     
      if(0 == m_params.output_gds)
      {
        //model prediction for testing data 
        learning_model_prediction(false);
        
        //prediction error analysis
        prediction_error_analysis(false);
      }
    }
}

bool MaskImage::read_cmd(int32_t argc, char** argv)
{
    maskPrint(kINFO, "commands =");
    for (int i = 0; i < argc; ++i)
        maskPrint(kNONE, " %s", argv[i]);
    maskPrint(kNONE, "\n");

    MaskOptParams default_params; // get default value from default constructor 
    bool help = false;
    // append options here 
    typedef limbo::programoptions::ProgramOptions po_type;
    using limbo::programoptions::Value;
    po_type desc (std::string("Aerial Image Energy Calculator Usage"));
    desc.add_option(Value<bool>("-help", &help, "print help message").toggle(true).default_value(false).toggle_value(true).help(true))
        .add_option(Value<std::string>("-param_json", &m_params.param_json, "input parameter file name").required(true));

    try
    {
        desc.parse(argc, argv);

        // print help message 
        if (help)
        {
            std::cout << desc << "\n";
            exit(1);
        }

        // read json parameter files
        read_param_json();

        // post check conditional options 
        if (!m_params.output_training_data.empty())
        {
            maskAssertMsg(m_params.effective_distance > 0 && m_params.effective_distance < std::numeric_limits<double>::max(), 
                    "invalid effective distance = %g", m_params.effective_distance);
            maskAssertMsg(m_params.pixel_size[0] > 0, "invalid image pixel size = %g", m_params.pixel_size);
            maskAssertMsg(m_params.sraf_grid[0] > 0, "invalid sraf pixel size = %g", m_params.sraf_grid);
        }
    }
    catch (std::exception& e)
    {
        // print help message and error message 
        std::cout << desc << "\n";
        maskPrint(kERROR, "%s\n", e.what());
        return false;
    }
    return true;
}

bool MaskImage::read_param_json()
{
    maskAssertMsg(m_params.param_json != "", "input json parameter file is empty\n");
    maskPrint(kINFO, "reading input parameter json %s\n", m_params.param_json.c_str());

    namespace pt = boost::property_tree;
    
    pt::ptree root;
    pt::read_json(m_params.param_json, root);
    //pt::write_json("test.json", root);

    //parse data 
    m_params.search             = root.get<int>("SEARCH");
    m_params.Lambda             = root.get<double>("PARAM_DIFFR.Lambda", 0.0);
    m_params.NA                 = root.get<double>("PARAM_DIFFR.NA", 0.0);
    m_params.Lx                 = root.get<double>("PARAM_DIFFR.Lx", 0.0);
    m_params.opc_region         = root.get<int>("OPC_REGION", 0);
    m_params.sraf_region        = root.get<int>("SRAF_REGION", 0);
    m_params.effective_distance = m_params.sraf_region;
    m_params.sraf_size          = root.get<int>("SRAFSIZE", 0);
    m_params.pixel_size[0]      = root.get<int>("IMGGRID", 0);
    m_params.pixel_size[1]      = m_params.pixel_size[0];
    m_params.sraf_grid[0]       = root.get<int>("GRIDSIZE", 0);
    m_params.sraf_grid[1]       = m_params.sraf_grid[0];
    m_params.training_gds_dir   = root.get<std::string>("LAYOUT_LEARN.MSGDSDIR");
    for(pt::ptree::const_iterator it = root.get_child("LAYOUT_LEARN.GDS").begin(), ite = root.get_child("LAYOUT_LEARN.GDS").end();
        it != ite; ++it)
    {
      // it->first contain the string ""
      m_params.training_gds.insert(it->second.data());
    }
    m_params.training_gds_cell = root.get<std::string>("LAYOUT_LEARN.TOPCELL");
    m_params.sCellNames.insert(m_params.training_gds_cell);
    m_params.training_layers[0]   = root.get<int>("LAYOUT_LEARN.TargetLayer");
    m_params.training_layers[1]   = root.get<int>("LAYOUT_LEARN.SRAFLayer");
    m_params.output_training_data = root.get<std::string>("LAYOUT_LEARN.OUTPUT_TRAINING_DATA");

    m_params.testing_gds_dir = root.get<std::string>("LAYOUT_TEST.MSGDSDIR");
    for(pt::ptree::const_iterator it = root.get_child("LAYOUT_TEST.GDS").begin(), ite = root.get_child("LAYOUT_TEST.GDS").end();
        it != ite; ++it)
    {
      // it->first contain the string ""
      m_params.testing_gds.insert(it->second.data());
    }
    m_params.testing_gds_cell = root.get<std::string>("LAYOUT_TEST.TOPCELL");
    m_params.sCellNames.insert(m_params.testing_gds_cell);
    m_params.testing_layers[0]   = root.get<int>("LAYOUT_TEST.TargetLayer");
    m_params.testing_layers[1]   = root.get<int>("LAYOUT_TEST.SRAFLayer");
    m_params.output_testing_data = root.get<std::string>("LAYOUT_TEST.OUTPUT_TESTING_DATA");
    m_params.output_gds          = root.get<int>("LAYOUT_TEST.OUTPUT_GDS");

    m_params.effective_distance  = root.get<double>("FEATURE.Wx", 0.0);
    m_params.feature_mode        = root.get<std::string>("FEATURE.Mode");
    m_params.rinSpace            = root.get<int>("FEATURE.CCAS.Rin");
    m_params.circleSR            = root.get<int>("FEATURE.CCAS.CircleSampleRate");
    m_params.dim_reduction       = root.get<std::string>("FEATURE.DIM_REDUCTION.Mode");
    m_params.learning_model      = root.get<std::string>("MODEL.Type");
    m_params.model_path          = root.get<std::string>("MODEL.PATH");
    m_params.prob_threshold      = root.get<int>("MODEL.ProbabilityThreshold");
    m_params.model_sf            = root.get<int>("MODEL.ParamScale");
    m_params.lgr_eps             = root.get<int>("MODEL.LGR.EPS");
    m_params.lgr_c               = root.get<int>("MODEL.LGR.C");
    m_params.lgr_w0              = root.get<int>("MODEL.LGR.W0");
    m_params.lgr_w1              = root.get<int>("MODEL.LGR.W1");
    m_params.svc_degree          = root.get<int>("MODEL.SVC.DEGREE");
    m_params.svc_gamma           = root.get<int>("MODEL.SVC.GAMMA");
    m_params.svc_coef0           = root.get<int>("MODEL.SVC.COEF0");
    m_params.svc_eps             = root.get<int>("MODEL.SVC.EPS");
    m_params.svc_c               = root.get<int>("MODEL.SVC.C");
    m_params.svc_w0              = root.get<int>("MODEL.SVC.W0");
    m_params.svc_w1              = root.get<int>("MODEL.SVC.W1");
    m_params.simulation_dir      = root.get<std::string>("LAYOUT_TEST.SIMDIR");
    m_params.result_dir          = root.get<std::string>("RESULT_DIR");
    m_params.debug_dir           = root.get<std::string>("DEBUG_DIR");

    m_prob_th                    = float(m_params.prob_threshold) / float(m_params.model_sf);
#ifdef DEBUG
    //check reading-in correctness
    m_params.print();
#endif
    return true;
}

bool MaskImage::read_layout(std::string const& filename, ReadStoreDB& db)
{
    maskPrint(kINFO, "reading input gdsii %s\n", filename.c_str());
    GdsReader gdsReader (db);
    return gdsReader(filename);
}

bool MaskImage::write_layout(std::string const& filename, ReadStoreDB const& db, bool training_phase, bool sraf) const 
{
    maskPrint(kINFO, "write output gdsii %s\n", filename.c_str());
    GdsWriter gw;
    gw.open(filename);
    gw.begin_lib(db.libname(), db.unit()*1.0e+6);

    // write cells 
    for (std::vector<cell_type*>::const_iterator it1 = db.cells().begin(), it1e = db.cells().end(); it1 != it1e; ++it1)
    {
        cell_type const& cell = **it1;
        if (!m_params.sCellNames.empty() && !m_params.sCellNames.count(cell.name()))
            continue;
        gw.begin_cell(cell.name());
        for (std::map<int32_t, std::vector<shape_base_type*> >::const_iterator it2 = cell.shapes().begin(), it2e = cell.shapes().end(); it2 != it2e; ++it2)
        {
            std::vector<shape_base_type*> const& vShapes = it2->second;
            for (std::vector<shape_base_type*>::const_iterator it3 = vShapes.begin(), it3e = vShapes.end(); it3 != it3e; ++it3)
            {
                shape_base_type* shape = *it3;
                gw.write_shape(shape, ShapeTag::RECTANGLE | ShapeTag::POLYGON90 | ShapeTag::POLYGON, gtl::construct<point_type>(0, 0));
            }
        }
        if(false == training_phase && true == sraf && m_test_predictions.size() == m_test_label.size())
        {
          sraf_generation(gw, 0);
        }//end if 
        maskPrint(kINFO, "write image bounding box\n");
        rectangle_type rect(xl(), yl(), xh(), yh());
        rect.layer(100);
        rect.tag(ShapeTag::RECTANGLE);
        shape_base_type* shape = &rect;
        gw.write_shape(shape, ShapeTag::RECTANGLE | ShapeTag::POLYGON90 | ShapeTag::POLYGON, gtl::construct<point_type>(0, 0));
#ifdef DEBUG
        maskPrint(kINFO, "m_pixel_img size: %d\n", m_pixel_img.size());
        for (unsigned int k = 0, ke = m_pixel_img.size(); k != ke; ++k)
        {
          if (m_pixel_img[k])
          {
            std::pair<uint32_t, uint32_t> pair = index2idxy_img(k);
            rectangle_type rect(pair.first*get_img_pixel_size(0), pair.second*get_img_pixel_size(1), (pair.first+1)*get_img_pixel_size(0), (pair.second+1)*get_img_pixel_size(1));
            rect.layer(101);
            rect.tag(ShapeTag::RECTANGLE);
            shape_base_type* shape = &rect;
            gw.write_shape(shape, ShapeTag::RECTANGLE | ShapeTag::POLYGON90 | ShapeTag::POLYGON, gtl::ll(m_img_bbox));
          }
        }
        maskPrint(kINFO, "m_sraf_region size: %d\n", m_sraf_region.size());
        for (unsigned int k = 0, ke = m_sraf_region.size(); k != ke; ++k)
        {
          if (valid_sraf_region(k))
          {
            std::pair<uint32_t, uint32_t> pair = index2idxy_sraf(k);
            rectangle_type rect(pair.first*get_sraf_pixel_size(0), pair.second*get_sraf_pixel_size(1), (pair.first+1)*get_sraf_pixel_size(0), (pair.second+1)*get_sraf_pixel_size(1));
            rect.layer(102);
            rect.tag(ShapeTag::RECTANGLE);
            shape_base_type* shape = &rect;
            gw.write_shape(shape, ShapeTag::RECTANGLE | ShapeTag::POLYGON90 | ShapeTag::POLYGON, gtl::ll(m_img_bbox));
          }
        }
        maskPrint(kINFO, "write golden SRAFs\n");
        pixel_img_type const* srafVecPtr = &(m_golden_sraf);
        for (unsigned int k = 0, ke = srafVecPtr->size(); k != ke; ++k)
        {
          if((*srafVecPtr)[k])
          {
            point_type gCenter = grid_center_sraf(k);
            rectangle_type rect(gtl::x(gCenter)-get_sraf_size()/2, gtl::y(gCenter)-get_sraf_size()/2, gtl::x(gCenter)+get_sraf_size()/2, gtl::y(gCenter)+get_sraf_size()/2);
            rect.layer(103);
            rect.tag(ShapeTag::RECTANGLE);
            shape_base_type* shape = &rect;
            gw.write_shape(shape, ShapeTag::RECTANGLE | ShapeTag::POLYGON90 | ShapeTag::POLYGON, gtl::construct<point_type>(0, 0));
          }
        }
        maskPrint(kNONE, "\n");
        m_train_feature->write_feature_shapes(gw, *this);
#endif
        gw.end_cell();
    }

    gw.end_lib();
    gw.close();
    return true;
}

bool MaskImage::write_training_data(std::string const& filename) const 
{
    maskPrint(kINFO, "writing training data to %s\n", filename.c_str());

    // assume rtree is ready 
    std::ofstream out (filename.c_str());
    if (!out.good())
        return false;

    write_pixel_data_header(out);
    
    //write feature vectors 
    //write traing label

    out.close();
    return true;
}

void MaskImage::write_pixel_data_header(std::ofstream& out) const 
{
    // any point is ok 
    gtl::rectangle_data<coordinate_type> eff_bbox = get_effective_bbox(point_type(0, 0));
    out << "energy";
    for (GridTraversal gt (eff_bbox, gtl::ll(eff_bbox), get_img_pixel_size(0), get_img_pixel_size(1)); 
            gt.has_next(); gt.next())
        out << ",g_" << gt.idxx << "_" << gt.idxy;
    out << std::endl;
}

void MaskImage::write_pixel_data(std::ofstream& out, MaskImage::shape_base_type const* shape) const
{
    std::set<std::pair<int32_t, int32_t> > sShapePixel; 
    gtl::rectangle_data<coordinate_type> bbox = shape->bbox();
    point_type center;
    gtl::center(center, bbox);
    gtl::rectangle_data<coordinate_type> eff_bbox = get_effective_bbox(center);

    // if effective bbox is not totally within the layout, skip this shape 
    // this is for shapes near the boundary of the layout 
    //if (!gtl::contains((LayoutDB::base_type)m_db, eff_bbox))
    //    return;

    // only write 1s 
    for (GridTraversal gt (eff_bbox, gtl::ll(eff_bbox), get_img_pixel_size(0), get_img_pixel_size(1)); 
            gt.has_next(); gt.next())
        if (sShapePixel.count(std::make_pair(gt.idxx, gt.idxy))) 
            out << "," << gt.count()+1 << ":" << 1;
    out << std::endl;
}

void MaskImage::init_rtrees(ReadStoreDB const& db, std::vector<std::map<int32_t, rtree_type> >& tShapes)
{
    //clear and resize tShapes
    tShapes.clear();
    tShapes.resize(db.cells().size());
    for (std::vector<cell_type*>::const_iterator it1 = db.cells().begin(), it1e = db.cells().end(); it1 != it1e; ++it1)
    {
        cell_type const& cell = **it1;
        if (!m_params.sCellNames.empty() && !m_params.sCellNames.count(cell.name()))
            continue;
        for (int32_t i = 0; i != NUM_LAYERS; ++i)
        {
            if (cell.shapes().count(m_params.training_layers[i]))
            {
                std::vector<shape_base_type*> const& vShapes = cell.shapes(m_params.training_layers[i]);
                std::vector<shape_base_type*> vTmpShapes; // collect useful shapes 
                for (std::vector<shape_base_type*>::const_iterator it2 = vShapes.begin(), it2e = vShapes.end(); it2 != it2e; ++it2)
                {
                    shape_base_type* shape = *it2;
                    if (shape->tag() & (ShapeTag::RECTANGLE | ShapeTag::POLYGON90 | ShapeTag::POLYGON))
                        vTmpShapes.push_back(shape);
                }
                rtree_type(vTmpShapes.begin(), vTmpShapes.end()).swap(tShapes[cell.cell_id()][m_params.training_layers[i]]);
            }
        }
    }
}

std::vector<MaskImage::shape_base_type*> const* MaskImage::get_target_shapes(ReadStoreDB const& db, bool training_phase) const
{
  uint32_t tCell_id = 0;
  std::vector<shape_base_type*> const* tShapesPtr = NULL;
  if (training_phase)
  {
    tCell_id = db.cell_index(m_params.training_gds_cell);
    tShapesPtr = &(db.cells().at(tCell_id)->shapes(m_params.training_layers[TARGET_LAYER]));
  }
  else
  {
    tCell_id = db.cell_index(m_params.testing_gds_cell);
    tShapesPtr = &(db.cells().at(tCell_id)->shapes(m_params.testing_layers[TARGET_LAYER]));
  }
  return tShapesPtr;
}

std::vector<MaskImage::shape_base_type*> const* MaskImage::get_sraf_shapes(ReadStoreDB const& db, bool training_phase) const
{
  uint32_t tCell_id = 0;
  std::vector<shape_base_type*> const* sShapesPtr = NULL;
  if (training_phase)
  {
    tCell_id = db.cell_index(m_params.training_gds_cell);
    sShapesPtr = &(db.cells().at(tCell_id)->shapes(m_params.training_layers[SRAF_LAYER]));
  }
  else
  {
    tCell_id = db.cell_index(m_params.testing_gds_cell);
    sShapesPtr = &(db.cells().at(tCell_id)->shapes(m_params.testing_layers[SRAF_LAYER]));
  }
  return sShapesPtr;
}

void MaskImage::init_opt_region(ReadStoreDB const& db, std::vector<shape_base_type*>& opc_region, std::vector<shape_base_type*>& sraf_region, bool training_phase)
{
  char buf[256];
  maskSPrint(kINFO, buf, "initialize opt region takes %%t seconds CPU, %%w seconds real\n");
  boost::timer::auto_cpu_timer timer (buf);

  std::vector<shape_base_type*> const* tShapesPtr = get_target_shapes(db, training_phase);
  //prepare for collecting data
  opc_region.clear(), sraf_region.clear();
  opc_region.reserve(tShapesPtr->size()), sraf_region.reserve(tShapesPtr->size());
  for(std::vector<shape_base_type*>::const_iterator it = tShapesPtr->begin(), ite = tShapesPtr->end(); it != ite; ++it)
  {
    opc_region.push_back((*it)->bloat(m_params.opc_region));
    sraf_region.push_back((*it)->bloat(m_params.sraf_region));
  }

#ifdef DEBUG
  //for debug 
  maskPrint(kINFO, "target shapes:\n");
  for(std::vector<shape_base_type*>::const_iterator it = tShapesPtr->begin(), ite = tShapesPtr->end(); it != ite; ++it)
  {
    maskPrint(kNONE, "\t%s \n", std::string(**it).c_str());
  }
  maskPrint(kINFO, "OPC region:\n");
  for(std::vector<shape_base_type*>::const_iterator it = opc_region.begin(), ite = opc_region.end(); it != ite; ++it)
  {
    maskPrint(kNONE, "\t%s \n", std::string(**it).c_str());
  }
  maskPrint(kINFO, "SRAF region:\n");
  for(std::vector<shape_base_type*>::const_iterator it = sraf_region.begin(), ite = sraf_region.end(); it != ite; ++it)
  {
    maskPrint(kNONE, "\t%s \n", std::string(**it).c_str());
  }
#endif
}

void MaskImage::init_img_bbox(ReadStoreDB const& db, bool training_phase)
{
  char buf[256];
  maskSPrint(kINFO, buf, "initialize image bbox takes %%t seconds CPU, %%w seconds real\n");
  boost::timer::auto_cpu_timer timer (buf);

  //get the target shapes
  std::vector<shape_base_type*> const* tShapesPtr = get_target_shapes(db, training_phase);
  gtl::rectangle_data<coordinate_type> target_bbox = (*(tShapesPtr->begin()))->bbox();
  for(std::vector<shape_base_type*>::const_iterator it = tShapesPtr->begin(), ite = tShapesPtr->end(); it != ite; ++it)
  {
    gtl::rectangle_data<coordinate_type> bbox = (*it)->bbox();
    gtl::encompass(target_bbox, bbox);
  }

  //center of target-shape bbox
  coordinate_type center_x = std::ceil((gtl::xl(target_bbox)+gtl::xh(target_bbox))/2.0);
  coordinate_type center_y = std::ceil((gtl::yl(target_bbox)+gtl::yh(target_bbox))/2.0);
  
  //get the size of entire optical window
  coordinate_type window_x = gtl::xh(target_bbox)-gtl::xl(target_bbox)+2*m_params.sraf_region;
  coordinate_type window_y = gtl::yh(target_bbox)-gtl::yl(target_bbox)+2*m_params.sraf_region;

  //discrete by the SRAF grid 
  window_x = window_x/get_sraf_size()*get_sraf_size() + get_sraf_size();
  window_y = window_y/get_sraf_size()*get_sraf_size() + get_sraf_size();

  //decide the bounding box 
  coordinate_type min_x = std::ceil((center_x - window_x/2.0)/get_sraf_size()*get_sraf_size());
  coordinate_type min_y = std::ceil((center_y - window_y/2.0)/get_sraf_size()*get_sraf_size());
  coordinate_type max_x = std::ceil((center_x + window_x/2.0)/get_sraf_size()*get_sraf_size());
  coordinate_type max_y = std::ceil((center_y + window_y/2.0)/get_sraf_size()*get_sraf_size());

  //set up the bounding box
  m_img_bbox = gtl::rectangle_data<coordinate_type>(min_x, min_y, max_x, max_y);

#ifdef DEBUG
  maskPrint(kINFO, "Mask image bbox: (%d, %d, %d, %d)\n", xl(), yl(), xh(), yh());
#endif
}

void MaskImage::init_img_pixel(ReadStoreDB const& db, bool training_phase)
{
  maskPrint(kINFO, "initialize pixel image\n");
  char buf[256];
  maskSPrint(kINFO, buf, "initialize pixel image takes %%t seconds CPU, %%w seconds real\n");
  boost::timer::auto_cpu_timer timer (buf);

  //clear pixel img 
  m_pixel_img.clear();
  
  //pixel grid size 
  coordinate_type width = xh() - xl();
  m_gridx = width / get_img_pixel_size(0);
  if(coordinate_type(m_gridx * get_img_pixel_size(0)) < width)
  {
    m_gridx += 1;
  }
  coordinate_type height = yh() - yl();
  m_gridy = height / get_img_pixel_size(1);
  if(coordinate_type(m_gridy * get_img_pixel_size(1)) < width)
  {
    m_gridy += 1;
  }
  
#ifdef DEBUG
  maskAssert(width > 0 && height > 0);
#endif
  maskPrint(kINFO, "Image grid size (%d x %d)\n", m_gridx, m_gridy);
  //initialize pixel image 
  m_pixel_img = pixel_img_type(m_gridx*m_gridy, 0);
  std::vector<shape_base_type*> const* tShapesPtr = get_target_shapes(db, training_phase);
  
  //fill in target-shape pixels 
  unsigned int tSize = tShapesPtr->size();
#ifdef _OPENMP
#pragma omp parallel for
#endif
  for(unsigned int k = 0; k < tSize; ++k)
  {
    gtl::rectangle_data<coordinate_type> eff_bbox = (tShapesPtr->at(k))->bbox();
    for (GridTraversal gt (eff_bbox, gtl::ll(m_img_bbox), get_img_pixel_size(0), get_img_pixel_size(1)); gt.has_next(); gt.next())
    {
      m_pixel_img[idxy2index_img(gt.idxx, gt.idxy)] = 1;
    }
  }
}

void MaskImage::init_sraf_region(ReadStoreDB const& db, std::vector<shape_base_type*> const& opc_region, std::vector<shape_base_type*> const& sraf_region, bool training_phase, bool symmetric_data)
{
  maskPrint(kINFO, "initialize valid SRAF region\n");
  char buf[256];
  maskSPrint(kINFO, buf, "initialize valid SRAF region takes %%t seconds CPU, %%w seconds real\n");
  boost::timer::auto_cpu_timer timer (buf);

  //clear sraf region 
  m_sraf_region.clear();

  //sraf region size 
  coordinate_type width = xh() - xl();
  m_srafx = width / get_sraf_pixel_size(0);
  if(coordinate_type(m_srafx * get_sraf_pixel_size(0)) < width)
  {
    m_srafx += 1;
  }
  coordinate_type height = yh() - yl();
  m_srafy = height / get_sraf_pixel_size(1);
  if(coordinate_type(m_srafy * get_sraf_pixel_size(1)) < width)
  {
    m_srafy += 1;
  }

#ifdef DEBUG
  maskAssert(width > 0 && height > 0);
  maskPrint(kINFO, "SRAF grid size (%d x %d)\n", m_srafx, m_srafy);
#endif
  //initialize sraf region 
  m_sraf_region = pixel_img_type(m_srafx*m_srafy, 0);

  maskPrint(kNONE, "\tfill in SRAF region\n");
  std::vector<shape_base_type*> const* tShapesPtr = get_target_shapes(db, training_phase);
  maskAssert(tShapesPtr->size()==sraf_region.size() && tShapesPtr->size()==opc_region.size());
  //The lower left quadrant of training data 
  gtl::rectangle_data<coordinate_type> llRect(xl(), yl(), (xl()+xh())/2, (yl()+yh())/2);
  //fill in SRAF region 
  for(unsigned int k = 0, ke = sraf_region.size(); k != ke; ++k)
  {
    gtl::rectangle_data<coordinate_type> eff_bbox = sraf_region.at(k)->bbox();
    //target pattern bbox
    gtl::rectangle_data<coordinate_type> target_box = tShapesPtr->at(k)->bbox();
    for (GridTraversal gt (eff_bbox, gtl::ll(m_img_bbox), get_sraf_pixel_size(0), get_sraf_pixel_size(1)); gt.has_next(); gt.next())
    {
      point_type gCenter = grid_ll_sraf(std::make_pair(gt.idxx, gt.idxy));
      //for training data, only use lower left quadrant for symmetric data
      if(true == symmetric_data && true == training_phase && false == gtl::contains(llRect, gCenter))
      {
        continue;
      }
      //check point to rectangle spacing  
      if(gtl::euclidean_distance(target_box, gCenter) <= m_params.sraf_region)
      {
        m_sraf_region[idxy2index_sraf(gt.idxx, gt.idxy)] = 1;
      }
    }
  }

  maskPrint(kNONE, "\tunfill OPC region\n");
  //unfill OPC region 
  for(std::vector<shape_base_type*>::const_iterator it = opc_region.begin(), ite = opc_region.end(); it != ite; ++it)
  {
    gtl::rectangle_data<coordinate_type> eff_bbox = (*it)->bbox();
    for (GridTraversal gt (eff_bbox, gtl::ll(m_img_bbox), get_sraf_pixel_size(0), get_sraf_pixel_size(1)); gt.has_next(); gt.next())
    {
      m_sraf_region[idxy2index_sraf(gt.idxx, gt.idxy)] = 0;
    }
  }
}

void MaskImage::init_label_data(ReadStoreDB const& db, bool training_phase)
{
  char buf[256];
  maskSPrint(kINFO, buf, "initialize golden SRAF data takes %%t seconds CPU, %%w seconds real\n");
  boost::timer::auto_cpu_timer timer (buf);

  unsigned int sraf_area_th = sraf_area_threshold();
  maskPrint(kINFO, "initialize golden SRAF data with area threshold: %d\n", sraf_area_th);
  pixel_img_type* srafVecPtr = &m_golden_sraf;
  //clear old data
  srafVecPtr->clear();
  (*srafVecPtr) = pixel_img_type(m_srafx*m_srafy, 0);
  //fill in golden SRAFs 
  std::vector<shape_base_type*> const* sShapesPtr = get_sraf_shapes(db, training_phase);
  unsigned int sSize = sShapesPtr->size();
//#ifdef _OPENMP
//#pragma omp parallel for
//#endif
  for(unsigned int k = 0; k < sSize; ++k)
  {
    gtl::rectangle_data<coordinate_type> eff_bbox = (sShapesPtr->at(k))->bbox();
    for (GridTraversal gt (eff_bbox, gtl::ll(m_img_bbox), get_sraf_pixel_size(0), get_sraf_pixel_size(1)); gt.has_next(); gt.next())
    {
      point_type gCenter = grid_center_sraf(std::make_pair(gt.idxx, gt.idxy));
      gtl::rectangle_data<coordinate_type> rect(gtl::x(gCenter)-get_sraf_size()/2, gtl::y(gCenter)-get_sraf_size()/2, gtl::x(gCenter)+get_sraf_size()/2, gtl::y(gCenter)+get_sraf_size()/2);
      if(gtl::intersect(rect, eff_bbox)
          && gtl::area(rect) >= sraf_area_th)
      {
        (*srafVecPtr)[idxy2index_sraf(gt.idxx, gt.idxy)] = 1;
      }
    }
  }

  //initialize from m_golden_sraf 
  unsigned sdata_size = 0;
  for (unsigned int k = 0, ke = m_golden_sraf.size(); k != ke; ++k)
  {
    if(valid_sraf_region(k))
    {
      sdata_size += 1;
    }
  }

  //collect data
  if(training_phase)
  {
    maskPrint(kINFO, "initialize training label data from %d to", m_train_label.size());
    unsigned int offset = m_train_label.size();
    m_train_label.resize(offset + sdata_size);
    for(unsigned int k = 0, ke = m_golden_sraf.size(); k != ke; ++k)
    {
      if(valid_sraf_region(k))
      {
        m_train_label(offset) = m_golden_sraf[k];
        offset += 1;
      }
    }
    maskPrint(kNONE, " %d\n", m_train_label.size());
  }
  else
  {
    //m_test_label.clear();
    //m_test_label.resize(0);
    maskPrint(kINFO, "initialize testing label data from %d to", m_test_label.size());
    unsigned int offset = m_test_label.size();
    m_test_label.resize(offset + sdata_size);
    for(unsigned int k = 0, ke = m_golden_sraf.size(); k != ke; ++k)
    {
      if(valid_sraf_region(k))
      {
        m_test_label(offset) = m_golden_sraf[k];
        offset += 1;
      }
    }
    maskPrint(kNONE, " %d\n", m_test_label.size());
  }
}

void MaskImage::init_feature_vectors(bool training_phase)
{

  char buf[256];
  maskSPrint(kINFO, buf, "initialize feature vectors takes %%t seconds CPU, %%w seconds real\n");
  boost::timer::auto_cpu_timer timer (buf);

  FeatureDB* featurePtr = NULL;

  if (training_phase)
  {
    maskPrint(kINFO, "feature extraction for training data\n");
    featurePtr = m_train_feature;
  }
  else 
  {
    maskPrint(kINFO, "feature extraction for testing data\n");
    featurePtr = m_test_feature;
  }

  if (m_params.feature_mode == "CCAS") 
  {
    if(featurePtr == NULL)
    {
      featurePtr = new CCAS(m_params.opc_region, m_params.rinSpace, std::floor((m_params.effective_distance*500-m_params.opc_region)/(float)m_params.rinSpace), m_params.circleSR);
    }
  }
  else
  {
    maskAssertMsg(false, "unsupported feature mode: ", m_params.feature_mode.c_str());
  }

  featurePtr->feature_extraction(*this);

  if (training_phase)
  {
    maskPrint(kINFO, "feature extraction done with size: %d\n", featurePtr->size());
    m_train_feature = featurePtr;
  }
  else 
  {
    maskPrint(kINFO, "feature extraction done with size: %d\n", featurePtr->size());
    m_test_feature = featurePtr;
  }

  featurePtr = NULL;
}

bool MaskImage::get_learning_data(bool training_phase)
{
  std::set<std::string> gdsSet;
  std::string gdsDir = "";
  if(training_phase)
  {
    gdsSet = m_params.training_gds;
    gdsDir = m_params.training_gds_dir;
  }
  else
  {
    gdsSet = m_params.testing_gds;
    gdsDir = m_params.testing_gds_dir;
  }

  //collect img data
  for(std::set<std::string>::const_iterator it = gdsSet.begin(), ite = gdsSet.end(); it != ite; ++it)
  {
    std::string gdsIn = gdsDir + *it + ".gds";
    ReadStoreDB db;
    maskAssert(read_layout(gdsIn, db));
    std::vector<shape_base_type*> opc_region; ///< expand target shapes for OPC region
    std::vector<shape_base_type*> sraf_region; ///< expand target shapes for SRAF region

    init_opt_region(db, opc_region, sraf_region, training_phase); ///< initialize mask optimization region

    init_img_bbox(db, training_phase); ///< initialize mask image bounding box 

    init_img_pixel(db, training_phase); ///< initialize pixel data: m_pixel_img

    bool symmetric_data = (it->find("test") == std::string::npos);
    init_sraf_region(db, opc_region, sraf_region, training_phase, symmetric_data); ///< initialize sraf region: m_sraf_region, must be called after init_img_pixel(db, true);

    init_label_data(db, training_phase);
    init_feature_vectors(training_phase);

#ifdef DEBUG
    //debug output gds
    std::string gdsOut = m_params.debug_dir + *it + ".gds"; 
    write_layout(gdsOut, db, training_phase, false);
#endif
    //predict separately for each gds layout
    if(false == training_phase && 0 != m_params.output_gds)
    {
      //model prediction 
      learning_model_prediction(false);

      //prediction error analysis
      prediction_error_analysis(false);
      
      if(0 == m_params.search)
      {
        //SRAF generation
        std::string gdsOut = m_params.result_dir + *it + "_sraf.gds"; 
        write_layout(gdsOut, db, false, true);
      }

      //generate contact centers for each layout for PV band simulation
      center_generation(db, *it);
      
      //clear testing data for next gds layout 
      delete m_test_feature;
      m_test_feature = NULL;
      m_test_label.clear();
      m_test_label.resize(0);
      m_test_predictions.clear();
      m_test_predictions.resize(0);
    }
  }

  return true;
}

bool MaskImage::learning_model_construction()
{
  char buf[256];
  maskSPrint(kINFO, buf, "model construction takes %%t seconds CPU, %%w seconds real\n");
  boost::timer::auto_cpu_timer timer (buf);

  if(m_params.learning_model == "LGR")
  {
    m_model = new LGR(L2R_LR);
  }
  else if(m_params.learning_model == "SVC")
  {
    //m_model = new LGR(L2R_L2LOSS_SVC);
    m_model = new SVC(LINEAR);
  }
  else
  {
    maskAssertMsg(false, "unsupported learning model %s\n", m_params.learning_model.c_str());
  }

  m_model->model_construction(m_params, m_train_feature, m_train_label);

  return true;
}

bool MaskImage::learning_model_prediction(bool training_data)
{
  char buf[256];
  maskSPrint(kINFO, buf, "model prediction takes %%t seconds CPU, %%w seconds real\n");
  boost::timer::auto_cpu_timer timer (buf);

  maskAssertMsg(m_model != NULL, "m_model is empty before model prediction\n");
  if(training_data)
  {
    maskPrint(kINFO, "model prediction for training data in progress\n");
    return m_model->prediction(m_train_feature, m_train_predictions); 
  }
  else
  {
    maskPrint(kINFO, "model prediction for testing data in progress\n");
    return m_model->prediction(m_test_feature, m_test_predictions);
  }
  return true;
}

void MaskImage::prediction_error_analysis(bool training_data)
{
  char buf[256];
  maskSPrint(kINFO, buf, "error analysis takes %%t seconds CPU, %%w seconds real\n");
  boost::timer::auto_cpu_timer timer (buf);

  label_vec_type const* iLabelPtr = NULL;
  label_vec_type const* pLabelPtr = NULL;
  if(training_data)
  {
    maskPrint(kINFO, "prediction error analysis for training data (%d)", m_train_label.size());
    iLabelPtr = &(m_train_label);
    pLabelPtr = &(m_train_predictions);
  }
  else
  {
    maskPrint(kINFO, "prediction error analysis for testing data (%d)", m_test_label.size());
    iLabelPtr = &(m_test_label);
    pLabelPtr = &(m_test_predictions);
  }

  //calculate errors 
  double tp = 0.0, fp = 0.0;
  double tn = 0.0, fn = 0.0;
  for(unsigned int k = 0, ke = iLabelPtr->size(); k != ke; ++k)
  {
    bool iCondition = ((*iLabelPtr)(k) > 0);
    bool pCondition = ((*pLabelPtr)(k) >= m_prob_th);
    if(iCondition)
    {
      if(pCondition)
      {
        tp += 1;
      }
      else
      {
        fn += 1;
      }
    } 
    else
    {
      if(pCondition)
      {
        fp += 1;
      }
      else
      {
        tn += 1;
      }
    }
  }//end for 
  
  //calculate rmse
  double rmse = std::sqrt((fn+fp)/double(iLabelPtr->size()));

  maskPrint(kNONE, "\trmse: %.4f, overall accuracy: %.4f, True Positive Rate (TPR): %.4f, False Negative Rate (FNR): %.4f, False Positive Rate (FPR): %.4f, True Negative Rate: %.4f\n", rmse, (tp+tn)/(tp+fn+fp+tn), tp/(tp+fn), fn/(tp+fn), fp/(fp+tn), tn/(fp+tn));
}

int32_t MaskImage::get_index(MaskImage::grid_coordinate_type x, MaskImage::grid_coordinate_type xl, MaskImage::grid_coordinate_type stepx) 
{
    return floor((x-xl)/stepx);
}

MaskImage::grid_coordinate_type MaskImage::get_coordinate(int32_t idx, MaskImage::grid_coordinate_type xl, MaskImage::grid_coordinate_type stepx) 
{
    return xl + idx*stepx;
}

bool MaskImage::check_grid_within_shape(int32_t idxx, int32_t idxy, MaskImage::shape_base_type const* shape, 
        MaskImage::point_type const& ll, MaskImage::grid_coordinate_type stepx, MaskImage::grid_coordinate_type stepy) const
{
    grid_coordinate_type gridxl = get_coordinate(idxx, ll.x(), stepx);
    grid_coordinate_type gridyl = get_coordinate(idxy, ll.y(), stepy);
    gtl::point_data<grid_coordinate_type> gridcenter (gridxl+stepx/2, gridyl+stepy/2);

    return check_point_within_shape(gridcenter, shape);
}

bool MaskImage::check_point_within_shape(MaskImage::point_type p, MaskImage::shape_base_type const* shape) const 
{
    bool within = false;
    if (shape->tag() & ShapeTag::RECTANGLE)
    {
        rectangle_type const* cshape = static_cast<rectangle_type const*>(shape);
        within = gtl::contains(*cshape, p);
    }
    else if (shape->tag() & ShapeTag::POLYGON90)
    {
        polygon_90_type const* cshape = static_cast<polygon_90_type const*>(shape);
        within = gtl::contains(*cshape, p);
    }
    else if (shape->tag() & ShapeTag::POLYGON)
    {
        polygon_type const* cshape = static_cast<polygon_type const*>(shape);
        within = gtl::contains(*cshape, p);
    }
    else maskAssert(0);

    return within;
}

gtl::rectangle_data<MaskImage::coordinate_type> MaskImage::get_effective_bbox(MaskImage::point_type const& center) const
{
    coordinate_type effective_distance = m_params.effective_distance;
    return gtl::construct<gtl::rectangle_data<coordinate_type> >(
            center.x() - effective_distance, 
            center.y() - effective_distance, 
            center.x() + effective_distance, 
            center.y() + effective_distance
            );
}

MaskImage::coordinate_type MaskImage::get_img_pixel_size(int32_t idx) const 
{
  return m_params.pixel_size[idx];
}

MaskImage::coordinate_type MaskImage::get_sraf_pixel_size(int32_t idx) const 
{
  return m_params.sraf_grid[idx];
}

MaskImage::coordinate_type MaskImage::get_sraf_size() const 
{
  return m_params.sraf_size;
}

MaskImage::point_type MaskImage::grid_center_sraf(std::pair<uint32_t, uint32_t> const& pair) const 
{
  return gtl::construct<point_type>(xl() + pair.first*get_sraf_pixel_size(0) + get_sraf_pixel_size(0)/2, yl() + pair.second*get_sraf_pixel_size(1) + get_sraf_pixel_size(1)/2);
}

MaskImage::point_type MaskImage::grid_center_sraf(uint32_t sxy) const
{
  return grid_center_sraf(index2idxy_sraf(sxy)); 
}

MaskImage::point_type MaskImage::grid_ll_sraf(std::pair<uint32_t, uint32_t> const& pair) const
{
  return gtl::construct<point_type>(xl() + pair.first*get_sraf_pixel_size(0), yl() + pair.second*get_sraf_pixel_size(1));
}

MaskImage::point_type MaskImage::grid_ll_sraf(uint32_t sxy) const 
{
  return grid_ll_sraf(index2idxy_sraf(sxy));
}

unsigned int MaskImage::sraf_area_threshold() const 
{
  return std::min((get_sraf_size() - get_sraf_pixel_size(0)) * (get_sraf_size() - get_sraf_pixel_size(0)), 
                  (get_sraf_size()/2) * (get_sraf_size()/2));
}

void MaskImage::sraf_generation(GdsWriter& gw, int layer) const
{
  maskPrint(kINFO, "generate SRAF based on prediction\n");

  //initialize usefulness_map 
  label_vec_type usefulness_map = label_vec_type(m_srafx*m_srafy, 0);
  unsigned int pIdx = 0;
  for(unsigned int k = 0, ke = m_sraf_region.size(); k != ke; ++k)
  {
    if(false == valid_sraf_region(k))
    {
      continue;
    }
    usefulness_map(k) = m_test_predictions(pIdx);
    pIdx += 1;
  }//end for k

  //sraf generation on probability maxima
  for(unsigned int k = 0, ke = usefulness_map.size(); k != ke; ++k)
  {
    std::pair<uint32_t, uint32_t> pair = index2idxy_sraf(k);
    uint32_t xl = pair.first,  xh = pair.first;
    uint32_t yl = pair.second, yh = pair.second;
    if(pair.first > 0) 
    {
      xl = pair.first - 1;
    }
    if(pair.first < m_srafx - 1)
    {
      xh = pair.first + 1;
    }
    if(pair.second > 0)
    {
      yl = pair.second - 1;
    }
    if(pair.second < m_srafy - 1)
    {
      yh = pair.second + 1;
    }
    float p = usefulness_map(k);
    //ignore label 0 grids
    if(p < m_prob_th)
    {
      continue;
    }
    //insert SRAFs at probability maxima
    if(p >= usefulness_map(idxy2index_sraf(xl, yl))
        && p >= usefulness_map(idxy2index_sraf(xl, yh))
        && p >= usefulness_map(idxy2index_sraf(xh, yh))
        && p >= usefulness_map(idxy2index_sraf(xh, yl)))
    {
      point_type gCenter = grid_center_sraf(k);
      rectangle_type rect(gtl::x(gCenter)-get_sraf_size()/2, gtl::y(gCenter)-get_sraf_size()/2, gtl::x(gCenter)+get_sraf_size()/2, gtl::y(gCenter)+get_sraf_size()/2);
      rect.layer(layer);
      rect.tag(ShapeTag::RECTANGLE);
      shape_base_type* shape = &rect;
      gw.write_shape(shape, ShapeTag::RECTANGLE | ShapeTag::POLYGON90 | ShapeTag::POLYGON, gtl::construct<point_type>(0, 0));              
    }
  }//end for k
}

void MaskImage::center_generation(ReadStoreDB const& db, std::string const& cell) const
{
  std::string xyOut = m_params.result_dir + cell + ".txt";
  std::ofstream out(xyOut.c_str());
  if(!out.good())
  {
    maskPrint(kERROR, "failed to open %s for write\n", xyOut.c_str());
    return;
  }
  
  std::vector<shape_base_type*> const* tShapesPtr = get_target_shapes(db, false);
  for(std::vector<shape_base_type*>::const_iterator it = tShapesPtr->begin(), ite = tShapesPtr->end(); it != ite; ++it)
  {
    gtl::rectangle_data<coordinate_type> box = (*it)->bbox();
    out << (gtl::xl(box) + gtl::xh(box))/2000.0 << " " << (gtl::yl(box)+gtl::yh(box))/2000.0 << "\n";
  }
  out.close();

  return;
}

MASKOPT_END_NAMESPACE
