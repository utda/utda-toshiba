/*************************************************************************
    > File Name: LayoutDB.cpp
    > Author: Yibo Lin
    > Mail: yibolin@utexas.edu
    > Created Time: Sat 29 Aug 2015 11:45:41 AM CDT
 ************************************************************************/

#include "LayoutDB.h"

MASKOPT_BEGIN_NAMESPACE

namespace gtl = boost::polygon;

LayoutDB::LayoutDB() 
    : LayoutDB::base_type(std::numeric_limits<LayoutDB::coordinate_type>::max(), 
            std::numeric_limits<LayoutDB::coordinate_type>::max(),
            std::numeric_limits<LayoutDB::coordinate_type>::min(),
            std::numeric_limits<LayoutDB::coordinate_type>::min())
{
    initialize();
}
LayoutDB::LayoutDB(LayoutDB const& rhs) 
    : LayoutDB::base_type(rhs)
{
    copy(rhs);
}
LayoutDB::~LayoutDB()
{
}
LayoutDB& LayoutDB::operator=(LayoutDB const& rhs)
{
    if (this != &rhs)
    {
        this->base_type::operator=(rhs);
        copy(rhs);
    }
    return *this;
}
void LayoutDB::initialize()
{
    m_libname = "TEST";
    m_unit = 0.001;
}
void LayoutDB::copy(LayoutDB const& rhs)
{
    m_libname = rhs.m_libname;
    m_unit = rhs.m_unit;
}
void LayoutDB::update_bbox(LayoutDB::base_type::base_type const& box)
{
    if (gtl::xl<base_type>(*this) == std::numeric_limits<coordinate_type>::min()
            || gtl::yl<base_type>(*this) == std::numeric_limits<coordinate_type>::min())
    {
        this->set(gtl::HORIZONTAL, gtl::get(box, gtl::HORIZONTAL));
        this->set(gtl::VERTICAL, gtl::get(box, gtl::VERTICAL));
    }
    else 
    {
        gtl::encompass<base_type, point_type>(*this, gtl::ll(box));
        gtl::encompass<base_type, point_type>(*this, gtl::ur(box));
    }
}

LayoutDB::rectangle_type* LayoutDB::create_rectangle(int32_t layer, std::vector<LayoutDB::point_type> const& vPoint) const
{
    rectangle_type* shape = new rectangle_type;
    for (std::vector<point_type>::const_iterator it = vPoint.begin(), ite = vPoint.end(); it != ite; ++it)
    {
        if (it == vPoint.begin())
            gtl::set_points(*shape, *it, *it);
        else 
            gtl::encompass(*shape, *it);
    }
    shape->layer(layer);

    return shape;
}
LayoutDB::polygon_90_type* LayoutDB::create_polygon_90(int32_t layer, std::vector<LayoutDB::point_type> const& vPoint) const
{
    polygon_90_type* shape = new polygon_90_type;
    shape->set(vPoint.begin(), vPoint.end());
    shape->layer(layer);

    return shape;
}
LayoutDB::polygon_type* LayoutDB::create_polygon(int32_t layer, std::vector<LayoutDB::point_type> const& vPoint) const 
{
    polygon_type* shape = new polygon_type;
    shape->set(vPoint.begin(), vPoint.end());
    shape->layer(layer);

    return shape;
}


MASKOPT_END_NAMESPACE
