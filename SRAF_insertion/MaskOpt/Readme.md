#Layout Utilities
-----------------

**Layout Utilities** includes various functionalities for layout modification

----------
## Features
 * Separate binaries
 * Aerial Image Energy Calculator
 * Flatten Layout Tool 

---------
## Authors

|  Name              | Affiliation                |  email                            |
| ------------------ | -------------------------- | --------------------------------- |
| Yibo Lin           | ECE Dept, UT Austin        | yibolin@cerc.utexas.edu           |
| David Z. Pan       | ECE Dept, UT Austin        | dpan@ece.utexas.edu               |

## Build
The program depends on [Boost library](www.boost.org) and [limbo library](https://bitbucket.org/limbo018/limbo).  
**Aerial Image Energy Calculator** needs Boost.Regex which requires compiled Boost library and link to it.  
Other utilities do not reply on linkage to Boost. 

* Build everything    
Make sure you have all required libraries ready and built.  
```
cd ./src  
make  
```

* Build specific utilities    
If the utilities you are going to build needs compiled Boost, then you have to get it ready.  
Otherwise, no need to build Boost. But still you need to make sure Boost is in the right directory for inclusion.  
```
cd ./src   
make ../bin/<binary_name>  
```

* Build Boost library with Clang    
Clang has two sets of implementation for STL. libstdc++ is compatible with C\+\+99 or earlier, while libc++ keeps the pace of C\+\+11 and C\+\+14... 
They are not compatible with each other and only one STL library should be linked for a program. 
For backward compatibility with old machines, we choose libstdc++. 
Hence, the Boost library in your machine should also be compiled with -stdlib=libstdc++; othewise, there will be linkage errors. 
On OSX, the default STL for Clang is libc++, so you have to explicitly specify libstdc++ for building Boost. 

* Advanced Build  
Build in debug mode  
```
make DBG=1
```
